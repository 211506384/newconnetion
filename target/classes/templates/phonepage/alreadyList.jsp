<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <meta charset="utf-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no,email=no,date=no,address=no">
    <link href="/static/css/aui/aui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/static/css/aui/api.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/phoneapp.css" />
  	<title>${name}</title>
</head>
<body class="wrap">
    <header class="aui-bar aui-bar-nav" id="aui-header">
        <a class="aui-btn aui-pull-left aui-padded-l-15" href="/phone/already">
            <span class="aui-iconfont aui-icon-left"></span>
        </a>
        <div class="aui-title">${name}</div>
    </header>
    <section class="aui-content flex-1 itemList">
   <c:if test='${name eq "项目信息"}'> 
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>
		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         项目名称:${varC.project_name}
                    </div>
                    <div class="aui-card-list-user-info">申请编号：${varC.project_application_no}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">申请时间</div>
                      <div class="timeB">${varC.application_createtime}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">项目进度: ${varC.project_progress}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                	<div>申请部门：${varC.project_department}</div>
                  <div>项目负责人:${varC.project_personname}</div>
            </div>
        </div>

<!-- 				<br> -->
		</c:forEach>
   </c:if> 
      <c:if test='${name eq "项目报销"}'> 
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>
		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         项目名称:${varC.project_name}
                    </div>
                    <div class="aui-card-list-user-info">项目编号：${varC.top_levelnumberId}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">开始时间</div>
                      <div class="timeB">${varC.project_starttime}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">项目进度: ${varC.project_progress}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                	<div>项目已报销：${varC.totalaccount}(万)</div>
                  <div>项目负责人:${varC.project_personname}</div>
            </div>
        </div>

<!-- 				<br> -->
		</c:forEach>
   </c:if> 
<%-- 
           <c:if test='${type eq "1"}'><!-- 校办会议记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         项目名称:${varC.project_name}
                    </div>
                    <div class="aui-card-list-user-info">申请编号：${varC.project_application_no}</div>
                    <div>会议文号：${varC.president_minutesno}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">会议时间</div>
                      <div class="timeB">${varC.president_meetingtime}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">会议结果: ${varC.president_result}</div>

                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                <div class="jind">会议预算: ${varC.president_budget}</div>
                  <div>项目负责人:${varC.project_personname}</div>
            </div>
        </div>
<!-- 				<br> -->
		</c:forEach>
    </c:if>


           <c:if test='${type eq "2"}'><!-- 校党委会议记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         项目名称:${varC.project_name}
                    </div>
                    <div class="aui-card-list-user-info">申请编号：${varC.project_application_no}</div>
                    <div>会议文号：${varC.committee_minutesno}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">会议时间</div>
                      <div class="timeB">${varC.committee_meettime}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">会议结果: ${varC.committee_result}</div>

                    </div>
                    <div class="aui-col-xs-4 endtime">
                       <div class="jind">会议预算: ${varC.committee_budget}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                	<div class="jind">会议预算: ${varC.committee_budget}</div>
                  <div>项目负责人:${varC.project_personname}</div>
            </div>
        </div>
<!-- 				<br> -->
		</c:forEach>
    </c:if>


    <c:if test='${type eq "3"}'><!-- 规划处录入顶级编号记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         项目名称:${varC.project_name}
                    </div>
                    <div class="aui-card-list-user-info">申请编号：${varC.project_application_no}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">确定时间</div>
                      <div class="timeB">${varC.createtime}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">项目编号: ${varC.top_levelnumberId}</div>

                    </div>
                    <div class="aui-col-xs-4 endtime">
                      <div class="jind">项目难度:${varC.executive_level}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                  <div>项目负责人:${varC.project_personname}</div>
            </div>
        </div>
<!-- 				<br> -->
		</c:forEach>
    </c:if>


    <c:if test='${type eq "4"}'><!-- 预算审核二级编号记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         项目名称:${var2.project_name}
                    </div>
                    <div class="aui-card-list-user-info">项目编号：${varC.top_levelnumberId}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">预算编号</div>
                      <div class="timeB">${varC.budget_reviewno}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">执行金额: ${varC.amount_cw}</div>
                    <!--   <div class="aui-progress aui-progress">
                          <div class="aui-progress-bar" style="width: 60%;"></div>
                      </div> -->
                    </div>
                    <div class="aui-col-xs-4 endtime">
                      <div class="timeH">执行年度</div>
                      <div class="timeB">${varC.execution_year}</div>
                    </div>
                </div>
            </div>
        </div>
<!-- 				<br> -->
		</c:forEach>
    </c:if>


    <c:if test='${type eq "5"}'><!-- 规划二审项目记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         项目名称:${var2.project_name}
                    </div>
                    <div class="aui-card-list-user-info">项目编号：${varC.top_levelnumberId}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">监控执行时间</div>
                      <div class="timeB">${varC.planning_time}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">二审结果: ${varC.planning_result}</div>
                    <!--   <div class="aui-progress aui-progress">
                          <div class="aui-progress-bar" style="width: 60%;"></div>
                      </div> -->
                    </div>
                    <div class="aui-col-xs-4 endtime">
                      <div class="timeH">结束时间</div>
                      <div class="timeB">${var2.project_endtime}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                	<div>执行部门：${varC.planning_department}</div>
                  <div>负责人:${var2.project_personname}</div>
            </div>
        </div>
<!-- 				<br> -->
		</c:forEach>
    </c:if>


    <c:if test='${type eq "6"}'><!-- 资产处申购记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">
            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                   		项目编号：${varC.top_levelnumberId}

                    </div>
                    <div class="aui-card-list-user-info">预算编号: ${varC.budget_reviewno}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">实施者</div>
                      <div class="timeB">${varC.planningproject_type}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">资(基)编号: ${varC.level3_zno}</div>

                    </div>
                    <div class="aui-col-xs-4 endtime">
                      <div class="timeH">资(基)编执行金额</div>
                      <div class="timeB">${varC.level3_znoamount}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                	<div>任务类型：${varC.task_type_name}</div>
                  <div>采购状态:${varC.procurement_status}</div>
            </div>
        </div>
<!-- 				<br> -->
		</c:forEach>
    </c:if>


    <c:if test='${type eq "7"}'><!-- 项目三级资编执行记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         资(基)编:${varC.level3_zno}
                    </div>
                    <div class="aui-card-list-user-info">任务类型：${varC.task_type_name}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">执行环节</div>
                      <div class="timeB">${varC.executionlink_name}</div>
                    </div>
                    <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">执行状态: ${varC.executionlink_status}</div>

                    </div>
                       <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">实际开始时间: ${varC.executionlink_actual_starttime}</div>

                    </div>
                      <div class="aui-col-xs-4"><!-- project_stage -->
                      <div class="jind">实际执行金额: ${varC.executionlink_actual_amount}</div>

                    </div>
                    <div class="aui-col-xs-4 endtime">
                      <div class="timeH">实际结束时间</div>
                      <div class="timeB">${varC.executionlink_actual_endtime}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                  <div>负责人:${varC.executionlink_chargepersonname}</div>
            </div>
        </div>
<!-- 				<br> -->
		</c:forEach>
    </c:if>

<c:if test='${type eq "8"}'><!-- 项目报账记录 -->
    <c:forEach var="item" items="${resultlist}">
		         <c:set value="${item}" var="varC"/>

		             <div class="aui-card-list">

            <div class="aui-card-list-header aui-card-list-user aui-border-b">
                    <div class="aui-card-list-user-name">
                         预算编号:${varC.budget_reviewno}
                    </div>
                    <div class="aui-card-list-user-info">财编号：${varC.level3_ccno}</div>
                </div>
            <div class="aui-card-list-content-padded">
              <div class="aui-row aui-row-padded">
                    <div class="aui-col-xs-4 startime">
                      <div class="timeH">执行环节</div>
                      <div class="timeB">${varC.executionlink_name}</div>
                    </div>
                    <div class="aui-col-xs-4">
                      <div class="jind">执行状态: ${varC.executionlink_status}</div>

                    </div>
                       <div class="aui-col-xs-4">
                      <div class="jind">实际开始时间: ${varC.executionlink_actual_starttime}</div>

                    </div>
                      <div class="aui-col-xs-4">
                      <div class="jind">实际执行金额: ${varC.executionlink_actual_amount}</div>

                    </div>
                    <div class="aui-col-xs-4 endtime">
                      <div class="timeH">实际结束时间</div>
                      <div class="timeB">${varC.executionlink_actual_endtime}</div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer">
                  <div>负责人:${varC.executionlink_chargepersonname}</div>
            </div>
        </div>
		</c:forEach>
    </c:if> --%>
    </section>

    <script src="/static/js/jquery-2.2.3.js" type="text/javascript"></script>
    <script src="/static/h-ui/js/H-ui.js" type="text/javascript"></script>
    <script src="/static/js/aui/api.js" type="text/javascript"></script>
   	<script>
   	</script>
</body>
</html>
