<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

    <meta charset="utf-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no,email=no,date=no,address=no">
    <link href="/static/css/aui/aui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/static/css/aui/api.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/phoneapp.css" />
	<title>项目管理</title>

</head>
<body class="wrap">
    <header class="aui-bar aui-bar-nav" id="aui-header">
<!--         <a class="aui-btn aui-pull-left" tapmode onclick="closeWin()"> -->
            <%-- <span class="aui-iconfont aui-icon-left"></span> --%>
<!--         </a> -->
        <div class="aui-title">项目管理</div>
        <a class="aui-pull-right aui-btn aui-padded-r-15" onclick="outIcon()">
             <span class="aui-iconfont aui-icon-close"></span>
        </a>
    </header>

    <section class="aui-content flex-1">
      <ul class="aui-list aui-list-in">
              <li class="aui-list-header">
               	项目管理
              </li>
              <a href="/phone/projectJieDuanList?type=0&project_application_no=${project_application_no}">
              	<li class="aui-list-item">
                 <div class="aui-list-item-inner aui-list-item-arrow">项目基本信息</div>
              	</li>
              </a>
              <a href="/phone/projectJieDuanList?type=1&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                      	校长办公会议信息
                  </div>
              </li>
              </a>
              <a href="/phone/projectJieDuanList?type=2&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                      	校党委常委会议信息
                  </div>
              </li>
              </a>
              <a href="/phone/projectJieDuanList?type=3&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                        	规划处确定项目编号信息
                  </div>
              </li>
              </a>

                  <a href="/phone/projectJieDuanList?type=4&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                        	财务预算审核编号信息
                  </div>
              </li>
              </a>

                  <a href="/phone/projectJieDuanList?type=5&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                        	规划项目执行监控结果
                  </div>
              </li>
              </a>

                  <a href="/phone/projectJieDuanList?type=6&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                        	资产(基建)处申购单信息
                  </div>
              </li>
              </a>

                  <a href="/phone/projectJieDuanList?type=7&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                        	项目三级资编执行记录
                  </div>
              </li>
              </a> 
                       <a href="/phone/projectJieDuanList?type=8&project_application_no=${project_application_no}">
              <li class="aui-list-item">
                  <div class="aui-list-item-inner aui-list-item-arrow">
                        	财务报销记录
                  </div>
              </li>
              </a>
          </ul>
    </section>
    <footer class="aui-bar aui-bar-tab" id="footer">
      <div class="aui-bar aui-bar-btn aui-bar-btn-round">
    	  <a class="aui-bar-btn-item aui-active" href="/phone/monitor">监控项目</a>
          <a class="aui-bar-btn-item " href="/phone/already">项目管理</a>
      </div>
    </footer>
    <script src="/static/js/jquery-2.2.3.js" type="text/javascript"></script>
    <script src="/static/h-ui/js/H-ui.js" type="text/javascript"></script>
    <script src="/static/js/aui/api.js" type="text/javascript"></script>
    <script src="/static/js/aui/aui-dialog.js" type="text/javascript" ></script>
    <script>
       	function outIcon(){
       		var dialog = new auiDialog({});
       		dialog.alert({
                title:"提示",
                msg:'是否退出登陆',
                buttons:['确定','取消']
            },function(ret){
            	if(ret.buttonIndex==1){
            		location.href="/login/sign_out";
            	}
            })
       	}
    </script>
</body>
</html>
