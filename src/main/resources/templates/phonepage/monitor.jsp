<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no,email=no,date=no,address=no">
    <link href="/static/css/aui/aui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/static/css/aui/api.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/phoneapp.css" />
    <link href="/static/css/steps.min.css" rel="stylesheet">
  	<title>监控项目</title>
    <style type="text/css">
    	body{
    		background-color: #ddd;
    		background-color:#e8ebef;
    	}
        .aui-searchbar {
            background: transparent;
        }
        .aui-bar-nav .aui-searchbar-input{
            background-color: #ffffff;
        }
        .aui-info{
          background-color: #fafafa;
          box-shadow: 1px 1px 1px #ddd;
        }
        .aui-popup-top, .aui-popup-top-left, .aui-popup-top-right{
          top: 4.25rem;
        }
         
    </style>
</head>
<body class="wrap">
    <header class="aui-bar aui-bar-nav" id="aui-header">
 		<a class="aui-pull-right aui-btn aui-padded-r-15" onclick="outIcon()">
             <span class="aui-iconfont aui-icon-close"></span>
        </a>
 		<div class="aui-title">重点项目库</div>
 		 <a class="aui-pull-right aui-padded-l-15 aui-btn"  aui-popup-for="searchpage">
           <span class="aui-iconfont aui-icon-search"></span>
      	</a>
       
    </header>
    <%-- <div class="aui-info aui-padded-l-15 aui-padded-r-15" id="tips-1">
      <div class="aui-tips-title"><i class="aui-iconfont aui-icon-menu"  onclick="showPopup()"></i>排序</div>
        <div class="aui-tips-title"><i class="aui-iconfont aui-icon-info"></i>消息</div>
          <div class="aui-tips-title"><i class="aui-iconfont aui-icon-info"></i>消息</div>
  </div> --%>
    <section class="aui-content flex-1 itemList">
		<c:forEach var="item" items="${list}"  varStatus="status"><%-- ${item.project_stage} --%>
       <div class="aui-card-list" data-index="${status.index}" data-stage="${item.project_stage}" data-text="${item.project_progress}">
 		   <a href="/phone/detial?id=${item.project_application_no}">
            <div class="aui-card-list-header aui-border-b" style="min-height: inherit;">
                    <div class="aui-card-list-user-name">${item.project_name}</div>
                    <div class="aui-card-list-user-info aui-text-default">项目编号：${item.top_levelnumberId}</div>
                </div>
            <div class="aui-card-list-content-padded aui-padded-b-0">
              <div class="aui-row aui-row-padded" style="overflow:inherit;">
                    <div class="aui-col-xs-4 startime aui-padded-t-5">
                      <div class="timeH">开始监控时间</div> 
                      <div class="timeB">${item.project_starttime}</div>
                    </div>
                    <div class="aui-col-xs-8 down-5">
                    <div id="steps-${status.index}" class="steps-item"></div>
                    </div>
                </div>
            </div>
            <div class="aui-card-list-footer aui-padded-t-0 aui-padded-b-0" style="min-height: inherit;">
                 	<div class="aui-text-default">当前进度:${item.project_progress}</div>
                	<div class="aui-text-default">当前环节:${item.project_stagename}</div>
            </div>
            <div class="aui-card-list-footer aui-padded-t-0 aui-padded-b-0" style="min-height: inherit;">
                	<div  class="aui-text-default">预算金额(万)：${item.budget_review_money}</div>
                  <div class="aui-text-default">完成金额(万):${item.totalaccount}</div>
            </div>
            <div class="aui-card-list-footer aui-padded-t-0" style="min-height: inherit;">
                	<div class="aui-text-default">现执行部门：${item.project_department}</div>
                  <div class="aui-text-default">现负责人:${item.project_personname}</div>
            </div>
          </a>
        </div>
		</c:forEach>
		
    </section>
<!--      <footer class="aui-bar aui-bar-tab" id="footer">
       <div class="aui-bar aui-bar-btn aui-bar-btn-round">
           <a class="aui-bar-btn-item aui-active" href="/phone/monitor">监控项目</a>
           <a class="aui-bar-btn-item" href="/phone/already">项目管理</a>
       </div>
     </footer> -->
	<div class="aui-popup aui-popup-search-page" id="searchpage"> 
	    <div class="aui-popup-content" style="padding-top:45%;">
	        	<div class="aui-searchbar" id="search">
				    <div class="aui-searchbar-input aui-border-radius">
				        <i class="aui-iconfont aui-icon-search"></i>
				        <form action="javascript:search();">
				            <input type="search" placeholder="请输入项目名称" id="search-input">
				        </form>
				    </div>
				</div>
				<div class="aui-popup-search-btn">
					<div class="aui-btn aui-btn-primary aui-btn-block aui-btn-sm" onclick="searchData()" style="background: #EAC773;">查询项目</div>
				</div>
	    </div>
	    
	</div>
    <script src="/static/js/jquery-2.2.3.js" type="text/javascript"></script>
    <script src="/static/js/aui/api.js" type="text/javascript"></script>
    <script src="/static/js/aui/aui-dialog.js" type="text/javascript" ></script>
    <script src="/static/js/aui/aui-toast.js" type="text/javascript"></script>
    <script src="/static/js/aui/aui-popup.js" type="text/javascript"></script>
    <script src="/static/js/steps.js" type="text/javascript"></script>
    <script>
      var pageSize = 10;
      var pageNow =1;
      var projectName = "";
      var timers;
      var toast = new auiToast();
      var popup = new auiPopup();

// 		初始化
		$(document).ready(function(){
			initSteps(".itemList .aui-card-list");
       	 });
//      退出登陆
    	function outIcon(){
     		var dialog = new auiDialog({});
     		dialog.alert({
              title:"提示",
              msg:'是否退出登陆',
              buttons:['确定','取消']
          },function(ret){
          	if(ret.buttonIndex==1){
          		location.href="/login/sign_out";
          	}
          })
     	}
       //初始化每个进度条
       function initSteps(dom){
    	   console.log(dom)
    	   $(dom).each(function(i){
    		   steps({
 		    	    el: "#steps-"+$(this).data("index"),
 		    	    data: [ { title: "", description: "" },
 		    		        { title: "", description: "" },
 		    		        { title: "", description: "" },
 		    		        { title: "", description: "" },
 		    		        { title: "", description: "" },
 		    		        { title: "", description: "" },
 		    		        { title: "", description: "" },
 		    		        { title: "", description: "" },
 		    		        { title: "", description: "" }],
 		    	    active: $(this).data("stage"),
 		    	    dataOrder: ["title", "line", "description"]});
    		   //高亮当前环节,并添加当前环节名称
    		   		var activeDom =$("#steps-"+$(this).data("index")+" .step-default-class .step-icon:eq("+$(this).data("stage")+")");
    		   		activeDom[0].dataset.text = $(this).data("text");
    		   		activeDom.addClass("active");
    		   		console.log(activeDom.data("text"))
 		      	//title和footer超出长度自动换行
 		      		var h = $(this).find(".aui-card-list-header").innerWidth()-2*Number($(this).find(".aui-card-list-header").css("padding-left").replace("px",""));
 		      		var l =$(this).find(".aui-card-list-user-name").outerWidth(); 
 		      		var r = $(this).find(".aui-card-list-user-info").outerWidth();
 		      		if(l+r>=h){
 		      			$(this).find(".aui-card-list-header").css("display","block");
 		      		}
 		      		$(this).find(".aui-card-list-footer").each(function (i) { 
 		      			var bh = $(this).innerWidth()-2*Number($(this).css("padding-left").replace("px","")); 
 		      			var bl = $(this).find("div:eq(0)").outerWidth(); 
 	 		      		var br = $(this).find("div:eq(1)").outerWidth();
	 	 		      	if(bl+br>=bh){
	 		      			$(this).css("display","block");
	 		      		}
 		      		});
 		      		if($(this).data("stage")<2){
 		      		//处理进度条的title超出
 						var wrapperWidth = $(this).find(".down-5").width();
 						var wrapperLeft = $(this).find(".down-5").offset().left;
 						var activeLeft = $(this).find(".step-icon.active").offset().left;
 						var activeR = activeLeft - wrapperLeft+50;
 						var width = window.getComputedStyle(
 						    document.querySelector('.step-icon.active'), ':before'
 						).getPropertyValue('width').replace("px","");
 						var rt = width/2;
 						if(Number(activeR)>Number(activeRight)){
 							if($(this).data("stage")=="0"){
 								$(this).find(".step-icon.active").addClass("left1");
 							}else{
 								$(this).find(".step-icon.active").addClass("left2");
 							}
 						}
 		      		}else if($(this).data("stage")>6){
 		      			var wrapperWidth = $(this).find(".down-5").width();//容器宽度
 						var wrapperLeft = $(this).find(".down-5").offset().left;//容器左窗口距离 
 						var activeLeft = $(this).find(".step-icon.active").offset().left;//当前环节点到左窗口距离
 						var activeRight = wrapperWidth-(activeLeft-wrapperLeft)+10;//
 						var width = window.getComputedStyle(
 						    document.querySelector('.step-icon.active'), ':before'
 						).getPropertyValue('width').replace("px","");
 						var rt = width/2;
 						if(Number(rt)>Number(activeRight)){
 							if($(this).data("stage")=="7"){
 								$(this).find(".step-icon.active").addClass("right8");
 							}else{
 								$(this).find(".step-icon.active").addClass("right9");
 							}
 						}
 		      		}
 		      		
    	   });
       }
       
       //搜索数据
      function searchData() {
    	  popup.show(document.getElementById("searchpage"));
          pageNow = 1;
          getData(function(ret){
        	  if(ret.code==0){
    			  if(ret.list.length>0){
    				  $(".itemList").html('');
    	        	  appendDom(".itemList",ret.list);
    	        	  initSteps('.itemList .aui-card-list');
        		  }else{
        			  toast.fail({
                          title:"未能查询到数据",
                          duration:2000
                      });
        			  $(".itemList").html('');
        		  }
    		  }else{
    			  toast.fail({
                      title:"获取失败",
                      duration:2000
                  });
    		  }
          })
      }
      
//       上拉加载数据
      function pullup() {
    	  getData(function(ret){
    		  if(ret.code==0){
    			  if(Math.ceil(ret.total/pageSize)<=pageNow){
//     				  toast.fail({
//                           title:"没有更多数据了",
//                           duration:2000
//                       });
    				  return;
    			  }
    			  if(ret.list.length>0){
    				  //获取当前存在的列表长度
    				  var len = $('.itemList .aui-card-list').length;
        			  appendDom(".itemList",ret.list);
        			  initSteps('.itemList .aui-card-list:gt('+len+')');
                	  pageNow += 1;
        		  }else{
        			  toast.fail({
                          title:"没有数据了",
                          duration:2000
                      });
        		  } 
    		  }else{
    			  toast.fail({
                      title:"获取失败",
                      duration:2000
                  });
    		  }
          },pageNow+1);
      }
    //获取ajax数据
      function getData(callback,pageadd){
    	  $.post("/phone/monitorFind", {
    		  projectName:$("#search-input").val(),
    		  pageNow:pageadd||pageNow,
    		  pageSize:pageSize
   		  },
          function(data){
   			callback(data)
          });
      }
      //渲染dom列表
      function appendDom(dom,list) {
    	  var len = $(dom).find('.aui-card-list').length;
    	  for (var i = 0; i < list.length; i++) {
    		  $(dom).append('<div class="aui-card-list" data-index="'+(i+len)+'" data-stage="'+list[i].project_stage+'" data-text="'+list[i].project_progress+'"><a href="/phone/detial?id='+list[i].project_application_no+'"><div class="aui-card-list-header aui-border-b" style="min-height: inherit;">'+
    			       '<div class="aui-card-list-user-name">'+list[i].project_name+'</div><div class="aui-card-list-user-info aui-text-default">项目编号：'+list[i].top_levelnumberId+'</div></div><div class="aui-card-list-content-padded aui-padded-b-0">'+
    			       '<div class="aui-row aui-row-padded" style="overflow:inherit;"><div class="aui-col-xs-4 startime aui-padded-t-5"><div class="timeH">开始时间</div><div class="timeB">'+list[i].project_starttime+'</div></div>'+
    			       '<div class="aui-col-xs-8 down-5"><div id="steps-'+(i+len)+'" class="steps-item"></div></div></div></div><div class="aui-card-list-footer aui-padded-t-0 aui-padded-b-0" style="min-height: inherit;"><div class="aui-text-default">当前进度：'+list[i].project_progress+'</div>'+
    			       '<div class="aui-text-default">当前环节：'+list[i].project_progress+'</div></div><div class="aui-card-list-footer aui-padded-t-0 aui-padded-b-0" style="min-height: inherit;"><div  class="aui-text-default">预算金额：'+360+'</div><div>消费金额：'+159+'</div>'+
    			       '</div><div class="aui-card-list-footer aui-padded-t-0" style="min-height: inherit;"><div class="aui-text-default">执行部门：'+list[i].project_department+'</div><div class="aui-text-default">执行人：'+list[i].project_personname+'</div></div></a></div>');
		  }
	 }
      
      
    //监听上拉加载
      $('.itemList').scroll(function() {
          //当时滚动条离底部60px时开始加载下一页的内容
          if (($(this)[0].scrollTop + $(this).height() + 60) >= $(this)[0].scrollHeight) {
              clearTimeout(timers);
              timers = setTimeout(function() {
                  console.log("第" + (pageNow+1) + "页");
                  pullup(); //调用执行上面的加载方法
              }, 300);
          }
      });
      
                  
    </script>
</body>
</html>
