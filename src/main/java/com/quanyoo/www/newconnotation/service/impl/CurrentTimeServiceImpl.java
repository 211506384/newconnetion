package com.quanyoo.www.newconnotation.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.TestServiceInterface;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CurrentTimeServiceImpl implements TestServiceInterface {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Resource
	private IBaseService baseService;

	/**
	 * 插入数据
	 */
	@Transactional
	public int add(String tableName, Map<String, Object> map) {
		StringBuffer str = new StringBuffer();
		str.append("insert into ");
		str.append(tableName + " ");

		if (map != null) {
			String fieldString = "";
			String fieldValue = "";
			for (String s : map.keySet()) {
				fieldString += s + ",";
				fieldValue += "'" + map.get(s) + "',";
			}
			fieldString = fieldString.substring(0, fieldString.length() - 1);
			fieldValue = fieldValue.substring(0, fieldValue.length() - 1);

			str.append("(" + fieldString + ") " + "values(" + fieldValue + ") ");
		}
		int add = jdbcTemplate.update(str.toString());
		return add;
	}

	/**
	 * 修改数据
	 */
	@Transactional
	public int update(String tableName, Map<String, Object> map, Integer Id) {
		StringBuffer str = new StringBuffer();
		str.append("update ");
		str.append(tableName + " SET ");

		if (map != null) {
			String fieldString = "";
			for (String s : map.keySet()) {
				fieldString += s + "='" + map.get(s) + "',";
			}
			fieldString = fieldString.substring(0, fieldString.length() - 1);

			str.append(fieldString);
		}

		str.append(" where 1=1 and " + "id=" + Id);
		int update = jdbcTemplate.update(str.toString());
		return update;
	}

	/**
	 * 删除数据
	 */
	@Transactional
	public int delete(String tableName, Map<String, Object> map, Integer Id) {
		StringBuffer str = new StringBuffer();
		str.append("delete from  ");
		str.append(tableName + " where id=" + Id);
		int delete = jdbcTemplate.update(str.toString());
		return delete;
	}

	/**
	 * select数据
	 */
	@Transactional
	public List<Map<String, Object>> select(String tableName, Map<String, Object> map, int pageNow, int pageSize) {
		StringBuffer str = new StringBuffer();
		str.append("select * from  ");
		str.append(tableName + " ");

		String fieldString = "";

		if (map != null) {

			for (String s : map.keySet()) {
				if(!"".equals(map.get(s))&&map.get(s)!=null) {
					fieldString += s + "='" + map.get(s) + "' and ";
				}
			}
		}
		str.append("where " + fieldString + "1=1 limit ");
		str.append((pageNow-1)*pageSize+","+pageSize);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(str.toString());
		
		return list;

	}



    @Override
	public List<Map<String, Object>> getTableColumnInfo(Map<String,Object> map) {
		return baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "getTableColumnInfo", map);
	}

    @Override
    public List<Map<String, Object>> assessmentIndexSelect(String tableName) {
        Map<String, Object> result=new HashMap<String, Object>();
        result.put("id","不采纳");
        result.put("text","不采纳");
        List<Map<String, Object>> list=baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "assmentIndexSelect", tableName);
        list.add(result);
        return list;
    }

    @Override
    public List<Map<String, Object>> assessmentIndexScoreSelect(String tableName) {
        Map<String, Object> result=new HashMap<String, Object>();
        result.put("id",0);
        result.put("text",0);
        List<Map<String, Object>> list=baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "assmentIndexScoreSelect", tableName);
        list.add(result);
        return list;
    }

    /**
	 * 自动建库建表
	 */
	@Override
	public int createTableData(String dataBaseName, List<Table> list) {

		try {
			//建库
			String sql_ku = "CREATE DATABASE " + dataBaseName;
			jdbcTemplate.update(sql_ku);
			// 连接数据库
			String sql_use = "USE " + dataBaseName;
			jdbcTemplate.update(sql_use);
			// 执行创表语句
			if (list.size() > 0) {
				for (Table table : list) {
					StringBuffer sql=new StringBuffer();
					String fieldValue = "";
					//表名
					String tableName=table.getCreate_table_english_name();
					//字段集合
					List<TableColumn> tableColumnList =table.getTableColumn();
					sql.append("CREATE TABLE ");
					sql.append(tableName + "(id int(11) primary key not null auto_increment,");
					for(TableColumn tableColumn:tableColumnList) {
						if("id".equals(tableColumn.getCreate_table_column())) {
							continue;
						}
						fieldValue+=tableColumn.getCreate_table_column()+" "+tableColumn.getCreate_table_column_type()+"  COMMENT '"+tableColumn.getCreate_table_chinese_column()+"',";

					}
						if(!"".equals(fieldValue)) {

							fieldValue = fieldValue.substring(0, fieldValue.length() - 1);
							sql.append(fieldValue+") COMMENT='"+table.getCreate_table_chinese_name()+"'");
							jdbcTemplate.update(sql.toString());
						}else {
							jdbcTemplate.update(sql.toString().substring(0, sql.toString().length() - 1)+")");
						}
				}
			}
			System.out.println("Created databaseName in given database...");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return -2;
		}
		System.out.println("Goodbye!");
		return 2;
	}
	/**
	 * 分页查找
	 * */
	@Override
	public  PageInfo<Map<String, Object>> selectListBypage(String tableName, Map<String, Object> params, int pageNow, int pageSize) {
		if(pageNow > 0 && pageSize > 0){
			PageHelper.startPage(pageNow, pageSize);
		}
		List<Map<String, Object>> list = select(tableName, params,  pageNow,  pageSize);
        //用PageInfo对结果进行包装
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		StringBuffer str=new StringBuffer();
		str.append("select count(*) from  ");
		str.append(tableName + " ");

		String fieldString = "";

		if (params != null) {

			for (String s : params.keySet()) {
				if(!"".equals(params.get(s))&&params.get(s)!=null) {
					fieldString += s + "='" + params.get(s) + "' and ";
				}
			}
		}
		str.append("where " + fieldString + "1=1 ");
		long total = jdbcTemplate.queryForObject(str.toString(),Integer.class);
		page.setTotal(total);
        return page;
	}
	
	/**
	 * 根据表名查找字段和表信息
	 */
	@Override
	public List<Table> getTableAndColumns(String tableName) {
		List<Table> list = new ArrayList<Table>();
		// 根据表名查找表信息
		StringBuffer tableSql = new StringBuffer();
		tableSql.append("select 	create_table_num,create_table_english_name,\r\n"
				+ "	ifnull(create_table_chinese_name,\"表未命名中文\") create_table_chinese_name ,create_table_type "
				+ "from  connotation_table_create ");

		tableSql.append("where  create_table_english_name= '" +tableName  + "' and 1=1 ");
		Map<String, Object> m = jdbcTemplate.queryForMap(tableSql.toString());
		Table table=JSON.parseObject(JSON.toJSONString(m), Table.class);
		// 根据表名查找表字段信息

		StringBuffer columnSql = new StringBuffer();
		columnSql.append("select 	create_table_column,\r\n"
				+ "	ifnull(create_table_chinese_column,\"表未命名中文\") create_table_chinese_column,is_need,"
				+ "is_statistics,field_weight  "
				+ " from  connotation_table_column_create ");

		columnSql.append("where  create_table_name= '" +tableName + "' and 1=1 ");
		List<Map<String, Object>> queryList = jdbcTemplate.queryForList(columnSql.toString());
		
	      List<TableColumn> dtoList = new ArrayList<TableColumn>();
        for (Map<String, Object> p:queryList) {
            TableColumn tableColumn=JSON.parseObject(JSON.toJSONString(p), TableColumn.class);
           
            dtoList.add(tableColumn );
        }
		table.setTableColumn(dtoList);
		list.add(table);
		return list;
	}

	@Override
	public List<Map<String, Object>> getDataStatistics(String tableName,String tableName1,String tableName2) {
		Map<String,Object> map=new HashMap<>();
		Map<String,Object> param=new HashMap<>();
		List<Map<String,Object>> list=new ArrayList<>();
		param.put("tableName",tableName);
		map=baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段","暂存");
		list.add(map);
		param.put("tableName",tableName1);
		map=baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段","采纳");
		list.add(map);
		param.put("tableName",tableName2);
		map=baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段","不采纳");
		list.add(map);
		return list;
	}
	/**
	 * 统计数据不分页
	 * 
	 * */
	@Override
	public List<Map<String, Object>> selectAll(String tableName, Map<String, Object> map) {
		StringBuffer str = new StringBuffer();
		str.append("select * from  ");
		str.append(tableName + " ");
		List<Map<String, Object>> list = jdbcTemplate.queryForList(str.toString());
			
		return list;
	}
	/**
	 * @param tableName		要插入的表
	 * @param keys			指定插入的字段值
	 * @param values		指定插入的数据值
	 */
	//添加数据
	@Override
	public void addInfo(String tableName,String keys,String values) {
		StringBuilder sql = new StringBuilder();
		sql.append("insert into ").append(tableName).append("(").append(keys).append(")").append(" values (").append(values).append(")");
		jdbcTemplate.update(sql.toString());
    }

}
