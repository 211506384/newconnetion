package com.quanyoo.www.newconnotation.service;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.Table;

public interface JdbcService {
	/**
	 * 插入数据
	 */
	public int add(JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map);
	/**
	 * 修改数据
	 */
	public int update(JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map,Integer Id);

	/**
	 * 删除数据
	 */
	public int delete(JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map,Integer Id);

	/**
	 * select数据
	 */
	public List<Map<String, Object>>  select(JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map, int pageNow, int pageSize) ;

	/**
	 * select数据
	 */
	public List<Map<String, Object>>  select1(JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map, int pageNow, int pageSize) ;

	/**
	 * 分页查找-带条件
	 * */
	public PageInfo<Map<String, Object>> selectListBypage(JdbcTemplate jdbcTemplate,String tableName,Map<String, Object>  params, int pageNow, int pageSize);
	
	/**
	 * 分页查找
	 * */
	public PageInfo<Map<String, Object>> selectListBypage1(JdbcTemplate jdbcTemplate,String tableName,Map<String, Object>  params, int pageNow, int pageSize);
	/**
	 * 根据表名查找字段和表信息
	 * */
	public List<Table> getTableAndColumns(JdbcTemplate jdbcTemplate,String tableName);
	
	/**
	 * 自动建库建表
	 * */
	public int createTableData(JdbcTemplate jdbcTemplate,String dataBaseName,List<Table> list) ;
		/**
		 * 查找所有表结构
		 * 建库表使用
		 * */
	public List<Table> getTableAndColumns(JdbcTemplate jdbcTemplate);
	
	/**
	 * 院系（部门）统计数据
	 * */
	public List<Map<String, Object>> getdepartMentStaticsList(JdbcTemplate jdbcTemplate);
	
	/**
	 * 院系（部门）统计
	 * */
	public List<Map<String, Object>> getdepartMentList(JdbcTemplate jdbcTemplate);
	/**
	 * 院系（部门）统计year
	 * */
	public List<Map<String, Object>> getdepartMentYearList(JdbcTemplate jdbcTemplate);
	
	
	/**
	 * 统计个人年度得分表数据
	 * */
	public List<Map<String, Object>> getIndividualSoreList(JdbcTemplate jdbcTemplate);
}
