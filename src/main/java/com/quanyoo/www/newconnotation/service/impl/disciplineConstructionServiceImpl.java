package com.quanyoo.www.newconnotation.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.CollaborativeInnovationAdopt;
import com.quanyoo.www.newconnotation.model.CollaborativeInnovationFile;
import com.quanyoo.www.newconnotation.model.CollaborativeInnovationNotadopted;
import com.quanyoo.www.newconnotation.model.CollaborativeInnovationTemporarystorage;
import com.quanyoo.www.newconnotation.model.DegreeProgramAdopt;
import com.quanyoo.www.newconnotation.model.DegreeProgramFile;
import com.quanyoo.www.newconnotation.model.DegreeProgramNotadopted;
import com.quanyoo.www.newconnotation.model.DegreeProgramTemporarystorage;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.FirstDisciplineAdopt;
import com.quanyoo.www.newconnotation.model.FirstDisciplineFile;
import com.quanyoo.www.newconnotation.model.FirstDisciplineNotadopted;
import com.quanyoo.www.newconnotation.model.FirstDisciplineTemporarystorage;
import com.quanyoo.www.newconnotation.model.UserInformation;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.UserService;
import com.quanyoo.www.newconnotation.service.disciplineConstructionService;
@Service
public class disciplineConstructionServiceImpl implements disciplineConstructionService{
	 @Resource
	    private IBaseService baseService;

// 获取数据-学科建设_学位点_不采纳表
	 /**
	  * 保存数据
	  * */
	@Transactional
	@Override
	public int saveDegree(DegreeProgramNotadopted degreeProgramNotadopted) {
		// TODO Auto-generated method stub
		try {
			baseService.saveOrUpdateByJpa(DegreeProgramNotadopted.class, Integer.class, degreeProgramNotadopted);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	 /**
	  * 删除数据
	  * */
	@Transactional
	@Override
	public int deleteDegree(Integer userId) {
		try {
			baseService.deleteByIdByJpa(DegreeProgramNotadopted.class, Integer.class, userId);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	/**
	 * 查找数据
	 * */
	@Override
	public Map<String, Object> getDegreeList(Map<String, Object> map,int page,int rows) {
		 PageInfo<DegreeProgramNotadopted> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findDegreeNotadopted",map,page,rows);
		 map = 	new HashMap<String, Object>();
		 map.put("rows", list.getList());
		 map.put("total", list.getTotal());
		return map;
	}

	
// 获取数据-学科建设_学位点_采纳表
		 /**
		  * 保存数据
		  * */
		@Transactional
		@Override
		public int saveDegreeAdopt(DegreeProgramAdopt degreeProgramAdopt) {
			// TODO Auto-generated method stub
			try {
				baseService.saveOrUpdateByJpa(DegreeProgramAdopt.class, Integer.class, degreeProgramAdopt);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		 /**
		  * 删除数据
		  * */
		@Transactional
		@Override
		public int deleteDegreeAdopt(Integer userId) {
			try {
				baseService.deleteByIdByJpa(DegreeProgramAdopt.class, Integer.class, userId);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		/**
		 * 查找数据
		 * */
		@Override
		public Map<String, Object> getDegreeAdoptList(Map<String, Object> map,int page,int rows) {
			 PageInfo<DegreeProgramAdopt> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findDegreeAdopt",map,page,rows);
			 map = 	new HashMap<String, Object>();
			 map.put("rows", list.getList());
			 map.put("total", list.getTotal());
			return map;
		}

		
// 获取数据-学科建设_学位点_暂存表
		 /**
		  * 保存数据
		  * */
		@Transactional
		@Override
		public int saveDegreeZC(DegreeProgramTemporarystorage degreeProgramTemporarystorage) {
			// TODO Auto-generated method stub
			try {
				baseService.saveOrUpdateByJpa(DegreeProgramTemporarystorage.class, Integer.class, degreeProgramTemporarystorage);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		 /**
		  * 删除数据
		  * */
		@Transactional
		@Override
		public int deleteDegreeZC(Integer userId) {
			try {
				baseService.deleteByIdByJpa(DegreeProgramTemporarystorage.class, Integer.class, userId);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		/**
		 * 查找数据
		 * */
		@Override
		public Map<String, Object> getDegreeZCList(Map<String, Object> map,int page,int rows) {
			 PageInfo<DegreeProgramTemporarystorage> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findDegreeTemporarystorage",map,page,rows);
			 map = 	new HashMap<String, Object>();
			 map.put("rows", list.getList());
			 map.put("total", list.getTotal());
			return map;
		}	
		
// 获取数据-学科建设_学位点_归档表
	/**
	 * 保存数据
	 */
	@Transactional
	@Override
	public int saveDegreeFile(DegreeProgramFile degreeProgramFile) {
		// TODO Auto-generated method stub
		try {
			baseService.saveOrUpdateByJpa(DegreeProgramFile.class, Integer.class,
					degreeProgramFile);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}

	/**
	 * 删除数据
	 */
	@Transactional
	@Override
	public int deleteDegreeFile(Integer userId) {
		try {
			baseService.deleteByIdByJpa(DegreeProgramFile.class, Integer.class, userId);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}

	/**
	 * 查找数据
	 */
	@Override
	public Map<String, Object> getDegreeFileList(Map<String, Object> map, int page, int rows) {
		PageInfo<DegreeProgramFile> list = baseService
				.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findDegreeFile", map, page, rows);
		map = new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());
		return map;
	}		
	


	// 获取数据-学科建设_一流学科_不采纳表
		 /**
		  * 保存数据
		  * */
		@Transactional
		@Override
		public int saveDiscipline(FirstDisciplineNotadopted firstDisciplineNotadopted) {
			// TODO Auto-generated method stub
			try {
				baseService.saveOrUpdateByJpa(FirstDisciplineNotadopted.class, Integer.class, firstDisciplineNotadopted);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		 /**
		  * 删除数据
		  * */
		@Transactional
		@Override
		public int deleteDiscipline(Integer userId) {
			try {
				baseService.deleteByIdByJpa(FirstDisciplineNotadopted.class, Integer.class, userId);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		/**
		 * 查找数据
		 * */
		@Override
		public Map<String, Object> getDisciplineList(Map<String, Object> map,int page,int rows) {
			 PageInfo<FirstDisciplineNotadopted> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findFirstDisciplineNotadopted",map,page,rows);
			 map = 	new HashMap<String, Object>();
			 map.put("rows", list.getList());
			 map.put("total", list.getTotal());
			return map;
		}

		
	// 获取数据-学科建设_一流学科_采纳表
			 /**
			  * 保存数据
			  * */
			@Transactional
			@Override
			public int saveDisciplineAdopt(FirstDisciplineAdopt firstDisciplineAdopt) {
				// TODO Auto-generated method stub
				try {
					baseService.saveOrUpdateByJpa(FirstDisciplineAdopt.class, Integer.class, firstDisciplineAdopt);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			 /**
			  * 删除数据
			  * */
			@Transactional
			@Override
			public int deleteDisciplineAdopt(Integer userId) {
				try {
					baseService.deleteByIdByJpa(FirstDisciplineAdopt.class, Integer.class, userId);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			/**
			 * 查找数据
			 * */
			@Override
			public Map<String, Object> getDisciplineAdoptList(Map<String, Object> map,int page,int rows) {
				 PageInfo<FirstDisciplineAdopt> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findFirstDisciplineAdopt",map,page,rows);
				 map = 	new HashMap<String, Object>();
				 map.put("rows", list.getList());
				 map.put("total", list.getTotal());
				return map;
			}

			
	// 获取数据-学科建设_一流学科_暂存表
			 /**
			  * 保存数据
			  * */
			@Transactional
			@Override
			public int saveDisciplineZC(FirstDisciplineTemporarystorage firstDisciplineTemporarystorage) {
				// TODO Auto-generated method stub
				try {
					baseService.saveOrUpdateByJpa(FirstDisciplineTemporarystorage.class, Integer.class, firstDisciplineTemporarystorage);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			 /**
			  * 删除数据
			  * */
			@Transactional
			@Override
			public int deleteDisciplineZC(Integer userId) {
				try {
					baseService.deleteByIdByJpa(FirstDisciplineTemporarystorage.class, Integer.class, userId);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			/**
			 * 查找数据
			 * */
			@Override
			public Map<String, Object> getDisciplineZCList(Map<String, Object> map,int page,int rows) {
				 PageInfo<FirstDisciplineTemporarystorage> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findFirstDisciplineTemporarystorage",map,page,rows);
				 map = 	new HashMap<String, Object>();
				 map.put("rows", list.getList());
				 map.put("total", list.getTotal());
				return map;
			}	
			
	// 获取数据-学科建设_一流学科_归档表
		/**
		 * 保存数据
		 */
		@Transactional
		@Override
		public int saveDisciplineFile(FirstDisciplineFile firstDisciplineFile) {
			// TODO Auto-generated method stub
			try {
				baseService.saveOrUpdateByJpa(FirstDisciplineFile.class, Integer.class,
						firstDisciplineFile);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}

		/**
		 * 删除数据
		 */
		@Transactional
		@Override
		public int deleteDisciplineFile(Integer userId) {
			try {
				baseService.deleteByIdByJpa(FirstDisciplineFile.class, Integer.class, userId);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}

		/**
		 * 查找数据
		 */
		@Override
		public Map<String, Object> getDisciplineFileList(Map<String, Object> map, int page, int rows) {
			PageInfo<FirstDisciplineTemporarystorage> list = baseService
					.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findFirstDisciplineFile", map, page, rows);
			map = new HashMap<String, Object>();
			map.put("rows", list.getList());
			map.put("total", list.getTotal());
			return map;
		}	
		
		

// 获取数据-学科建设_协同创新_不采纳表
		 /**
		  * 保存数据
		  * */
		@Transactional
		@Override
		public int saveCollaborative(CollaborativeInnovationNotadopted collaborativeInnovationNotadopted) {
			// TODO Auto-generated method stub
			try {
				baseService.saveOrUpdateByJpa(CollaborativeInnovationNotadopted.class, Integer.class, collaborativeInnovationNotadopted);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		 /**
		  * 删除数据
		  * */
		@Transactional
		@Override
		public int deleteCollaborative(Integer userId) {
			try {
				baseService.deleteByIdByJpa(CollaborativeInnovationNotadopted.class, Integer.class, userId);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
		/**
		 * 查找数据
		 * */
		@Override
		public Map<String, Object> getCollaborativeList(Map<String, Object> map,int page,int rows) {
			 PageInfo<CollaborativeInnovationNotadopted> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findCollaborativeNotadopted",map,page,rows);
			 map = 	new HashMap<String, Object>();
			 map.put("rows", list.getList());
			 map.put("total", list.getTotal());
			return map;
		}

		
	// 获取数据-学科建设_协同创新_采纳表
			 /**
			  * 保存数据
			  * */
			@Transactional
			@Override
			public int saveCollaborativeAdopt(CollaborativeInnovationAdopt collaborativeInnovationAdopt) {
				// TODO Auto-generated method stub
				try {
					baseService.saveOrUpdateByJpa(CollaborativeInnovationAdopt.class, Integer.class, collaborativeInnovationAdopt);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			 /**
			  * 删除数据
			  * */
			@Transactional
			@Override
			public int deleteCollaborativeAdopt(Integer userId) {
				try {
					baseService.deleteByIdByJpa(CollaborativeInnovationAdopt.class, Integer.class, userId);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			/**
			 * 查找数据
			 * */
			@Override
			public Map<String, Object> getCollaborativeAdoptList(Map<String, Object> map,int page,int rows) {
				 PageInfo<CollaborativeInnovationAdopt> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findCollaborativeAdopt",map,page,rows);
				 map = 	new HashMap<String, Object>();
				 map.put("rows", list.getList());
				 map.put("total", list.getTotal());
				return map;
			}

			
	// 获取数据-学科建设_协同创新_暂存表
			 /**
			  * 保存数据
			  * */
			@Transactional
			@Override
			public int saveCollaborativeZC(CollaborativeInnovationTemporarystorage collaborativeInnovationTemporarystorage) {
				// TODO Auto-generated method stub
				try {
					baseService.saveOrUpdateByJpa(CollaborativeInnovationTemporarystorage.class, Integer.class, collaborativeInnovationTemporarystorage);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			 /**
			  * 删除数据
			  * */
			@Transactional
			@Override
			public int deleteCollaborativeZC(Integer userId) {
				try {
					baseService.deleteByIdByJpa(CollaborativeInnovationTemporarystorage.class, Integer.class, userId);
				} catch (Exception e) {
					return -1;
				}
				return 1;
			}
			/**
			 * 查找数据
			 * */
			@Override
			public Map<String, Object> getCollaborativeZCList(Map<String, Object> map,int page,int rows) {
				 PageInfo<CollaborativeInnovationTemporarystorage> list = baseService.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findCollaborativeTemporarystorage",map,page,rows);
				 map = 	new HashMap<String, Object>();
				 map.put("rows", list.getList());
				 map.put("total", list.getTotal());
				return map;
			}	
			
	// 获取数据-学科建设_协同创新_归档表
		/**
		 * 保存数据
		 */
		@Transactional
		@Override
		public int saveCollaborativeFile(CollaborativeInnovationFile collaborativeInnovationFile) {
			// TODO Auto-generated method stub
			try {
				baseService.saveOrUpdateByJpa(CollaborativeInnovationFile.class, Integer.class,
						collaborativeInnovationFile);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}

		/**
		 * 删除数据
		 */
		@Transactional
		@Override
		public int deleteCollaborativeFile(Integer userId) {
			try {
				baseService.deleteByIdByJpa(CollaborativeInnovationFile.class, Integer.class, userId);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}

		/**
		 * 查找数据
		 */
		@Override
		public Map<String, Object> getCollaborativeFileList(Map<String, Object> map, int page, int rows) {
			PageInfo<CollaborativeInnovationFile> list = baseService
					.selectListByBatis(MapperNSEnum.disciplineConstructionMaper, "findCollaborativeFile", map, page, rows);
			map = new HashMap<String, Object>();
			map.put("rows", list.getList());
			map.put("total", list.getTotal());
			return map;
		}	
		
}
