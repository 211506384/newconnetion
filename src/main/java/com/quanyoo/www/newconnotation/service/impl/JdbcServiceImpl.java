package com.quanyoo.www.newconnotation.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.service.JdbcService;

@Service
public class JdbcServiceImpl implements JdbcService {

	/**
	 * 插入数据
	 */
	@Transactional
	public int add(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map) {
		StringBuffer str = new StringBuffer();
		str.append("insert into ");
		str.append(tableName + " ");

		if (map != null) {
			String fieldString = "";
			String fieldValue = "";
			for (String s : map.keySet()) {
				fieldString += s + ",";
				fieldValue += "'" + map.get(s) + "',";
			}
			fieldString = fieldString.substring(0, fieldString.length() - 1);
			fieldValue = fieldValue.substring(0, fieldValue.length() - 1);

			str.append("(" + fieldString + ") " + "values(" + fieldValue + ") ");
		}
		int add = jdbcTemplate.update(str.toString());
		return add;
	}

	/**
	 * 修改数据
	 */
	@Transactional
	public int update(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id) {
		StringBuffer str = new StringBuffer();
		str.append("update ");
		str.append(tableName + " SET ");

		if (map != null) {
			String fieldString = "";
			for (String s : map.keySet()) {
				fieldString += s + "='" + map.get(s) + "',";
			}
			fieldString = fieldString.substring(0, fieldString.length() - 1);

			str.append(fieldString);
		}

		str.append(" where 1=1 and " + "id=" + Id);
		int update = jdbcTemplate.update(str.toString());
		return update;
	}

	/**
	 * 删除数据
	 */
	@Transactional
	public int delete(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id) {
		StringBuffer str = new StringBuffer();
		str.append("delete from  ");
		str.append(tableName + " where id=" + Id);
		int delete = jdbcTemplate.update(str.toString());
		return delete;
	}

	/**
	 * select数据-带条件
	 */
	@Transactional
	public List<Map<String, Object>> select(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map,
			int pageNow, int pageSize) {
		StringBuffer str = new StringBuffer();
		str.append("select * from  ");
		str.append(tableName + " ");

		String fieldString = "";

		if (map != null) {

			for (String s : map.keySet()) {
				if (!"".equals(map.get(s)) && map.get(s) != null) {
					fieldString += s + "='" + map.get(s) + "' and ";
				}
			}
		}
		str.append("where " + fieldString + "1=1 limit ");
		str.append((pageNow - 1) * pageSize + "," + pageSize);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(str.toString());

		return list;

	}

	/**
	 * 分页查找-带条件
	 */
	@Override
	public PageInfo<Map<String, Object>> selectListBypage(JdbcTemplate jdbcTemplate, String tableName,
			Map<String, Object> params, int pageNow, int pageSize) {
		if (pageNow > 0 && pageSize > 0) {
			PageHelper.startPage(pageNow, pageSize);
		}
		List<Map<String, Object>> list = select(jdbcTemplate, tableName, params, pageNow, pageSize);
		// 用PageInfo对结果进行包装
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		StringBuffer str = new StringBuffer();
		str.append("select count(*) from  ");
		str.append(tableName + " ");

		String fieldString = "";

		if (params != null) {

			for (String s : params.keySet()) {
				if (!"".equals(params.get(s)) && params.get(s) != null) {
					fieldString += s + "='" + params.get(s) + "' and ";
				}
			}
		}
		str.append("where " + fieldString + "1=1 ");
		long total = jdbcTemplate.queryForObject(str.toString(), Integer.class);
		page.setTotal(total);
		return page;
	}

	/**
	 * select数据
	 */
	@Transactional
	public List<Map<String, Object>> select1(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map,
			int pageNow, int pageSize) {
		StringBuffer str = new StringBuffer();
		str.append("select * from  ");
		str.append(tableName + " ");

		str.append("where 1=1 limit ");
		str.append((pageNow - 1) * pageSize + "," + pageSize);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(str.toString());

		return list;

	}
	
	/**
	 * 分页查找
	 */
	@Override
	public PageInfo<Map<String, Object>> selectListBypage1(JdbcTemplate jdbcTemplate, String tableName,
			Map<String, Object> params, int pageNow, int pageSize) {
		if (pageNow > 0 && pageSize > 0) {
			PageHelper.startPage(pageNow, pageSize);
		}
		List<Map<String, Object>> list = select1(jdbcTemplate, tableName, params, pageNow, pageSize);
		// 用PageInfo对结果进行包装
		PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
		StringBuffer str = new StringBuffer();
		str.append("select count(*) from  ");
		str.append(tableName + " ");


		str.append("where 1=1 ");
		long total = jdbcTemplate.queryForObject(str.toString(), Integer.class);
		page.setTotal(total);
		return page;
	}

	
	/**
	 * 根据表名查找字段和表信息
	 */
	@Override
	public List<Table> getTableAndColumns(JdbcTemplate jdbcTemplate, String tableName) {
		List<Table> list = new ArrayList<Table>();
		// 根据表名查找表信息
		StringBuffer tableSql = new StringBuffer();
		tableSql.append("select 	create_table_english_name,\r\n"
				+ "	ifnull(create_table_chinese_name,\"表未命名中文\") create_table_chinese_name "
				+ "from  connotation_table_create ");

		tableSql.append("where  create_table_english_name='" + tableName + "' and 1=1 ");
		Map<String, Object> m = jdbcTemplate.queryForMap(tableSql.toString());
		Table table=JSON.parseObject(JSON.toJSONString(m), Table.class);

		// 根据表名查找表字段信息

		StringBuffer columnSql = new StringBuffer();
		columnSql.append("select 	create_table_column,\r\n"
				+ "	ifnull(create_table_chinese_column,\"表未命名中文\") create_table_chinese_column "
				+ "from  connotation_table_column_create ");

		columnSql.append("where  create_table_name='" + tableName + "' and 1=1 ");
		List<Map<String, Object>> queryList = jdbcTemplate.queryForList(columnSql.toString());
		
	      List<TableColumn> dtoList = new ArrayList<TableColumn>();
      for (Map<String, Object> p:queryList) {
          TableColumn tableColumn=JSON.parseObject(JSON.toJSONString(p), TableColumn.class);
         
          dtoList.add(tableColumn );
      }
		table.setTableColumn(dtoList);
		list.add(table);
		return list;
	}
	
	/**
	 * 自动建库建表
	 * */
	@Override
	public int createTableData(JdbcTemplate jdbcTemplate, String dataBaseName, List<Table> list) {

		try {
			//建库
			String sql_ku = "CREATE DATABASE " + dataBaseName;
			jdbcTemplate.update(sql_ku);
			// 连接数据库
			String sql_use = "USE " + dataBaseName;
			jdbcTemplate.update(sql_use);
			// 执行创表语句
			if (list.size() > 0) {
				for (Table table : list) {
					StringBuffer sql=new StringBuffer();
					String fieldValue = "";
					//表名
					String tableName=table.getCreate_table_english_name();
					//字段集合
					List<TableColumn> tableColumnList =table.getTableColumn();
					sql.append("CREATE TABLE ");
					sql.append(tableName + "(id int(11) primary key not null auto_increment,");
					for(TableColumn tableColumn:tableColumnList) {
						if("id".equals(tableColumn.getCreate_table_column())) {
							continue;
						}
						fieldValue+=tableColumn.getCreate_table_column()+" "+tableColumn.getCreate_table_column_type()+",";

					}
						if(!"".equals(fieldValue)) {

							fieldValue = fieldValue.substring(0, fieldValue.length() - 1);
							sql.append(fieldValue+")");
							jdbcTemplate.update(sql.toString());
						}else {
							jdbcTemplate.update(sql.toString().substring(0, sql.toString().length() - 1)+")");
						}
				}
			}
			System.out.println("Created databaseName in given database...");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return -2;
		}
		System.out.println("Goodbye!");
		return 2;
	}
	/**
	 * 建库表结构
	 * 
	 * */
	@Override
	public List<Table> getTableAndColumns(JdbcTemplate jdbcTemplate) {
		List<Table> list = new ArrayList<Table>();
		// 根据表名查找表信息
		StringBuffer tableSql = new StringBuffer();
		tableSql.append("select 	id,create_table_english_name "
				
				+ " from  connotation_table_create ");

		tableSql.append("where   1=1 ");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(tableSql.toString());
		for(Map<String, Object> a:m) {
			Table table=JSON.parseObject(JSON.toJSONString(a), Table.class);
			// 根据表名查找表字段信息

			StringBuffer columnSql = new StringBuffer();
			columnSql.append("select  create_table_column_type,create_table_column "
					+ " from  connotation_table_column_create ");

			columnSql.append("where  create_table_name ='"+table.getCreate_table_english_name()+"' and  1=1 ");
			List<Map<String, Object>> queryList = jdbcTemplate.queryForList(columnSql.toString());
			
		      List<TableColumn> dtoList = new ArrayList<TableColumn>();
	        for (Map<String, Object> p:queryList) {
	            TableColumn tableColumn=JSON.parseObject(JSON.toJSONString(p), TableColumn.class);
	           
	            dtoList.add(tableColumn );
	        }

			table.setTableColumn(dtoList);
			list.add(table);
		}
		return list;
	}
	/**
	 * 院系（部门）统计
	 * */
	@Override
	public List<Map<String, Object>> getdepartMentList(JdbcTemplate jdbcTemplate) {
		StringBuffer str = new StringBuffer();
		str.append("select DISTINCT(department_name) as department_name  from  calendar_year ");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());
		return m;
	}
	/**
	 * 院系（部门）统计year
	 * */
	@Override
	public List<Map<String, Object>> getdepartMentYearList(JdbcTemplate jdbcTemplate) {
		StringBuffer str = new StringBuffer();
		str.append("select DISTINCT(year) as year  from  calendar_year  ORDER BY year asc");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());
		return m;
	}

	@Override
	public List<Map<String, Object>> getdepartMentStaticsList(JdbcTemplate jdbcTemplate) {
		StringBuffer str = new StringBuffer();
		str.append("select department_name,progress_score,total_project_score,total_score,year"
				+ "  from  calendar_year  GROUP BY department_name,year ");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());
		return m;
	}
	
	/**
	 * 
	 *	统计个人年度得分表数据
	 */
	@Override
	public List<Map<String, Object>> getIndividualSoreList(JdbcTemplate jdbcTemplate) {
		StringBuffer str = new StringBuffer();
		str.append("select id,job_number,full_name,department,total_score,year"
				+ "  from  personal_score ");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());
		return m;
	}
}
