package com.quanyoo.www.newconnotation.service;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.DbInfo;
import com.quanyoo.www.newconnotation.pojo.Table;

public interface ModelService {
	
	/**
	 * 根据年度动态获取表格字段属性
	 */
	List<Table> getTablesByontextKey(String tableName, DbInfo dbInfo,String year);

	/**
	 * 根据年度动态获取一级菜单
	 */

	List<Map<String, Object>> getParentList(Map<String, Object> map, DbInfo dbInfo,String year);

	/**
	 * 根据年度动态获取二级菜单
	 */

	Map<String, Object> getSecondMenu(Map<String, Object> map, DbInfo dbInfo,String year);

	/**
	 * 根据年度动态获取考核数据集合分页
	 */
	PageInfo<Map<String, Object>> getTableList(Map<String, Object> map, DbInfo dbInfo, Integer pageNow,
			Integer pageSize,String year);

	/**
	 * 
	 * 新增数据
	 */

	public int add(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, DbInfo dbInfo,String year);

	/**
	 * 
	 * 更新数据
	 */
	public int update(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id, DbInfo dbInfo,String year);

	/**
	 * 
	 * 删除数据
	 */

	public int delete(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id, DbInfo dbInfo,String year);

	/**
	 * 根据年度动态获取考核数据集合不分页
	 */
	public List<Map<String, Object>> getTableListNoPage(Map<String, Object> map, DbInfo dbInfo,String year);

	/**
	 * 考核指标数据三表历集合
	 */
	public List<Map<String, Object>> getDataStatistics(String tableName, String tableName1, String tableName2,
			DbInfo dbInfo,String year);

	/**
	 * 获取jdbctemplate
	 */
	public JdbcTemplate getJdbcTemplate(DbInfo dbInfo,String year);
	
	/**
	 * 自动建表
	 */
	public int createTableData(JdbcTemplate jdbcTemplate,String dataBaseName, List<Table> list,String year);
	
	
	
	/**
	 * 院系（部门）统计数据
	 * */
	public List<Map<String, Object>> getdepartMentStaticsList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year);
	
	/**
	 * 院系（部门）统计
	 * */
	public List<Map<String, Object>> getdepartMentList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year);
	/**
	 * 院系（部门）统计year
	 * */
	public List<Map<String, Object>> getdepartMentYearList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year);
	
	
	/**
	 * 统计个人年度得分表数据
	 * */
	public List<Map<String, Object>> getIndividualSoreList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year);
}
