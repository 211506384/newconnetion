package com.quanyoo.www.newconnotation.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.StatisticsService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
@Service
public class StatisticsServiceImpl  implements StatisticsService{
	@Resource
	private IBaseService baseService;
	@Override
	public List<Table> getTemporarystorageList(Map<String,Object> map) {
		
		List<Table> list=	baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTemporarystorageList", map);
	
		return list;
	}

}
