package com.quanyoo.www.newconnotation.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.UserInformation;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.YearSystemService;
import com.quanyoo.www.newconnotation.sqlDataSource.DataSourceUtil;
import com.quanyoo.www.newconnotation.sqlDataSource.DynamicDataSourceContextHolder;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;

@Service
public class YearSystemServiceImpl implements YearSystemService {
	 @Resource
	    private IBaseService baseService;
	@Override
	public Map<String, Object> getUserList(String year, Map<String, Object> map, int page, int rows) {
		// 数据源key
				String newDsKey = year;
				// 添加数据源
				DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
				DynamicDataSourceContextHolder.setContextKey(newDsKey);
		 PageInfo<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "findUserInformationListAll",map,page,rows);
		 map = 	new HashMap<String, Object>();
		 map.put("rows", list.getList());
		 map.put("total", list.getTotal());
			DynamicDataSourceContextHolder.removeContextKey();
		return map;
	}

	@Override
	public Map<String, Object> saveRolePermissionList(String year, Integer roleId, List<Integer> permissionIdList) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		try {
			//删除原来的权限
			baseService.deleteByBatis(MapperNSEnum.UserManagementMaper, "deleteRolePermission", roleId);
			
					//新增角色权限
				for(Integer integer: permissionIdList) {
					map = new HashMap<String, Object>();
					map.put("roleId",roleId);
					map.put("permissionId",integer);
					baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "saveRolePermission", map);
				}
			
		} catch (Exception e) {
			map = new HashMap<String, Object>();
			map.put("code", -1);
			map.put("msg", "分配失败");
			DynamicDataSourceContextHolder.removeContextKey();
			return map;
		}
		map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("msg", "分配成功");
		DynamicDataSourceContextHolder.removeContextKey();
		return map;
	
	}

	@Override
	public List<DepartmentInformation> getDepartment(String year, Map<String, Object> map) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		List<DepartmentInformation> departmentInformations = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "getDepartmentSelect", map);
		DynamicDataSourceContextHolder.removeContextKey();
		return departmentInformations;
	}

	@Override
	public int saveUser(String year, UserInformation userInformation) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		try {
			baseService.saveOrUpdateByJpa(UserInformation.class, Integer.class, userInformation);
		} catch (Exception e) {
			return -1;
		}
		DynamicDataSourceContextHolder.removeContextKey();
		return 1;
	}

	@Override
	public int deleteUser(String year, Integer userId) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		try {
			baseService.deleteByIdByJpa(UserInformation.class, Integer.class, userId);
		} catch (Exception e) {

			DynamicDataSourceContextHolder.removeContextKey();
			return -1;
		}

		DynamicDataSourceContextHolder.removeContextKey();
		return 1;
	}

	@Override
	public int resetPassword(String year, Map<String, Object> params) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		try {
			baseService.updateByBatis(MapperNSEnum.UserManagementMaper, "resetPassword", params);
		} catch (Exception e) {

			DynamicDataSourceContextHolder.removeContextKey();
			return -1;
		}

		DynamicDataSourceContextHolder.removeContextKey();
		return 1;
	}

	@Override
	public Map<String, Object> getDepartmentTable(String year, Map<String, Object> map, int page, int rows) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		PageInfo<UserInformation> list = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "getDepartmentInformation",map,page,rows);
		map = 	new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());
		DynamicDataSourceContextHolder.removeContextKey();
		return map;
	}

	@Override
	public int saveDepartment(String year, Map<String, Object> map) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		// TODO Auto-generated method stub
		Map<String,Object> departMap = new HashMap<>();
		departMap.put("departName",map.get("name"));
		departMap.put("department_serial_number",map.get("department_serial_number"));
		departMap.put("department_category",map.get("department_category"));
		
		List<Map<String,Object>> dertList = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,"getDepartMentByNameList",departMap);
		if (dertList.size()>0){
			DynamicDataSourceContextHolder.removeContextKey();
			return -2;
		}else {
			try {
				baseService.insertByBatis(MapperNSEnum.UserManagementMaper,"addDeprmet",map);
			} catch (Exception e) {
				DynamicDataSourceContextHolder.removeContextKey();
				return -1;
			}
			DynamicDataSourceContextHolder.removeContextKey();
			return 1;
		}
	}

	@Override
	public int updateDepartment(String year, Map<String, Object> map) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		// TODO Auto-generated method stub
				Map<String,Object> departMap = new HashMap<>();
				departMap.put("departName",map.get("name"));
				departMap.put("department_serial_number",map.get("department_serial_number"));
				departMap.put("department_category",map.get("department_category"));
				departMap.put("id",map.get("id"));
				List<Map<String,Object>> dertList = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,"getDepartMentByNameList",departMap);
				if (dertList.size()>0){
					DynamicDataSourceContextHolder.removeContextKey();
					return -2;
				}else {
					try {
						baseService.insertByBatis(MapperNSEnum.UserManagementMaper,"updateDeprmet",map);
					} catch (Exception e) {
						DynamicDataSourceContextHolder.removeContextKey();
						return -1;
					}
					DynamicDataSourceContextHolder.removeContextKey();
					return 1;
				}
	}

	@Override
	public int deleteDepartment(String year, Integer userId) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		try {
			baseService.deleteByBatis(MapperNSEnum.UserManagementMaper,"deleteDepartment",userId);
		} catch (Exception e) {
			DynamicDataSourceContextHolder.removeContextKey();
			return -1;
		}
		DynamicDataSourceContextHolder.removeContextKey();
		return 1;
	}
		
}
