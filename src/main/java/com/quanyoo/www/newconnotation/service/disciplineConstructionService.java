package com.quanyoo.www.newconnotation.service;

import java.util.List;
import java.util.Map;

import com.quanyoo.www.newconnotation.model.CollaborativeInnovationAdopt;
import com.quanyoo.www.newconnotation.model.CollaborativeInnovationFile;
import com.quanyoo.www.newconnotation.model.CollaborativeInnovationNotadopted;
import com.quanyoo.www.newconnotation.model.CollaborativeInnovationTemporarystorage;
import com.quanyoo.www.newconnotation.model.DegreeProgramAdopt;
import com.quanyoo.www.newconnotation.model.DegreeProgramFile;
import com.quanyoo.www.newconnotation.model.DegreeProgramNotadopted;
import com.quanyoo.www.newconnotation.model.DegreeProgramTemporarystorage;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.FirstDisciplineAdopt;
import com.quanyoo.www.newconnotation.model.FirstDisciplineFile;
import com.quanyoo.www.newconnotation.model.FirstDisciplineNotadopted;
import com.quanyoo.www.newconnotation.model.FirstDisciplineTemporarystorage;
import com.quanyoo.www.newconnotation.model.UserInformation;

public interface disciplineConstructionService {

// 获取数据-学科建设_学位点_不采纳表
	public Map<String, Object> getDegreeList(Map<String, Object> map, int page, int rows);

	// 保存数据
	public int saveDegree(DegreeProgramNotadopted degreeProgramNotadopted);

	// 删除数据
	public int deleteDegree(Integer userId);
	
	
// 获取数据-学科建设_学位点_采纳表
	public Map<String, Object> getDegreeAdoptList(Map<String, Object> map, int page, int rows);

	// 保存数据
	public int saveDegreeAdopt(DegreeProgramAdopt degreeProgramAdopt);

	// 删除数据
	public int deleteDegreeAdopt(Integer userId);

	
// 获取数据-学科建设_学位点_暂存表
	public Map<String, Object> getDegreeZCList(Map<String, Object> map, int page, int rows);

		// 保存数据
	public int saveDegreeZC(DegreeProgramTemporarystorage degreeProgramTemporarystorage);

		// 删除数据
	public int deleteDegreeZC(Integer userId);
	
	
// 获取数据-学科建设_学位点_归档表
	public Map<String, Object> getDegreeFileList(Map<String, Object> map, int page, int rows);

			// 保存数据
	public int saveDegreeFile(DegreeProgramFile degreeProgramFile);

			// 删除数据
	public int deleteDegreeFile(Integer userId);	
	
	
	
	// 获取数据-学科建设_一流学科_不采纳表
	public Map<String, Object> getDisciplineList(Map<String, Object> map, int page, int rows);

		// 保存数据
	public int saveDiscipline(FirstDisciplineNotadopted firstDisciplineNotadopted);

		// 删除数据
	public int deleteDiscipline(Integer userId);
		
		
	// 获取数据-学科建设_一流学科_采纳表
	public Map<String, Object> getDisciplineAdoptList(Map<String, Object> map, int page, int rows);

		// 保存数据
	public int saveDisciplineAdopt(FirstDisciplineAdopt firstDisciplineAdopt);

		// 删除数据
	public int deleteDisciplineAdopt(Integer userId);

		
	// 获取数据-学科建设_一流学科_暂存表
	public Map<String, Object> getDisciplineZCList(Map<String, Object> map, int page, int rows);

			// 保存数据
	public int saveDisciplineZC(FirstDisciplineTemporarystorage firstDisciplineTemporarystorage);

			// 删除数据
	public int deleteDisciplineZC(Integer userId);
	
		
// 获取数据-学科建设_一流学科_归档表
	public Map<String, Object> getDisciplineFileList(Map<String, Object> map, int page, int rows);

		// 保存数据
	public int saveDisciplineFile(FirstDisciplineFile firstDisciplineFile);
	
		// 删除数据
	public int deleteDisciplineFile(Integer userId);
		

	
		// 获取数据-学科建设_协同创新_不采纳表
	public Map<String, Object> getCollaborativeList(Map<String, Object> map, int page, int rows);

		// 保存数据
	public int saveCollaborative(CollaborativeInnovationNotadopted collaborativeInnovationNotadopted);

		// 删除数据
	public int deleteCollaborative(Integer userId);
		
		
	// 获取数据-学科建设_协同创新_采纳表
	public Map<String, Object> getCollaborativeAdoptList(Map<String, Object> map, int page, int rows);

		// 保存数据
	public int saveCollaborativeAdopt(CollaborativeInnovationAdopt collaborativeInnovationAdopt);

		// 删除数据
	public int deleteCollaborativeAdopt(Integer userId);

		
	// 获取数据-学科建设_协同创新_暂存表
	public Map<String, Object> getCollaborativeZCList(Map<String, Object> map, int page, int rows);

			// 保存数据
	public int saveCollaborativeZC(CollaborativeInnovationTemporarystorage collaborativeInnovationTemporarystorage);

			// 删除数据
	public int deleteCollaborativeZC(Integer userId);
		
		
// 获取数据-学科建设_协同创新_归档表
	public Map<String, Object> getCollaborativeFileList(Map<String, Object> map, int page, int rows);

		// 保存数据
	public int saveCollaborativeFile(CollaborativeInnovationFile collaborativeInnovationFile);

		// 删除数据
	public int deleteCollaborativeFile(Integer userId);		
		
}
