package com.quanyoo.www.newconnotation.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.dao.IBaseDao;
import com.quanyoo.www.newconnotation.dao.impl.BaseRepositoryImpl;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;


@Transactional
@Repository("baseService")
public class BaseServiceImpl implements IBaseService {
	@Autowired
	IBaseDao baseDao;
	private final EntityManager entityManager;

	public BaseServiceImpl(EntityManager entityManager) {
	    this.entityManager = entityManager;
	}

	private Map<String, Object> repositoryDaos = new HashMap<String, Object>();

	@Override
	public <T> List<T> findByHql(Class<T> entityClass, String qlString, Object[] values) {
		TypedQuery<T> query = entityManager.createQuery(qlString, entityClass);
		if (values != null && values.length > 0) {
			int i = 1;
			for (Object value : values)
				query.setParameter(i++, value);
		}
		return query.getResultList();
	}

	@Override
	public int execByHql(String qlString, Object[] values) {
		Query query = entityManager.createQuery(qlString);
		if (values != null && values.length > 0) {
			int i = 1;
			for (Object value : values)
				query.setParameter(i++, value);
		}
		return query.executeUpdate();
	}

	@Override
	public void execProcByJpa(String callString, Object[] values) {
		Query query = entityManager.createNativeQuery("{ call " + callString + "}");
		if (values != null && values.length > 0) {
			int i = 1;
			for (Object value : values)
				query.setParameter(i++, value);
		}
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T, ID extends Serializable> BaseRepositoryImpl<T, ID> getRepositoryDao(Class<T> entityClass, Class<ID> idClass) {
		if (repositoryDaos.containsKey(entityClass.getName())) {
			return (BaseRepositoryImpl<T, ID>) repositoryDaos.get(entityClass.getName());
		} else {
			BaseRepositoryImpl<T, ID> dao = new BaseRepositoryImpl<T, ID>(entityClass, entityManager);
			repositoryDaos.put(entityClass.getName(), dao);
			return dao;
		}
	}


	@Override
	public <T,ID extends Serializable> T saveOrUpdateByJpa(Class<T> entityClass, Class<ID> idClass, T entity){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		return dao.save(entity);
	}

	@Override
	public <T,ID extends Serializable> List<T> saveOrUpdateBatchByJpa(Class<T> entityClass, Class<ID> idClass, List<T> entities){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		return dao.saveAll(entities);
	}

	@Override
	public <T,ID extends Serializable> void deleteByJpa(Class<T> entityClass, Class<ID> idClass, T entity){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		dao.delete(entity);
	}

	@Override
	public <T,ID extends Serializable> void deleteByIdByJpa(Class<T> entityClass, Class<ID> idClass, ID id){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		dao.deleteById(id);
	}

	@Override
	public <T,ID extends Serializable> void deleteBatchByJpa(Class<T> entityClass, Class<ID> idClass, List<T> entities){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		dao.deleteAll(entities);
	}

	@Override
	public <T,ID extends Serializable> T findByIdByJpa(Class<T> entityClass, Class<ID> idClass, ID id){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		return dao.getOne(id);
	}

	@Override
	public <T,ID extends Serializable> List<T> findAllByJpa(Class<T> entityClass, Class<ID> idClass){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		return dao.findAll();
	}

	@Override
	public <T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity,ExampleMatcher matcher){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		Example<T> example = Example.of(entity ,matcher);
		return  dao.findAll(example);
	}

	@Override
	public <T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity,Sort sort){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		Example<T> example = Example.of(entity);
		return  dao.findAll(example,sort);
	}

	@Override
	public <T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity,ExampleMatcher matcher,Sort sort){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		Example<T> example = null;
		if(matcher != null){
			example = Example.of(entity ,matcher);
		}
		else {
			example = Example.of(entity);
		}
		return  dao.findAll(example, sort);
	}

	@Override
	public <T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, Specification<T> specification){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		return  dao.findAll(specification);
	}

	@Override
	public <T,ID extends Serializable> Page<T> findPageByJpa(Class<T> entityClass, Class<ID> idClass, Specification<T> specification,Pageable pageable){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		return  dao.findAll(specification,pageable);
	}

	@Override
	public <T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		Example<T> example = Example.of(entity);
		return  dao.findAll(example);
	}

	@Override
	public <T,ID extends Serializable> Page<T> findPageByJpa(Class<T> entityClass, Class<ID> idClass, T entity, Pageable pagealbe){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		Example<T> example = Example.of(entity);
		return  dao.findAll(example,pagealbe);
	}

	@Override
	public <T,ID extends Serializable> Page<T> findPageByJpa(Class<T> entityClass, Class<ID> idClass, T entity,ExampleMatcher matcher, Pageable pagealbe){
		BaseRepositoryImpl<T,ID> dao = getRepositoryDao(entityClass,idClass);
		Example<T> example = Example.of(entity,matcher);
		return  dao.findAll(example,pagealbe);
	}

	@Override
	public <T, E> E selectByBatis(MapperNSEnum namespace, String id, T params) {
		return baseDao.select(namespace, id, params);
	}

	@Override
	public <T, E> List<E> selectListByBatis(MapperNSEnum namespace, String id, T params) {
		return baseDao.selectList(namespace, id, params);
	}

	@Override
	public <T, E> PageInfo<E> selectListByBatis(MapperNSEnum namespace, String id, T params, int pageNow, int pageSize) {
		return baseDao.selectList(namespace, id, params, pageNow, pageSize);
	}

	@Override
	public <T> int updateByBatis(MapperNSEnum namespace, String id, T params) {
		return baseDao.update(namespace, id, params);
	}

	@Override
	public <T> List<Long> updateListByBatis(MapperNSEnum namespace, String id, List<T> list) {
		return baseDao.updateList(namespace, id, list);
	}

	@Override
	public <T> long insertByBatis(MapperNSEnum namespace, String id, T params) {
		return baseDao.insert(namespace, id, params);
	}

	@Override
	public <T> List<Long> insertListByBatis(MapperNSEnum namespace, String id, List<T> list) {
		return baseDao.insertList(namespace, id, list);
	}

	@Override
	public <T> int deleteByBatis(MapperNSEnum namespace, String id, T params) {
		return baseDao.delete(namespace, id, params);
	}

	@Override
	public <T> List<Long> deleteListByBatis(MapperNSEnum namespace, String id, List<T> list) {
		return baseDao.deleteList(namespace, id, list);
	}

	@Override
	public <T> void batchALLByBatis(MapperNSEnum namespace, String id, List<T> params, Integer bathcount) {
		baseDao.batchAll(namespace, id, params, bathcount);
	}

}
