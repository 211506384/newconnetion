package com.quanyoo.www.newconnotation.service.impl;

import org.springframework.stereotype.Service;

import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.LoginService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LoginServiceImpl implements LoginService {

	@Resource
	private IBaseService baseService;

    /**
     * 数据库查询登入login
     * @param jobNum;password
     * @return
     */
	@Override
	public User findUserByphoneAndPassword(String jobNum, String password) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("jobNum",jobNum);
		map.put("password",password);
    User user = baseService.selectByBatis(MapperNSEnum.LoginMaper, "findUserByphoneAndPassword", map);
    return user;
	}
	  /**
     * 数据库查询用户角色
     * @param userId
     * @return List<Map<String, Object>>
     */
	@Override
	public List<Map<String, Object>> getUserRolesByUserId(String userId) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userId",userId);
		List<Map<String, Object>> roleslist = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "getUserRolesByUserId", map);
		return roleslist;
	}
	  /**
     * 数据库查询用户权限
     * @param userId
     * @return
     */
	@Override
	public List<Map<String, Object>> getUserPermissonsByUserId(String userId) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userId",userId);
		List<Map<String, Object>> permissionlist = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "getUserPermissonsByUserId", map);

		return permissionlist;
	}
	/**
	 * 数据库查询用户
	 * @param jobNum
	 * @return
	 */
	@Override
	public User findUserByphone(String jobNum) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("jobNum",jobNum);
		User user = baseService.selectByBatis(MapperNSEnum.LoginMaper, "findUserByphone", map);
		return user;
	}
}
