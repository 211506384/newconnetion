package com.quanyoo.www.newconnotation.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.permission.RolePermissionResolver;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.UserInformation;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.UserService;
@Service
public class UserServiceImpl implements UserService{
	 @Resource
	    private IBaseService baseService;
	 
	 /**
	  * 保存用户
	  * */
	@Transactional
	@Override
	public int saveUser(UserInformation userInformation) {
		// TODO Auto-generated method stub
		try {
			baseService.saveOrUpdateByJpa(UserInformation.class, Integer.class, userInformation);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	 /**
	  * 删除用户
	  * */
	@Transactional
	@Override
	public int deleteUser(Integer userId) {
		try {
			baseService.deleteByIdByJpa(UserInformation.class, Integer.class, userId);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	/**
	 * 查找用户
	 * */
	@Override
	public Map<String, Object> getUserList(Map<String, Object> map,int page,int rows) {
		 PageInfo<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "findUserInformationListAll",map,page,rows);
		 map = 	new HashMap<String, Object>();
		 map.put("rows", list.getList());
		 map.put("total", list.getTotal());
		return map;
	}
	/**
	 * 部门下拉框
	 * */
	@Override
	public List<DepartmentInformation> getDepartment(Map<String, Object> map) {
		List<DepartmentInformation> departmentInformations = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "getDepartmentSelect", map);
	
		return departmentInformations;
	}
/**
 * 重置密码
 * */
	@Override
	public int resetPassword(Map<String, Object>  params ) {
		try {
			baseService.updateByBatis(MapperNSEnum.UserManagementMaper, "resetPassword", params);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
/**
    * @param map
	* @param page
	* @param rows
    * @描述 获取部门列表
    * @return java.util.Map<java.lang.String,java.lang.Object>
    * @author WuRongPeng
    * @date 2020/8/3 14:10
*/
	@Override
	public Map<String, Object> getDepartmentTable(Map<String, Object> map, int page, int rows) {
		PageInfo<UserInformation> list = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "getDepartmentInformation",map,page,rows);
		map = 	new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());
		return map;
	}
/**
    * @param map
    * @描述 保存部门
    * @return int
    * @author WuRongPeng
    * @date 2020/8/3 11:27
*/
	@Override
	public int saveDepartment(Map<String,Object> map) {
		// TODO Auto-generated method stub
		Map<String,Object> departMap = new HashMap<>();
		departMap.put("departName",map.get("name"));
		departMap.put("department_serial_number",map.get("department_serial_number"));
		departMap.put("department_category",map.get("department_category"));
		
		List<Map<String,Object>> dertList = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,"getDepartMentByNameList",departMap);
		if (dertList.size()>0){
			return -2;
		}else {
			try {
				baseService.insertByBatis(MapperNSEnum.UserManagementMaper,"addDeprmet",map);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
	}

	@Override
	public int updateDepartment(Map<String, Object> map) {
		// TODO Auto-generated method stub
		Map<String,Object> departMap = new HashMap<>();
		departMap.put("departName",map.get("name"));
		departMap.put("department_serial_number",map.get("department_serial_number"));
		departMap.put("department_category",map.get("department_category"));
		departMap.put("id",map.get("id"));
		List<Map<String,Object>> dertList = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,"getDepartMentByNameList",departMap);
		if (dertList.size()>0){
			return -2;
		}else {
			try {
				baseService.insertByBatis(MapperNSEnum.UserManagementMaper,"updateDeprmet",map);
			} catch (Exception e) {
				return -1;
			}
			return 1;
		}
	}

	/**
    * @param userId
    * @描述 删除部门
    * @return int
    * @author WuRongPeng
    * @date 2020/8/3 11:28
*/
	@Transactional
	@Override
	public int deleteDepartment(Integer userId) {
		try {
			baseService.deleteByBatis(MapperNSEnum.UserManagementMaper,"deleteDepartment",userId);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	/**
	    * @param roleId
	    * @描述 分配角色权限
	    * @return  Map
	    * @author 
	    * @param permissionIdList
	*/
	@Transactional
	@Override
	public Map<String, Object> saveRolePermissionList(Integer roleId, List<Integer> permissionIdList) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//删除原来的权限
			baseService.deleteByBatis(MapperNSEnum.UserManagementMaper, "deleteRolePermission", roleId);
			
					//新增角色权限
				for(Integer integer: permissionIdList) {
					map = new HashMap<String, Object>();
					map.put("roleId",roleId);
					map.put("permissionId",integer);
					baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "saveRolePermission", map);
				}
			
		} catch (Exception e) {
			map = new HashMap<String, Object>();
			map.put("code", -1);
			map.put("msg", "分配失败");
			return map;
		}
		map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("msg", "分配成功");
		return map;
	}

}
