package com.quanyoo.www.newconnotation.service;

import com.quanyoo.www.newconnotation.model.DoctorsDegreeTemporarystorage;
import com.quanyoo.www.newconnotation.model.InnovationteamTemporarystorage;
import com.quanyoo.www.newconnotation.model.TalentIntroductionTemporarystorage;

import java.util.List;
import java.util.Map;

public interface InnovationteamService {
    // 查找创新团队
    Map<String, Object> getInnovationteamAdoptList(Map<String, Object> map, int page, int rows);
    // 保存创新团队
    int saveInnovationteamAdopt(Map<String, Object> map);
    // 删除用户
    int deleteInnovationteamAdopt(Integer id);
    int addInnovationteamAdopt(InnovationteamTemporarystorage innovationteamTemporarystorage);
    List<Map<String, Object>> teamBuildingSelect(Map<String, Object> map);
    List<Map<String, Object>> assmentIndexScoreSelect(Map<String, Object> map);
    Map<String, Object> getTalentIntroductionList(Map<String, Object> map, int page, int rows);
    int saveTalentIntroduction(Map<String, Object> map);
    int addTalentIntroduction(TalentIntroductionTemporarystorage talentIntroductionTemporarystorage);
    Map<String, Object> getDoctorsDegreeTable(Map<String, Object> map,int page,int rows);
    int saveDoctorsDegree(Map<String, Object> map);
    int addDoctorsDegree(DoctorsDegreeTemporarystorage doctorsDegreeTemporarystorage);
    Map<String, Object> getNewTeachersVisitingTable(Map<String, Object> map,int page,int rows);
    Map<String,Object> getFieldInformation(String tableName);
}
