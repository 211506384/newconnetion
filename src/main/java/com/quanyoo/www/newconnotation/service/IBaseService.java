package com.quanyoo.www.newconnotation.service;

import java.io.Serializable;

import java.util.List;

import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.dao.impl.BaseRepositoryImpl;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;


public interface IBaseService {

	public <T, ID extends Serializable> BaseRepositoryImpl<T, ID> getRepositoryDao(Class<T> entityClass, Class<ID> idClass);

	/**
	 * hql语句查询, 注意参数的序号从1开始，如："select detailId from SubsBindingDetail where detailId in(?1)"
	 * @param  entityClass  实体对象
	 * @param  qlString hql语句
	 * @return T 返回对象类型
	 */
	public <T> List<T> findByHql(Class<T> entityClass, String qlString, Object[] values);

	/**
	 * 执行hql语句, 注意参数的序号从1开始，如："select detailId from SubsBindingDetail where detailId in(?1)"
	 * @param  qlString 返回对象类型
	 * @param qlString hql语句
	 * @return int
	 */
	public int execByHql(String qlString, Object[] values);

	/**
	 * 执行存储过程
	 * @param callString，如：call ProName(?)
	 * @param values 参数
	 */
	public void execProcByJpa(String callString, Object[] values);

	/**
	 * JPA方法，保存对象：
	 * 调用示例：
	 * Systempreferences sys = new Systempreferences();
	   sys.setVariable("lipttest");
	   baseService.saveOrUpdateByJpa(Systempreferences.class, String.class, sys);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 操作对象实体
	 */
	<T,ID extends Serializable> T saveOrUpdateByJpa(Class<T> entityClass, Class<ID> idClass, T entity);

	/**
	 * JPA方法，批量保存对象：
	 * 调用示例：
	 * Systempreferences sys = new Systempreferences();
	   sys.setVariable("lipttest");
	   List<Systempreferences> batchList = new ArrayList<Systempreferences>();
	   batchList.add(sys);
	   baseService.saveOrUpdateBatchByJpa(Systempreferences.class, String.class, batchList);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entitys 操作对象实体
	 */
	<T,ID extends Serializable>  List<T> saveOrUpdateBatchByJpa(Class<T> entityClass, Class<ID> idClass, List<T> entitys);

	/**
	 * JPA方法，删除对象：
	 * 调用示例：
	 * Systempreferences sys = new Systempreferences();
	   sys.setVariable("lipttest");
	   baseService.deleteByJpa(Systempreferences.class, String.class, sys);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 操作对象实体
	 */
	<T,ID extends Serializable> void deleteByJpa(Class<T> entityClass, Class<ID> idClass, T entity);

	/**
	 * JPA方法，删除对象：
	 * 调用示例：
	 * Systempreferences sys = new Systempreferences();
	   sys.setVariable("lipttest");
	   baseService.deleteByJpa(Systempreferences.class, String.class, sys);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param id 操作对象主键值
	 */
	<T,ID extends Serializable> void deleteByIdByJpa(Class<T> entityClass, Class<ID> idClass, ID id);

	/**
	 * JPA方法，批量删除对象：
	 * 调用示例：
	 * Systempreferences sys = new Systempreferences();
	   sys.setVariable("lipttest");
	   List<Systempreferences> batchList = new ArrayList<Systempreferences>();
	   batchList.add(sys);
	   baseService.deleteByJpa(Systempreferences.class, String.class, batchList);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entities 操作对象实体
	 */
	public <T,ID extends Serializable> void deleteBatchByJpa(Class<T> entityClass, Class<ID> idClass, List<T> entities);

	/**
	 * JPA方法，根据主键值对象：
	 * 调用示例：
	 * Systempreferences sys = new Systempreferences();
	   sys.setVariable("lipttest");
	   baseService.findByIdByJpa(Systempreferences.class, String.class, sys);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param id 主键值
	 */
	<T,ID extends Serializable> T findByIdByJpa(Class<T> entityClass, Class<ID> idClass, ID id);

	/**
	 * JPA方法，查找所有对象：
	 * 调用示例：
	   baseService.findAllByJpa(Systempreferences.class, String.class);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 */
	<T,ID extends Serializable> List<T> findAllByJpa(Class<T> entityClass, Class<ID> idClass);

	/**
	 * JPA方法，根据条件和匹配规则查找对象：
	 * 调用示例：
		Systempreferences exampleEntity = new Systempreferences();
		exampleEntity.setVariable("lipttest");
		list1 = baseService.findListByJpa(Systempreferences.class, String.class, exampleEntity);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 查询条件存放的实体对象
	 */
	<T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity);

	/**
	 * JPA方法，根据条件和匹配规则查找对象：
	 * 调用示例：
		Systempreferences exampleEntity = new Systempreferences();
		exampleEntity.setVariable("lipttest");
		ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("variable", GenericPropertyMatchers.startsWith());
		list1 = baseService.findListByJpa(Systempreferences.class, String.class, exampleEntity, matcher);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 查询条件存放的实体对象
	 * @param matcher ExampleMatcher对象
	 */
	<T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity, ExampleMatcher matcher);

	/**
	 * JPA方法，根据条件和匹配规则查找对象：
	 * 调用示例：
		Systempreferences exampleEntity = new Systempreferences();
		exampleEntity.setVariable("lipttest");
		ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("variable", GenericPropertyMatchers.startsWith());
		list1 = baseService.findListByJpa(Systempreferences.class, String.class, exampleEntity, matcher);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 查询条件存放的实体对象
	 * @param sort sort对象
	 */
	<T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity, Sort sort);

	/**
	 * JPA方法，根据条件和匹配规则查找对象：
	 * 调用示例：
		Systempreferences exampleEntity = new Systempreferences();
		exampleEntity.setVariable("lipttest");
		ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("variable", GenericPropertyMatchers.startsWith());
		list1 = baseService.findListByJpa(Systempreferences.class, String.class, exampleEntity, matcher);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 查询条件存放的实体对象
	 * @param matcher ExampleMatcher对象
	 */
	<T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, T entity, ExampleMatcher matcher, Sort sort);

	/**
	 * JPA方法，根据条件和匹配规则查找对象：
	 * 调用示例：
		Systempreferences exampleEntity = new Systempreferences();
		exampleEntity.setVariable("lipttest");
		Pageable pageable = new PageRequest(currentPage, pagesize, Sort.Direction.DESC, "keywords");
		list1 = baseService.findPageByJpa(Systempreferences.class, String.class, exampleEntity, pageable);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 查询条件存放的实体对象
	 * @param pagealbe 分页参数对象对象
	 */
	<T,ID extends Serializable> Page<T> findPageByJpa(Class<T> entityClass, Class<ID> idClass, T entity, Pageable pagealbe);

	/**
	 * JPA方法，根据条件和匹配规则查找对象：
	 * 调用示例：
		Systempreferences exampleEntity = new Systempreferences();
		exampleEntity.setVariable("lipttest");
		Pageable pageable = new PageRequest(currentPage, pagesize, Sort.Direction.DESC, "keywords");
		ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("variable", GenericPropertyMatchers.startsWith());
		list1 = baseService.findPageByJpa(Systempreferences.class, String.class, exampleEntity, matcher, pageable);
	 * @param entityClass 操作对象类型
	 * @param idClass 主键id字段类型
	 * @param entity 查询条件存放的实体对象
	 * @param matcher ExampleMatcher对象
	 * @param pagealbe 分页参数对象对象
	 */
	<T,ID extends Serializable> Page<T> findPageByJpa(Class<T> entityClass, Class<ID> idClass, T entity, ExampleMatcher matcher, Pageable pagealbe);

	<T,ID extends Serializable> List<T> findListByJpa(Class<T> entityClass, Class<ID> idClass, Specification<T> specification);

	<T,ID extends Serializable> Page<T> findPageByJpa(Class<T> entityClass, Class<ID> idClass, Specification<T> specification, Pageable pageable);

	<T, E> E selectByBatis(MapperNSEnum namespace, String id, T params);

    <T, E> List<E> selectListByBatis(MapperNSEnum namespace, String id, T params);

    <T, E> PageInfo<E> selectListByBatis(MapperNSEnum namespace, String id, T params, int pageNow, int pageSize);

    <T> int updateByBatis(MapperNSEnum namespace, String id, T params);

    <T> List<Long> updateListByBatis(MapperNSEnum namespace, String id, List<T> list);

    <T> long insertByBatis(MapperNSEnum namespace, String id, T params);

    <T> List<Long> insertListByBatis(MapperNSEnum namespace, String id, List<T> list);

    <T> int deleteByBatis(MapperNSEnum namespace, String id, T params);

    <T> List<Long> deleteListByBatis(MapperNSEnum namespace, String id, List<T> list);

    <T> void batchALLByBatis(MapperNSEnum namespace, String id, List<T> params, Integer bathcount);


}
