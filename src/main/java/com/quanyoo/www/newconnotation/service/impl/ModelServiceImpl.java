package com.quanyoo.www.newconnotation.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.DbInfo;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.ModelService;
import com.quanyoo.www.newconnotation.sqlDataSource.DataSourceUtil;
import com.quanyoo.www.newconnotation.sqlDataSource.DynamicDataSourceContextHolder;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.MenuUtil;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
@Service
public class ModelServiceImpl implements ModelService {
	@Autowired
	IBaseService baseService;

	/**
	 * 根据年度动态获取表格字段属性
	 */
	@Override
	public List<Table> getTablesByontextKey(String tableName, DbInfo dbInfo,String year) {
		Map<String, Object> parms = new HashMap<String, Object>();
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		// 查询表信息
		List<Table> list = baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTable", parms);
		DynamicDataSourceContextHolder.removeContextKey();
		return list;
	}

	/**
	 * 根据年度动态获取一级菜单
	 */

	@Override
	public List<Map<String, Object>> getParentList(Map<String, Object> map, DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		List<Map<String, Object>> menuList = baseService.selectListByBatis(MapperNSEnum.LoginMaper,
				"findParentMenuList", map);
		DynamicDataSourceContextHolder.removeContextKey();
		return menuList;
	}

	/**
	 * 根据年度动态获取二级菜单
	 */
	@Override
	public Map<String, Object> getSecondMenu(Map<String, Object> map, DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		Map<String, Object> treeMap = MenuUtil.findTree(baseService, 0);
		DynamicDataSourceContextHolder.removeContextKey();
		return treeMap;
	}

	/**
	 * 根据年度动态获取考核数据集合
	 */
	@Override
	public PageInfo<Map<String, Object>> getTableList(Map<String, Object> params, DbInfo dbInfo, Integer pageNow,
			Integer pageSize,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		PageInfo<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableList",
				params, pageNow, pageSize);
		DynamicDataSourceContextHolder.removeContextKey();
		return info;
	}

	/**
	 * 新增
	 */
	@Override
	public int add(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		
		StringBuffer str = new StringBuffer();
		str.append("insert into ");
		str.append(tableName + " ");

		if (map != null) {
			String fieldString = "";
			String fieldValue = "";
			for (String s : map.keySet()) {
				fieldString += s + ",";
				fieldValue += "'" + map.get(s) + "',";
			}
			fieldString = fieldString.substring(0, fieldString.length() - 1);
			fieldValue = fieldValue.substring(0, fieldValue.length() - 1);

			str.append("(" + fieldString + ") " + "values(" + fieldValue + ") ");
		}
		int add = jdbcTemplate.update(str.toString());
		DynamicDataSourceContextHolder.removeContextKey();
		return add;
	}

	/**
	 * 更新
	 */
	@Override
	public int update(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id, DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		StringBuffer str = new StringBuffer();
		str.append("update ");
		str.append(tableName + " SET ");

		if (map != null) {
			String fieldString = "";
			for (String s : map.keySet()) {
				fieldString += s + "='" + map.get(s) + "',";
			}
			fieldString = fieldString.substring(0, fieldString.length() - 1);

			str.append(fieldString);
		}

		str.append(" where 1=1 and " + "id=" + Id);
		int update = jdbcTemplate.update(str.toString());
		
		DynamicDataSourceContextHolder.removeContextKey();
		return update;
	}

	/**
	 * 删除
	 */
	@Override
	public int delete(JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id, DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		
		StringBuffer str = new StringBuffer();
		str.append("delete from  ");
		str.append(tableName + " where id=" + Id);
		int delete = jdbcTemplate.update(str.toString());
		DynamicDataSourceContextHolder.removeContextKey();
		return delete;
	}

	/**
	 * 根据年度动态获取考核数据集合不分页
	 */
	@Override
	public List<Map<String, Object>> getTableListNoPage(Map<String, Object> params, DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		
		List<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableListNoPage",
				params);
		DynamicDataSourceContextHolder.removeContextKey();
		return info;
	}
	/**
	 * 考核指标数据三表历集合
	 * */
	@Override
	public List<Map<String, Object>> getDataStatistics(String tableName,String tableName1,String tableName2, DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		Map<String,Object> map=new HashMap<>();
		Map<String,Object> param=new HashMap<>();
		List<Map<String,Object>> list=new ArrayList<>();
		param.put("tableName",tableName);
		map=baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段","暂存");
		list.add(map);
		param.put("tableName",tableName1);
		map=baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段","采纳");
		list.add(map);
		param.put("tableName",tableName2);
		map=baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段","不采纳");
		list.add(map);
		DynamicDataSourceContextHolder.removeContextKey();
		return list;
	}
	/**
	 * 获取jdbctemplate
	 */
	@Override
	public JdbcTemplate getJdbcTemplate( DbInfo dbInfo,String year) {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		dataSource.setDriverClassName(dbInfo.getDriveClassName());
		dataSource.setUrl(dbInfo.getUrl());
		dataSource.setUsername(dbInfo.getUserName());
		dataSource.setPassword(dbInfo.getPassword());
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		DynamicDataSourceContextHolder.removeContextKey();
		return jdbcTemplate;
	}
	
	
    /**
	 * 自动建库建表
	 */
	@Override
	public int createTableData(JdbcTemplate jdbcTemplate,String dataBaseName, List<Table> list,String year) {

		try {
			//建库
			String sql_ku = "CREATE DATABASE " + dataBaseName;
			jdbcTemplate.update(sql_ku);
			// 连接数据库
			String sql_use = "USE " + dataBaseName;
			jdbcTemplate.update(sql_use);
			// 执行创表语句
			if (list.size() > 0) {
				for (Table table : list) {
					StringBuffer sql=new StringBuffer();
					String fieldValue = "";
					//表名
					String tableName=table.getCreate_table_english_name();
					//字段集合
					List<TableColumn> tableColumnList =table.getTableColumn();
					sql.append("CREATE TABLE ");
					sql.append(tableName + "(id int(11) primary key not null auto_increment,");
					for(TableColumn tableColumn:tableColumnList) {
						if("id".equals(tableColumn.getCreate_table_column())) {
							continue;
						}
						fieldValue+=tableColumn.getCreate_table_column()+" "+tableColumn.getCreate_table_column_type()+"  COMMENT "+tableColumn.getCreate_table_chinese_column()+",";

					}
						if(!"".equals(fieldValue)) {

							fieldValue = fieldValue.substring(0, fieldValue.length() - 1);
							sql.append(fieldValue+")");
							jdbcTemplate.update(sql.toString());
						}else {
							jdbcTemplate.update(sql.toString().substring(0, sql.toString().length() - 1)+")");
						}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return -2;
		}
		return 2;
	}
	
	
	
	//////////////////////////////
	/**
	 * 院系（部门）统计数据
	 * */
	@Override
	public List<Map<String, Object>> getdepartMentStaticsList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		StringBuffer str = new StringBuffer();
		str.append("select DISTINCT(department_name) as department_name  from  calendar_year ");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());

		DynamicDataSourceContextHolder.removeContextKey();
		return m;
	}
	/**
	 * 院系（部门）统计
	 * */
	@Override
	public List<Map<String, Object>> getdepartMentList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		StringBuffer str = new StringBuffer();
		str.append("select DISTINCT(year) as year  from  calendar_year  ORDER BY year asc");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());

		DynamicDataSourceContextHolder.removeContextKey();
		return m;
	}
	/**
	 * 院系（部门）统计year
	 * */
	@Override
	public List<Map<String, Object>> getdepartMentYearList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		StringBuffer str = new StringBuffer();
		str.append("select department_name,progress_score,total_project_score,total_score,year"
				+ "  from  calendar_year  GROUP BY department_name,year ");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());

		DynamicDataSourceContextHolder.removeContextKey();
		return m;
	}
	
	/**
	 * 
	 *	统计个人年度得分表数据
	 */
	@Override
	public List<Map<String, Object>> getIndividualSoreList(JdbcTemplate jdbcTemplate,DbInfo dbInfo,String year) {
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		StringBuffer str = new StringBuffer();
		str.append("select id,job_number,full_name,department,total_score,year"
				+ "  from  personal_score ");
		List<Map<String, Object>> m = jdbcTemplate.queryForList(str.toString());

		DynamicDataSourceContextHolder.removeContextKey();
		return m;
	}


}
