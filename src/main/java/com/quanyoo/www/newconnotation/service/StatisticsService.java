package com.quanyoo.www.newconnotation.service;

import java.util.List;
import java.util.Map;

import com.quanyoo.www.newconnotation.pojo.Table;

public interface StatisticsService {
	
	public List<Table>  getTemporarystorageList(Map<String,Object> map);
}
