package com.quanyoo.www.newconnotation.service.impl;

/**
 * @author ：WURP
 * @description：TODO
 * @date ：2020/8/5 17:41
 * 系统日志：切面处理类
 */

import com.quanyoo.www.newconnotation.pojo.SysLog;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.CusAccessObjectUtil;
import com.quanyoo.www.newconnotation.util.ConfigManager;
import com.quanyoo.www.newconnotation.util.MyLog;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Aspect
@Component
public class SysLogAspect {
    @Resource
    private IBaseService baseService;
    //定义切点 @Pointcut
    //在注解的位置切入代码
    @Pointcut("@annotation( com.quanyoo.www.newconnotation.util.MyLog)")
    public void logPoinCut() {
    }

    //切面 配置通知
    @AfterReturning("logPoinCut()")
    public void saveSysLog(JoinPoint joinPoint) throws SQLException, ClassNotFoundException {
        System.out.println("切面。。。。。");
        //保存日志
        SysLog sysLog = new SysLog();

        //从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        //获取切入点所在的方法
        Method method = signature.getMethod();

        //获取操作
        MyLog myLog = method.getAnnotation(MyLog.class);
        if (myLog != null) {
            String value = myLog.value();
            sysLog.setOperation(value);//保存获取的操作
        }

        //获取请求的类名
        String className = joinPoint.getTarget().getClass().getName();
        //获取请求的方法名
        String methodName = method.getName();
        sysLog.setMethod(className + "." + methodName);

        //请求的参数
        //Object[] args = joinPoint.getArgs();
        //将参数所在的数组转换成json
        //String params = JSON.toJSONString(args);
        //sysLog.setParams(params);
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/M/d HH:mm:ss");
        String dateString = formatter.format(currentTime);
        sysLog.setCreateDate(dateString);
        //获取用户ip地址
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getResponse();
        String ip = CusAccessObjectUtil.getIpAddress(request);
        sysLog.setIp(ip);
        //获取用户名
//        User user = (User) SecurityUtils.getSubject().getPrincipal();
        User user = (User) request.getSession().getAttribute("user");
        sysLog.setUsername(user.getUserName());
        sysLog.setFullName(user.getFullName());
        //用户工号
        sysLog.setJobNum(user.getJobNum());
        //用户部门
        sysLog.setDept(user.getDepartment());
        //将日志插入到日志表
        ConfigManager.saveDept(sysLog);
        //调用service保存SysLog实体类到数据库
    }

}
