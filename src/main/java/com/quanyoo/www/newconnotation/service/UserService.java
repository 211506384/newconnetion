package com.quanyoo.www.newconnotation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.UserInformation;

public interface UserService {

	// 查找用户
	public Map<String, Object> getUserList(Map<String, Object> map, int page, int rows);
	
	//分配角色权限
	public Map<String, Object> saveRolePermissionList(Integer roleId,List<Integer> permissionIdList);

	// 获取部门下拉框
	public List<DepartmentInformation> getDepartment(Map<String, Object> map);

	// 保存用户
	public int saveUser(UserInformation userInformation);

	// 删除用户
	public int deleteUser(Integer userId);

	// 重置用户密码
	public int resetPassword(Map<String, Object>  params );
	// 查找用户部门
	public Map<String, Object> getDepartmentTable(Map<String, Object> map, int page, int rows);
	// 保存部门
	public int saveDepartment(Map<String, Object> map);
	// 修改部门
	public int updateDepartment(Map<String, Object> map);
	// 删除部门
	public int deleteDepartment(Integer userId);

}
