package com.quanyoo.www.newconnotation.service;

import java.util.List;
import java.util.Map;

import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.UserInformation;

public interface YearSystemService{
		
	/**
	 * 用户管理
	 * */
	

	// 年度保存用户
	public int saveUser(String year,UserInformation userInformation);

	// 年度删除用户
	public int deleteUser(String year,Integer userId);
	
	// 年度查找用户
	public Map<String, Object> getUserList(String year,Map<String, Object> map, int page, int rows);
	
	//年度分配角色权限
	public Map<String, Object> saveRolePermissionList(String year,Integer roleId,List<Integer> permissionIdList);

	// 年度获取部门下拉框
	public List<DepartmentInformation> getDepartment(String year,Map<String, Object> map);


	// 年度重置用户密码
	public int resetPassword(String year,Map<String, Object>  params );
	// 年度查找用户部门
	public Map<String, Object> getDepartmentTable(String year,Map<String, Object> map, int page, int rows);
	// 年度保存部门
	public int saveDepartment(String year,Map<String, Object> map);
	// 年度修改部门
	public int updateDepartment(String year,Map<String, Object> map);
	// 年度删除部门
	public int deleteDepartment(String year,Integer userId);
}
