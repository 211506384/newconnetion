package com.quanyoo.www.newconnotation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.ConnotationTableCreate;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.UserInformation;
import com.quanyoo.www.newconnotation.pojo.DbInfo;
import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;

public interface AopYearService {

	/**
	 * 用户功能
	 */
	// 年度查找用户
	public Map<String, Object> getUserList(String year, Map<String, Object> map, int page, int rows);

	// 年度保存用户
	public int addUser(String year, Map<String, Object> params);

	// 年度更新用户
	public int updateUser(String year, Map<String, Object> params);

	// 年度删除用户
	public int deleteUser(String year, Integer userId);

	// 年度重置用户密码
	public int resetPassword(String year, Map<String, Object> params);

	// 年度分配角色权限
	public Map<String, Object> saveRolePermissionList(String year, Integer roleId, List<Integer> permissionIdList);

	// 年度获取部门下拉框
	public List<DepartmentInformation> getDepartment(String year, Map<String, Object> map);

	// 用户角色新增
	public Map<String, Object> saveUserRoleOne(String year, Map<String, Object> params);

	// 角色下拉
	public Map<String, Object> getRoleListAllSelect(String year, Map<String, Object> map);

	/**
	 * 角色功能
	 */
	// 角色列表
	public Map<String, Object> getRoleListPage(String year, Map<String, Object> params, int page, int rows);

	// add角色
	public Map<String, Object> addRole(String year, Map<String, Object> params);

	// update角色
	public Map<String, Object> updateRole(String year, Map<String, Object> params);

	// delete角色
	public Map<String, Object> deleteRole(String year, Map<String, Object> params);

	/**
	 * 权限功能
	 */

	// 获取权限下拉
	public List<Map<String, Object>> getPermissionSelect(String year, Map<String, Object> params);

	public Map<String, Object> getPermissionAllSelect(String year, Map<String, Object> params);

	// 根据角色Id获取权限
	public JSONArray getPermissionAllByroleId(String year, Map<String, Object> params);

	// 权限列表
	public Map<String, Object> getPermissionList(String year, Map<String, Object> params, int page, int rows);

	// 权限新增
	public Map<String, Object> addPermissionOne(String year, Map<String, Object> params);

	// 权限修改
	public Map<String, Object> updatePermissionOne(String year, Map<String, Object> params);

	// 权限删除
	public Map<String, Object> deletePermissionOne(String year, Map<String, Object> params);

	/**
	 * 菜单功能
	 */
	// 菜单下拉
	public List<Menu> getMenusListSelect(String year, Map<String, Object> map);

	// 菜单列表
	public Map<String, Object> getMenusListPage(String year, Map<String, Object> params, int page, int rows);

	// add菜单
	public Map<String, Object> addMenu(String year, Map<String, Object> params);

	// update菜单
	public Map<String, Object> updateMenu(String year, Map<String, Object> params);

	// delete菜单
	public Map<String, Object> deleteMenu(String year, Map<String, Object> params);

	/**
	 * 部门功能
	 */
	// 年度查找用户部门
	public Map<String, Object> getDepartmentTable(String year, Map<String, Object> map, int page, int rows);

	// 年度保存部门
	public int saveDepartment(String year, Map<String, Object> map);

	// 年度修改部门
	public int updateDepartment(String year, Map<String, Object> map);

	// 年度删除部门
	public int deleteDepartment(String year, Integer userId);

	// 获取字段前台页
	public List<Map<String, Object>> getTableColumnInfo(String year, Map<String, Object> map);

	// 获取table,tablecolumn
	public List<Table> getTableAndColumns(String year, Map<String, Object> map);

	// 获取table,tablecolumn,模板
	public List<Table> getTableAndColumnsModel(String year, Map<String, Object> map);

	/**
	 * 根据表名指标等级信息
	 */
	public List<Map<String, Object>> assessmentIndexSelect(String year, String tableName);

	/**
	 * 根据表名指标分数信息
	 */
	public List<Map<String, Object>> assessmentIndexScoreSelect(String year, String tableName);

	/**
	 * 根据表名查找数据统计指标在审，采纳和不采纳
	 */
	public List<Map<String, Object>> getDataStatistics(String year, String tableName, String tableName1,
			String tableName2);

	/**
	 * 添加字段
	 */
	public Map<String, Object> addColumn(String year, String tableName,Map<String, Object> params);

	/**
	 * 根据年度动态获取一级菜单
	 */
	public List<Map<String, Object>> getParentList(String year, Map<String, Object> map);

	/**
	 * 根据年度动态获取二级菜单
	 */
	public Map<String, Object> getSecondMenu(String year, Map<String, Object> map);

	/**
	 * 年度集合
	 */
	public List<Map<String, Object>> getYearList(String year, Map<String, Object> map);

	/**
	 * //查询本年度表名翻译
	 */
	public Map<String, Object> getTableNameTranslate(String year, Map<String, Object> map);

	// 查询本年度字段翻译
	public Map<String, Object> getFieldNameTranslate(String year, Map<String, Object> map);

	/**
	 * //查询next年度表名翻译
	 */
	public Map<String, Object> getNextTableNameTranslate(String year, Map<String, Object> map);

	// 查询next年度字段翻译
	public Map<String, Object> getNextFieldNameTranslate(String year, Map<String, Object> map);
	// 查询年度表名下拉
	public List<ConnotationTableCreate> getConnotationTableCreateList(String year, Map<String, Object> map);
	// 新增年度表
	public int addYear(String year, Map<String, Object> map);
	/**
	 * 年度集合
	 */
	public Map<String, Object> getYearTableList(String year, Map<String, Object> map, int page, int rows);
    //新增表名翻译
	public int addTableNameTranslate(String year,Map<String, Object> map);
	 //修改表名翻译
	public int setTableNameTranslate(String year,Map<String, Object> map);
	// 删除表名翻译
	public int deleteTableNameTranslate(String year,Map<String, Object> map);
	//新增字段翻译
	public int addFieldNameTranslate(String year,Map<String, Object> map);
	 //修改字段翻译
	public int  setFieldNameTranslate(String year,Map<String, Object> map);
	// 删除字段翻译
	public int deleteFieldNameTranslate(String year,Map<String, Object> map);
	
	//初始化导入新增表名信息
	public int addInitTable(String year,Map<String, Object> map);
	//初始化导入新增表字段信息
	public int addInitTableField(String year,Map<String, Object> map);
	public Map<String, Object> batch_addInitTable(String year,MultipartFile file);
	public Map<String, Object> batch_addInitTableField(String year,MultipartFile file);
	
	public Map<String, Object> batch_addInitTableModel(String year,MultipartFile file);
	public Map<String, Object> batch_addInitTableFieldModel(String year,MultipartFile file);
	public List<ConnotationTableCreate> getTableListBySelectByCreateTableType(String year, Map<String, Object> map);
	
	public List<Table> getTemporarystorageList(String year,Map<String,Object> map);
	
	
	///统计积分结果
	public List<Table> getTableResultScoreList(String year,Map<String,Object> map);
	
	public List<Map<String, Object>> getTableResultScoreByLLevelList(String year,Map<String,Object> map);

	public List<Map<String, Object>> getTableResultScoreByLLevelList1(String year,Map<String,Object> map);
}
