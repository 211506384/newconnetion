package com.quanyoo.www.newconnotation.service;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.Table;

import java.util.List;
import java.util.Map;

public interface TestServiceInterface {

	/**
	 * 插入数据
	 */
	public int add(String tableName,Map<String, Object> map);
	/**
	 * 修改数据
	 */
	public int update(String tableName,Map<String, Object> map,Integer Id);

	/**
	 * 删除数据
	 */
	public int delete(String tableName,Map<String, Object> map,Integer Id);

	/**
	 * select数据
	 */
	public List<Map<String, Object>>  select(String tableName,Map<String, Object> map, int pageNow, int pageSize) ;
	/**
	 * 根据表名查找字段和表信息
	 */
	List<Map<String,Object>> getTableColumnInfo(Map<String, Object> map);
	/**
	 * 根据表名指标等级信息
	 */
	List<Map<String, Object>> assessmentIndexSelect(String tableName);
	/**
	 * 根据表名指标分数信息
	 */
	List<Map<String, Object>> assessmentIndexScoreSelect(String tableName);
	/**
	 * 自动建库建表
	 * */
	
	public int createTableData(String dataBaseName,List<Table> list) ;
	
	/**
	 * 分页查找
	 * */
	public PageInfo<Map<String, Object>> selectListBypage(String tableName,Map<String, Object>  params, int pageNow, int pageSize);
	/**
	 * 根据表名查找字段和表信息
	 */
	public List<Table> getTableAndColumns(String tableName);
	/**
	 * 根据表名查找数据统计指标在审，采纳和不采纳
	 */
	public List<Map<String,Object>> getDataStatistics(String tableName,String tableName1,String tableName2);
	
	
	/**
	 * select数据不分页
	 */
	public List<Map<String, Object>>  selectAll(String tableName,Map<String, Object> map) ;
	/**
	 * 导入新增信息
	 * */
	
	public void addInfo(String tableName,String keys,String values) ;
	
}
