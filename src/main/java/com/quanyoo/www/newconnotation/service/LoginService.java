package com.quanyoo.www.newconnotation.service;

import java.util.Map;

import com.quanyoo.www.newconnotation.pojo.User;

import java.util.List;


public interface LoginService {

	public User findUserByphoneAndPassword(String phoneNumber, String password);
	public List<Map<String, Object>> getUserRolesByUserId(String userId);
	public List<Map<String, Object>> getUserPermissonsByUserId(String userId);
	public User findUserByphone(String phoneNumber);
}
