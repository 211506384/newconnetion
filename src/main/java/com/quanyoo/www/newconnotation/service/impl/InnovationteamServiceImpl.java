package com.quanyoo.www.newconnotation.service.impl;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.DoctorsDegreeTemporarystorage;
import com.quanyoo.www.newconnotation.model.InnovationteamAdopt;
import com.quanyoo.www.newconnotation.model.InnovationteamTemporarystorage;
import com.quanyoo.www.newconnotation.model.TalentIntroductionTemporarystorage;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.InnovationteamService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InnovationteamServiceImpl implements InnovationteamService {
	 @Resource
	    private IBaseService baseService;

	@Transactional
	@Override
	public int saveInnovationteamAdopt(Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			baseService.updateByBatis(MapperNSEnum.InnovationteamMaper,"saveInnovationteam",map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	/**
	 * 添加创新团队内涵奖
	 * */
	@Transactional
	@Override
	public int addInnovationteamAdopt(InnovationteamTemporarystorage innovationteamTemporarystorage) {
		// TODO Auto-generated method stub
		try {
			baseService.saveOrUpdateByJpa(InnovationteamTemporarystorage.class, Integer.class, innovationteamTemporarystorage);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}

	@Override
	public List<Map<String, Object>> teamBuildingSelect(Map<String, Object> map) {
		return baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper,"teamBuildingSelect",map);
	}

	@Override
	public List<Map<String, Object>> assmentIndexScoreSelect(Map<String, Object> map) {
		return baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper,"assmentIndexScoreSelect",map);
	}

	@Transactional
	@Override
	public int deleteInnovationteamAdopt(Integer userId) {
		try {
			baseService.deleteByIdByJpa(InnovationteamAdopt.class, Integer.class, userId);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	/**
	 * 查找创新团队
	 * */
	@Override
	public Map<String, Object> getInnovationteamAdoptList(Map<String, Object> map,int page,int rows) {
		 PageInfo<InnovationteamTemporarystorage> list = baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "findInnovationteamAdoptInformationListAll",map,page,rows);
		 map = 	new HashMap<String, Object>();
		 map.put("rows", list.getList());
		 map.put("total", list.getTotal());
		return map;
	}
	/**
	 * 查找人才引进
	 * */
	@Override
	public Map<String, Object> getTalentIntroductionList(Map<String, Object> map,int page,int rows) {
		PageInfo<DoctorsDegreeTemporarystorage> list = baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "getTalentIntroductionList",map,page,rows);
		map = 	new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());
		return map;
	}
	@Override
	public int saveTalentIntroduction(Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			baseService.updateByBatis(MapperNSEnum.InnovationteamMaper,"saveTalentIntroduction",map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	@Transactional
	@Override
	public int addTalentIntroduction(TalentIntroductionTemporarystorage talentIntroductionTemporarystorage) {
		// TODO Auto-generated method stub
		try {
			baseService.saveOrUpdateByJpa(TalentIntroductionTemporarystorage.class, Integer.class, talentIntroductionTemporarystorage);
		} catch (Exception e) {
			System.out.println(e);
			return -1;
		}
		return 1;
	}
	/**
	 * 查找获得博士学位教室
	 * */
	@Override
	public Map<String, Object> getDoctorsDegreeTable(Map<String, Object> map,int page,int rows) {
		PageInfo<DoctorsDegreeTemporarystorage> list = baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "getDoctorsDegreeList",map,page,rows);
		map = 	new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());
		return map;
	}
	@Override
	public int saveDoctorsDegree(Map<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			baseService.updateByBatis(MapperNSEnum.InnovationteamMaper,"saveDoctorsDegree",map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	@Transactional
	@Override
	public int addDoctorsDegree(DoctorsDegreeTemporarystorage doctorsDegreeTemporarystorage) {
		// TODO Auto-generated method stub
		try {
			baseService.saveOrUpdateByJpa(DoctorsDegreeTemporarystorage.class, Integer.class, doctorsDegreeTemporarystorage);
		} catch (Exception e) {
			System.out.println(e);
			return -1;
		}
		return 1;
	}

	@Override
	public Map<String, Object> getNewTeachersVisitingTable(Map<String, Object> map, int page, int rows) {
		PageInfo<DoctorsDegreeTemporarystorage> list = baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "getNewTeachersVisitingList",map,page,rows);
		map = new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());
		return map;
	}

	@Override
	public Map<String, Object> getFieldInformation(String tableName) {
		baseService.selectByBatis(MapperNSEnum.InnovationteamMaper,"",tableName);
		return null;
	}
}
