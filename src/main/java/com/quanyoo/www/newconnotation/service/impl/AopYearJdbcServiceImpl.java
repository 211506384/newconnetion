package com.quanyoo.www.newconnotation.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.service.AopYearJdbcService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/*
 * 跨年度JdbcTemplate
 * */
@Service
public class AopYearJdbcServiceImpl implements AopYearJdbcService {
	 
		/**
		 * 插入数据
		 */
		
		public int add(String year, JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map) {
			StringBuffer str = new StringBuffer();
			str.append("insert into ");
			str.append(tableName + " ");

			if (map != null) {
				String fieldString = "";
				String fieldValue = "";
				for (String s : map.keySet()) {
					fieldString += s + ",";
					fieldValue += "'" + map.get(s) + "',";
				}
				fieldString = fieldString.substring(0, fieldString.length() - 1);
				fieldValue = fieldValue.substring(0, fieldValue.length() - 1);

				str.append("(" + fieldString + ") " + "values(" + fieldValue + ") ");
			}
			int add = jdbcTemplate.update(str.toString());
			return add;
		}

		/**
		 * 修改数据
		 */
		
		public int update(String year, JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id) {
			StringBuffer str = new StringBuffer();
			str.append("update ");
			str.append(tableName + " SET ");

			if (map != null) {
				String fieldString = "";
				for (String s : map.keySet()) {
					fieldString += s + "='" + map.get(s) + "',";
				}
				fieldString = fieldString.substring(0, fieldString.length() - 1);

				str.append(fieldString);
			}

			str.append(" where 1=1 and " + "id=" + Id);
			int update = jdbcTemplate.update(str.toString());
			return update;
		}

		/**
		 * 删除数据
		 */
		public int delete(String year, JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map, Integer Id) {
			StringBuffer str = new StringBuffer();
			str.append("delete from  ");
			str.append(tableName + " where id=" + Id);
			int delete = jdbcTemplate.update(str.toString());
			return delete;
		}

		/**
		 * select数据-带条件
		 */
		public List<Map<String, Object>> select(String year, JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map,
				int pageNow, int pageSize,String college,String job_number) {
			StringBuffer str = new StringBuffer();
			str.append("select * from  ");
			str.append(tableName + " ");

			String fieldString = "";

			if (map != null) {

				for (String s : map.keySet()) {
					if (!"".equals(map.get(s)) && map.get(s) != null) {
						fieldString += s + "='" + map.get(s) + "' and ";
					}
				}
			}
			if(college!=null&&!"".equals(college)) {
				fieldString+= "  college="+college +" and";
			}
			if(job_number!=null&&!"".equals(job_number)) {
				fieldString+= " job_number= "+job_number+" and";
			}
			str.append("where " + fieldString + "1=1 limit ");
			str.append((pageNow - 1) * pageSize + "," + pageSize);
			List<Map<String, Object>> list = null;
			try {
				list=jdbcTemplate.queryForList(str.toString());
			} catch (Exception e) {
				e.getStackTrace();
				return null;

			}

			return list;

		}

		/**
		 * 分页查找-带条件
		 */
		@Override
		public PageInfo<Map<String, Object>> selectListBypage(String year, JdbcTemplate jdbcTemplate, String tableName,
				Map<String, Object> params, int pageNow, int pageSize,String college,String job_number) {
			if (pageNow > 0 && pageSize > 0) {
				PageHelper.startPage(pageNow, pageSize);
			}
			List<Map<String, Object>> list = select(year,jdbcTemplate, tableName, params, pageNow, pageSize,college,job_number);
			// 用PageInfo对结果进行包装
			PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
			StringBuffer str = new StringBuffer();
			str.append("select count(*) from  ");
			str.append(tableName + " ");

			String fieldString = "";

			if (params != null) {

				for (String s : params.keySet()) {
					if (!"".equals(params.get(s)) && params.get(s) != null) {
						fieldString += s + "='" + params.get(s) + "' and ";
					}
				}
			}
			
			if(college!=null&&!"".equals(college)) {
				fieldString+= "  college="+college +" and";
			}
			if(job_number!=null&&!"".equals(job_number)) {
				fieldString+= " job_number= "+job_number+" and";
			}
			str.append("where " + fieldString + "1=1 ");
			long total = 0;
			try {
				total=jdbcTemplate.queryForObject(str.toString(), Integer.class);
			} catch (Exception e) {
				
					e.printStackTrace();
			}
			page.setTotal(total);
			return page;
		}

		/**
		 * select数据
		 */
		public List<Map<String, Object>> select1(String year, JdbcTemplate jdbcTemplate, String tableName, Map<String, Object> map,
				int pageNow, int pageSize) {
			StringBuffer str = new StringBuffer();
			str.append("select * from  ");
			str.append(tableName + " ");

			str.append("where 1=1 limit ");
			str.append((pageNow - 1) * pageSize + "," + pageSize);
			List<Map<String, Object>> list = jdbcTemplate.queryForList(str.toString());

			return list;

		}
		
		/**
		 * 分页查找
		 */
		@Override
		public PageInfo<Map<String, Object>> selectListBypage1(String year, JdbcTemplate jdbcTemplate, String tableName,
				Map<String, Object> params, int pageNow, int pageSize) {
			if (pageNow > 0 && pageSize > 0) {
				PageHelper.startPage(pageNow, pageSize);
			}
			List<Map<String, Object>> list = select1(year,jdbcTemplate, tableName, params, pageNow, pageSize);
			// 用PageInfo对结果进行包装
			PageInfo<Map<String, Object>> page = new PageInfo<Map<String, Object>>(list);
			StringBuffer str = new StringBuffer();
			str.append("select count(*) from  ");
			str.append(tableName + " ");


			str.append("where 1=1 ");
			long total = jdbcTemplate.queryForObject(str.toString(), Integer.class);
			page.setTotal(total);
			return page;
		}

		
		/**
		 * 根据表名查找字段和表信息
		 */
		
		/**
		 * 自动建库建表
		 * */

		
		@Override
		public int createTableData(String year, JdbcTemplate jdbcTemplate, String dataBaseName, List<Table> list) {

			try {
				//建库
				String sql_ku = "CREATE DATABASE " + dataBaseName;
				jdbcTemplate.update(sql_ku);
				// 连接数据库
				String sql_use = "USE " + dataBaseName;
				jdbcTemplate.update(sql_use);
				// 执行创表语句
				if (list.size() > 0) {
					for (Table table : list) {
						StringBuffer sql=new StringBuffer();
						String fieldValue = "";
						//表名
						String tableName=table.getCreate_table_english_name();
						//字段集合
						List<TableColumn> tableColumnList =table.getTableColumn();
						sql.append("CREATE TABLE ");
						sql.append(tableName + "(id int(11) primary key not null auto_increment,");
						for(TableColumn tableColumn:tableColumnList) {
							if("id".equals(tableColumn.getCreate_table_column())) {
								continue;
							}
							fieldValue+=tableColumn.getCreate_table_column()+" "+tableColumn.getCreate_table_column_type()+",";

						}
							if(!"".equals(fieldValue)) {

								fieldValue = fieldValue.substring(0, fieldValue.length() - 1);
								sql.append(fieldValue+")");
								jdbcTemplate.update(sql.toString());
							}else {
								jdbcTemplate.update(sql.toString().substring(0, sql.toString().length() - 1)+")");
							}
					}
				}
				System.out.println("Created databaseName in given database...");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
				return -2;
			}
			System.out.println("Goodbye!");
			return 2;
		}
		/**
		 * 根据表名查找字段和表信息
		 */
		@Override
		public List<Table> getTableAndColumns(String year,JdbcTemplate jdbcTemplate, String tableName) {
			List<Table> list = new ArrayList<Table>();
			// 根据表名查找表信息
			StringBuffer tableSql = new StringBuffer();
			tableSql.append("select 	create_table_english_name,\r\n"
					+ "	ifnull(create_table_chinese_name,\"表未命名中文\") create_table_chinese_name "
					+ "from  connotation_table_create ");

			tableSql.append("where  create_table_english_name='" + tableName + "' and 1=1 ");
			Map<String, Object> m = jdbcTemplate.queryForMap(tableSql.toString());
			Table table=JSON.parseObject(JSON.toJSONString(m), Table.class);

			// 根据表名查找表字段信息

			StringBuffer columnSql = new StringBuffer();
			columnSql.append("select 	create_table_column,\r\n"
					+ "	ifnull(create_table_chinese_column,\"表未命名中文\") create_table_chinese_column "
					+ "from  connotation_table_column_create ");

			columnSql.append("where  create_table_name='" + tableName + "' and 1=1 ");
			List<Map<String, Object>> queryList = jdbcTemplate.queryForList(columnSql.toString());
			
		      List<TableColumn> dtoList = new ArrayList<TableColumn>();
	      for (Map<String, Object> p:queryList) {
	          TableColumn tableColumn=JSON.parseObject(JSON.toJSONString(p), TableColumn.class);
	         
	          dtoList.add(tableColumn );
	      }
			table.setTableColumn(dtoList);
			list.add(table);
			return list;
		}
	 
		/**
		 * @param tableName		要插入的表
		 * @param keys			指定插入的字段值
		 * @param values		指定插入的数据值
		 */
		//添加数据
		@Override
		public void addInfo(String year,JdbcTemplate jdbcTemplate,String tableName,String keys,String values) {
			StringBuilder sql = new StringBuilder();
			sql.append("insert into ").append(tableName).append("(").append(keys).append(")").append(" values (").append(values).append(")");
			jdbcTemplate.update(sql.toString());
	    }
		//更新数据
		@Override
		public void updateInfo(String year,JdbcTemplate jdbcTemplate,String tableName,String keys,String values) {
			StringBuilder sql = new StringBuilder();
			sql.append("update ").append(tableName).append(" set ").append(keys).append(" where ").append(" ").append(values).append(" ");
			jdbcTemplate.update(sql.toString());
	    }
		
		/**
		 * 统计数据不分页
		 * 
		 * */
		@Override
		public List<Map<String, Object>> selectAll(String year,JdbcTemplate jdbcTemplate,String tableName, Map<String, Object> map,String college,String jobNum) {
			StringBuffer str = new StringBuffer();
			str.append("select * from  " +tableName);
			if(college!=null&&!"".equals(college)) {
				str.append( " where college= '"+college +"'");
			}
			if(jobNum!=null&&!"".equals(jobNum)) {
				str.append(" where job_number= '"+jobNum +"'");
			}
			List<Map<String, Object>> list=null;
			try {
				 list = jdbcTemplate.queryForList(str.toString());
			} catch (Exception e) {
				return list;
			}
		
				
			return list;
		}
		
		
		/**
		 * 统计数据不分页
		 * 
		 * */
		@Override
		public List<Map<String, Object>> selectlist(String year,JdbcTemplate jdbcTemplate,String tableName, Map<String, Object> map) {
			StringBuffer str = new StringBuffer();
			str.append("select * from  " +tableName);
			
			List<Map<String, Object>> list=null;
			try {
				 list = jdbcTemplate.queryForList(str.toString());
			} catch (Exception e) {
				return list;
			}
		
				
			return list;
		}
}
