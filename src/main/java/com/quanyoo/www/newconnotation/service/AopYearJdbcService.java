package com.quanyoo.www.newconnotation.service;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.Table;

public interface AopYearJdbcService {
	/**
	 * 插入数据
	 */
	public int add(String year,JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map);
	/**
	 * 修改数据
	 */
	public int update(String year,JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map,Integer Id);

	/**
	 * 删除数据
	 */
	public int delete(String year,JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map,Integer Id);

	/**
	 * select数据
	 */
	public List<Map<String, Object>>  select(String year,JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map, int pageNow, int pageSize,String college,String job_number) ;

	/**
	 * select数据
	 */
	public List<Map<String, Object>>  select1(String year,JdbcTemplate jdbcTemplate,String tableName,Map<String, Object> map, int pageNow, int pageSize) ;

	/**
	 * 分页查找-带条件
	 * */
	public PageInfo<Map<String, Object>> selectListBypage(String year,JdbcTemplate jdbcTemplate,String tableName,Map<String, Object>  params, int pageNow, int pageSize,String college,String job_number);
	
	/**
	 * 分页查找
	 * */
	public PageInfo<Map<String, Object>> selectListBypage1(String year,JdbcTemplate jdbcTemplate,String tableName,Map<String, Object>  params, int pageNow, int pageSize);
	/**
	 * 根据表名查找字段和表信息
	 * */
	public List<Table> getTableAndColumns(String year,JdbcTemplate jdbcTemplate,String tableName);
	
	/**
	 * 自动建库建表
	 * */
	public int createTableData(String year,JdbcTemplate jdbcTemplate,String dataBaseName,List<Table> list) ;
		/**
		 * 查找所有表结构
		 * 建库表使用
		 * */
	//public List<Table> getTableAndColumns(String year,JdbcTemplate jdbcTemplate);
		//导入新增
	public void addInfo(String year,JdbcTemplate jdbcTemplate,String tableName,String keys,String values);
	//导入更新
	public void updateInfo(String year,JdbcTemplate jdbcTemplate,String tableName,String keys,String values) ;
	public List<Map<String, Object>> selectAll(String year,JdbcTemplate jdbcTemplate,String tableName, Map<String, Object> map,String college,String jobNum) ;

	public List<Map<String, Object>> selectlist(String year,JdbcTemplate jdbcTemplate,String tableName, Map<String, Object> map) ;

}
