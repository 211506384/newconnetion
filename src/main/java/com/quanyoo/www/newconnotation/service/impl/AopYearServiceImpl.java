package com.quanyoo.www.newconnotation.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageInfo;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.quanyoo.www.newconnotation.exportExcel.toStringUtil;
import com.quanyoo.www.newconnotation.model.ConnotationTableCreate;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.UserInformation;
import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.service.AopYearService;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.MenuUtil;
import com.quanyoo.www.newconnotation.util.SystemUtil;

/**
 * 跨年度查找
 */
@Service
public class AopYearServiceImpl implements AopYearService {
	@Resource
	private IBaseService baseService;

	// 年度查找用户
	@Override
	public Map<String, Object> getUserList(String year, Map<String, Object> map, int page, int rows) {

		PageInfo<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,
				"findUserInformationListAll", map, page, rows);
		map = new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());
		return map;
	}

	// 年度分配角色权限

	@Override
	public Map<String, Object> saveRolePermissionList(String year, Integer roleId, List<Integer> permissionIdList) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			// 删除原来的权限
			baseService.deleteByBatis(MapperNSEnum.UserManagementMaper, "deleteRolePermission", roleId);

			// 新增角色权限
			for (Integer integer : permissionIdList) {
				map = new HashMap<String, Object>();
				map.put("roleId", roleId);
				map.put("permissionId", integer);
				baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "saveRolePermission", map);
			}

		} catch (Exception e) {
			map = new HashMap<String, Object>();
			map.put("code", -1);
			map.put("msg", "分配失败");
			return map;
		}
		map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("msg", "分配成功");
		return map;

	}

	// 年度获取部门下拉框
	@Override
	public List<DepartmentInformation> getDepartment(String year, Map<String, Object> map) {

		List<DepartmentInformation> departmentInformations = baseService
				.selectListByBatis(MapperNSEnum.UserManagementMaper, "getDepartmentSelect", map);

		return departmentInformations;
	}

	// 年度保存用户
	@Override
	public int addUser(String year, Map<String, Object> params) {

		try {
			long i = baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "addUserOne", params);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return -1;
		}
		return 1;
	}

	// 年度保存用户
	@Override
	public int updateUser(String year, Map<String, Object> params) {
		System.out.println(params);
		try {
			long i = baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "updateUserOne", params);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return -1;
		}
		return 1;
	}
	// 年度删除用户

	@Override
	public int deleteUser(String year, Integer userId) {

		try {
			baseService.deleteByBatis(MapperNSEnum.UserManagementMaper, "deleteUser", userId);

		} catch (Exception e) {

			return -1;
		}

		return 1;
	}

	// 年度重置用户密码

	@Override
	public int resetPassword(String year, Map<String, Object> params) {

		try {
			baseService.updateByBatis(MapperNSEnum.UserManagementMaper, "resetPassword", params);
		} catch (Exception e) {

			return -1;
		}

		return 1;
	}

	// 年度查找用户部门
	@Override
	public Map<String, Object> getDepartmentTable(String year, Map<String, Object> map, int page, int rows) {

		PageInfo<UserInformation> list = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,
				"getDepartmentInformation", map, page, rows);
		map = new HashMap<String, Object>();
		map.put("rows", list.getList());
		map.put("total", list.getTotal());

		return map;
	}

	// 年度保存部门

	@Override
	public int saveDepartment(String year, Map<String, Object> map) {

		Map<String, Object> departMap = new HashMap<>();
		departMap.put("departName", map.get("name"));
		departMap.put("department_serial_number", map.get("department_serial_number"));
		departMap.put("department_category", map.get("department_category"));

		List<Map<String, Object>> dertList = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,
				"getDepartMentByNameList", departMap);
		if (dertList.size() > 0) {

			return -2;
		} else {
			try {
				baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "addDeprmet", map);
			} catch (Exception e) {

				return -1;
			}

			return 1;
		}
	}

	// 年度修改部门

	@Override
	public int updateDepartment(String year, Map<String, Object> map) {

		Map<String, Object> departMap = new HashMap<>();
		departMap.put("departName", map.get("name"));
		departMap.put("department_serial_number", map.get("department_serial_number"));
		departMap.put("department_category", map.get("department_category"));
		departMap.put("id", map.get("id"));
		List<Map<String, Object>> dertList = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper,
				"getDepartMentByNameList", departMap);
		if (dertList.size() > 0) {

			return -2;
		} else {
			try {
				baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "updateDeprmet", map);
			} catch (Exception e) {

				return -1;
			}

			return 1;
		}
	}

	// 年度删除部门

	@Override
	public int deleteDepartment(String year, Integer userId) {

		try {
			baseService.deleteByBatis(MapperNSEnum.UserManagementMaper, "deleteDepartment", userId);
		} catch (Exception e) {
			return -1;
		}

		return 1;
	}

	// 获取字段前台页
	@Override
	public List<Map<String, Object>> getTableColumnInfo(String year, Map<String, Object> map) {
		return baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper, "getTableColumnInfo", map);
	}

	/**
	 * 根据表名指标等级信息
	 */
	@Override
	public List<Map<String, Object>> assessmentIndexSelect(String year, String tableName) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("id", "不采纳");
		result.put("text", "不采纳");
		List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper,
				"assmentIndexSelect", tableName);
		list.add(result);
		return list;
	}

	/**
	 * 根据表名指标分数信息
	 */
	@Override
	public List<Map<String, Object>> assessmentIndexScoreSelect(String year, String tableName) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("id", 0);
		result.put("text", 0);
		List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.InnovationteamMaper,
				"assmentIndexScoreSelect", tableName);
		list.add(result);
		return list;
	}

	/**
	 * 根据表名查找数据统计指标在审，采纳和不采纳
	 */
	@Override
	public List<Map<String, Object>> getDataStatistics(String year, String tableName, String tableName1,
			String tableName2) {
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> param = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		param.put("tableName", tableName);
		map = baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段", "暂存");
		list.add(map);
		param.put("tableName", tableName1);
		map = baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段", "采纳");
		list.add(map);
		param.put("tableName", tableName2);
		map = baseService.selectByBatis(MapperNSEnum.InnovationteamMaper, "getDataStatistics", param);
		map.put("处理阶段", "不采纳");
		list.add(map);
		return list;
	}

	/**
	 * //用户角色新增
	 */

	@Override
	public Map<String, Object> saveUserRoleOne(String year, Map<String, Object> params) {

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在
			List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
					"findRoleByUserId", params);

			if (list.size() > 0) {
				params.put("user_role_id", list.get(0).get("user_role_id"));
				int update = baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateRoleUserByOne",
						params);
				if (update > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "分配成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "分配失败");
				}
			} else {
				long insert = baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addRoleUserByOne", params);

				if (insert > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "分配成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "分配失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

	// 角色下拉
	@Override
	public Map<String, Object> getRoleListAllSelect(String year, Map<String, Object> params) {

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		JSONArray array = new JSONArray();
		JSONArray arraylist = new JSONArray();
		List<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
				"getRoleListAll", params);
		if (info.size() > 0) {
			for (Map<String, Object> map : info) {
				Map<String, Object> map2 = new HashMap<String, Object>();
				map2.put("id", map.get("id"));
				map2.put("text", map.get("roleName"));
				array.add(map2);
			}
		}
		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("children", array);
		map3.put("id", "");
		map3.put("text", "All");
		arraylist.add(map3);
		resuleMap.put("roleList", arraylist);
		return resuleMap;
	}

	// 角色列表
	@Override
	public Map<String, Object> getRoleListPage(String year, Map<String, Object> params, int page, int rows) {

		PageInfo<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
				"findRoleListAll", params, Integer.valueOf(params.get("page").toString()),
				Integer.valueOf(params.get("rows").toString()));

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		resuleMap.put("rows", info.getList());
		resuleMap.put("total", info.getTotal());
		return resuleMap;
	}

	// add角色

	@Override
	public Map<String, Object> addRole(String year, Map<String, Object> params) {
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在角色
			List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
					"findRoleByRoleName", params);

			if (list.size() > 0) {
				resuleMap.put("code", -3);
				resuleMap.put("errorMsg", "角色名已存在");
			} else {
				long insert = baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addRoleOne", params);

				if (insert > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "新增成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "新增失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}

		return resuleMap;

	}

	// update角色
	@Override

	public Map<String, Object> updateRole(String year, Map<String, Object> params) {

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在角色
			List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
					"findRoleByRoleName", params);
			if (list.size() > 0) {
				resuleMap.put("code", -3);
				resuleMap.put("errorMsg", "角色名已存在");
			} else {
				int update = baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateRoleOne", params);

				if (update > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "修改成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "修改失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

	// delete角色
	@Override

	public Map<String, Object> deleteRole(String year, Map<String, Object> params) {

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			int delete = baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "deleteRoleOne", params);

			if (delete > 0) {
				resuleMap.put("code", 200);

				resuleMap.put("errorMsg", "删除成功");
			} else {
				resuleMap.put("code", -1);
				resuleMap.put("errorMsg", "删除失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

	// 获取权限
	@Override
	public Map<String, Object> getPermissionAllSelect(String year, Map<String, Object> params) {

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		JSONArray array = new JSONArray();
		JSONArray arraylist = new JSONArray();
		List<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
				"findPermissionListAll", params);
		if (info.size() > 0) {
			for (Map<String, Object> map : info) {
				Map<String, Object> map2 = new HashMap<String, Object>();
				map2.put("id", map.get("id"));
				map2.put("text", map.get("permissionName"));
				array.add(map2);
			}
		}
		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("children", array);
		map3.put("id", "");
		map3.put("text", "All");
		arraylist.add(map3);
		resuleMap.put("permissionList", arraylist);

		return resuleMap;
	}

	// 根据角色Id获取权限
	@Override
	public JSONArray getPermissionAllByroleId(String year, Map<String, Object> params) {
		JSONArray array = new JSONArray();
		JSONArray arraylist = new JSONArray();
		List<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
				"getPermissionAllByroleId", params);
		if (info.size() > 0) {
			for (Map<String, Object> map : info) {
				Map<String, Object> map2 = new HashMap<String, Object>();
				map2.put("id", map.get("id"));
				map2.put("text", map.get("permissionName"));
				array.add(map2);
			}
		}
		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("children", array);
		map3.put("id", "");
		map3.put("text", "All");
		arraylist.add(map3);
		return arraylist;
	}

	// 权限列表
	@Override
	public Map<String, Object> getPermissionList(String year, Map<String, Object> params, int page, int rows) {
		PageInfo<List<Map<String, Object>>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
				"findPermissionListAll", params, page, rows);

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		resuleMap.put("rows", info.getList());
		resuleMap.put("total", info.getTotal());
		return resuleMap;
	}

	// 权限新增
	@Override

	public Map<String, Object> addPermissionOne(String year, Map<String, Object> params) {
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在菜单
			List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
					"findPermissionByPermissionName", params);

			if (list.size() > 0) {
				resuleMap.put("code", -3);
				resuleMap.put("errorMsg", "权限名已存在");
			} else {
				long insert = baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addPermissionOne", params);

				if (insert > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "新增成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "新增失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

// 	权限修改
	@Override

	public Map<String, Object> updatePermissionOne(String year, Map<String, Object> params) {
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在菜单
			List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
					"findPermissionByPermissionName", params);
			if (list.size() > 0) {
				resuleMap.put("code", -3);
				resuleMap.put("errorMsg", "权限名已存在");
			} else {
				int update = baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updatePermissionOne",
						params);

				if (update > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "修改成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "修改失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

	// 权限删除
	@Override

	public Map<String, Object> deletePermissionOne(String year, Map<String, Object> params) {
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			int delete = baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "deletePermissionOne", params);

			if (delete > 0) {
				resuleMap.put("code", 200);

				resuleMap.put("errorMsg", "删除成功");
			} else {
				resuleMap.put("code", -1);
				resuleMap.put("errorMsg", "删除失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

	// 菜单下拉
	@Override
	public List<Menu> getMenusListSelect(String year, Map<String, Object> params) {
		List<Menu> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuListAll", params);
		return list;
	}

	// 菜单列表
	@Override
	public Map<String, Object> getMenusListPage(String year, Map<String, Object> params, int page, int rows) {
		PageInfo<List<Menu>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuListAll",
				params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		resuleMap.put("rows", info.getList());
		resuleMap.put("total", info.getTotal());
		return resuleMap;
	}

	// add菜单
	@Override

	public Map<String, Object> addMenu(String year, Map<String, Object> params) {
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在菜单
			List<Menu> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuByMenuName",
					params);

			if (list.size() > 0) {
				resuleMap.put("code", -3);
				resuleMap.put("errorMsg", "菜单名已存在");
			} else {
				long insert = baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addMenuOne", params);

				if (insert > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "新增成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "新增失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}

		return resuleMap;
	}

	// update菜单
	@Override

	public Map<String, Object> updateMenu(String year, Map<String, Object> params) {
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在菜单
			List<Menu> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuByMenuName",
					params);
			if (list.size() > 0) {
				resuleMap.put("code", -3);
				resuleMap.put("errorMsg", "菜单名已存在");
			} else {
				int update = baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateMenuOne", params);

				if (update > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "修改成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "修改失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

	// delete菜单
	@Override

	public Map<String, Object> deleteMenu(String year, Map<String, Object> params) {
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			int delete = baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "deleteMenuOne", params);

			if (delete > 0) {
				resuleMap.put("code", 200);

				resuleMap.put("errorMsg", "删除成功");
			} else {
				resuleMap.put("code", -1);
				resuleMap.put("errorMsg", "删除失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		return resuleMap;
	}

	/**
	 * 获取库表结构
	 */
	@Override
	public List<Table> getTableAndColumns(String year, Map<String, Object> map) {
		List<Table> tables = baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTable", map);
		return tables;
	}

	/**
	 * 获取模板库表结构
	 */
	@Override
	public List<Table> getTableAndColumnsModel(String year, Map<String, Object> map) {
		List<Table> list = baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableAndModel",
				new HashMap<String, Object>());

		return list;
	}

	// 添加字段
	@Override

	public Map<String, Object> addColumn(String year, String tableName, Map<String, Object> params) {
		Map<String, Object> result = new HashMap<String, Object>();
		// 添加前先查找该表是否含有该字段了create_table_id create_table_column
		List<String> list = baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "descTableName", tableName);
		if (SystemUtil.ifInclude(list, params.get("create_table_column").toString())) {
			result.put("code", -2);
			result.put("msg", "该字段已存在");
			return result;
		}
		;

		int tableNumberId = baseService.selectByBatis(MapperNSEnum.UserManagementMaper, "getTableNumId", tableName);
		params.put("create_table_name", tableName);
		params.put("create_table_id", tableNumberId);
		String need = (String) params.get("is_need");
		if (need.equals("是")) {
			params.put("is_need", "1");
		} else {
			params.put("is_need", "0");
		}
		// 添加
		try {

			baseService.updateByBatis(MapperNSEnum.UserManagementMaper, "alterCol", params);
			// 添加到字段描述
			long al = baseService.insertByBatis(MapperNSEnum.UserManagementMaper, "addColumn", params);
			if (al > 0) {
				result.put("code", 0);
				result.put("msg", "新增字段成功");
			} else {
				result.put("code", -1);
				result.put("msg", "新增字段描述表失败，请前往字段描述表新增该信息");
			}
		} catch (Exception e) {

			result.put("code", -1);
			result.put("msg", "新增字段失败");
		}
		return result;
	}

	// 获取权限下拉
	@Override
	public List<Map<String, Object>> getPermissionSelect(String year, Map<String, Object> params) {
		List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper,
				"findPermissionListByParent", params);

		return list;
	}

	/**
	 * 根据年度动态获取一级菜单
	 */

	@Override
	public List<Map<String, Object>> getParentList(String year, Map<String, Object> map) {

		List<Map<String, Object>> menuList = baseService.selectListByBatis(MapperNSEnum.LoginMaper,
				"findParentMenuList", map);
		return menuList;
	}

	/**
	 * 根据年度动态获取二级菜单
	 */
	@Override
	public Map<String, Object> getSecondMenu(String year, Map<String, Object> map) {

		Map<String, Object> treeMap = MenuUtil.findTree(baseService, 0);

		return treeMap;
	}

	/**
	 * 年度集合
	 */
	@Override
	public List<Map<String, Object>> getYearList(String year, Map<String, Object> map) {
		List<Map<String, Object>> yearList = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "findYearList",
				new HashMap<String, Object>());

		return yearList;
	}

	// 查询本年度表名翻译
	@Override
	public Map<String, Object> getTableNameTranslate(String year, Map<String, Object> map) {
		// TODO Auto-generated method stub

		Map<String, Object> result = new HashMap<>();

		int page = Integer.valueOf(map.get("page").toString());
		int rows = Integer.valueOf(map.get("rows").toString());
		PageInfo<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.TranslateMaper,
				"getTableNameTranslate", map, page, rows);

		result.put("code", 0);
		result.put("rows", list.getList());
		result.put("total", list.getTotal());

		return result;

	}

	// 查询本年度字段翻译
	@Override
	public Map<String, Object> getFieldNameTranslate(String year, Map<String, Object> map) {
		Map<String, Object> result = new HashMap<>();

		int page = Integer.valueOf(map.get("page").toString());
		int rows = Integer.valueOf(map.get("rows").toString());
		PageInfo<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.TranslateMaper,
				"getFieldNameTranslate", map, page, rows);

		result.put("code", 0);
		result.put("rows", list.getList());
		result.put("total", list.getTotal());
		return result;
	}

	// 查询next年度模板表名翻译
	@Override
	public Map<String, Object> getNextTableNameTranslate(String year, Map<String, Object> map) {

		Map<String, Object> result = new HashMap<>();

		int page = Integer.valueOf(map.get("page").toString());
		int rows = Integer.valueOf(map.get("rows").toString());
		PageInfo<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.TranslateMaper,
				"getTableNameTranslateModel", map, page, rows);

		result.put("code", 0);
		result.put("rows", list.getList());
		result.put("total", list.getTotal());
		return result;
	}

	// 查询next年度模板字段翻译
	@Override
	public Map<String, Object> getNextFieldNameTranslate(String year, Map<String, Object> map) {
		Map<String, Object> result = new HashMap<>();

		int page = Integer.valueOf(map.get("page").toString());
		int rows = Integer.valueOf(map.get("rows").toString());
		PageInfo<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.TranslateMaper,
				"getFieldNameTranslateModel", map, page, rows);

		result.put("code", 0);
		result.put("rows", list.getList());
		result.put("total", list.getTotal());
		return result;
	}

	// 查询年度表名下拉
	@Override
	public List<ConnotationTableCreate> getConnotationTableCreateList(String year, Map<String, Object> map) {
		List<ConnotationTableCreate> list = baseService.selectListByBatis(MapperNSEnum.TableMaper,
				"getTableListBySelect", map);

		return list;
	}
	//暂存，不采纳，采纳，归档
	@Override
	public List<ConnotationTableCreate> getTableListBySelectByCreateTableType(String year, Map<String, Object> map) {
		List<ConnotationTableCreate> list = baseService.selectListByBatis(MapperNSEnum.TableMaper,
				"getTableListBySelectByCreateTableType", map);

		return list;
	}
	
	// 新增年度
	@Override
	public int addYear(String year, Map<String, Object> map) {
		try {
			baseService.insertByBatis(MapperNSEnum.LoginMaper, "addyearOne", map);

		} catch (Exception e) {
			return -1;
		}

		return 1;
	}

	/**
	 * 年度集合
	 */
	@Override
	public Map<String, Object> getYearTableList(String year, Map<String, Object> map, int page, int rows) {
		PageInfo<List<Map<String, Object>>> yearList = baseService.selectListByBatis(MapperNSEnum.TableMaper,
				"getYearTableList", map, page, rows);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows", yearList.getList());
		result.put("total", yearList.getTotal());
		return result;
	}

	// 新增表名翻译
	@Override
	public int addTableNameTranslate(String year, Map<String, Object> map) {
		try {
			baseService.insertByBatis(MapperNSEnum.TranslateMaper, "addTableNameTranslate", map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}

	// 修改表名翻译
	@Override
	public int setTableNameTranslate(String year, Map<String, Object> map) {
		try {
			baseService.updateByBatis(MapperNSEnum.TranslateMaper, "setTableNameTranslate", map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}

	// 新增字段翻译
	@Override
	public int addFieldNameTranslate(String year, Map<String, Object> map) {
		try {
			baseService.insertByBatis(MapperNSEnum.TranslateMaper, "addFieldNameTranslate", map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}

	// 修改字段翻译
	@Override
	public int setFieldNameTranslate(String year, Map<String, Object> map) {
		try {
			baseService.updateByBatis(MapperNSEnum.TranslateMaper, "setFieldNameTranslate", map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	
	// 删除模板表名翻译
	@Override
	public int deleteTableNameTranslate(String year, Map<String, Object> map) {
		try {
			// 删除模板表名翻译
			baseService.deleteByBatis(MapperNSEnum.TableMaper, "deleteTableNameTranslateModel", map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	// 删除模板字段翻译
	@Override
	public int deleteFieldNameTranslate(String year, Map<String, Object> map) {
		try {
			baseService.deleteByBatis(MapperNSEnum.TableMaper, "deleteFieldNameTranslateModel", map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	//初始化导入新增表名信息
	@Override
	public int addInitTable(String year, Map<String, Object> map) {
			try {
				baseService.insertByBatis(MapperNSEnum.TableMaper, "addInitTable", map);
			} catch (Exception e) {
				return -1;
			}
			return 1;
	}
	//初始化导入新增表字段信息
	@Override
	public int addInitTableField(String year, Map<String, Object> map) {
		try {
			baseService.insertByBatis(MapperNSEnum.TableMaper, "addInitTableField", map);
		} catch (Exception e) {
			return -1;
		}
		return 1;
	}
	
	@Override
	public Map<String, Object> batch_addInitTable(String year,MultipartFile file) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String filename=file.getOriginalFilename();

		try {
			
			if (!filename.matches("^.+\\.(?i)(xls)$") && !filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    throw new Exception("上传文件格式不正确");
	                }
			//判断Excel文件的版本
			boolean isExcel2003=true;
		        if (filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    isExcel2003 = false;
	                }
		    
		        InputStream is=file.getInputStream();
		        Workbook wb=null;	    
		        if (isExcel2003) {
	                wb = new HSSFWorkbook(is);
	                } else {
	                wb = new XSSFWorkbook(is);
	                }
			// 得到第一页 sheet
			// 页Sheet是从0开始索引的
			Sheet sheet = wb.getSheetAt(0);
			// 利用foreach循环 遍历sheet中的所有行
			// sheet.getLastRowNum()获取总行数，sheet.getRow(i)获取某行
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				// 遍历row中的所有方格
				Row row = sheet.getRow(i);
				for (int j = 0; j < row.getLastCellNum(); j++) {
					// 输出方格中的内容，以空格间隔
					Cell cell = sheet.getRow(i).getCell(j);
					String cell_1 = "";// 单元格的值

					if (null == cell) {
						cell_1 = "";
					} else {
						cell.setCellType(CellType.STRING);
						cell_1 = cell.getStringCellValue();
					}
					// 表中的字段
					Cell cell_2 = sheet.getRow(0).getCell(j);
					cell_2.setCellType(CellType.STRING);
					String cell_3 = cell_2.getStringCellValue();
					switch (cell_3) {
					case "表编号":

						map.put("create_table_num", cell_1);
						break;
					case "英文表名":
						map.put("create_table_english_name", cell_1);
						
						break;
					case "中文表名":
						map.put("create_table_chinese_name", cell_1);
						break;

					case "表类型":
						map.put("create_table_type", cell_1);
					case "表描述":
						map.put("create_table_description", cell_1);
							break;
					}
				}
				baseService.insertByBatis(MapperNSEnum.TableMaper, "addInitTable", map);

			}

		} catch (Exception e1) {
			e1.printStackTrace();
			resultMap.put("code", -1);

			resultMap.put("msg", "导入失败");
			return resultMap;
		}

		resultMap.put("code", 200);

		resultMap.put("msg", "导入成功");
		return resultMap;
	}
	@Override
	public Map<String, Object> batch_addInitTableField(String year,MultipartFile file) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String filename=file.getOriginalFilename();
		try {
			
			if (!filename.matches("^.+\\.(?i)(xls)$") && !filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    throw new Exception("上传文件格式不正确");
	                }
			//判断Excel文件的版本
			boolean isExcel2003=true;
		        if (filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    isExcel2003 = false;
	                }
		    
		        InputStream is=file.getInputStream();
		        Workbook wb=null;	    
		        if (isExcel2003) {
	                wb = new HSSFWorkbook(is);
	                } else {
	                wb = new XSSFWorkbook(is);
	                }
			// 得到第一页 sheet
			// 页Sheet是从0开始索引的
			Sheet sheet = wb.getSheetAt(0);
			// 利用foreach循环 遍历sheet中的所有行
			// sheet.getLastRowNum()获取总行数，sheet.getRow(i)获取某行
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				// 遍历row中的所有方格
				Row row = sheet.getRow(i);
				for (int j = 0; j < row.getLastCellNum(); j++) {
					// 输出方格中的内容，以空格间隔
					Cell cell = sheet.getRow(i).getCell(j);
					String cell_1 = "";// 单元格的值

					if (null == cell) {
						cell_1 = "";
					} else {
						cell.setCellType(CellType.STRING);
						cell_1 = cell.getStringCellValue();
					}
					// 表中的字段
					Cell cell_2 = sheet.getRow(0).getCell(j);
					cell_2.setCellType(CellType.STRING);
					String cell_3 = cell_2.getStringCellValue();
				
					switch (cell_3) {
					case "表编号":

						map.put("create_table_id", cell_1);
						break;
					case "英文表名":
						map.put("create_table_name", cell_1);
						
						break;
					case "表字段":
						map.put("create_table_column", cell_1);
						break;

					case "表字段类型":
						map.put("create_table_column_type", cell_1);
						break;
					case "表字段中文":
						map.put("create_table_chinese_column", cell_1);
							break;
					case "前台是否需要填写":
						if("是".equals(cell_1)) {

							map.put("is_need", 1);
							}else {
								map.put("is_need", 0);	
							}
							break;
							
					case "统计模板页面是否显示":
							map.put("is_statistics", cell_1);
							break;
							
					case "字段权重":
						map.put("field_weight", cell_1);
							break;
					}
				}
				baseService.insertByBatis(MapperNSEnum.TableMaper, "addInitTableField", map);

			}

		} catch (Exception e1) {
			e1.printStackTrace();
			resultMap.put("code", -1);
			resultMap.put("msg", "导入失败");
			return resultMap;
		}
		resultMap.put("code", 200);

		resultMap.put("msg", "导入成功");
		return resultMap;
	}
	
	
	@Override
	public Map<String, Object> batch_addInitTableModel(String year,MultipartFile file) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String filename=file.getOriginalFilename();

		try {
			
			if (!filename.matches("^.+\\.(?i)(xls)$") && !filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    throw new Exception("上传文件格式不正确");
	                }
			//判断Excel文件的版本
			boolean isExcel2003=true;
		        if (filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    isExcel2003 = false;
	                }
		    
		        InputStream is=file.getInputStream();
		        Workbook wb=null;	    
		        if (isExcel2003) {
	                wb = new HSSFWorkbook(is);
	                } else {
	                wb = new XSSFWorkbook(is);
	                }
			// 得到第一页 sheet
			// 页Sheet是从0开始索引的
			Sheet sheet = wb.getSheetAt(0);
			// 利用foreach循环 遍历sheet中的所有行
			// sheet.getLastRowNum()获取总行数，sheet.getRow(i)获取某行
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				// 遍历row中的所有方格
				Row row = sheet.getRow(i);
				for (int j = 0; j < row.getLastCellNum(); j++) {
					// 输出方格中的内容，以空格间隔
					Cell cell = sheet.getRow(i).getCell(j);
					String cell_1 = "";// 单元格的值

					if (null == cell) {
						cell_1 = "";
					} else {
						cell.setCellType(CellType.STRING);
						cell_1 = cell.getStringCellValue();
					}
					// 表中的字段
					Cell cell_2 = sheet.getRow(0).getCell(j);
					cell_2.setCellType(CellType.STRING);
					String cell_3 = cell_2.getStringCellValue();
					switch (cell_3) {
					case "表编号":

						map.put("create_table_num", cell_1);
						break;
					case "英文表名":
						map.put("create_table_english_name", cell_1);
						
						break;
					case "中文表名":
						map.put("create_table_chinese_name", cell_1);
						break;

					case "表类型":
						map.put("create_table_type", cell_1);
					case "表描述":
						map.put("create_table_description", cell_1);
							break;
					}
				}
				baseService.insertByBatis(MapperNSEnum.TableMaper, "addInitTableModel", map);

			}

		} catch (Exception e1) {
			e1.printStackTrace();
			resultMap.put("code", -1);

			resultMap.put("msg", "导入失败");
			return resultMap;
		}

		resultMap.put("code", 200);

		resultMap.put("msg", "导入成功");
		return resultMap;
	}
	@Override
	public Map<String, Object> batch_addInitTableFieldModel(String year,MultipartFile file) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String filename=file.getOriginalFilename();
		try {
			
			if (!filename.matches("^.+\\.(?i)(xls)$") && !filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    throw new Exception("上传文件格式不正确");
	                }
			//判断Excel文件的版本
			boolean isExcel2003=true;
		        if (filename.matches("^.+\\.(?i)(xlsx)$")) {
	                    isExcel2003 = false;
	                }
		    
		        InputStream is=file.getInputStream();
		        Workbook wb=null;	    
		        if (isExcel2003) {
	                wb = new HSSFWorkbook(is);
	                } else {
	                wb = new XSSFWorkbook(is);
	                }
			// 得到第一页 sheet
			// 页Sheet是从0开始索引的
			Sheet sheet = wb.getSheetAt(0);
			// 利用foreach循环 遍历sheet中的所有行
			// sheet.getLastRowNum()获取总行数，sheet.getRow(i)获取某行
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				// 遍历row中的所有方格
				Row row = sheet.getRow(i);
				for (int j = 0; j < row.getLastCellNum(); j++) {
					// 输出方格中的内容，以空格间隔
					Cell cell = sheet.getRow(i).getCell(j);
					String cell_1 = "";// 单元格的值

					if (null == cell) {
						cell_1 = "";
					} else {
						cell.setCellType(CellType.STRING);
						cell_1 = cell.getStringCellValue();
					}
					// 表中的字段
					Cell cell_2 = sheet.getRow(0).getCell(j);
					cell_2.setCellType(CellType.STRING);
					String cell_3 = cell_2.getStringCellValue();
				
					switch (cell_3) {
					case "表编号":

						map.put("create_table_id", cell_1);
						break;
					case "英文表名":
						map.put("create_table_name", cell_1);
						
						break;
					case "表字段":
						map.put("create_table_column", cell_1);
						break;

					case "表字段类型":
						map.put("create_table_column_type", cell_1);
						break;
					case "表字段中文":
						map.put("create_table_chinese_column", cell_1);
							break;
					case "前台是否需要填写":
						if("是".equals(cell_1)) {

							map.put("is_need", 1);
							}else {
								map.put("is_need", 0);	
							}
							break;
							
					case "统计模板页面是否显示":
							map.put("is_statistics", cell_1);
							break;
							
					case "字段权重":
						map.put("field_weight", cell_1);
							break;
					}
				}
				baseService.insertByBatis(MapperNSEnum.TableMaper, "addInitTableFieldModel", map);

			}

		} catch (Exception e1) {
			e1.printStackTrace();
			resultMap.put("code", -1);
			resultMap.put("msg", "导入失败");
			return resultMap;
		}
		resultMap.put("code", 200);

		resultMap.put("msg", "导入成功");
		return resultMap;
	}
	/**
	 * 统计暂存，采纳列表
	 * */
	@Override
	public List<Table> getTemporarystorageList(String year,Map<String,Object> map) {
		
		List<Table> list=	baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTemporarystorageList", map);
	
		return list;
	}
	///统计积分结果
	@Override
	public List<Table> getTableResultScoreList(String year,Map<String,Object> map) {
		
		List<Table> list=	baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableResultScoreList", map);
	
		return list;
	}
	///统计积分结果
	@Override
	public List<Map<String, Object>> getTableResultScoreByLLevelList(String year,Map<String,Object> map) {
		
		List<Map<String, Object>> list=	baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableResultScoreByLLevelList", map);
	
		return list;
	}

	///统计积分结果 条件
	@Override
	public List<Map<String, Object>> getTableResultScoreByLLevelList1(String year, Map<String, Object> map) {
		List<Map<String, Object>> list=	baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableResultScoreByLLevelList1", map);

		return list;
	}
}
