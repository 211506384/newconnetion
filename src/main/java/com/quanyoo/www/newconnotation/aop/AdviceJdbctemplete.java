package com.quanyoo.www.newconnotation.aop;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import com.quanyoo.www.newconnotation.sqlDataSource.DataSourceUtil;
import com.quanyoo.www.newconnotation.sqlDataSource.DynamicDataSourceContextHolder;

/*
 * jdbctemplete
 * */
@Component
@Aspect
public class AdviceJdbctemplete {

	
	  @Around("execution(* com.quanyoo.www.newconnotation.service.impl.AopYearJdbcServiceImpl.*(..))"
	  ) public Object process(ProceedingJoinPoint point) throws Throwable {
	  System.out.println("@Around：执行目标方法之前..."); 
	  //访问目标方法的参数：
	  Object[] args = point.getArgs();
		SimpleDateFormat df = new SimpleDateFormat("yyyy");// 设置日期格式
		String year = df.format(new Date());// new Date()为获取当前系统时间
		if (args != null && args.length > 0 && args[0].getClass() == String.class) {
			year = (String) args[0];
		}
		// 数据源key
		String newDsKey = year;
		// 添加数据源
		DataSourceUtil.addDataSourceToDynamic(newDsKey, DataSourceUtil.getDbInfo(year));
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
		DataSource dataSource = DataSourceUtil.getJdbcDataSource(year);
		args[1] = new JdbcTemplate(dataSource);
		Object returnValue = point.proceed(args); 
	 return returnValue; 
	 }
	 

	@Before("execution(* com.quanyoo.www.newconnotation.service.impl.AopYearJdbcServiceImpl.*(..))")
	public void permissionCheck(JoinPoint point) {
		// System.out.println("@Before：模拟权限检查...");
		// System.out.println("@Before：参数为：" + Arrays.toString(point.getArgs()));
		

	}

	@AfterReturning(pointcut = "execution(* com.quanyoo.www.newconnotation.service.impl.AopYearJdbcServiceImpl.*(..))", returning = "returnValue")
	public void log(JoinPoint point, Object returnValue) {
		// System.out.println("@AfterReturning：模拟日志记录功能...");
		// System.out.println("@AfterReturning：目标方法为：" +
		// point.getSignature().getDeclaringTypeName() +
		// "." + point.getSignature().getName());
		// System.out.println("@AfterReturning：参数为：" +
		// Arrays.toString(point.getArgs()));
		// System.out.println("@AfterReturning：返回值为：" + returnValue);
		// System.out.println("@AfterReturning：被织入的目标对象为：" + point.getTarget());

	}

	@After("execution(* com.quanyoo.www.newconnotation.service.impl.AopYearJdbcServiceImpl.*(..))")
	public void releaseResource(JoinPoint point) {
		DynamicDataSourceContextHolder.removeContextKey();
	}
}