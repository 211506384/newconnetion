package com.quanyoo.www.newconnotation.aop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;


@Component
public class ReshTask {
	  @Resource
	    private IBaseService baseService;
 	@Scheduled(cron = "0/90 * * * * ? ") // 间隔90秒执行
 	public void taskCycle() {
 	List<Map<String, Object>> list=baseService.selectListByBatis(MapperNSEnum.UserManagementMaper, "getMysqlprocesslist", new HashMap<String, Object>());
 		if(list.size()>0) {
 			for(Map<String, Object> map:list) {
 				System.out.println(map.get("id"));
 				try {
 					baseService.updateByBatis(MapperNSEnum.UserManagementMaper, "killprocesslist", map);
				} catch (Exception e) {
					continue;
				}
 			}
 		}
 	}
}
