package com.quanyoo.www.newconnotation.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.*;
import com.quanyoo.www.newconnotation.util.JDBCTemplate;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
public class ModelViewController {
	@Autowired
	private IBaseService baseService;
	@Autowired
	private TestServiceInterface testServiceInterface;
	@Autowired
	private StatisticsService statisticsService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private JdbcService jdbcService;
	@Autowired
	private AopYearJdbcService aopYearJdbcService;
	/**
	 * 跳转页面restful
	 */
	@RequestMapping("/findSystemView/{tableName}")
	public ModelAndView findStudentRestful(@PathVariable("tableName") String tableName) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tableName", tableName);
		// 获取表字段
		// List<Table> tableColumn =
		// baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTable", map);
		List<Table> tableColumn = testServiceInterface.getTableAndColumns(tableName);

		mv.addObject("tableName", tableName);
		mv.addObject("create_table_chinese_name", tableColumn.get(0).getCreate_table_chinese_name());
		Collections.sort(tableColumn.get(0).getTableColumn()); 
		System.out.println(tableColumn.get(0).getTableColumn().toString());
		mv.addObject("tableColumn", tableColumn.get(0).getTableColumn());
		mv.setViewName("modelView/systemView");
		return mv;
	}

	/**
	 * 表数据
	 */
	@RequestMapping("/getTableList")
	public void getTableList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, Object> params) {
		Map<String, Object> p = (Map) JSONObject.parse(params.get("map").toString());

		PageInfo<Map<String, Object>> pageInfo = testServiceInterface.selectListBypage(
				params.get("tableName").toString(), p, Integer.valueOf(params.get("page").toString()),
				Integer.valueOf(params.get("rows").toString()));
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows", pageInfo.getList());
		result.put("total", pageInfo.getTotal());

		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}

	/**
	 * 统计在审数据
	 */
	@RequestMapping("/getTemporarystorageList")
	public void getTemporarystorageList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, Object> params) {
		// 表名集合
		// List<Map<String, Object>> list
		// =statisticsService.getTemporarystorageList(params);

		// ResponseUtil.outputJson(response,list,"yyyy-MM-dd");
	}

	/**
	 * 跳转暂存统计页面restful
	 */
	@RequestMapping("/findTemporarystorageStatisticsView")
	public ModelAndView findTemporarystorageStatisticsView() {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("create_table_type", "暂存");
		// 获取表字段
		List<Table> list = statisticsService.getTemporarystorageList(map);

		mv.addObject("tableList", list);

		mv.setViewName("modelView/TemporarystorageStatisticsView");
		return mv;
	}

	/**
	 * 跳转采纳统计页面restful
	 */
	@RequestMapping("/findAdoptStatisticsView")
	public ModelAndView findAdoptStatisticsView() {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("create_table_type", "采纳");
		// 获取表字段
		List<Table> list = statisticsService.getTemporarystorageList(map);

		mv.addObject("tableList", list);

		mv.setViewName("modelView/statisticsAdoptView");
		return mv;
	}

	/**
	 * 统计表数据回显
	 */
	@RequestMapping("/getStaticsTableList/{tableName}")
	public void getStaticsTableList(String year,HttpServletRequest request, HttpServletResponse response,
			@PathVariable("tableName") String tableName) {

		//判断权限是否个人，集体
		Subject subject = SecurityUtils.getSubject();
		Session session =subject.getSession();
		if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
			try {
				response.sendRedirect("/login");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		User user=   (User)   session.getAttribute("user");

		String jobNum=null;
		if(subject.isPermitted("admin")) {
		}else {
			jobNum= user.getJobNum();
		}

		List<Map<String, Object>> pageInfo = aopYearJdbcService.selectAll(year,jdbcTemplate,tableName, new HashMap<String, Object>(),null,jobNum);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows",pageInfo );
		result.put("total", pageInfo.size());
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}
	/**
	 * 统计表数据回显
	 */
	@RequestMapping("/getStaticsTableList1/{tableName}")
	public void getStaticsTableList1(String year,HttpServletRequest request, HttpServletResponse response,
									@PathVariable("tableName") String tableName) {

		//判断权限是否个人，集体
		Subject subject = SecurityUtils.getSubject();
		Session session =subject.getSession();
		if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
			try {
				response.sendRedirect("/login");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		User user=   (User)   session.getAttribute("user");
		String college=user.getDepartment();
		String jobNum=null;
		if(subject.isPermitted("admin")) {
			college = null;
		}else if(subject.isPermitted("集体身份")) {
			college = user.getDepartment();
		}
		List<Map<String, Object>> pageInfo = aopYearJdbcService.selectAll(year,jdbcTemplate,tableName, new HashMap<String, Object>(),college,jobNum);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows",pageInfo );
		result.put("total", pageInfo.size());
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}

	/**
	 * 统计院系(部门)数据回显(超级管理员的权限)
	 */
	@RequestMapping("/getCalendarYearList")
	public void getCalendarYearList(HttpServletRequest request, HttpServletResponse response) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		// 年份
		List<Map<String, Object>> yearList = jdbcService
				.getdepartMentYearList(JDBCTemplate.getDataBaseTableJdbcTemplate());
		// 部门
		List<Map<String, Object>> departmentList = jdbcService
				.getdepartMentList(JDBCTemplate.getDataBaseTableJdbcTemplate());
		// 总分数据
		List<Map<String, Object>> departMentStaticsList = jdbcService
				.getdepartMentStaticsList(JDBCTemplate.getDataBaseTableJdbcTemplate());

		if (departmentList.size() > 0) {
			for (Map<String, Object> deMap : departmentList) {
				String departmentNameString = deMap.get("department_name").toString();
				if (yearList.size() <= 1) {
					String year = yearList.get(0).toString();
					int total_project_score = 0;
					for (Map<String, Object> staticsMap : departMentStaticsList) {
						if (departmentNameString.equals(staticsMap.get("department_name"))
								&& year.equals(staticsMap.get("year"))) {
							total_project_score = Integer.valueOf(staticsMap.get("total_project_score").toString());
						}
					}
					deMap.put("total_project_score_" + year, total_project_score);
					deMap.put("deffient_core_" + year, 0);

				} else {
					for (int i = 0; i < yearList.size(); i++) {
						String year = yearList.get(i).get("year").toString();
						Double total_project_score = 0.0d;
						Double next_total_project_score = 0.0d;
						int status_pre = 0;
						int status_next = 0;
						for (Map<String, Object> staticsMap : departMentStaticsList) {
							if (departmentNameString.equals(staticsMap.get("department_name"))
									&& year.equals(staticsMap.get("year"))) {
								total_project_score = Double.valueOf(staticsMap.get("total_project_score").toString());
								status_pre = 2;
							}
							if (departmentNameString.equals(staticsMap.get("department_name")) && year
									.equals(String.valueOf(Integer.valueOf((String) staticsMap.get("year")) + 1))) {
								next_total_project_score = Double
										.valueOf(staticsMap.get("total_project_score").toString());
								status_next = 2;
							}
							if (status_pre == 2 && status_next == 2) {
								break;
							}
						}
						Double deffient_core = total_project_score - next_total_project_score;
						deMap.put("total_project_score_" + year, String.format("%.3f", total_project_score));
						deMap.put("deffient_core_" + year, String.format("%.3f", deffient_core));
					}
				}
				list.add(deMap);
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows", list);
		result.put("total", list.size());
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}

	/**
	 * 统计表数据回显(超级管理员)
	 */
	@RequestMapping("/getCalendarYearListView")
	public ModelAndView getCalendarYearListView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		// 年份
		List<Map<String, Object>> yearList = jdbcService
				.getdepartMentYearList(JDBCTemplate.getDataBaseTableJdbcTemplate());
		List<TableColumn> tableColumns = new ArrayList<TableColumn>();

		Table table = new Table();
		if (yearList.size() > 0) {
			table.setCreate_table_chinese_name("院系部门数据总分统计");
			TableColumn columnd = new TableColumn();
			columnd.setCreate_table_chinese_column("部门名称");
			columnd.setCreate_table_column("department_name");
			tableColumns.add(columnd);
			for (int i = 0; i < yearList.size(); i++) {
				TableColumn column = new TableColumn();
				String year = yearList.get(i).get("year").toString();
				column.setCreate_table_chinese_column(year + "年度项目总分");
				column.setCreate_table_column("total_project_score_" + year);
				tableColumns.add(column);
				column = new TableColumn();
				column.setCreate_table_column("deffient_core_" + year);
				column.setCreate_table_chinese_column(year + "年度环比");
				tableColumns.add(column);
			}
			table.setTableColumn(tableColumns);

		}

		mv.addObject("tableList", table);

		mv.setViewName("modelView/calendarYearView");
		return mv;
	}

	/**
	 * 统计个人年度得分表数据回显
	 */
	@RequestMapping("/getIndividualSoreList")
	public void getIndividualSoreList(HttpServletRequest request, HttpServletResponse response) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		// 年份
		List<Map<String, Object>> yearList = jdbcService
				.getdepartMentYearList(JDBCTemplate.getDataBaseTableJdbcTemplate());
		// 部门
		List<Map<String, Object>> departmentList = jdbcService
				.getdepartMentList(JDBCTemplate.getDataBaseTableJdbcTemplate());
		// 总分数据
		List<Map<String, Object>> departMentStaticsList = jdbcService
				.getdepartMentStaticsList(JDBCTemplate.getDataBaseTableJdbcTemplate());

		if (departmentList.size() > 0) {
			for (Map<String, Object> deMap : departmentList) {
				String departmentNameString = deMap.get("department_name").toString();
				if (yearList.size() <= 1) {
					String year = yearList.get(0).toString();
					int total_project_score = 0;
					for (Map<String, Object> staticsMap : departMentStaticsList) {
						if (departmentNameString.equals(staticsMap.get("department_name"))
								&& year.equals(staticsMap.get("year"))) {
							total_project_score = Integer.valueOf(staticsMap.get("total_project_score").toString());
						}
					}
					deMap.put("total_project_score_" + year, total_project_score);
					deMap.put("deffient_core_" + year, 0);

				} else {
					for (int i = 0; i < yearList.size(); i++) {
						String year = yearList.get(i).get("year").toString();
						Double total_project_score = 0.0d;
						Double next_total_project_score = 0.0d;
						int status_pre = 0;
						int status_next = 0;
						for (Map<String, Object> staticsMap : departMentStaticsList) {
							if (departmentNameString.equals(staticsMap.get("department_name"))
									&& year.equals(staticsMap.get("year"))) {
								total_project_score = Double.valueOf(staticsMap.get("total_project_score").toString());
								status_pre = 2;
							}
							if (departmentNameString.equals(staticsMap.get("department_name")) && year
									.equals(String.valueOf(Integer.valueOf((String) staticsMap.get("year")) + 1))) {
								next_total_project_score = Double
										.valueOf(staticsMap.get("total_project_score").toString());
								status_next = 2;
							}
							if (status_pre == 2 && status_next == 2) {
								break;
							}
						}
						Double deffient_core = total_project_score - next_total_project_score;
						deMap.put("total_project_score_" + year, String.format("%.3f", total_project_score));
						deMap.put("deffient_core_" + year, String.format("%.3f", deffient_core));
					}
				}
				list.add(deMap);
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows", list);
		result.put("total", list.size());
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}

	/**
	 * 统计个人年度得分表数据页
	 */
	@RequestMapping("/getIndividualSoreListView")
	public ModelAndView getIndividualSoreListView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();
		// 年份
		List<Map<String, Object>> yearList = jdbcService
				.getdepartMentYearList(JDBCTemplate.getDataBaseTableJdbcTemplate());
		List<TableColumn> tableColumns = new ArrayList<TableColumn>();

		Table table = new Table();
		if (yearList.size() > 0) {
			table.setCreate_table_chinese_name("院系部门数据总分统计");
			TableColumn columnd = new TableColumn();
			columnd.setCreate_table_chinese_column("部门名称");
			columnd.setCreate_table_column("department_name");
			tableColumns.add(columnd);
			for (int i = 0; i < yearList.size(); i++) {
				TableColumn column = new TableColumn();
				String year = yearList.get(i).get("year").toString();
				column.setCreate_table_chinese_column(year + "年度项目总分");
				column.setCreate_table_column("total_project_score_" + year);
				tableColumns.add(column);
				column = new TableColumn();
				column.setCreate_table_column("deffient_core_" + year);
				column.setCreate_table_chinese_column(year + "年度环比");
				tableColumns.add(column);
			}
			table.setTableColumn(tableColumns);

		}

		mv.addObject("tableList", table);

		mv.setViewName("modelView/calendarYearView");
		return mv;
	}
}
