package com.quanyoo.www.newconnotation.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.UserService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import com.quanyoo.www.newconnotation.util.SystemUtil;
import com.alibaba.fastjson.JSON;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.UserInformation;
/**
 * 用户、部门模块
 * */
@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
    @Resource
    private IBaseService baseService;
    //跳转页面
    @RequestMapping("/userManagement")
    public ModelAndView userManagement() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("department_management_module/user_management/usermanagement");
        List<DepartmentInformation> departmentList = userService.getDepartment(new HashMap<String, Object>());
      mvc.addObject("departmentList",departmentList);
        return mvc;
    }
    //获取用户表
    @RequestMapping("/getUserTable")
    public void getUserTable(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) {
        Map<String,Object> result=userService.getUserList(params,Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response,result,"yyyy-MM-dd");
    }
    //跳转页面用户部门
    @RequestMapping("/userDepartment")
    public ModelAndView userDepartment() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("department_management_module/department_management/deptmanagement");
        return mvc;
    }
	/**
	 * 保存用户
	 * */
	@RequestMapping("/saveUser")
	public void addUser(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		String pString =SystemUtil.getMapToString(params);
		UserInformation user= JSON.parseObject(pString, UserInformation.class);
		int  add = userService.saveUser(user);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "保存成功");
		}else {

			result.put("code", -1);
			result.put("msg", "保存失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};	
	
	/**
	 * 删除用户
	 * */
	@RequestMapping("/deleteUser")
	public void deleteUser(Integer id ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = userService.deleteUser(id);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "删除成功");
		}else {

			result.put("code", -1);
			result.put("msg", "删除失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};
	
	/**
	 * 重置用户密码
	 * */
	@RequestMapping("/resetPassword")
	public void resetPassword(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		params.put("pwd",123456);
		
		int  update = userService.resetPassword(params);
		if(update>0) {

			result.put("code", 0);
			result.put("msg", "重置成功");
		}else {

			result.put("code", -1);
			result.put("msg", "重置失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};
	/**
	    * @param request
		* @param response
		* @param params
	    * @描述 获取用户部门列表
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:16
	*/
	@RequestMapping("/getDepartmentTable")
	public void getDepartmentTable(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) {
		Map<String,Object> result=userService.getDepartmentTable(params,Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
		ResponseUtil.outputJson(response,result,"yyyy-MM-dd");
	}
	/**
	    * @param params
		* @param request
		* @param response
	    * @描述 保存部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:31
	*/
	@RequestMapping("/saveDepartment")
	public void addDepartment(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = userService.saveDepartment(params);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "保存成功");
		}else if (add==-1){
			result.put("code", -1);
			result.put("msg", "保存失败");
		}else {
			result.put("code", -2);
			result.put("msg", "该部门已存在！");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	/**
	    * @param params
		* @param request
		* @param response
	    * @描述 修改部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 14:00
	*/
	@RequestMapping("/updateDepartment")
	public void updateDepartment(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = userService.updateDepartment(params);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "保存成功");
		}else if (add==-1){

			result.put("code", -1);
			result.put("msg", "保存失败");
		}else {
			result.put("code", -2);
			result.put("msg", "该部门已存在！");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	/**
	    * @param id
		* @param request
		* @param response
	    * @描述 删除部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:32
	*/
	@RequestMapping("/deleteDepartment")
	public void deleteDepartment(Integer id ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = userService.deleteDepartment(id);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "删除成功");
		}else {
			result.put("code", -1);
			result.put("msg", "删除失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

	};
	
//	
	@RequestMapping("/addColumn")
	public void addColumn(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		 params.put("create_table_name", "department_information");
		 params.put("create_table_column_type", "varchar(255)");
		 String need =(String) params.get("is_need");
		 if (need.equals("是")) {
			 params.put("is_need", "1");
		}else{
			params.put("is_need", "0");
		}
		long  add= baseService.insertByBatis(MapperNSEnum.UserManagementMaper,"addColumn",params);
		
		if(add>0) {
			int al=baseService.updateByBatis(MapperNSEnum.UserManagementMaper, "alterCol", params);
			if (al>0) {
				result.put("code", 0);
				result.put("msg", "新增字段成功");
			}else {
				result.put("code", -1);
				result.put("msg", "新增字段失败");
			}
			
		}else if (add==-1){
			result.put("code", -1);
			result.put("msg", "新增字段失败");
		}else {
			result.put("code", -2);
			result.put("msg", "该部门已存在！");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
}
