package com.quanyoo.www.newconnotation.controller;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.exportExcel.ExcelImportService;
import com.quanyoo.www.newconnotation.model.ConnotationTableCreate;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.service.AopYearJdbcService;
import com.quanyoo.www.newconnotation.service.AopYearService;
import com.quanyoo.www.newconnotation.service.TestServiceInterface;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
 
/**
 * 导入导出
 * */
@RestController
@RequestMapping("excelImport")
public class ExcelImportController {

	@Autowired
	private ExcelImportService excelImportService;
	@Autowired
	private AopYearJdbcService aopYearJdbcService;
	@Autowired
	private AopYearService aopYearService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	/**
	 * @param tableName		Excel文件要导入的表名
	 * @param file			Excel文件
	 * @param start			Excel文件导入内容的起始点。全导入则为1
	 * @return
	 */
	@PostMapping("importExcel")
	public void importExcel(String year,String tableName, @RequestParam("fileImport") MultipartFile file,HttpServletRequest request, HttpServletResponse response){
		// int start
		Map<String, Object> map=new HashMap<String, Object>();
		boolean a=false;
		try {
			a=excelImportService.ExcelImport(year,tableName,file,1);
			if(a) {
				map.put("code",200);
				map.put("msg","导入成功");
			}else {
				map.put("code",-1);
				map.put("msg","导入失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
		
				map.put("code",-2);
				map.put("msg",e.getMessage());
			
		}
		ResponseUtil.outputJson(response, map, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * @param tableName		Excel文件要导入的表名
	 * @param file			Excel文件
	 * @param start			Excel文件导入内容的起始点。全导入则为1
	 * @return
	 */
	@PostMapping("importUpdate")
	public void importUpdate(String year,String tableName, @RequestParam("fileImport") MultipartFile file,HttpServletRequest request, HttpServletResponse response){
		// int start
		Map<String, Object> map=new HashMap<String, Object>();
		boolean a=false;
		try {
			a=excelImportService.ExcelImportUpdate(year,tableName,file,1);
			if(a) {
				map.put("code",200);
				map.put("msg","导入更新成功");
			}else {
				map.put("code",-1);
				map.put("msg","导入更新失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
		
				map.put("code",-2);
				map.put("msg",e.getMessage());
			
		}
		ResponseUtil.outputJson(response, map, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 跳转导入页面admin
	 */
	@RequestMapping("/exportExcel")
	public ModelAndView exportExcel(String year,HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		List<ConnotationTableCreate> list= aopYearService.getConnotationTableCreateList(year,map);
		mv.addObject("tableList", list);
		mv.addObject("yearValue", year);
		mv.setViewName("modelView/exportExcel");
		return mv;
	}
	
	/**
	 * 跳转导入页面其他角色暂存
	 */
	@RequestMapping("/exportExcelOther")
	public ModelAndView exportExcelOther(String year,HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("create_table_type","暂存");
		List<ConnotationTableCreate> list= aopYearService.getTableListBySelectByCreateTableType(year,map);
		mv.addObject("tableList", list);
		mv.addObject("yearValue", year);
		mv.setViewName("pastYearModelView/exportExcelModelView");
		return mv;
	}
	
	/**
	 * 下载模板
	 * 
	 * @param params
	 * @param file
	 * @param request
	 * @param response
	 */
	@RequestMapping("export_excle")
	public void export_excle(String year,String tableName,HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> projectlist = new ArrayList<Map<String, Object>>();
    	Map<String, Object> params=new HashMap<String, Object>();
    	params.put("tableName", tableName);

		Map<String, Object> resultMap=new HashMap<String,Object>();
    	List<Table> listMap=  aopYearService.getTableAndColumns(year,params);
    	if(listMap.size()>0) {
    		List<TableColumn> columns = listMap.get(0).getTableColumn();
     	   String [] biaoti =new String[columns.size()] ;	// 列头

   			int []rowsWidth  =new int[columns.size()] ;
   			String keys[] =new String[columns.size()];
   			Map<String, Object> map=new HashMap<String, Object>();
 		for(int i=0;i< columns.size();i++) {
 			biaoti[i] = columns.get(i).getCreate_table_chinese_column();
 			
 			rowsWidth[i]=3000;// 列宽度
 			keys[i]= columns.get(i).getCreate_table_column();
 			map.put(columns.get(i).getCreate_table_chinese_column(), columns.get(i).getCreate_table_chinese_column());
 		}
 		projectlist.add(map);
 		resultMap.put("list", projectlist);
 		
 		resultMap.put("tName", listMap.get(0).getCreate_table_chinese_name());
    	}
		ResponseUtil.outputJson(response, resultMap, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 下载表名翻译模板导入
	 * 
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	
	@RequestMapping("/downTableFile")
	public void downFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
		URL url = new ClassPathResource("static/downFile/表名翻译模板导入.xlsx").getURL();
		File file_1 = new File(url.getFile());
		//InputStream inputStream = new FileInputStream(file);
		//File file_1 = new File("src/main/resources/static/downFile/表名翻译模板导入.xlsx");
		String userAgent = request.getHeader("User-Agent");
		// 创建下载文件
	
		// 获取文件名
		String formFileName = file_1.getName();
		try {
			// 读取数据
			InputStream bis = new BufferedInputStream(new FileInputStream(file_1));
			// 针对IE或者以IE为内核的浏览器：
			if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
				formFileName = java.net.URLEncoder.encode(formFileName, "UTF-8");
			} else {
				// 非IE浏览器的处理：
				formFileName = new String(formFileName.getBytes("UTF-8"), "ISO-8859-1");
			}

			response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", formFileName));
			response.setContentType("multipart/form-data");
			response.setCharacterEncoding("UTF-8");
			BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
			int len = 0;
			while ((len = bis.read()) != -1) {
				out.write(len);
				out.flush();
			}
			out.close();
			bis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * 下载表字段名翻译模板导入.xlsx
	 * 
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	
	@RequestMapping("/downTableFieldFile")
	public void downTableFieldFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// 创建下载文件
		URL url = new ClassPathResource("static/downFile/表字段名翻译模板导入.xlsx").getURL();
		File file_1 = new File(url.getFile());
		//File file_1 = new File("src/main/resources/static/downFile/表字段名翻译模板导入.xlsx");
		String userAgent = request.getHeader("User-Agent");
		
		// 获取文件名
		String formFileName = file_1.getName();
		try {
			// 读取数据
			InputStream bis = new BufferedInputStream(new FileInputStream(file_1));
			// 针对IE或者以IE为内核的浏览器：
			if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
				formFileName = java.net.URLEncoder.encode(formFileName, "UTF-8");
			} else {
				// 非IE浏览器的处理：
				formFileName = new String(formFileName.getBytes("UTF-8"), "ISO-8859-1");
			}

			response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", formFileName));
			response.setContentType("multipart/form-data");
			response.setCharacterEncoding("UTF-8");
			BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
			int len = 0;
			while ((len = bis.read()) != -1) {
				out.write(len);
				out.flush();
			}
			out.close();

			bis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	//查询数据
	@RequestMapping("/exportTbData")
	public void exportTbData(String year,String tableName,HttpServletRequest request, HttpServletResponse response) {
		
    	Map<String, Object> params=new HashMap<String, Object>();
    	params.put("tableName", tableName);

		Map<String, Object> resultMap=new HashMap<String,Object>();
		List<Map<String, Object>> pageInfo = aopYearJdbcService.selectlist(
				year,jdbcTemplate,tableName, params);
    	   			
 		resultMap.put("list", pageInfo);
 		
 		resultMap.put("tName", tableName);
    	
		ResponseUtil.outputJson(response, resultMap, "yyyy-MM-dd HH:mm:ss");
	}
}