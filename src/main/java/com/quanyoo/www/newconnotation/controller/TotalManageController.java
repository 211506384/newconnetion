package com.quanyoo.www.newconnotation.controller;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.model.PersonalScore;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.AopYearJdbcService;
import com.quanyoo.www.newconnotation.service.AopYearService;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/totalManage")
public class TotalManageController {
	@Autowired
	private AopYearJdbcService aopYearJdbcService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private AopYearService aopYearService;
    @Resource
    private IBaseService baseService;

    @RequestMapping("/personScore")
    public ModelAndView personScore(){
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("modelView/TotalPersonScore");
        return mvc;
    }
    //跳转计分结果
    @RequestMapping("/scoringResult/{tableName}")
    public ModelAndView scoringResult(String year,@PathVariable("tableName") String tableName){
        ModelAndView mvc = new ModelAndView();
        Map<String, Object> map=new HashMap<String, Object>();
        map.put("create_table_english_name", tableName);
        List<Table>  tables= aopYearService.getTableResultScoreList(year,map);
        TableColumn  tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("得分");
        tabled.setCreate_table_column("getScore");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("数量");
        tabled.setCreate_table_column("getNumber");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        Collections.sort(tables.get(0).getTableColumn());
        mvc.addObject("tableColumn", tables.get(0).getTableColumn());
        map=new HashMap<String, Object>();
        map.put("is_it_assessed", "是");
        List<DepartmentInformation> departmentInformations= aopYearService.getDepartment(year,map);
        mvc.addObject("departmentInformations",departmentInformations);
        mvc.addObject("yearValue",year);
        mvc.addObject("tableName",tableName);
     
        mvc.setViewName("modelView/ScoringResult");
        return mvc;
    }

    //跳转计分结果
    @RequestMapping("/scoringResult1/{tableName}")
    public ModelAndView scoringResult1(String year,@PathVariable("tableName") String tableName){
        ModelAndView mvc = new ModelAndView();
        Map<String, Object> map=new HashMap<String, Object>();
        map.put("create_table_english_name", tableName);
        List<Table>  tables= aopYearService.getTableResultScoreList(year,map);
        TableColumn  tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("得分");
        tabled.setCreate_table_column("getScore");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("数量");
        tabled.setCreate_table_column("getNumber");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        Collections.sort(tables.get(0).getTableColumn());
        mvc.addObject("tableColumn", tables.get(0).getTableColumn());
        map=new HashMap<String, Object>();
        map.put("is_it_assessed", "是");
        List<DepartmentInformation> departmentInformations= aopYearService.getDepartment(year,map);
        mvc.addObject("departmentInformations",departmentInformations);
        mvc.addObject("yearValue",year);
        mvc.addObject("tableName",tableName);

        mvc.setViewName("modelView/assessmentIndexView");
        return mvc;
    }

    //跳转计分结果
    @RequestMapping("/scoringResultPerson/{tableName}")
    public ModelAndView scoringResultPerson(String year,@PathVariable("tableName") String tableName){
        ModelAndView mvc = new ModelAndView();
        Map<String, Object> map=new HashMap<String, Object>();
        map.put("create_table_english_name", tableName);
        List<Table>  tables= aopYearService.getTableResultScoreList(year,map);
        TableColumn  tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("得分");
        tabled.setCreate_table_column("getScore");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("数量");
        tabled.setCreate_table_column("getNumber");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        Collections.sort(tables.get(0).getTableColumn());

        mvc.addObject("tableColumn", tables.get(0).getTableColumn());
        map=new HashMap<String, Object>();
        map.put("is_it_assessed", "是");
        List<DepartmentInformation> departmentInformations= aopYearService.getDepartment(year,map);
        mvc.addObject("departmentInformations",departmentInformations);
        mvc.addObject("yearValue",year);
        mvc.addObject("tableName",tableName);

        mvc.setViewName("modelView/scoringResultPerson");
        return mvc;
    }

    //跳转计分结果
    @RequestMapping("/scoringResultCollege/{tableName}")
    public ModelAndView scoringResultCollege(String year,@PathVariable("tableName") String tableName){
        ModelAndView mvc = new ModelAndView();
        Map<String, Object> map=new HashMap<String, Object>();
        map.put("create_table_english_name", tableName);
        List<Table>  tables= aopYearService.getTableResultScoreList(year,map);
        TableColumn  tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("得分");
        tabled.setCreate_table_column("getScore");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        tabled= new TableColumn();
        tabled.setCreate_table_chinese_column("数量");
        tabled.setCreate_table_column("getNumber");
        tabled.setField_weight(0);
        tables.get(0).getTableColumn().add(tabled);
        Collections.sort(tables.get(0).getTableColumn());
        mvc.addObject("tableColumn", tables.get(0).getTableColumn());
        map=new HashMap<String, Object>();
        map.put("is_it_assessed", "是");
        List<DepartmentInformation> departmentInformations= aopYearService.getDepartment(year,map);
        mvc.addObject("departmentInformations",departmentInformations);
        mvc.addObject("yearValue",year);
        mvc.addObject("tableName",tableName);

        mvc.setViewName("modelView/scoringResultCollege");
        return mvc;
    }

    /**
     *根据学院查询个人得分
     * @param map
     * @param request
     * @param response
     */
    @RequestMapping(value = "/queryByCollege")
    public void queryByCollege(String year,String tableColumn,@RequestParam Map<String,Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        PageInfo<PersonalScore> list = baseService.selectListByBatis(MapperNSEnum.TotalManageMaper, "queryByCollege", map, Integer.valueOf(map.get("page").toString()), Integer.valueOf(map.get("rows").toString()));

        result.put("code",0);
        result.put("rows", list.getList());
        result.put("total", list.getTotal());
        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 计分结果
     */
    @RequestMapping(value = "/queryScoringResult/{tableName}")
    public void queryScoringResult(String tableColumn,@PathVariable("tableName") String tableName,String year,@RequestParam Map<String,Object> map, HttpServletRequest request, HttpServletResponse response){
            Map<String,Object> result = new HashMap<>();
        int page = Integer.valueOf(map.get("page").toString());
        int rows = Integer.valueOf(map.get("rows").toString());
        map.put("page",page);
        map.put("rows",rows);
        map.put("create_table_type", "采纳");
      List<Table> tables= aopYearService.getTemporarystorageList(year, map);
      List<Map<String, Object>> dataList=new ArrayList<Map<String,Object>>();
      if(tables.size()>0) {
    	  Map<String, Object> map2=new HashMap<String, Object>();
    	  for(Table table:tables) {
    		  String tableNameString=table.getCreate_table_english_name();
    		  map2.put("tableName", tableNameString);
    		  List<Map<String, Object>> list=  aopYearService.getTableResultScoreByLLevelList(year, map2);
    		  dataList.addAll(list);
    	  }
      }
		/*
		 * List<Map<String,Object>> list =
		 * baseService.selectListByBatis(MapperNSEnum.TotalManageMaper,
		 * "queryScoringResult", map); List<Map<String,Object>> list1 =
		 * baseService.selectListByBatis(MapperNSEnum.TotalManageMaper, "getNumParam",
		 * map); List<Map<String,Object>> list2 = new ArrayList<>();
		 * 
		 * for (int i = 0; i < list1.size(); i++) { Map<String,Object> map1 = new
		 * HashMap<>(); Map<String,Object> map2 = list1.get(i);
		 * map1.put("main_table_name",map2.get("main_table_name"));
		 * map1.put("grade",map2.get("grade")); Map<String,Object> numMap = new
		 * HashMap<>(); double num =
		 * baseService.selectByBatis(MapperNSEnum.TotalManageMaper, "getNum", map1);
		 * if(Double.isNaN(num)){ num = 0; } numMap.put("num",num); double score = 0;
		 * if(map2.get("score") != null){ String s = (String) map2.get("score"); score =
		 * Double.valueOf(s) ; } numMap.put("goal",num*score); list2.add(numMap); }
		 * 
		 * 
		 * 
		 * for (int i = 0; i < list.size(); i++) { Map<String,Object> map1 =
		 * list.get(i); map1.put("num",list2.get(i).get("num"));
		 * map1.put("goal",list2.get(i).get("goal")); } result.put("code",0);
		 * 
		 * PageInfo pageInfo = new PageInfo(list,page);
		 */
       // result.put("rows",pageInfo.getList());
       // int total = baseService.selectByBatis(MapperNSEnum.TotalManageMaper, "getTotal", map);
        //result.put("total",pageInfo.getTotal());
        result.put("rows", dataList);
       result.put("total", dataList.size());
        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    @RequestMapping(value = "/queryScoringResult1/{tableName}")
    public void queryScoringResult1(String tableColumn,@PathVariable("tableName") String tableName,String year,String yearUrlId, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();
        Map<String,Object> map = new HashMap<>();
        int page = 1;
        int rows = 10;
        map.put("page",page);
        map.put("rows",rows);
        map.put("create_table_type", "采纳");
        List<Table> tables= aopYearService.getTemporarystorageList(year, map);
        List<Map<String, Object>> dataList=new ArrayList<Map<String,Object>>();
        if(tables.size()>0) {
            Map<String, Object> map2=new HashMap<String, Object>();
            for(Table table:tables) {
                String tableNameString=table.getCreate_table_english_name();
                map2.put("tableName", tableNameString);
                map2.put("yearUrlId",yearUrlId);
                List<Map<String, Object>> list=  aopYearService.getTableResultScoreByLLevelList1(year, map2);
                dataList.addAll(list);
            }
        }
        result.put("rows", dataList);
        result.put("total", dataList.size());
        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 个人计分结果
     */
    @RequestMapping(value = "/queryScoringResultPerson/{tableName}")
    public void queryScoringResultPerson(String tableColumn,@PathVariable("tableName") String tableName,String year,@RequestParam Map<String,Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();
        int page = Integer.valueOf(map.get("page").toString());
        int rows = Integer.valueOf(map.get("rows").toString());
        map.put("page",page);
        map.put("rows",rows);
        map.put("create_table_type", "采纳");
        List<Table> tables= aopYearService.getTemporarystorageList(year, map);
        List<Map<String, Object>> dataList=new ArrayList<Map<String,Object>>();
        //判断权限是否个人，集体
        Subject subject = SecurityUtils.getSubject();
        Session session =subject.getSession();
        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
            try {
                response.sendRedirect("/login");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        User user=   (User)   session.getAttribute("user");
        String jobNum = user.getJobNum();
        if(subject.isPermitted("admin")) {
            jobNum = null;
        }
        if(tables.size()>0) {
            Map<String, Object> map2=new HashMap<String, Object>();
            for(Table table:tables) {
                String tableNameString=table.getCreate_table_english_name();
                map2.put("tableName", tableNameString);
                map2.put("jobNum1",jobNum);
                List<Map<String, Object>> list=  aopYearService.getTableResultScoreByLLevelList(year, map2);
                dataList.addAll(list);
            }
        }

        result.put("rows", dataList);
        result.put("total", dataList.size());
        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 集体计分结果
     */
    @RequestMapping(value = "/queryScoringResultCollege/{tableName}")
    public void queryScoringResultCollege(String tableColumn,@PathVariable("tableName") String tableName,String year,@RequestParam Map<String,Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();
        int page = Integer.valueOf(map.get("page").toString());
        int rows = Integer.valueOf(map.get("rows").toString());
        map.put("page",page);
        map.put("rows",rows);
        map.put("create_table_type", "采纳");
        List<Table> tables= aopYearService.getTemporarystorageList(year, map);
        List<Map<String, Object>> dataList=new ArrayList<Map<String,Object>>();
        //判断权限是否个人，集体
        Subject subject = SecurityUtils.getSubject();
        Session session =subject.getSession();
        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
            try {
                response.sendRedirect("/login");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        User user=   (User)   session.getAttribute("user");
        String college = user.getDepartment();
        Map<String,Object> map0 = new HashMap<>();
        map0.put("grade",college);
        if(subject.isPermitted("admin")) {
            college = null;
            map0.put("grade","全校");
        }
        dataList.add(map0);
        if(tables.size()>0) {
            double sum = 0.0;
            Map<String, Object> map2=new HashMap<String, Object>();
            for(Table table:tables) {
                String tableNameString=table.getCreate_table_english_name();
                map2.put("tableName", tableNameString);
                map2.put("college1",college);
                List<Map<String, Object>> list=  aopYearService.getTableResultScoreByLLevelList(year, map2);
                for(Map map1 : list){
                     sum  += Double.parseDouble(map1.get("getScore")+"");
                }
                dataList.addAll(list);
            }
            map0.put("getScore",sum);
        }

        result.put("rows", dataList);
        result.put("total", dataList.size());
        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
}
