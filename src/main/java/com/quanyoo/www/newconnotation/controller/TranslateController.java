package com.quanyoo.www.newconnotation.controller;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.bytecode.enhance.internal.javassist.PersistentAttributesHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

import static com.mysql.cj.conf.PropertyKey.PATH;

@Controller
@RequestMapping("/explain")
public class TranslateController {

    @Resource
    private IBaseService baseService;

    //跳转页面
    @RequestMapping("tname_explain")
    public ModelAndView tname_explain() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("system_core_management/tname_explain");
        return mvc;
    }

    //跳转页面
    @RequestMapping("field_explain")
    public ModelAndView field_explain() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("system_core_management/field_explain");
        return mvc;
    }


    //查询表名翻译
    @RequestMapping(value = "/getTableNameTranslate")
    public void getTableNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = new HashMap<>();

        int page = Integer.valueOf(map.get("page").toString());
        int rows = Integer.valueOf(map.get("rows").toString());
        PageInfo<Map<String,Object>> list = baseService.selectListByBatis(MapperNSEnum.TranslateMaper, "getTableNameTranslate", map, page, rows);

        result.put("code", 0);
        result.put("rows", list.getList());
        result.put("total", list.getTotal());

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

    }

    //查询字段翻译
    @RequestMapping(value = "/getFieldNameTranslate")
    public void getFieldNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int page = Integer.valueOf(map.get("page").toString());
        int rows = Integer.valueOf(map.get("rows").toString());
        PageInfo<Map<String,Object>> list = baseService.selectListByBatis(MapperNSEnum.TranslateMaper, "getFieldNameTranslate", map, page, rows);

        result.put("code", 0);
        result.put("rows", list.getList());
        result.put("total", list.getTotal());

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");


    }

    //新增表名翻译
    @RequestMapping(value = "/addTableNameTranslate")
    public void addTableNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        long res = baseService.insertByBatis(MapperNSEnum.TranslateMaper, "addTableNameTranslate", map);

        if(res > 0){
            result.put("msg", "新增成功");
        }else{
            result.put("msg", "新增失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    //新增字段翻译
    @RequestMapping(value = "/addFieldNameTranslate")
    public void addFieldNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = new HashMap<>();

        long res = baseService.insertByBatis(MapperNSEnum.TranslateMaper, "addFieldNameTranslate", map);

        if(res > 0){
            result.put("msg", "新增成功");
        }else{
            result.put("msg", "新增失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    //修改表名翻译
    @RequestMapping(value = "/setTableNameTranslate")
    public void setTableNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = baseService.updateByBatis(MapperNSEnum.TranslateMaper, "setTableNameTranslate", map);

        if(res > 0){
            result.put("msg", "修改成功");
        }else{
            result.put("msg", "修改失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    //修改字段翻译
    @RequestMapping(value = "/setFieldNameTranslate")
    public void setFieldNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = baseService.updateByBatis(MapperNSEnum.TranslateMaper, "setFieldNameTranslate", map);


        if(res > 0){
            result.put("msg", "修改成功");
        }else{
            result.put("msg", "修改失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    //导入excel
    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public void importTable(HttpServletRequest request, HttpServletResponse response, @RequestParam MultipartFile file){
        Map<String,Object> result = new HashMap<>();
        Workbook workbook = null;
        try {
            String fileName = file.getOriginalFilename();
            if(fileName.endsWith("xls")){
                workbook = new HSSFWorkbook(file.getInputStream());
            }else if(fileName.endsWith("xlsx")){
                workbook = new XSSFWorkbook(file.getInputStream());
            }
            Sheet sheet = workbook.getSheetAt(0);
            // sheet.getRows()返回该页的总行数
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                // 遍历row中的所有方格
                Row row = sheet.getRow(i);
                Map<String, Object> map = new HashMap<>();
                for (int j = 0; j < row.getLastCellNum(); j++) {
                    // 输出方格中的内容，以空格间隔
                    Cell cell = sheet.getRow(i).getCell(j);

                    String cell_1 = "";// 单元格的值

                    if (null == cell) {
                        cell_1 = "";
                    } else {
                        cell.setCellType(CellType.STRING);
                        cell_1 = cell.getStringCellValue().replaceAll(" ", "");
                    }
                    // 表中的字段
                    Cell cell_2 = sheet.getRow(0).getCell(j);
                    cell_2.setCellType(CellType.STRING);
                    String cell_3 = cell_2.getStringCellValue();
                    cell_3 = cell_3.replaceAll(" ", "");
                    switch (cell_3) {
                        case "表名中文":
                            map.put("create_table_chinese_name",cell_1);
                            break;
                        case "表描述":
                            map.put("create_table_description",cell_1);
                            break;
                        case "表名英文":
                            map.put("create_table_english_name",cell_1);
                            break;
                        case "表编号":
                            map.put("create_table_num",cell_1);
                            break;
                        case "id":
                            map.put("id",cell_1);
                            break;
                        case "表类型":
                            map.put("create_table_type",cell_1);
                            break;
                    }
                    if( null == map || map.isEmpty() ){
                        result.put("msg", "导入失败");
                        return;
                    }
                }
                List<Map<String,Object>> list = null;
                list = baseService.selectListByBatis(MapperNSEnum.TranslateMaper, "checkTableName", map);

                if( null != list && !list.isEmpty()){
                    result.put("msg", "导入失败");
                    return;
                }
                //导入
                int create_table_num = baseService.selectByBatis(MapperNSEnum.TranslateMaper, "selectMaxNum", map);
                map.put("create_table_num", create_table_num+1);
                baseService.insertByBatis(MapperNSEnum.TranslateMaper, "addTableNameTranslate", map);
            }
            result.put("msg", "导入成功");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
        }
    }


}
