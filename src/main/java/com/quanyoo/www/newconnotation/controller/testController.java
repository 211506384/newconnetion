package com.quanyoo.www.newconnotation.controller;

import com.alibaba.fastjson.JSONObject;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.JdbcService;
import com.quanyoo.www.newconnotation.service.TestServiceInterface;
import com.quanyoo.www.newconnotation.sqlDataSource.DataSourceUtil;
import com.quanyoo.www.newconnotation.sqlDataSource.DynamicDataSourceContextHolder;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 现在时
 * */
@Controller
public class testController {
	@Autowired
	private TestServiceInterface testServiceInterface;
	@Autowired
	private IBaseService baseService;
	@Autowired
	private JdbcService jdbcService;
	/**
	 * 跳转页面restful
	 * */
	@RequestMapping("/findView/{tableName}/{tableName1}/{tableName2}")
	public ModelAndView findStudentRestful(HttpServletResponse response, HttpServletRequest request,@PathVariable("tableName") String tableName,@PathVariable("tableName1") String tableName1,@PathVariable("tableName2") String tableName2) {
	
		ModelAndView mv=new ModelAndView();
		Map map=new HashMap<String,Object>();
		map.put("tableName",tableName);
		mv.addObject("tableName",tableName);
		mv.addObject("tableName1",tableName1);
		mv.addObject("tableName2",tableName2);
		List<Table> tableColumn=testServiceInterface.getTableAndColumns(tableName);
		Collections.sort(tableColumn.get(0).getTableColumn());
		map.put("isNeed",1);
        List<Map<String,Object>> tableMainColumn=testServiceInterface.getTableColumnInfo(map);
        List<Map<String,Object>> assessmentIndex=testServiceInterface.assessmentIndexSelect(tableName);
        List<Map<String,Object>> assessmentIndexScore=testServiceInterface.assessmentIndexScoreSelect(tableName);
		mv.addObject("tableColumn",tableColumn.get(0).getTableColumn());
        mv.addObject("tableMainColumn",tableMainColumn);
        mv.addObject("assementIndex",assessmentIndex);
        mv.addObject("assementScore",assessmentIndexScore);
		mv.setViewName("teambuilding_management_module/innovation_team/innovationteam");
		return mv;
	}

	/**
	 * 新增
	 * tableName 表名
	 * map 表字段为key，表值为value
	 * */
	@RequestMapping("/add")
	public void add(@RequestParam Map<String, Object>  params, HttpServletResponse response, HttpServletRequest request) {
        Map<String, Object> result=new HashMap<String,Object>();
        String tableName=(String)params.get("tableName");
        params.remove("tableName");
		User userBean=(User) request.getSession().getAttribute("user");
		params.put("nameof_uploader",userBean.getUserName());
		params.put("uploader_num",userBean.getJobNum());
		LocalDateTime rightNow=LocalDateTime.now();
		params.put("upload_time",rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		int add = testServiceInterface.add(tableName, params);
		if(add>0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
            result.put("msg", "添加成功");
		}else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
            result.put("msg", "添加失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 修改
	 * tableName 表名
	 * id 主键id
	 * map 表字段为key，表值为value
	 * */
	@RequestMapping("/update")
	public void update(@RequestParam Map<String, Object>  params, HttpServletResponse response, HttpServletRequest request) {
		String tableName=(String)params.get("tableName");
        Integer Id=Integer.parseInt((String)params.get("id"));
        Map<String, Object> result=new HashMap<String,Object>();
        params.remove("tableName");
        params.remove("id");
        int update = testServiceInterface.update(tableName, params, Id);
		if(update>0) {
            result = new HashMap<String, Object>();
            result.put("code", 0);
            result.put("msg", "更新成功");
		}else {
            result = new HashMap<String, Object>();
            result.put("code", -1);
            result.put("msg", "更新失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 提交
	 * tableName 表名
	 * id 主键id
	 * map 表字段为key，表值为value
	 * */
	@RequestMapping("/verifyInfomation")
	public void verifyInfomation(@RequestParam Map<String, Object>  params, HttpServletResponse response, HttpServletRequest request) {
		String tableName=(String)params.get("tableName");
		Integer Id=Integer.parseInt((String)params.get("id"));
		Map<String, Object> result=new HashMap<String,Object>();
		User userBean=(User) request.getSession().getAttribute("user");
		params.put("nameof_submitted",userBean.getUserName());
		params.put("submitted_jobno",userBean.getJobNum());
		LocalDateTime rightNow=LocalDateTime.now();
		params.put("submission_time",rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.remove("tableName");
		params.remove("id");
		int update = testServiceInterface.update(tableName, params, Id);
		if(update>0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "提交成功");
		}else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "提交失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 提交审核
	 * tableName 表名
	 * id 主键id
	 * map 表字段为key，表值为value
	 * */
	@RequestMapping("/submitAudit")
	public void submitAudit(@RequestParam Map<String, Object>  params, HttpServletResponse response, HttpServletRequest request) {
		String tableName=(String)params.get("tableName");
		Integer Id=Integer.parseInt((String)params.get("id"));
		Map<String, Object> result=new HashMap<String,Object>();
		User userBean=(User) request.getSession().getAttribute("user");
		params.put("personnel_last",userBean.getUserName());
		params.put("number_last_reviewer",userBean.getJobNum());
		LocalDateTime rightNow=LocalDateTime.now();
		params.put("time_last_trial",rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.remove("tableName");
		params.remove("id");
		int update = testServiceInterface.update(tableName, params, Id);
		if(update>0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "提交审核成功");
		}else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "提交审核失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 提交审核
	 * tableName 表名
	 * id 主键id
	 * map 表字段为key，表值为value
	 * */
	@RequestMapping("/submitSecondAudit")
	public void submitSecondAudit(@RequestParam Map<String, Object>  params, HttpServletResponse response, HttpServletRequest request) {
		String tableName=(String)params.get("tableName");
		Integer Id=Integer.parseInt((String)params.get("id"));
		Map<String, Object> result=new HashMap<String,Object>();
		User userBean=(User) request.getSession().getAttribute("user");
		params.put("personnel_final_second",userBean.getUserName());
		params.put("number_final_second",userBean.getJobNum());
		LocalDateTime rightNow=LocalDateTime.now();
		params.put("time_final_second",rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.remove("tableName");
		params.remove("id");
		int update = testServiceInterface.update(tableName, params, Id);
		if(update>0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "提交审核成功");
		}else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "提交审核失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 退回一审
	 * tableName 表名
	 * id 主键id
	 * map 表字段为key，表值为value
	 * */
	@RequestMapping("/backToModify")
	public void backToModify(@RequestParam Map<String, Object>  params, HttpServletResponse response,HttpServletRequest request) {
		String tableName=(String)params.get("tableName");
		Integer Id=Integer.parseInt((String)params.get("id"));
		Map<String, Object> result=new HashMap<String,Object>();
		params.remove("id");
		params.remove("tableName");
		params.put("returned_receivername",params.get("nameof_submitted"));
		params.put("returned_receivernum",params.get("submitted_jobno"));
		//二审信息清空
		params.put("approval_scoreof_second","");
		params.put("second_approval_level","");
		params.put("remarks_second","");
		//一审信息清空
		params.put("approval_scoreof_first","");
		params.put("first_approval_level","");
		params.put("remarks_first","");
		params.put("time_last_trial","");
		params.put("personnel_last","");
		params.put("number_last_reviewer","");
		//提交信息清空
		params.put("submitted_jobno","");
		params.put("nameof_submitted","");
		params.put("submission_time","");
		User userBean=(User) request.getSession().getAttribute("user");
		params.put("name_originator",userBean.getUserName());
		params.put("number_initiating",userBean.getJobNum());
		LocalDateTime rightNow=LocalDateTime.now();
		params.put("return_time",rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		int auditNumber=Integer.parseInt((String)params.get("auditNumber"));
		if(auditNumber==2){
			params.put("return_remarks","二审退回修改");
		}else{
			params.put("return_remarks","一审退回修改");
		}
		params.remove("auditNumber");
		int update = testServiceInterface.update(tableName, params, Id);
		if(update>0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "退回成功");
		}else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "退回失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 退回一审
	 * tableName 表名
	 * id 主键id
	 * map 表字段为key，表值为value
	 * */
	@RequestMapping("/backToFirstAudit")
	public void backToFirstAudit(@RequestParam Map<String, Object>  params, HttpServletResponse response,HttpServletRequest request) {
		String tableName=(String)params.get("tableName");
		Integer Id=Integer.parseInt((String)params.get("id"));
		Map<String, Object> result=new HashMap<String,Object>();
		params.remove("id");
		params.remove("tableName");
		params.put("returned_receivername",params.get("personnel_last"));
		params.put("returned_receivernum",params.get("number_last_reviewer"));
		//二审信息清空
		params.put("approval_scoreof_second","");
		params.put("second_approval_level","");
		params.put("remarks_second","");
		//一审信息清空
		params.put("time_last_trial","");
		params.put("personnel_last","");
		params.put("number_last_reviewer","");
		User userBean=(User) request.getSession().getAttribute("user");
		params.put("name_originator",userBean.getUserName());
		params.put("number_initiating",userBean.getJobNum());
		LocalDateTime rightNow=LocalDateTime.now();
		params.put("return_time",rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.put("return_remarks","二审退回一审");
		int update = testServiceInterface.update(tableName, params, Id);
		if(update>0) {
			result.put("code", 0);
			result.put("msg", "退回成功");
		}else {
			result.put("code", -1);
			result.put("msg", "退回失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 移入采纳表
	 * tableName 表名
	 * id 主键id
	 * */
	@RequestMapping("/transferInto")
	public void transferInto(int id,String tableName,String transferTableName,HttpServletRequest request,HttpServletResponse response){
		Map<String,Object> params=new HashMap<>();
		Map<String,Object> result=new HashMap<>();
		params.put("id",id);
		List<Map<String,Object>> list=testServiceInterface.select(tableName,params,1,10);
		int add=testServiceInterface.add(transferTableName,list.get(0));
		if (add>0){
			result.put("code", 0);
			result.put("msg", "转入成功");
		}else{
			result.put("code", 0);
			result.put("msg", "转入失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 删除
	 * tableName 表名
	 * id 主键id
	 * */
	@RequestMapping("/delete")
	public void delete(String tableName,@RequestParam(value="map",required=false)  String map,Integer Id, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> jsonObject=(Map)JSONObject.parse(map);
		int delete=	testServiceInterface.delete(tableName, jsonObject, Id);;
	if(delete>0) {
		jsonObject = new HashMap<String, Object>();
		jsonObject.put("code", 0);
	}else {
		jsonObject = new HashMap<String, Object>();
		jsonObject.put("code", -1);
	}
		ResponseUtil.outputJson(response, jsonObject, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 查找
	 * tableName 表名
	 * map 查找条件 表字段为key，表值为value
	 * */
	@RequestMapping("/select")
	public void select(String tableName,@RequestParam Map<String, Object> params, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> jsonObject=(Map)JSONObject.parse(params.get("map").toString());
		List< Map<String, Object>> list = testServiceInterface.select(tableName, jsonObject,Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
		ResponseUtil.outputJson(response, list, "yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * 自动建库建表
	 * dataBaseName 库名
	 */
	@RequestMapping("/createTableData")
	public void createTableData(String dataBaseName, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> dataMap = baseService.selectByBatis(MapperNSEnum.TableMaper, "selectDataBaseName", dataBaseName);
		Map<String, Object> result=new HashMap<String, Object>();
		//判断数据是否已存在
		if(dataMap!=null) {
			result.put("code",-1);
			result.put("msg","该数据库已存在，请重新输入");
			ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
			return;
		}else {
			List<Table> list = baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableAndModel", new HashMap<String, Object>());
			int create=	testServiceInterface.createTableData(dataBaseName, list);
		
			if(create>0) {
				result.put("code",200);
				result.put("msg","创建库表成功");
			}else {
				result.put("code",-2);
				result.put("msg","创建库表失败");
			}
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 本指标数据总览
     * tableName 库名
     */
    @RequestMapping("/getDataStatistics")
    public void getDataStatistics(String tableName,String tableName1,String tableName2, HttpServletResponse response, HttpServletRequest request) {
        List<Map<String, Object>> list = testServiceInterface.getDataStatistics(tableName,tableName1,tableName2);
        ResponseUtil.outputJson(response, list, "yyyy-MM-dd HH:mm:ss");
    }
}
