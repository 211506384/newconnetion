package com.quanyoo.www.newconnotation.controller;

import com.quanyoo.www.newconnotation.exception.ResponseBean;
import com.quanyoo.www.newconnotation.pojo.DbInfo;
import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.LoginService;
import com.quanyoo.www.newconnotation.sqlDataSource.DataSourceUtil;
import com.quanyoo.www.newconnotation.sqlDataSource.DynamicDataSourceContextHolder;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.MenuUtil;
import com.quanyoo.www.newconnotation.util.MyLog;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * 登入层
 * */
@Controller
public class LoginController {

    @Resource
    private IBaseService baseService;
    @Autowired
    private LoginService loginService;
    /**
     * 登入页
     * @param
     */
    @RequestMapping("/login")
    public ModelAndView  loginIndex() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("login");
        return mvc;

    }
    /**
        * @param 
        * @描述 手机登录页
        * @return org.springframework.web.servlet.ModelAndView
        * @author WuRongPeng
        * @date 2020/7/31 14:09
    */
    @RequestMapping("/Phonelogin")
    public ModelAndView  PhoneIndex() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("phonelogin/login");
        return mvc;

    }
    /**
     * 验证用户登入
     * @param
     */
    @MyLog(value = "密码验证通过，进入系统")// 这里添加了AOP的自定义注解
    @RequestMapping("/dologin")
    public void checklogin( String jobNum,String password,HttpServletRequest request,HttpServletResponse response) {
        Map<String,Object> result = new HashMap<>();
        User userBean = loginService.findUserByphone(jobNum);

        if(userBean!=null) {

            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(jobNum,password);
            if (userBean.getPassword().equals(password)) {
            	Subject subject = SecurityUtils.getSubject();
                subject.login(usernamePasswordToken);
                Session session =subject.getSession();
                session.setAttribute("user", userBean);
                session.setAttribute("abc",userBean.getJobNum());
               // String token = JWTUtil.sign(jobNum, password);

            } else {
                result.put("code",1);
                ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
                return;
            }
        }else {

            result.put("code", 2);
            ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
            return;
        }


        result.put("code", 200);
        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    @RequestMapping("/dologinPhone")
    public void dologinPhone( String jobNum,String password,HttpServletRequest request,HttpServletResponse response) {
        Map<String,Object> result = new HashMap<>();
        User userBean = loginService.findUserByphone(jobNum);
        if(userBean!=null) {

            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(jobNum,password);
            if (userBean.getPassword().equals(password)) {
            	Subject subject = SecurityUtils.getSubject();
                subject.login(usernamePasswordToken);
                Session session =subject.getSession();
                session.setAttribute("user", userBean);

            } else {
                result.put("code",1);
                ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
                return;
            }
        }else {

            result.put("code", 2);
            ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
            return ;
        }

        result.put("code", 200);
        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    @RequestMapping(path = "/401")
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseBean unauthorized() {
        return new ResponseBean(401, "Unauthorized", null);
    }
    /*
     * 访问域名时默认登入
     * */
    @RequestMapping("/")
    public ModelAndView  index() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("index");
        Map<String, Object> map =new HashMap<String, Object>();
        List<Map<String, Object>> menuList = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "findParentMenuList", map);
        mvc.addObject("menuList", menuList);

        return mvc;

    }
    /*
     * 首页
     * */
    @RequestMapping("/index")
    public ModelAndView  doindex() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("index");
        Map<String, Object> map =new HashMap<String, Object>();
        List<Map<String, Object>> menuList = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "findParentMenuList", map);
        //获取year集合
       	// 数据源key
		String newDsKey =   "aop";
		// 添加数据源
		DbInfo dbInfo=DataSourceUtil.getDbInfo("aop");
		DataSourceUtil.addDataSourceToDynamic(newDsKey, dbInfo);
		DynamicDataSourceContextHolder.setContextKey(newDsKey);
        List<Map<String, Object>> yearList = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "findYearList", map);
        DynamicDataSourceContextHolder.removeContextKey();
        mvc.addObject("yearList", yearList);
        mvc.addObject("menuList", menuList);
       
        return mvc;

    }
    /**
        * @param
        * @描述 手机端主页
        * @return org.springframework.web.servlet.ModelAndView
        * @author WuRongPeng
        * @date 2020/7/31 14:59
    */
    @RequestMapping("/home")
    public ModelAndView  dohome() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("phonepage/index");
        return mvc;

    }
    /*
     * 二级菜单
     * */
    @RequestMapping("/secondMenu")
    public void  secondMenu(String parentMenuId,HttpServletResponse response,HttpServletRequest request) {
        Map<String, Object> map =new HashMap<String, Object>();
        Subject subject = SecurityUtils.getSubject();
        Session session =subject.getSession();
        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
            map.put("code", 1);
            ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
            return;
        }
    	Map<String, Object> treeMap = MenuUtil.findTree(baseService,0);
    	List<Menu> list= (List<Menu>)treeMap.get("list");
    	  List<Menu>	 secondMenu = new ArrayList<Menu>();
    	for(Menu m:list) {
    		if(parentMenuId.equals(m.getId())) {
    			secondMenu.add(m);
    			break;
    		}
    	}
        map= new HashMap<String, Object>();
        map.put("data", secondMenu);
        ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
    }
    /*
     * 登出
     * */
    @RequestMapping("/logout")
    public void logout(HttpServletResponse response) {
        Subject lvSubject=SecurityUtils.getSubject();
        lvSubject.logout();
        try {
            response.sendRedirect("/login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
