package com.quanyoo.www.newconnotation.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.AopYearJdbcService;
import com.quanyoo.www.newconnotation.service.AopYearService;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.ResponseUtil;

@RequestMapping("phone")
@Controller
public class PhoneController {
	@Autowired
	private AopYearJdbcService aopYearJdbcService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private AopYearService aopYearService;
	 @RequestMapping("/phoneIndex")
	    public ModelAndView  year_index(String year,HttpServletResponse response,HttpServletRequest request) throws Exception {
	        ModelAndView mvc = new ModelAndView();
	        mvc.setViewName("phonelogin/phoneIndex");
	        //年度集合yearlist
	    	List<Map<String, Object>> menuList= null;
	        try {
	        	menuList= aopYearService.getParentList(year,new HashMap<String, Object>());
			} catch (Exception e) {
				throw new Exception("配置文件是否配置正确");
			}
	    	 List<Map<String, Object>> yearList=	aopYearService.getYearList("aop", new HashMap<String, Object>());
	        mvc.addObject("menuList", menuList);

	       mvc.addObject("yearList",yearList);
	        mvc.addObject("yearValue",year);
	        return mvc;

	    }
	 
	 @RequestMapping("/phoneIndex_treeMenu")
	    public ModelAndView  phoneIndex_treeMenu(String year,String parentMenuId,HttpServletResponse response,HttpServletRequest request) throws Exception {
	        ModelAndView mvc = new ModelAndView();
	        mvc.setViewName("phonelogin/phoneIndex_treeMenu");
	        Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	            mvc.setViewName("Phonelogin"); 
	            return mvc;
	        }
	 
	        mvc.addObject("parentMenuId",parentMenuId);
	        mvc.addObject("yearValue",year);
	        return mvc;

	    }
	 
	    /*
	     * 跨年度二级菜单
	     * */
	    @RequestMapping("/year_phonesecondMenu")
	    public void  year_phonesecondMenu(String year,String parentMenuId,HttpServletResponse response,HttpServletRequest request) {
	        Map<String, Object> map =new HashMap<String, Object>();
	        Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	            map.put("code", 1);
	            ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
	            return;
	        }
	        Map<String, Object> treeMap=  aopYearService.getSecondMenu(year,new HashMap<String, Object>());
	    	List<Menu> list= (List<Menu>)treeMap.get("list");
	    	  List<Menu>	 secondMenu = new ArrayList<Menu>();
	    	for(Menu m:list) {
	    		if(parentMenuId.equals(m.getId())) {
	    			secondMenu.add(m);
	    			break;
	    		}
	    	}
	        map= new HashMap<String, Object>();
	        map.put("data", secondMenu);
	        ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
	    }
	    
		@RequestMapping("/findTemporarystorageStatisticsView")
		public ModelAndView findTemporarystorageStatisticsView(String year) {

			ModelAndView mv = new ModelAndView();
			 List<Map<String, Object>> yearList=	aopYearService.getYearList("aop", new HashMap<String, Object>());
		       
			 mv.addObject("yearList",yearList);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("create_table_type", "暂存");
			// 获取表字段
			List<Table> list = aopYearService.getTemporarystorageList(year,map);

			mv.addObject("tableList", list);
			mv.addObject("yearValue", year);
			mv.setViewName("phonelogin/phoneStatisticsAdoptView");
			return mv;
		}
		/**
		 * 统计表数据回显
		 */
		@RequestMapping("/getStaticsTableList/{tableName}")
		public void getStaticsTableList(String year,HttpServletRequest request, HttpServletResponse response,
				@PathVariable("tableName") String tableName) {
			 Subject subject = SecurityUtils.getSubject();
		        Session session =subject.getSession();
		        User user=new User();
		        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){

			        user= (User)  session.getAttribute("user");
		        }
			List<Map<String, Object>> pageInfo = aopYearJdbcService.selectAll(year,jdbcTemplate,tableName, new HashMap<String, Object>(), user.getDepartment(), user.getJobNum());
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("rows",pageInfo );
			result.put("total", pageInfo.size());
			ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
		}
}
