package com.quanyoo.www.newconnotation.controller;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.FieldInformation;
import com.quanyoo.www.newconnotation.model.SystemState;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@RequestMapping("/db")
public class DBController {

    @Resource
    private IBaseService baseService;


    //跳转页面
    @RequestMapping("/system_state")
    public ModelAndView system_state() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("system_core_management/system_state");

        return mvc;
    }

    //跳转页面
    @RequestMapping("/field_core")
    public ModelAndView degree_programManagement() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("system_core_management/field_core");

        return mvc;
    }

    //跳转页面
    @RequestMapping("/core_test")
    public ModelAndView core_test() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("system_core_management/core_test");

        return mvc;
    }
    
    /**
     * 动态建表
     */
//    @RequestMapping(value = "/creatTable")
    public void createTable(){
        Map<String,Object> map = new HashMap<>();
        try {
            //查询主表结构
            List<Map<String,String>> list = baseService.selectListByBatis(MapperNSEnum.DBMaper, "selectMainTable", map);

            //判断表是否存在，若存在，continue，否则创建该表
            for (int i = 0; i < list.size(); i++) {
                Map<String,String> map1 = list.get(i);
                String tableName=  map1.get("table_name");
                map1.remove("id");
                map1.remove("form_no");
                map1.remove("assessment_classification");
                map1.remove("second_level");
                map1.remove("third_level");
                map1.remove("fourth_level");
                map1.remove("five_level");
                map1.remove("table_name");
                Map<String,String> map2 = new HashMap<>();
                map2.put("table_name",tableName);
                for (Map.Entry<String,String> entry: map1.entrySet()
                        ) {
                    if(entry.getValue() == null || "".equals(entry.getValue())){
                        map1.remove(entry.getKey());//min bug
                    }
                }
                Map<String,Map<String,String>> param = new HashMap<>();
                param.put("keys", map1);
                param.put("tableName",map2);
                baseService.updateByBatis(MapperNSEnum.DBMaper, "createNewTable", param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 动态建立单表
     * @param map1 单表字段集合
     */
    public void createOneTable(Map<String,String> map1){
        try {
            String tableName=  map1.get("table_name");
            map1.remove("table_name");
            Map<String,String> map2 = new HashMap<>();
            map2.put("table_name",tableName);
            for (Map.Entry<String,String> entry: map1.entrySet()
                    ) {
                if(entry.getValue() == null || "".equals(entry.getValue())){
                    map1.remove(entry.getKey());//min bug
                }
            }
            Map<String,Map<String,String>> param = new HashMap<>();
            param.put("keys", map1);
            param.put("tableName",map2);
            baseService.updateByBatis(MapperNSEnum.DBMaper, "createNewTable", param);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 增加需要创建的表信息到主表
     */
    @RequestMapping(value = "/addMainTable")
    public void addMainTable(@RequestParam LinkedHashMap<String,Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        //判断表名是否存在，若存在则结束，否则新增
        String existsTableName = baseService.selectByBatis(MapperNSEnum.DBMaper, "existsTableName", map);

        if(existsTableName != null && !"".equals(existsTableName)){
            result.put("msg", "表名重复");
            ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
            return;
        }

        //新增
        long res = baseService.insertByBatis(MapperNSEnum.DBMaper, "addMainTable", map);

        //查询新增记录
        LinkedHashMap<String,String> list = baseService.selectByBatis(MapperNSEnum.DBMaper, "quaryOneMainTable", map);


        if(res > 0){
            //新增成功，建立该表
            Map<String,String> typemap = getFieldType(map);
            Map<String,String> tempmap = groupField(list,typemap);
            createOneTable(tempmap);
            result.put("code", 0);
            result.put("msg", "保存成功");
        }else{
            result.put("code", 1);
            result.put("msg", "保存失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 修改主表字段
     */
    @RequestMapping(value = "/setMainTable")
    public void setMainTable(@RequestParam Map<String,Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        try {

            Map<String,String> bmap = baseService.selectByBatis(MapperNSEnum.DBMaper, "quaryOneMainTable", map);

            int res = baseService.updateByBatis(MapperNSEnum.DBMaper, "setMainTable", map);

            if(res <= 0){
                return;
            }

            Map<String,String> emap = baseService.selectByBatis(MapperNSEnum.DBMaper, "quaryOneMainTable", map);

                List<String> map1list = new ArrayList<>();
                List<String> map2list = new ArrayList<>();
                for (Map.Entry<String,String> entry: bmap.entrySet()
                        ) {
                    map1list.add(entry.getValue());
                }
                for (Map.Entry<String,String> entry: emap.entrySet()
                        ) {
                    map2list.add(entry.getValue());
                }
                List<String> temp1list = new ArrayList<>();
                List<String> temp2list = new ArrayList<>();
                Map<String,List<String>> param1 = new HashMap<>();
                Map<String,List<String>> param2 = new HashMap<>();
                String tableName1=  bmap.get("table_name");
                String tableName2=  emap.get("table_name");
                List<String> name1list = new ArrayList<>();
                name1list.add(tableName1);
                List<String> name2list = new ArrayList<>();
                name2list.add(tableName2);
                for (int i = 0; i <map1list.size() ; i++) {
                    if((map1list.get(i) == null || "".equals(map1list.get(i))) && (map2list.get(i) != null || !"".equals(map2list.get(i)))){
                        temp1list.add(map2list.get(i));
                        param1.put("keys", temp1list);
                        param1.put("tableName",name1list);
                        baseService.updateByBatis(MapperNSEnum.DBMaper, "setField", param1);
                    }
                    if((map1list.get(i) != null || !"".equals(map1list.get(i))) && (map2list.get(i) == null || "".equals(map2list.get(i)))){
                        temp2list.add(map1list.get(i));
                        param2.put("keys", temp2list);
                        param2.put("tableName",name2list);
                        baseService.updateByBatis(MapperNSEnum.DBMaper, "removeField", param2);
                    }
                    if(map1list.get(i) != null || !"".equals(map1list.get(i))){
                        String vaule1 = map1list.get(i);
                        String vaule2 = map2list.get(i);
                        if(vaule1.equals(vaule2)){
                            temp2list.add(map1list.get(i));
                            param2.put("keys", temp2list);
                            param2.put("tableName",name2list);
                            baseService.updateByBatis(MapperNSEnum.DBMaper, "removeField", param2);
                            temp1list.add(map2list.get(i));
                            param1.put("keys", temp1list);
                            param1.put("tableName",name1list);
                            baseService.updateByBatis(MapperNSEnum.DBMaper, "setField", param1);
                        }

                    }
                    temp1list.clear();
                    temp2list.clear();
                }
            result.put("code", 0);
            result.put("msg", "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
        }

    }

    /**
     * 查询主表
     */
    @RequestMapping(value = "/getMainTable")
    public void getMainTable(@RequestParam Map<String,Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        //查询主表结构
//        List<Map<String,String>> list = baseService.selectListByBatis(MapperNSEnum.DBMaper, "selectMainTable", map);
        int page = Integer.valueOf(map.get("page").toString());
        int rows = Integer.valueOf(map.get("rows").toString());
        PageInfo<FieldInformation> list = baseService.selectListByBatis(MapperNSEnum.DBMaper, "selectMainTable",map,page,rows);

        result.put("code", 0);
        result.put("rows", list.getList());
        result.put("total", list.getTotal());

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 查询主表字段
     */
    public List<String> queryField() {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> fieldMap = baseService.selectByBatis(MapperNSEnum.DBMaper, "queryField", map);
        fieldMap.remove("id");
        fieldMap.remove("form_no");
        fieldMap.remove("assessment_classification");
        fieldMap.remove("second_level");
        fieldMap.remove("third_level");
        fieldMap.remove("fourth_level");
        fieldMap.remove("five_level");
        fieldMap.remove("table_name");
        List<String> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : fieldMap.entrySet()
                ) {
            list.add(entry.getValue().toString());
        }
        return list;
    }

    /**
     * 字段类型
     * @param  map 新增功能传入的参数
     */
    public Map<String,String> getFieldType(LinkedHashMap<String,Object> map){

        Map<String,String> typemap = new LinkedHashMap<>();
        for(Map.Entry<String,Object> entry: map.entrySet()){
            String key = entry.getKey();
            if(key.indexOf("type_")>-1){
                String values = (String) entry.getValue();
                typemap.put(key, values);
            }
        }
        for(Map.Entry<String,String> entry: typemap.entrySet()){
            String key = entry.getKey();
            String values = entry.getValue();
            if("".equals(values) || values == null || "默认文字".equals(values)){
                typemap.put(key," "+"varchar(45)");
            }else if("整数".equals(values)){
                typemap.put(key," "+"int(10)");
            }else if("时间".equals(values)){
                typemap.put(key," "+"datetime");
            }else if("货币".equals(values)){
                typemap.put(key," "+"decimal(9,2)");
            }
        }
        return typemap;
    }

    /**
     * 组合 字段+字段类型
     */
    public Map<String,String> groupField(Map<String,String> map, Map<String,String> typemap){
        Map<String,String> tempmap = new HashMap<>();
        tempmap.put("table_name",map.get("table_name"));
        map.remove("table_name");
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        for(Map.Entry<String,String> entry: map.entrySet()){
            list1.add(entry.getValue());
        }
        for(Map.Entry<String,String> entry: typemap.entrySet()){
            list2.add(entry.getValue());
        }
        for (int i = 0; i < list1.size(); i++) {
            if(list1.get(i) != null || !"".equals(list1.get(i))){
                String values = list1.get(i)+list2.get(i);
                tempmap.put("field_"+i,values);
            }
        }
        return tempmap;
    }

    
    /**
   	*核心_系统状态 -2020.08.06 cc
   **/
  @RequestMapping(value = "/getSystemStateList")
  public void getSystemStateList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
     
      
      PageInfo<List<SystemState>> info = baseService.selectListByBatis(MapperNSEnum.DBMaper, "findSystemState",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
    
      	Map<String, Object> resuleMap=new HashMap<String, Object>();
      	resuleMap.put("rows", info.getList());
      	resuleMap.put("total", info.getTotal());
      ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
  }
  
/**
 * 核心_系统状态新增 -2020.08.06 cc
 * 
 * */
@RequestMapping(value = "/addSystemStateOne")
public void addSystemStateOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
    //System.out.println(params);
    System.out.println(params);
    Map<String, Object> resuleMap=new HashMap<String, Object>();
    try {
    	//新增前先查找是否存在菜单
    	List<SystemState> list = baseService.selectListByBatis(MapperNSEnum.DBMaper, "quaryOneSystemState", params);
       
    	if (list.size()>0) {
    		 resuleMap.put("code", -3);
    		 resuleMap.put("errorMsg","数据已存在");
		}else {
		  	long insert =  baseService.insertByBatis(MapperNSEnum.DBMaper, "addSystemState", params);
        	
            if(insert>0) {
         	   resuleMap.put("code", 200);
         	  resuleMap.put("errorMsg","新增成功");
            } else {
         	   resuleMap.put("code", -1);
         	  resuleMap.put("errorMsg","新增失败");
            }
		}
  
	} catch (Exception e) {
		e.printStackTrace();
		resuleMap.put("code", -2);
		resuleMap.put("errorMsg","系统错误");
	}

    ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");

}

/**
 * 核心_系统状态修改 -2020.08.06 cc
 * 
 * */
@RequestMapping(value = "/updateSystemStateOne")
public void updateSystemStateOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

    	Map<String, Object> resuleMap=new HashMap<String, Object>();
    	try {
    		//新增前先查找是否存在菜单
        	
        		int update =	baseService.updateByBatis(MapperNSEnum.DBMaper, "updateSystemState", params);
           	 
                if(update>0) {
             	   resuleMap.put("code", 200);
             	  resuleMap.put("errorMsg","修改成功");
                } else {
             	   resuleMap.put("code", -1);
             	  resuleMap.put("errorMsg","修改失败");
                }
        	      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
    ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
}

/**
 * 核心_系统状态删除     -2020.08.06 cc* 
 * */
@RequestMapping(value = "/deleteSystemStateOne")
public void deleteSystemStateOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

    	Map<String, Object> resuleMap=new HashMap<String, Object>();
    	try {
    	int delete =	baseService.deleteByBatis(MapperNSEnum.DBMaper, "removeSystemState", params);
    	 
        if(delete>0) {
     	   resuleMap.put("code", 200);
      	  resuleMap.put("errorMsg","删除成功");
        } else {
     	   resuleMap.put("code", -1);
     	  resuleMap.put("errorMsg","删除失败");
        }
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
    ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
}


/**
	*核心_试验 -2020.08.07 cc
**/
	@RequestMapping(value = "/getCoreTestList")
	public void getCoreTestList(@RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {

		PageInfo<List<SystemState>> info = baseService.selectListByBatis(MapperNSEnum.DBMaper, "findCoreTest", params,
		Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		resuleMap.put("rows", info.getList());
		resuleMap.put("total", info.getTotal());
		ResponseUtil.outputJson(response, resuleMap, "yyyy-MM-dd HH:mm:ss");
	}

/**
* 核心_试验新增 -2020.08.07 cc
* 
* */
	@RequestMapping(value = "/addCoreTestOne")
	public void addCoreTestOne(@RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {

		System.out.println(params);
		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在菜单
			List<SystemState> list = baseService.selectListByBatis(MapperNSEnum.DBMaper, "quaryOneCoreTest", params);

			if (list.size() > 0) {
				resuleMap.put("code", -3);
				resuleMap.put("errorMsg", "数据已存在");
			} else {
				long insert = baseService.insertByBatis(MapperNSEnum.DBMaper, "addCoreTest", params);

				if (insert > 0) {
					resuleMap.put("code", 200);
					resuleMap.put("errorMsg", "新增成功");
				} else {
					resuleMap.put("code", -1);
					resuleMap.put("errorMsg", "新增失败");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}

		ResponseUtil.outputJson(response, resuleMap, "yyyy-MM-dd HH:mm:ss");

	}

/**
* 核心_试验修改 -2020.08.07 cc
* 
* */
	@RequestMapping(value = "/updateCoreTestOne")
	public void updateCoreTestOne(@RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			// 新增前先查找是否存在菜单

			int update = baseService.updateByBatis(MapperNSEnum.DBMaper, "updateCoreTest", params);

			if (update > 0) {
				resuleMap.put("code", 200);
				resuleMap.put("errorMsg", "修改成功");
			} else {
				resuleMap.put("code", -1);
				resuleMap.put("errorMsg", "修改失败");
			}

		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		ResponseUtil.outputJson(response, resuleMap, "yyyy-MM-dd HH:mm:ss");
	}

/**
* 核心_试验删除     -2020.08.07 cc* 
* */
	@RequestMapping(value = "/deleteCoreTestOne")
	public void deleteCoreTestOne(@RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {

		Map<String, Object> resuleMap = new HashMap<String, Object>();
		try {
			int delete = baseService.deleteByBatis(MapperNSEnum.DBMaper, "removeCoreTest", params);

			if (delete > 0) {
				resuleMap.put("code", 200);
				resuleMap.put("errorMsg", "删除成功");
			} else {
				resuleMap.put("code", -1);
				resuleMap.put("errorMsg", "删除失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg", "系统错误");
		}
		ResponseUtil.outputJson(response, resuleMap, "yyyy-MM-dd HH:mm:ss");
	}

}
