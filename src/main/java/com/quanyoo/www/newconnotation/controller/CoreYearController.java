package com.quanyoo.www.newconnotation.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.AssessmentIndex;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.AopYearJdbcService;
import com.quanyoo.www.newconnotation.service.AopYearService;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.JDBCTemplate;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.MenuUtil;
import com.quanyoo.www.newconnotation.util.ResponseUtil;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * core的controller
 */
@RequestMapping("/core")
@Controller
public class CoreYearController {
	@Autowired
	private AopYearJdbcService aopYearJdbcService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private AopYearService aopYearService;
	@Autowired
	private IBaseService baseService;
	private String year="aop";
	/**
	 * 不需要上传者该参数
	 * */
	@RequestMapping("/addOther")
	public void addOther(@RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		String tableName = (String) params.get("tableName");
		params.remove("tableName");

		params.remove("year");
		
		
		int add = aopYearJdbcService.add(year, jdbcTemplate, tableName, params);
		if (add > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "添加成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "添加失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 修改 tableName 表名 id 主键id map 表字段为key，表值为value
	 */
	@RequestMapping("/update")
	public void update(@RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		String tableName = (String) params.get("tableName");
		Integer Id = Integer.parseInt((String) params.get("id"));
		Map<String, Object> result = new HashMap<String, Object>();
		params.remove("tableName");
		params.remove("id");

		params.remove("year");
		int update = aopYearJdbcService.update(year, jdbcTemplate, tableName, params, Id);
		if (update > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "更新成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "更新失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 删除 tableName 表名 id 主键id
	 */
	@RequestMapping("/delete")
	public void delete(String tableName, @RequestParam(value = "map", required = false) String map,
			Integer Id, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> jsonObject = (Map) JSONObject.parse(map);
		int delete = aopYearJdbcService.delete(year, jdbcTemplate, tableName, jsonObject, Id);
		;
		if (delete > 0) {
			jsonObject = new HashMap<String, Object>();
			jsonObject.put("code", 0);
		} else {
			jsonObject = new HashMap<String, Object>();
			jsonObject.put("code", -1);
		}
		ResponseUtil.outputJson(response, jsonObject, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 查找 tableName 表名 map 查找条件 表字段为key，表值为value
	 * 
	 */
	@RequestMapping("/select")
	public void select(String tableName, @RequestParam Map<String, Object> params,
			HttpServletResponse response, HttpServletRequest request) {
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        	  try {
	                  response.sendRedirect("/login");
	              } catch (IOException e) {
	                  e.printStackTrace();
	              }
	            return;
	        }
	        String college=null;
	        String job_number=null;
	  User user=   (User)   session.getAttribute("user");
	        if(subject.isPermitted("admin")) {
	        	
	        }else if(subject.isPermitted("集体身份")) {
	        	college=user.getDepartment();
	        }else {
	        	job_number= user.getJobNum();
	        }
		params.remove("year");
		Map<String, Object> jsonObject = (Map) JSONObject.parse(params.get("map").toString());
		List<Map<String, Object>> list = aopYearJdbcService.select(year, jdbcTemplate, tableName, jsonObject,
				Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()),college,job_number);
		ResponseUtil.outputJson(response, list, "yyyy-MM-dd HH:mm:ss");
	}



	/**
	 * 表数据
	 */
	@RequestMapping("/getTableList")
	public void getTableList(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, Object> params) {
		Map<String, Object> p = (Map) JSONObject.parse(params.get("map").toString());
		//判断权限是否个人，集体
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        	  try {
	                  response.sendRedirect("/login");
	              } catch (IOException e) {
	                  e.printStackTrace();
	              }
	            return;
	        }
	        String college=null;
	        String job_number=null;
	  User user=   (User)   session.getAttribute("user");
	        if(subject.isPermitted("admin")) {
	        	
	        }else if(subject.isPermitted("集体身份")) {
	        	college=user.getDepartment();
	        }else {
	        	job_number= user.getJobNum();
	        }
		params.remove("year");
		PageInfo<Map<String, Object>> pageInfo = aopYearJdbcService.selectListBypage(year, jdbcTemplate,
				params.get("tableName").toString(), p, Integer.valueOf(params.get("page").toString()),
				Integer.valueOf(params.get("rows").toString()),college,job_number);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows", pageInfo.getList());
		result.put("total", pageInfo.getTotal());

		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}
	/**
	 * 跳转页面通用表格
	 */
	@RequestMapping("/findSystemView/{tableName}")
	public ModelAndView findStudentRestful(@PathVariable("tableName") String tableName) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tableName", tableName);
		// 获取表字段
		List<Table> tableColumn = aopYearService.getTableAndColumns(year,map);

		mv.addObject("tableName", tableName);
		mv.addObject("create_table_chinese_name", tableColumn.get(0).getCreate_table_chinese_name());
		Collections.sort(tableColumn.get(0).getTableColumn()); 
		mv.addObject("tableColumn", tableColumn.get(0).getTableColumn());
		mv.addObject("yearValue",year);
		mv.setViewName("coreYearModelView/systemModelView");
		return mv;
	}
	
	//跳转用户页
    @RequestMapping("/userManagement")
    public ModelAndView userManagement(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("coreYearModelView/usermanagementModelView");
        List<DepartmentInformation> departmentList = aopYearService.getDepartment(year,new HashMap<String, Object>());
      mvc.addObject("departmentList",departmentList);
      mvc.addObject("yearValue",year);
        return mvc;
    }
    //获取用户表
    @RequestMapping("/getUserTable")
    public void getUserTable(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) {
        Map<String,Object> result=aopYearService.getUserList(year,params,Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response,result,"yyyy-MM-dd");
    }
    //跳转页面用户部门
    @RequestMapping("/userDepartment/{tableName}")
    public ModelAndView userDepartment(@PathVariable("tableName") String tableName) {
        ModelAndView mvc = new ModelAndView();
        mvc.addObject("yearValue",year);
        Map<String, Object> map = new HashMap<String, Object>();
		map.put("tableName", tableName);
		// 获取表字段
		List<Table> tableColumn = aopYearService.getTableAndColumns(year,map);

		mvc.addObject("tableName", tableName);
		mvc.addObject("create_table_chinese_name", tableColumn.get(0).getCreate_table_chinese_name());
		Collections.sort(tableColumn.get(0).getTableColumn()); 
		mvc.addObject("tableColumn", tableColumn.get(0).getTableColumn());
        mvc.setViewName("coreYearModelView/deptmanagementModelView");
        return mvc;
    }
	/**
	 * 新增用户
	 * */
	@RequestMapping("/addUser")
	public void addUser(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.addUser(year, params);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "保存成功");
		}else {

			result.put("code", -1);
			result.put("msg", "保存失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};	
	/**
	 * 跟新用户
	 * */
	@RequestMapping("/updateUser")
	public void updateUser(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.updateUser(year, params);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "保存成功");
		}else {

			result.put("code", -1);
			result.put("msg", "保存失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};	
	/**
	 * 删除用户
	 * */
	@RequestMapping("/deleteUser")
	public void deleteUser(Integer id ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.deleteUser(year,id);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "删除成功");
		}else {

			result.put("code", -1);
			result.put("msg", "删除失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};
	
	/**
	 * 重置用户密码
	 * */
	@RequestMapping("/resetPassword")
	public void resetPassword(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		params.put("pwd",123456);
		
		int  update = aopYearService.resetPassword(year,params);
		if(update>0) {

			result.put("code", 0);
			result.put("msg", "重置成功");
		}else {

			result.put("code", -1);
			result.put("msg", "重置失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};
	/**
	    * @param request
		* @param response
		* @param params
	    * @描述 获取用户部门列表
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:16
	*/
	@RequestMapping("/getDepartmentTable")
	public void getDepartmentTable(HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) {
		Map<String,Object> result=aopYearService.getDepartmentTable(year,params,Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
		ResponseUtil.outputJson(response,result,"yyyy-MM-dd");
	}
	
	/**
	    * @param params
		* @param request
		* @param response
	    * @描述 保存部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:31
	*/
	@RequestMapping("/saveDepartment")
	public void addDepartment(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.saveDepartment(year,params);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "保存成功");
		}else if (add==-1){
			result.put("code", -1);
			result.put("msg", "保存失败");
		}else {
			result.put("code", -2);
			result.put("msg", "该部门已存在！");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	/**
	    * @param params
		* @param request
		* @param response
	    * @描述 修改部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 14:00
	*/
	@RequestMapping("/updateDepartment")
	public void updateDepartment(@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.updateDepartment(year,params);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "保存成功");
		}else if (add==-1){

			result.put("code", -1);
			result.put("msg", "保存失败");
		}else {
			result.put("code", -2);
			result.put("msg", "该部门已存在！");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	/**
	    * @param id
		* @param request
		* @param response
	    * @描述 删除部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:32
	*/
	@RequestMapping("/deleteDepartment")
	public void deleteDepartment(Integer id ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.deleteDepartment(year,id);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "删除成功");
		}else {
			result.put("code", -1);
			result.put("msg", "删除失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

	};
	
	//	添加字段
	@RequestMapping("/addColumn")
	public void addColumn(String tableName,@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=	aopYearService.addColumn(year,tableName, params);
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	
	///////////
	
	 /**
     * 菜单列表页
     * */
    @RequestMapping(value = "/menu_list",method = RequestMethod.GET)
    public ModelAndView  menu_list(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("coreYearModelView/menuListModelView");
       	//菜单下拉
       List<Menu> list= aopYearService.getMenusListSelect(year, params);
       mvc.addObject("menuList",list);
       mvc.addObject("yearValue",year);
       return mvc;
    }
    /**
     * 角色管理页
     * */
    @RequestMapping(value = "/role_management",method = RequestMethod.GET)
    public ModelAndView  role_management(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("coreYearModelView/roleListModelView");
       mvc.addObject("yearValue",year);
       return mvc;
    }
    /**
     * 权限管理页
     * */
    @RequestMapping(value = "/permission_management",method = RequestMethod.GET)
    public ModelAndView  permission_management(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("coreYearModelView/permissionListModelView");
       	//权限下拉
   	List<Map<String, Object>> list = aopYearService.getPermissionSelect(year,params);
       mvc.addObject("permissionParent",list);
       mvc.addObject("yearValue",year);
       return mvc;
    }
   
    
    /**
     * 菜单列表
     **/
    @RequestMapping(value = "/getMenuList")
    public void getMenuList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
     
        	Map<String, Object> resuleMap=aopYearService.getMenusListPage(year, params,  Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 菜单新增
     * 
     * */
    @RequestMapping(value = "/addMenuOne")
    public void addMenuOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        Map<String, Object> resuleMap=aopYearService.addMenu(year, params);
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 菜单修改
     * 
     * */
    @RequestMapping(value = "/updateMenuOne")
    public void updateMenuOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.updateMenu(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 菜单删除     * 
     * */
    @RequestMapping(value = "/deleteMenuOne")
    public void deleteMenuOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.deleteMenu(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色权限分配
     **/
    @RequestMapping(value = "/saveRolePermissionList")
    public void saveRolePermissionList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
       Integer roleId =  Integer.valueOf(params.get("roleId").toString());
      
       List<Integer> integerList = JSONArray.parseArray(params.get("permissionIdList").toString(), Integer.class);
     Map<String, Object> map=   aopYearService.saveRolePermissionList(year,roleId, integerList);
      
        ResponseUtil.outputJson(response, map , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 角色列表
     **/
    @RequestMapping(value = "/getRoleList")
    public void getRoleList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
      
        	Map<String, Object> resuleMap=aopYearService.getRoleListPage(year, params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色新增
     * 
     * */
    @RequestMapping(value = "/addRoleOne")
    public void addRoleOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        Map<String, Object> resuleMap=aopYearService.addRole(year, params);
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色修改
     * 
     * */
    @RequestMapping(value = "/updateRoleOne")
    public void updateRoleOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.updateRole(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 角色删除     * 
     * */
    @RequestMapping(value = "/deleteRoleOne")
    public void deleteRoleOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.deleteRole(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 获取角色下拉
     * */
    @RequestMapping(value = "/getRoleListAll")
    public void getRoleListAll(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
      
    	Map<String, Object> resuleMap=aopYearService.getRoleListAllSelect(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 用户角色新增
     * 
     * */
    @RequestMapping(value = "/saveUserRoleOne")
    public void saveUserRoleOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        Map<String, Object> resuleMap=aopYearService.saveUserRoleOne(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 获取权限下拉
     * */
    @RequestMapping(value = "/getPermissionAll")
    public void getPermissionAll(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);

    	Map<String, Object> resuleMap=aopYearService.getPermissionAllSelect(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 根据角色Id获取权限
     * */
    @RequestMapping(value = "/getPermissionAllByroleId")
    public void getPermissionAllByroleId(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
    	
    	JSONArray arraylist = aopYearService.getPermissionAllByroleId(year, params);
        ResponseUtil.outputJson(response, arraylist , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限列表
     **/
    @RequestMapping(value = "/getPermissionList")
    public void getPermissionList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
    
        	Map<String, Object> resuleMap=aopYearService.getPermissionList(year, params, Integer.valueOf(params.get("page").toString()),
    				Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限新增
     * 
     * */
    @RequestMapping(value = "/addPermissionOne")
    public void addPermissionOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
     
        Map<String, Object> resuleMap=aopYearService.addPermissionOne(year, params);
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限修改
     * 
     * */
    @RequestMapping(value = "/updatePermissionOne")
    public void updatePermissionOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.updatePermissionOne(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 权限删除     * 
     * */
    @RequestMapping(value = "/deletePermissionOne")
    public void deletePermissionOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.deletePermissionOne(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    ///////
    //查询表名翻译
    @RequestMapping(value = "/getTableNameTranslate")
    public void getTableNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = aopYearService.getTableNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

    }

    //查询字段翻译
    @RequestMapping(value = "/getFieldNameTranslate")
    public void getFieldNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result =aopYearService.getFieldNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");


    }

    //新增表名翻译
    @RequestMapping(value = "/addTableNameTranslate")
    public void addTableNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.addTableNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "新增成功");
        }else{
            result.put("msg", "新增失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    //新增字段翻译
    @RequestMapping(value = "/addFieldNameTranslate")
    public void addFieldNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.addFieldNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "新增成功");
        }else{
            result.put("msg", "新增失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    //修改表名翻译
    @RequestMapping(value = "/setTableNameTranslate")
    public void setTableNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.setTableNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "修改成功");
        }else{
            result.put("msg", "修改失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    //修改字段翻译
    @RequestMapping(value = "/setFieldNameTranslate")
    public void setFieldNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.setFieldNameTranslate(year, map);


        if(res > 0){
            result.put("msg", "修改成功");
        }else{
            result.put("msg", "修改失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    
    
    //删除模板表名翻译
    @RequestMapping(value = "/deleteTableNameTranslateModel")
    public void deleteTableNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.deleteTableNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "删除成功");
        }else{
            result.put("msg", "删除失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    //删除模板字段翻译
    @RequestMapping(value = "/deleteFieldNameTranslateModel")
    public void deleteFieldNameTranslate(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.deleteFieldNameTranslate(year, map);


        if(res > 0){
            result.put("msg", "删除成功");
        }else{
            result.put("msg", "删除失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    //查询模板表名翻译
    @RequestMapping(value = "/getTableNameTranslateModel")
    public void getTableNameTranslateModel(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = aopYearService.getNextTableNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

    }

    //查询模板字段翻译
    @RequestMapping(value = "/getFieldNameTranslateModel")
    public void getFieldNameTranslateModel(@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result =aopYearService.getNextFieldNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");


    }
    
    //跳转翻译table页面
    @RequestMapping("tname_explain")
    public ModelAndView tname_explain() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("coreYearModelView/tnameExplainView");
        mvc.addObject("yearValue",year);
        return mvc;
    }

    //跳转翻译field页面
    @RequestMapping("field_explain")
    public ModelAndView field_explain() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("coreYearModelView/fieldExplainView");
        mvc.addObject("yearValue",year);
        return mvc;
    }
    //跳转翻译tableModel页面
    @RequestMapping("tname_explain_model")
    public ModelAndView tname_explain_model() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("coreYearModelView/tnameExplainModelView");
        mvc.addObject("yearValue",year);
        return mvc;
    }

    //跳转翻译fieldModel页面
    @RequestMapping("field_explain_model")
    public ModelAndView field_explain_model() {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("coreYearModelView/fieldExplainModelView");
        mvc.addObject("yearValue",year);
        return mvc;
    }
    
    
    /**
     * 年度列表，
     **/
    @RequestMapping(value = "/getYearTable")
    public void getYearTable(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.getYearTableList("aop", params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 年度新增，下一年度初始化
     * 
     * */
    @RequestMapping(value = "/addYearOne")
    public void addYearOne(String thisYear,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        
        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        List<Map<String, Object>> yearlist=	(List<Map<String, Object>>)aopYearService.getYearTableList("aop", params,1,10).get("rows");
          if (yearlist.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","该年度已存在");
			}else {
				
			 int insert= 	aopYearService.addYear("aop", params);	        	
	            if(insert>0) {
	            	String dataBaseName="newconnotation_"+params.get("year");
	            	
	        		Map<String, Object> dataMap = baseService.selectByBatis(MapperNSEnum.TableMaper, "selectDataBaseName", dataBaseName);
	        		Map<String, Object> result=new HashMap<String, Object>();
	        		//判断数据是否已存在
	        		if(dataMap!=null) {
	        			result.put("code",-1);
	        			result.put("msg","该数据库已存在，请重新输入");
	        			ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	        			return;
	        		}else {
	        			//建表模板
	        			List<Table> list =aopYearService.getTableAndColumnsModel(thisYear,new HashMap<String, Object>());
	        			int create=	aopYearJdbcService.createTableData(thisYear, jdbcTemplate, dataBaseName, list);
	        		
	        			if(create>0) {
	        				result.put("code",200);
	        				result.put("msg","创建库表成功");
	        			}else {
	        				result.put("code",-2);
	        				result.put("msg","创建库表失败");
	        			}
	        		}
	            	resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功并成功创建库表");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}

}
