package com.quanyoo.www.newconnotation.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.DepartmentInformation;
import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.AopYearJdbcService;
import com.quanyoo.www.newconnotation.service.AopYearService;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.MenuUtil;
import com.quanyoo.www.newconnotation.util.ResponseUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 带year年份的controller
 */
@RequestMapping("/PastYear")
@Controller
public class PastYearController {
	@Autowired
	private AopYearJdbcService aopYearJdbcService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private AopYearService aopYearService;
	@Autowired
	private IBaseService baseService;
    /*
     * //跨年度首页
     * */
    @RequestMapping("/year_index")
    public ModelAndView  year_index(String year,HttpServletResponse response,HttpServletRequest request) throws Exception {
        ModelAndView mvc = new ModelAndView();
        Subject subject = SecurityUtils.getSubject();
        Session session =subject.getSession();
        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
        	  try {
                  response.sendRedirect("/login");
              } catch (IOException e) {
                  e.printStackTrace();
              }
        }
        mvc.setViewName("year_index");
        //年度集合yearlist
    	List<Map<String, Object>> menuList= null;
        try {
        	menuList= aopYearService.getParentList(year,new HashMap<String, Object>());
		} catch (Exception e) {
			throw new Exception("配置文件是否配置正确");
		}
    	 List<Map<String, Object>> yearList=	aopYearService.getYearList("aop", new HashMap<String, Object>());
        mvc.addObject("menuList", menuList);

       mvc.addObject("yearList",yearList);
        mvc.addObject("yearValue",year);
        return mvc;

    }
    
    /*
     * 跨年度二级菜单
     * */
    @RequestMapping("/year_secondMenu")
    public void  year_secondMenu(String year,String parentMenuId,HttpServletResponse response,HttpServletRequest request) {
        Map<String, Object> map =new HashMap<String, Object>();
        Subject subject = SecurityUtils.getSubject();
        Session session =subject.getSession();
        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
            map.put("code", 1);
            ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
            return;
        }
        Map<String, Object> treeMap=  aopYearService.getSecondMenu(year,new HashMap<String, Object>());
    	List<Menu> list= (List<Menu>)treeMap.get("list");
    	  List<Menu>	 secondMenu = new ArrayList<Menu>();
    	for(Menu m:list) {
    		if(parentMenuId.equals(m.getId())) {
    			secondMenu.add(m);
    			break;
    		}
    	}
        map= new HashMap<String, Object>();
        map.put("data", secondMenu);
        ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
    }
    
    /*
     * 跨年度初始化参数配置二级菜单
     * */
    @RequestMapping("/init_year_secondMenu")
    public void  init_year_secondMenu(String year,String parentMenuId,HttpServletResponse response,HttpServletRequest request) {
        Map<String, Object> map =new HashMap<String, Object>();
        Subject subject = SecurityUtils.getSubject();
        Session session =subject.getSession();
        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
            map.put("code", 1);
            ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
            return;
        }
        	Map<String, Object> treeMap=  MenuUtil.findTreeInit( parentMenuId);
        
    	List<Menu> list= (List<Menu>)treeMap.get("list");
  
        map= new HashMap<String, Object>();
        map.put("data", list);
        ResponseUtil.outputJson(response,map , "yyyy-MM-dd HH:mm:ss");
    }
	/**
	 * 跳转页面考核记录
	 */
	@RequestMapping("/findView/{tableName}/{tableName1}/{tableName2}")
	public ModelAndView findStudentRestful(String year, HttpServletResponse response, HttpServletRequest request,
			@PathVariable("tableName") String tableName, @PathVariable("tableName1") String tableName1,
			@PathVariable("tableName2") String tableName2) {
	    Subject subject = SecurityUtils.getSubject();
        Session session =subject.getSession();
        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
        	  try {
                  response.sendRedirect("/login");
              } catch (IOException e) {
                  e.printStackTrace();
              }
        }
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tableName", tableName);
		mv.addObject("tableName", tableName);
		mv.addObject("tableName1", tableName1);
		mv.addObject("tableName2", tableName2);
		List<Table> tableColumn = aopYearService.getTableAndColumns(year, map);
		Collections.sort(tableColumn.get(0).getTableColumn());
		map.put("isNeed", 1);
		List<Map<String, Object>> tableMainColumn = aopYearService.getTableColumnInfo(year, map);
		List<Map<String, Object>> assessmentIndex = aopYearService.assessmentIndexSelect(year, tableName);
		List<Map<String, Object>> assessmentIndexScore = aopYearService.assessmentIndexScoreSelect(year, tableName);
		mv.addObject("tableColumn", tableColumn.get(0).getTableColumn());
		mv.addObject("tableMainColumn", tableMainColumn);
		mv.addObject("assementIndex", assessmentIndex);
		mv.addObject("assementScore", assessmentIndexScore);
		mv.addObject("yearValue", year);
		mv.setViewName("pastYearModelView/assessmentIndexModel");
		return mv;
	}
	/**
	 * 提交 tableName 表名 id 主键id map 表字段为key，表值为value
	 */
	@RequestMapping("/verifyInfomation")
	public void verifyInfomation(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		String tableName = (String) params.get("tableName");
		Integer Id = Integer.parseInt((String) params.get("id"));
		Map<String, Object> result = new HashMap<String, Object>();
		User userBean = (User) request.getSession().getAttribute("user");
		params.put("nameof_submitted", userBean.getUserName());
		params.put("submitted_jobno", userBean.getJobNum());
		LocalDateTime rightNow = LocalDateTime.now();
		params.put("submission_time", rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.remove("tableName");
		params.remove("id");
		params.remove("year");
		int update = aopYearJdbcService.update(year, jdbcTemplate, tableName, params, Id);
		if (update > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "提交成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "提交失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 提交审核 tableName 表名 id 主键id map 表字段为key，表值为value
	 */
	@RequestMapping("/submitAudit")
	public void submitAudit(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		String tableName = (String) params.get("tableName");
		Integer Id = Integer.parseInt((String) params.get("id"));
		Map<String, Object> result = new HashMap<String, Object>();
		User userBean = (User) request.getSession().getAttribute("user");
		params.put("personnel_last", userBean.getUserName());
		params.put("number_last_reviewer", userBean.getJobNum());
		LocalDateTime rightNow = LocalDateTime.now();
		params.put("time_last_trial", rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.remove("tableName");
		params.remove("id");

		params.remove("year");
		int update = aopYearJdbcService.update(year, jdbcTemplate, tableName, params, Id);
		if (update > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "提交审核成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "提交审核失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 提交审核 tableName 表名 id 主键id map 表字段为key，表值为value
	 */
	@RequestMapping("/submitSecondAudit")
	public void submitSecondAudit(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		String tableName = (String) params.get("tableName");
		Integer Id = Integer.parseInt((String) params.get("id"));
		Map<String, Object> result = new HashMap<String, Object>();
		User userBean = (User) request.getSession().getAttribute("user");
		params.put("personnel_final_second", userBean.getUserName());
		params.put("number_final_second", userBean.getJobNum());
		LocalDateTime rightNow = LocalDateTime.now();
		params.put("time_final_second", rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.remove("tableName");
		params.remove("id");

		params.remove("year");
		int update = aopYearJdbcService.update(year, jdbcTemplate, tableName, params, Id);
		if (update > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "提交审核成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "提交审核失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 退回一审 tableName 表名 id 主键id map 表字段为key，表值为value
	 */
	@RequestMapping("/backToModify")
	public void backToModify(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		String tableName = (String) params.get("tableName");

		params.remove("year");
		Integer Id = Integer.parseInt((String) params.get("id"));
		Map<String, Object> result = new HashMap<String, Object>();
		params.remove("id");
		params.remove("tableName");
		params.put("returned_receivername", params.get("nameof_submitted"));
		params.put("returned_receivernum", params.get("submitted_jobno"));
		// 二审信息清空
		params.put("approval_scoreof_second", "");
		params.put("second_approval_level", "");
		params.put("remarks_second", "");
		// 一审信息清空
		params.put("approval_scoreof_first", "");
		params.put("first_approval_level", "");
		params.put("remarks_first", "");
		params.put("time_last_trial", "");
		params.put("personnel_last", "");
		params.put("number_last_reviewer", "");
		// 提交信息清空
		params.put("submitted_jobno", "");
		params.put("nameof_submitted", "");
		params.put("submission_time", "");
		User userBean = (User) request.getSession().getAttribute("user");
		params.put("name_originator", userBean.getUserName());
		params.put("number_initiating", userBean.getJobNum());
		LocalDateTime rightNow = LocalDateTime.now();
		params.put("return_time", rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		int auditNumber = Integer.parseInt((String) params.get("auditNumber"));
		if (auditNumber == 2) {
			params.put("return_remarks", "二审退回修改");
		} else {
			params.put("return_remarks", "一审退回修改");
		}
		params.remove("auditNumber");
		int update = aopYearJdbcService.update(year, jdbcTemplate, tableName, params, Id);
		if (update > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "退回成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "退回失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 退回一审 tableName 表名 id 主键id map 表字段为key，表值为value
	 */
	@RequestMapping("/backToFirstAudit")
	public void backToFirstAudit(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {

		params.remove("year");
		String tableName = (String) params.get("tableName");
		Integer Id = Integer.parseInt((String) params.get("id"));
		Map<String, Object> result = new HashMap<String, Object>();
		params.remove("id");
		params.remove("tableName");
		params.put("returned_receivername", params.get("personnel_last"));
		params.put("returned_receivernum", params.get("number_last_reviewer"));
		// 二审信息清空
		params.put("approval_scoreof_second", "");
		params.put("second_approval_level", "");
		params.put("remarks_second", "");
		// 一审信息清空
		params.put("time_last_trial", "");
		params.put("personnel_last", "");
		params.put("number_last_reviewer", "");
		User userBean = (User) request.getSession().getAttribute("user");
		params.put("name_originator", userBean.getUserName());
		params.put("number_initiating", userBean.getJobNum());
		LocalDateTime rightNow = LocalDateTime.now();
		params.put("return_time", rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		params.put("return_remarks", "二审退回一审");
		int update = aopYearJdbcService.update(year, jdbcTemplate, tableName, params, Id);
		if (update > 0) {
			result.put("code", 0);
			result.put("msg", "退回成功");
		} else {
			result.put("code", -1);
			result.put("msg", "退回失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 移入采纳表 tableName 表名 id 主键id
	 */
	@RequestMapping("/transferInto")
	public void transferInto(String year, int id, String tableName, String transferTableName,
			HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> params = new HashMap<>();
		Map<String, Object> result = new HashMap<>();
		params.put("id", id);
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        	  try {
	                  response.sendRedirect("/login");
	              } catch (IOException e) {
	                  e.printStackTrace();
	              }
	            return;
	        }
	        String college=null;
	        String job_number=null;
	  User user=   (User)   session.getAttribute("user");
	        if(subject.isPermitted("admin")) {
	        	
	        }else if(subject.isPermitted("集体身份")) {
	        	college=user.getDepartment();
	        }else {
	        	job_number= user.getJobNum();
	        }
		List<Map<String, Object>> list = aopYearJdbcService.select(year, jdbcTemplate, tableName, params, 1, 10,college,job_number);
		int add = aopYearJdbcService.add(year, jdbcTemplate, transferTableName, list.get(0));
		
		if (add > 0) {
			//int delete=aopYearJdbcService.delete(year, jdbcTemplate, tableName, params, id);
			result.put("code", 0);
			result.put("msg", "转入成功");
		} else {
			result.put("code", -1);
			result.put("msg", "转入失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 新增 tableName 表名 map 表字段为key，表值为value
	 */
	@RequestMapping("/add")
	public void add(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		String tableName = (String) params.get("tableName");
		params.remove("tableName");

		params.remove("year");
		User userBean = (User) request.getSession().getAttribute("user");
		params.put("nameof_uploader", userBean.getUserName());
		params.put("uploader_num", userBean.getJobNum());
		LocalDateTime rightNow = LocalDateTime.now();
		params.put("upload_time", rightNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		int add = aopYearJdbcService.add(year, jdbcTemplate, tableName, params);
		if (add > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "添加成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "添加失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 不需要上传者该参数
	 * */
	@RequestMapping("/addOther")
	public void addOther(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		String tableName = (String) params.get("tableName");
		params.remove("tableName");

		params.remove("year");
		
		
		int add = aopYearJdbcService.add(year, jdbcTemplate, tableName, params);
		if (add > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "添加成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "添加失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 修改 tableName 表名 id 主键id map 表字段为key，表值为value
	 */
	@RequestMapping("/update")
	public void update(String year, @RequestParam Map<String, Object> params, HttpServletResponse response,
			HttpServletRequest request) {
		String tableName = (String) params.get("tableName");
		Integer Id = Integer.parseInt((String) params.get("id"));
		Map<String, Object> result = new HashMap<String, Object>();
		params.remove("tableName");
		params.remove("id");

		params.remove("year");
		int update = aopYearJdbcService.update(year, jdbcTemplate, tableName, params, Id);
		if (update > 0) {
			result = new HashMap<String, Object>();
			result.put("code", 0);
			result.put("msg", "更新成功");
		} else {
			result = new HashMap<String, Object>();
			result.put("code", -1);
			result.put("msg", "更新失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}
	/**
	 * 删除 tableName 表名 id 主键id
	 */
	@RequestMapping("/delete")
	public void delete(String year, String tableName, @RequestParam(value = "map", required = false) String map,
			Integer Id, HttpServletResponse response, HttpServletRequest request) {
		Map<String, Object> jsonObject = (Map) JSONObject.parse(map);
		int delete = aopYearJdbcService.delete(year, jdbcTemplate, tableName, jsonObject, Id);
		;
		if (delete > 0) {
			jsonObject = new HashMap<String, Object>();
			jsonObject.put("code", 0);
		} else {
			jsonObject = new HashMap<String, Object>();
			jsonObject.put("code", -1);
		}
		ResponseUtil.outputJson(response, jsonObject, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 查找 tableName 表名 map 查找条件 表字段为key，表值为value
	 * 
	 */
	@RequestMapping("/select")
	public void select(String year, String tableName, @RequestParam Map<String, Object> params,
			HttpServletResponse response, HttpServletRequest request) {
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        	  try {
	                  response.sendRedirect("/login");
	              } catch (IOException e) {
	                  e.printStackTrace();
	              }
	            return;
	        }
	        String college=null;
	        String job_number=null;
	  User user=   (User)   session.getAttribute("user");
	        if(subject.isPermitted("admin")) {
	        	
	        }else if(subject.isPermitted("集体身份")) {
	        	college=user.getDepartment();
	        }else {
	        	job_number= user.getJobNum();
	        }
		params.remove("year");
		Map<String, Object> jsonObject = (Map) JSONObject.parse(params.get("map").toString());
		List<Map<String, Object>> list = aopYearJdbcService.select(year, jdbcTemplate, tableName, jsonObject,
				Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()),college,job_number);
		ResponseUtil.outputJson(response, list, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 自动建库建表 dataBaseName 库名
	 */
	@RequestMapping("/createTableData")
	public void createTableData(String year, String dataBaseName, HttpServletResponse response,
			HttpServletRequest request) {
		Map<String, Object> dataMap = baseService.selectByBatis(MapperNSEnum.TableMaper, "selectDataBaseName",
				dataBaseName);
		Map<String, Object> result = new HashMap<String, Object>();
		// 判断数据是否已存在
		if (dataMap != null) {
			result.put("code", -1);
			result.put("msg", "该数据库已存在，请重新输入");
			ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
			return;
		} else {
			List<Table> list = baseService.selectListByBatis(MapperNSEnum.TableMaper, "getTableAndModel",
					new HashMap<String, Object>());
			int create = aopYearJdbcService.createTableData(year, jdbcTemplate, dataBaseName, list);

			if (create > 0) {
				result.put("code", 200);
				result.put("msg", "创建库表成功");
			} else {
				result.put("code", -2);
				result.put("msg", "创建库表失败");
			}
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 表数据
	 */
	@RequestMapping("/getTableList")
	public void getTableList(String year, HttpServletRequest request, HttpServletResponse response,
			@RequestParam Map<String, Object> params) {
		Map<String, Object> p = (Map) JSONObject.parse(params.get("map").toString());
		//判断权限是否个人，集体
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        	  try {
	                  response.sendRedirect("/login");
	              } catch (IOException e) {
	                  e.printStackTrace();
	              }
	            return;
	        }
	        String college=null;
	        String job_number=null;
	  User user=   (User)   session.getAttribute("user");
	        if(subject.isPermitted("admin")) {
	        	
	        }else if(subject.isPermitted("集体身份")) {
	        	college=user.getDepartment();
	        }else {
	        	job_number= user.getJobNum();
	        }
		params.remove("year");
		PageInfo<Map<String, Object>> pageInfo = aopYearJdbcService.selectListBypage(year, jdbcTemplate,
				params.get("tableName").toString(), p, Integer.valueOf(params.get("page").toString()),
				Integer.valueOf(params.get("rows").toString()),college,job_number);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows", pageInfo.getList());
		result.put("total", pageInfo.getTotal());

		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}

	/**
	 * 本指标数据总览 tableName 库名
	 */
	@RequestMapping("/getDataStatistics")
	public void getDataStatistics(String year, String tableName, String tableName1, String tableName2,
			HttpServletResponse response, HttpServletRequest request) {
		List<Map<String, Object>> list = aopYearService.getDataStatistics(year, tableName, tableName1, tableName2);
		ResponseUtil.outputJson(response, list, "yyyy-MM-dd HH:mm:ss");
	}
	
	
	/**
	 * 跳转页面通用表格
	 */
	@RequestMapping("/findSystemView/{tableName}")
	public ModelAndView findStudentRestful(String year,@PathVariable("tableName") String tableName) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tableName", tableName);
		// 获取表字段
		List<Table> tableColumn = aopYearService.getTableAndColumns(year,map);

		mv.addObject("tableName", tableName);
		mv.addObject("create_table_chinese_name", tableColumn.get(0).getCreate_table_chinese_name());
		Collections.sort(tableColumn.get(0).getTableColumn()); 
		mv.addObject("tableColumn", tableColumn.get(0).getTableColumn());
		mv.addObject("yearValue",year);
		mv.setViewName("pastYearModelView/systemModelView");
		return mv;
	}
	
	//跳转用户页
    @RequestMapping("/userManagement")
    public ModelAndView userManagement(HttpServletRequest request, HttpServletResponse response,String year) {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("pastYearModelView/usermanagementModelView");
        List<DepartmentInformation> departmentList = aopYearService.getDepartment(year,new HashMap<String, Object>());
      mvc.addObject("departmentList",departmentList);
      mvc.addObject("yearValue",year);
        return mvc;
    }
    //获取用户表
    @RequestMapping("/getUserTable")
    public void getUserTable(String year,HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) {
        Map<String,Object> result=aopYearService.getUserList(year,params,Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response,result,"yyyy-MM-dd");
    }
    //跳转页面用户部门
    @RequestMapping("/userDepartment/{tableName}")
    public ModelAndView userDepartment(String year,@PathVariable("tableName") String tableName) {
        ModelAndView mvc = new ModelAndView();
        mvc.addObject("yearValue",year);
        Map<String, Object> map = new HashMap<String, Object>();
		map.put("tableName", tableName);
		// 获取表字段
		List<Table> tableColumn = aopYearService.getTableAndColumns(year,map);

		mvc.addObject("tableName", tableName);
		mvc.addObject("create_table_chinese_name", tableColumn.get(0).getCreate_table_chinese_name());
		Collections.sort(tableColumn.get(0).getTableColumn()); 
		mvc.addObject("tableColumn", tableColumn.get(0).getTableColumn());
        mvc.setViewName("pastYearModelView/deptmanagementModelView");
        return mvc;
    }
	/**
	 * 新增用户
	 * */
	@RequestMapping("/addUser")
	public void addUser(String year,@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.addUser(year, params);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "保存成功");
		}else {

			result.put("code", -1);
			result.put("msg", "保存失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};	
	/**
	 * 跟新用户
	 * */
	@RequestMapping("/updateUser")
	public void updateUser(String year,@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.updateUser(year, params);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "保存成功");
		}else {

			result.put("code", -1);
			result.put("msg", "保存失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};	
	/**
	 * 删除用户
	 * */
	@RequestMapping("/deleteUser")
	public void deleteUser(String year,Integer id ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.deleteUser(year,id);
		if(add>0) {

			result.put("code", 0);
			result.put("msg", "删除成功");
		}else {

			result.put("code", -1);
			result.put("msg", "删除失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};
	
	/**
	 * 重置用户密码
	 * */
	@RequestMapping("/resetPassword")
	public void resetPassword(String year,@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		params.put("pwd",123456);
		
		int  update = aopYearService.resetPassword(year,params);
		if(update>0) {

			result.put("code", 0);
			result.put("msg", "重置成功");
		}else {

			result.put("code", -1);
			result.put("msg", "重置失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
		
	};
	/**
	    * @param request
		* @param response
		* @param params
	    * @描述 获取用户部门列表
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:16
	*/
	@RequestMapping("/getDepartmentTable")
	public void getDepartmentTable(String year,HttpServletRequest request, HttpServletResponse response,@RequestParam Map<String, Object> params) {
		Map<String,Object> result=aopYearService.getDepartmentTable(year,params,Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
		ResponseUtil.outputJson(response,result,"yyyy-MM-dd");
	}
	
	/**
	    * @param params
		* @param request
		* @param response
	    * @描述 保存部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:31
	*/
	@RequestMapping("/saveDepartment")
	public void addDepartment(String year,@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.saveDepartment(year,params);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "保存成功");
		}else if (add==-1){
			result.put("code", -1);
			result.put("msg", "保存失败");
		}else {
			result.put("code", -2);
			result.put("msg", "该部门已存在！");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	/**
	    * @param params
		* @param request
		* @param response
	    * @描述 修改部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 14:00
	*/
	@RequestMapping("/updateDepartment")
	public void updateDepartment(String year,@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.updateDepartment(year,params);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "保存成功");
		}else if (add==-1){

			result.put("code", -1);
			result.put("msg", "保存失败");
		}else {
			result.put("code", -2);
			result.put("msg", "该部门已存在！");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	/**
	    * @param id
		* @param request
		* @param response
	    * @描述 删除部门
	    * @return void
	    * @author WuRongPeng
	    * @date 2020/8/3 11:32
	*/
	@RequestMapping("/deleteDepartment")
	public void deleteDepartment(String year,Integer id ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=new HashMap<String, Object>();
		int  add = aopYearService.deleteDepartment(year,id);
		if(add>0) {
			result.put("code", 0);
			result.put("msg", "删除成功");
		}else {
			result.put("code", -1);
			result.put("msg", "删除失败");
		}
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

	};
	
	//	添加字段
	@RequestMapping("/addColumn")
	public void addColumn(String tableName,String year,@RequestParam Map<String, Object>  params ,HttpServletRequest request,HttpServletResponse response) {

		Map<String, Object> result=	aopYearService.addColumn(year,tableName, params);
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	};
	
	///////////
	
	 /**
     * 菜单列表页
     * */
    @RequestMapping(value = "/menu_list",method = RequestMethod.GET)
    public ModelAndView  menu_list(String year,@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("pastYearModelView/menuListModelView");
       	//菜单下拉
       List<Menu> list= aopYearService.getMenusListSelect(year, params);
       mvc.addObject("menuList",list);
       mvc.addObject("yearValue",year);
       return mvc;
    }
    /**
     * 角色管理页
     * */
    @RequestMapping(value = "/role_management",method = RequestMethod.GET)
    public ModelAndView  role_management(String year,@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("pastYearModelView/roleListModelView");
       mvc.addObject("yearValue",year);
       return mvc;
    }
    /**
     * 权限管理页
     * */
    @RequestMapping(value = "/permission_management",method = RequestMethod.GET)
    public ModelAndView  permission_management(String year,@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("pastYearModelView/permissionListModelView");
       	//权限下拉
   	List<Map<String, Object>> list = aopYearService.getPermissionSelect(year,params);
       mvc.addObject("permissionParent",list);
       mvc.addObject("yearValue",year);
       return mvc;
    }
   
    
    /**
     * 菜单列表
     **/
    @RequestMapping(value = "/getMenuList")
    public void getMenuList(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
     
        	Map<String, Object> resuleMap=aopYearService.getMenusListPage(year, params,  Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 菜单新增
     * 
     * */
    @RequestMapping(value = "/addMenuOne")
    public void addMenuOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        Map<String, Object> resuleMap=aopYearService.addMenu(year, params);
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 菜单修改
     * 
     * */
    @RequestMapping(value = "/updateMenuOne")
    public void updateMenuOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.updateMenu(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 菜单删除     * 
     * */
    @RequestMapping(value = "/deleteMenuOne")
    public void deleteMenuOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.deleteMenu(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色权限分配
     **/
    @RequestMapping(value = "/saveRolePermissionList")
    public void saveRolePermissionList(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
       Integer roleId =  Integer.valueOf(params.get("roleId").toString());
      
       List<Integer> integerList = JSONArray.parseArray(params.get("permissionIdList").toString(), Integer.class);
     Map<String, Object> map=   aopYearService.saveRolePermissionList(year,roleId, integerList);
      
        ResponseUtil.outputJson(response, map , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 角色列表
     **/
    @RequestMapping(value = "/getRoleList")
    public void getRoleList(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
      
        	Map<String, Object> resuleMap=aopYearService.getRoleListPage(year, params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色新增
     * 
     * */
    @RequestMapping(value = "/addRoleOne")
    public void addRoleOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        Map<String, Object> resuleMap=aopYearService.addRole(year, params);
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色修改
     * 
     * */
    @RequestMapping(value = "/updateRoleOne")
    public void updateRoleOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.updateRole(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 角色删除     * 
     * */
    @RequestMapping(value = "/deleteRoleOne")
    public void deleteRoleOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.deleteRole(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 获取角色下拉
     * */
    @RequestMapping(value = "/getRoleListAll")
    public void getRoleListAll(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
      
    	Map<String, Object> resuleMap=aopYearService.getRoleListAllSelect(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 用户角色新增
     * 
     * */
    @RequestMapping(value = "/saveUserRoleOne")
    public void saveUserRoleOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        Map<String, Object> resuleMap=aopYearService.saveUserRoleOne(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 获取权限下拉
     * */
    @RequestMapping(value = "/getPermissionAll")
    public void getPermissionAll(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);

    	Map<String, Object> resuleMap=aopYearService.getPermissionAllSelect(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 根据角色Id获取权限
     * */
    @RequestMapping(value = "/getPermissionAllByroleId")
    public void getPermissionAllByroleId(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
    	
    	JSONArray arraylist = aopYearService.getPermissionAllByroleId(year, params);
        ResponseUtil.outputJson(response, arraylist , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限列表
     **/
    @RequestMapping(value = "/getPermissionList")
    public void getPermissionList(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
    
        	Map<String, Object> resuleMap=aopYearService.getPermissionList(year, params, Integer.valueOf(params.get("page").toString()),
    				Integer.valueOf(params.get("rows").toString()));
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限新增
     * 
     * */
    @RequestMapping(value = "/addPermissionOne")
    public void addPermissionOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
     
        Map<String, Object> resuleMap=aopYearService.addPermissionOne(year, params);
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限修改
     * 
     * */
    @RequestMapping(value = "/updatePermissionOne")
    public void updatePermissionOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.updatePermissionOne(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 权限删除     * 
     * */
    @RequestMapping(value = "/deletePermissionOne")
    public void deletePermissionOne(String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.deletePermissionOne(year, params);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    ///////
    //查询表名翻译
    @RequestMapping(value = "/getTableNameTranslate")
    public void getTableNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = aopYearService.getTableNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

    }

    //查询字段翻译
    @RequestMapping(value = "/getFieldNameTranslate")
    public void getFieldNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result =aopYearService.getFieldNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");


    }

    //新增表名翻译
    @RequestMapping(value = "/addTableNameTranslate")
    public void addTableNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.addTableNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "新增成功");
        }else{
            result.put("msg", "新增失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    //新增字段翻译
    @RequestMapping(value = "/addFieldNameTranslate")
    public void addFieldNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.addFieldNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "新增成功");
        }else{
            result.put("msg", "新增失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }

    //修改表名翻译
    @RequestMapping(value = "/setTableNameTranslate")
    public void setTableNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.setTableNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "修改成功");
        }else{
            result.put("msg", "修改失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    //修改字段翻译
    @RequestMapping(value = "/setFieldNameTranslate")
    public void setFieldNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.setFieldNameTranslate(year, map);


        if(res > 0){
            result.put("msg", "修改成功");
        }else{
            result.put("msg", "修改失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    
    
    //删除模板表名翻译
    @RequestMapping(value = "/deleteTableNameTranslateModel")
    public void deleteTableNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.deleteTableNameTranslate(year, map);

        if(res > 0){
            result.put("msg", "删除成功");
        }else{
            result.put("msg", "删除失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    //删除模板字段翻译
    @RequestMapping(value = "/deleteFieldNameTranslateModel")
    public void deleteFieldNameTranslate(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result = new HashMap<>();

        int res = aopYearService.deleteFieldNameTranslate(year, map);


        if(res > 0){
            result.put("msg", "删除成功");
        }else{
            result.put("msg", "删除失败");
        }

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
    }
    //查询模板表名翻译
    @RequestMapping(value = "/getTableNameTranslateModel")
    public void getTableNameTranslateModel(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){

        Map<String,Object> result = aopYearService.getNextTableNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");

    }

    //查询模板字段翻译
    @RequestMapping(value = "/getFieldNameTranslateModel")
    public void getFieldNameTranslateModel(String year,@RequestParam Map<String, Object> map, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> result =aopYearService.getNextFieldNameTranslate(year, map);

        ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");


    }
    
    //跳转翻译table页面
    @RequestMapping("tname_explain")
    public ModelAndView tname_explain(String year) {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("pastYearModelView/tnameExplainView");
        mvc.addObject("yearValue",year);
        return mvc;
    }

    //跳转翻译field页面
    @RequestMapping("field_explain")
    public ModelAndView field_explain(String year) {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("pastYearModelView/fieldExplainView");
        mvc.addObject("yearValue",year);
        return mvc;
    }
    //跳转翻译tableModel页面
    @RequestMapping("tname_explain_model")
    public ModelAndView tname_explain_model(String year) {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("pastYearModelView/tnameExplainModelView");
        mvc.addObject("yearValue",year);
        return mvc;
    }

    //跳转翻译fieldModel页面
    @RequestMapping("field_explain_model")
    public ModelAndView field_explain_model(String year) {
        ModelAndView mvc = new ModelAndView();
        mvc.setViewName("pastYearModelView/fieldExplainModelView");
        mvc.addObject("yearValue",year);
        return mvc;
    }
    
    
    /**
     * 年度列表，
     **/
    @RequestMapping(value = "/getYearTable")
    public void getYearTable(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.getYearTableList("aop", params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 年度新增，下一年度初始化
     * 
     * */
    @RequestMapping(value = "/addYearOne")
    public void addYearOne(String thisYear,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        
        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        List<Map<String, Object>> yearlist=	(List<Map<String, Object>>)aopYearService.getYearTableList("aop", params,1,10).get("rows");
          if (yearlist.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","该年度已存在");
			}else {
				
			 int insert= 	aopYearService.addYear("aop", params);	        	
	            if(insert>0) {
	            	String dataBaseName="newconnotation_"+params.get("year");
	            	
	        		Map<String, Object> dataMap = baseService.selectByBatis(MapperNSEnum.TableMaper, "selectDataBaseName", dataBaseName);
	        		Map<String, Object> result=new HashMap<String, Object>();
	        		//判断数据是否已存在
	        		if(dataMap!=null) {
	        			result.put("code",-1);
	        			result.put("msg","该数据库已存在，请重新输入");
	        			ResponseUtil.outputJson(response, result, "yyyy-MM-dd HH:mm:ss");
	        			return;
	        		}else {
	        			//建表模板
	        			List<Table> list =aopYearService.getTableAndColumnsModel(thisYear,new HashMap<String, Object>());
	        			int create=	aopYearJdbcService.createTableData(thisYear, jdbcTemplate, dataBaseName, list);
	        		
	        			if(create>0) {
	        				result.put("code",200);
	        				result.put("msg","创建库表成功");
	        			}else {
	        				result.put("code",-2);
	        				result.put("msg","创建库表失败");
	        			}
	        		}
	            	resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功并成功创建库表");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 年库表初始化
     * */
    @RequestMapping(value = "/system_maintenance",method = RequestMethod.GET)
    public ModelAndView  system_maintenance(String year,@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("pastYearModelView/systemMaintenanceModelView");

       mvc.addObject("yearValue",year);
       return mvc;
    }
    /**
     * 年度初始化库表结构
     * */
    @RequestMapping(value = "/addBatch_addInitTableField")
    public void batch_addInitTableField(@RequestParam(value="fileImport",required=false) MultipartFile file,String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.batch_addInitTableField(year,file);
        
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 年度初始化库表结构
     * */
    @RequestMapping(value = "/addBatch_addInitTable")
    public void addBatch_addInitTable(String year,@RequestParam(value="fileImport",required=false) MultipartFile file,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.batch_addInitTable(year,file);
        
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 年度初始化库表结构模板
     * */
    @RequestMapping(value = "/addBatch_addInitTableFieldModel")
    public void addBatch_addInitTableFieldModel(@RequestParam(value="fileImport",required=false) MultipartFile file,String year,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.batch_addInitTableFieldModel(year,file);
        
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 年度初始化库表结构模板
     * */
    @RequestMapping(value = "/addBatch_addInitTableModel")
    public void addBatch_addInitTableModel(String year,@RequestParam(value="fileImport",required=false) MultipartFile file,@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=aopYearService.batch_addInitTableModel(year,file);
        
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
	 * 跳转暂存统计页面restful
	 */
	@RequestMapping("/findTemporarystorageStatisticsView")
	public ModelAndView findTemporarystorageStatisticsView(String year) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        
	                  mv.setViewName("/login");
	            return mv;
	        }
		map.put("create_table_type", "暂存");
		// 获取表字段
		List<Table> list = aopYearService.getTemporarystorageList(year,map);

		mv.addObject("tableList", list);
		mv.addObject("yearValue", year);
		mv.setViewName("modelView/TemporarystorageStatisticsView");
		return mv;
	}

	/**
	 * 跳转暂存统计页面restful(集体)
	 */
	@RequestMapping("/findTemporarystorageStatisticsView1")
	public ModelAndView findTemporarystorageStatisticsView1(String year) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		Subject subject = SecurityUtils.getSubject();
		Session session =subject.getSession();
		if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){

			mv.setViewName("/login");
			return mv;
		}
		map.put("create_table_type", "暂存");
		// 获取表字段
		List<Table> list = aopYearService.getTemporarystorageList(year,map);

		mv.addObject("tableList", list);
		mv.addObject("yearValue", year);
		mv.setViewName("modelView/TemporarystorageStatisticsView1");
		return mv;
	}

	/**
	 * 跳转采纳统计页面restful
	 */
	@RequestMapping("/findAdoptStatisticsView")
	public ModelAndView findAdoptStatisticsView(String year) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        
	                  mv.setViewName("/login");
	            return mv;
	        }
		map.put("create_table_type", "采纳");
		// 获取表字段
		List<Table> list = aopYearService.getTemporarystorageList(year,map);

		mv.addObject("tableList", list);
		mv.addObject("yearValue", year);
		mv.setViewName("modelView/statisticsAdoptView");
		return mv;
	}

	/**
	 * 跳转采纳统计页面restful(集体)
	 */
	@RequestMapping("/findAdoptStatisticsCollectiveView")
	public ModelAndView findAdoptStatisticsCollectiveView(String year) {
		ModelAndView mv = new ModelAndView();
		Map<String, Object> map = new HashMap<String, Object>();
		Subject subject = SecurityUtils.getSubject();
		Session session =subject.getSession();
		if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){

			mv.setViewName("/login");
			return mv;
		}
		map.put("create_table_type", "采纳");
		// 获取表字段
		List<Table> list = aopYearService.getTemporarystorageList(year,map);

		mv.addObject("tableList", list);
		mv.addObject("yearValue", year);
		mv.setViewName("modelView/statisticsAdoptCollectiveView");
		return mv;
	}
	
	/**
	 * 统计表数据回显
	 */
	@RequestMapping("/getStaticsTableList/{tableName}")
	public void getStaticsTableList(String year,HttpServletRequest request, HttpServletResponse response,
			@PathVariable("tableName") String tableName) {
		//判断权限是否个人，集体
	     Subject subject = SecurityUtils.getSubject();
	        Session session =subject.getSession();
	        if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
	        	  try {
	                  response.sendRedirect("/login");
	              } catch (IOException e) {
	                  e.printStackTrace();
	              }
	            return;
	        }
	        User user=   (User)   session.getAttribute("user");
	  			String college=null;
			  String jobNum=null;
	        if(subject.isPermitted("admin")) {
	        }else {
	        	jobNum= user.getJobNum();
	        }
		List<Map<String, Object>> pageInfo = aopYearJdbcService.selectAll(year,jdbcTemplate,tableName, new HashMap<String, Object>(),college,jobNum);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows",pageInfo );
		result.put("total", pageInfo.size());
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}


	/**
	 * 统计表数据回显
	 */
	@RequestMapping("/getStaticsTableList1/{tableName}")
	public void getStaticsTableList1(String year,HttpServletRequest request, HttpServletResponse response,
									@PathVariable("tableName") String tableName) {
		//判断权限是否个人，集体
		Subject subject = SecurityUtils.getSubject();
		Session session =subject.getSession();
		if(session.getAttribute("user")==null||"".equals(session.getAttribute("user"))){
			try {
				response.sendRedirect("/login");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		User user=   (User)   session.getAttribute("user");
		String college=user.getDepartment();
		String jobNum=null;
		if(subject.isPermitted("admin")) {
			college = null;
		}else if(subject.isPermitted("集体身份")) {
			college = user.getDepartment();
		}
		List<Map<String, Object>> pageInfo = aopYearJdbcService.selectAll(year,jdbcTemplate,tableName, new HashMap<String, Object>(),college,jobNum);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("rows",pageInfo );
		result.put("total", pageInfo.size());
		ResponseUtil.outputJson(response, result, "yyyy-MM-dd");
	}
}
