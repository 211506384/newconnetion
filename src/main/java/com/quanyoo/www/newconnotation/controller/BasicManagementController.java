package com.quanyoo.www.newconnotation.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.model.AssessmentIndex;
import com.quanyoo.www.newconnotation.model.FieldInformation;
import com.quanyoo.www.newconnotation.pojo.DbInfo;
import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.service.IBaseService;
import com.quanyoo.www.newconnotation.service.JdbcService;
import com.quanyoo.www.newconnotation.service.TestServiceInterface;
import com.quanyoo.www.newconnotation.service.UserService;
import com.quanyoo.www.newconnotation.sqlDataSource.DataSourceUtil;
import com.quanyoo.www.newconnotation.sqlDataSource.DynamicDataSourceContextHolder;
import com.quanyoo.www.newconnotation.util.JDBCTemplate;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.MyLog;
import com.quanyoo.www.newconnotation.util.ResponseUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 * 功能模块：基础管理
 * */
@RequestMapping("/basicManagement")
@Controller
public class BasicManagementController {
    @Resource
    private IBaseService baseService;
    @Resource
    private UserService userService;
	@Autowired
	private JdbcService jdbcService;
    
	@Autowired
	private TestServiceInterface f;
	
	private testController tc;
    
    /**
     * 菜单列表页
     * */
    @RequestMapping(value = "/menu_list",method = RequestMethod.GET)
    public ModelAndView  menu_list(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("basic_management_module/menu_management/menu_list");

   	List<Menu> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuListAll",params);

       mvc.addObject("menuList",list);

       return mvc;
    }
    /**
     * 角色管理页
     * */
    @RequestMapping(value = "/role_management",method = RequestMethod.GET)
    public ModelAndView  role_management(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("basic_management_module/authority_management/role_list");

       return mvc;
    }
    /**
     * 权限管理页
     * */
    @RequestMapping(value = "/permission_management",method = RequestMethod.GET)
    public ModelAndView  permission_management(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("basic_management_module/authority_management/permission_list");
       
   	List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findPermissionListByParent",params);
       mvc.addObject("permissionParent",list);

       return mvc;
    }
    
    /**
     * 考核指标查看页-2020.08.06 cc
     * */
    @RequestMapping(value = "/assessment_list",method = RequestMethod.GET)
    public ModelAndView  assessment_list(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("basic_management_module/Yearinitialization_management/assessment_list");

   	   List<AssessmentIndex> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findAssessmentIndex",params);
       mvc.addObject("List",list);

       return mvc;
    }
    
    /**
     * 考核指标查看页-2020.08.06 cc
     * */
    @RequestMapping(value = "/system_maintenance",method = RequestMethod.GET)
    public ModelAndView  system_maintenance(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("basic_management_module/Yearinitialization_management/system_maintenance");

   	   List<AssessmentIndex> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findAssessmentIndex",params);
       mvc.addObject("List",list);

       return mvc;
    }
    
    /**
     * 查看字段表页-2020.08.06 cc
     * */
    @RequestMapping(value = "/field_information",method = RequestMethod.GET)
    public ModelAndView  field_information(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("basic_management_module/Yearinitialization_management/field_information");

   	   List<FieldInformation> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findFieldInformation",params);
       mvc.addObject("List",list);

       return mvc;
    }
    
    /**
     * 核查指标与字段表-2020.08.06 cc
     * */
    @RequestMapping(value = "/form_match",method = RequestMethod.GET)
    public ModelAndView form_match(@RequestParam Map<String, Object> params,HttpServletResponse response, HttpServletRequest request) {
        ModelAndView mvc = new ModelAndView();
       mvc.setViewName("basic_management_module/Yearinitialization_management/form_match");

   	   List<FieldInformation> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findFormMatch",params);
       mvc.addObject("List",list);

       return mvc;
    }
    

    
    /**
     * 菜单列表
     **/
    @RequestMapping(value = "/getMenuList")
    public void getMenuList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);
        
        PageInfo<List<Menu>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuListAll",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
      
        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	resuleMap.put("rows", info.getList());
        	resuleMap.put("total", info.getTotal());
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 菜单新增
     * 
     * */
    @RequestMapping(value = "/addMenuOne")
    public void addMenuOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        	//新增前先查找是否存在菜单
        	List<Menu> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuByMenuName", params);
           
        	if (list.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","菜单名已存在");
			}else {
			  	long insert =  baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addMenuOne", params);
	        	
	            if(insert>0) {
	         	   resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 菜单修改
     * 
     * */
    @RequestMapping(value = "/updateMenuOne")
    public void updateMenuOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        		//新增前先查找是否存在菜单
            	List<Menu> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findMenuByMenuName", params);
            	if (list.size()>0) {
            		 resuleMap.put("code", -3);
            		 resuleMap.put("errorMsg","菜单名已存在");
            	}else {
            		int update =	baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateMenuOne", params);
               	 
                    if(update>0) {
                 	   resuleMap.put("code", 200);
                 	  resuleMap.put("errorMsg","修改成功");
                    } else {
                 	   resuleMap.put("code", -1);
                 	  resuleMap.put("errorMsg","修改失败");
                    }
            	}
        
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 菜单删除     * 
     * */
    @RequestMapping(value = "/deleteMenuOne")
    public void deleteMenuOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        	int delete =	baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "deleteMenuOne", params);
        	 
            if(delete>0) {
         	   resuleMap.put("code", 200);

          	  resuleMap.put("errorMsg","删除成功");
            } else {
         	   resuleMap.put("code", -1);
         	  resuleMap.put("errorMsg","删除失败");
            }
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色权限分配
     **/
    @RequestMapping(value = "/saveRolePermissionList")
    public void saveRolePermissionList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
       Integer roleId =  Integer.valueOf(params.get("roleId").toString());
      
       List<Integer> integerList = JSONArray.parseArray(params.get("permissionIdList").toString(), Integer.class);
     Map<String, Object> map=   userService.saveRolePermissionList(roleId, integerList);
      
        ResponseUtil.outputJson(response, map , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 角色列表
     **/
    @RequestMapping(value = "/getRoleList")
    public void getRoleList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);
        
        PageInfo<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findRoleListAll",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
      
        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	resuleMap.put("rows", info.getList());
        	resuleMap.put("total", info.getTotal());
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色新增
     * 
     * */
    @RequestMapping(value = "/addRoleOne")
    public void addRoleOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        	//新增前先查找是否存在菜单
        	List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findRoleByRoleName", params);
           
        	if (list.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","角色名已存在");
			}else {
			  	long insert =  baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addRoleOne", params);
	        	
	            if(insert>0) {
	         	   resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 角色修改
     * 
     * */
    @RequestMapping(value = "/updateRoleOne")
    public void updateRoleOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        		//新增前先查找是否存在菜单
            	List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findRoleByRoleName", params);
            	if (list.size()>0) {
            		 resuleMap.put("code", -3);
            		 resuleMap.put("errorMsg","角色名已存在");
            	}else {
            		int update =	baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateRoleOne", params);
               	 
                    if(update>0) {
                 	   resuleMap.put("code", 200);
                 	  resuleMap.put("errorMsg","修改成功");
                    } else {
                 	   resuleMap.put("code", -1);
                 	  resuleMap.put("errorMsg","修改失败");
                    }
            	}
        
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 角色删除     * 
     * */
    @RequestMapping(value = "/deleteRoleOne")
    public void deleteRoleOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        	int delete =	baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "deleteRoleOne", params);
        	 
            if(delete>0) {
         	   resuleMap.put("code", 200);

          	  resuleMap.put("errorMsg","删除成功");
            } else {
         	   resuleMap.put("code", -1);
         	  resuleMap.put("errorMsg","删除失败");
            }
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 获取角色下拉
     * */
    @RequestMapping(value = "/getRoleListAll")
    public void getRoleListAll(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);

    	Map<String, Object> resuleMap=new HashMap<String, Object>();
    	JSONArray array = new JSONArray();
    	JSONArray arraylist = new JSONArray();
      List<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "getRoleListAll",params);
      	if(info.size()>0) {
      		for(Map<String, Object> map:info) {
      			Map<String, Object> map2=new HashMap<String, Object>();
      			map2.put("id",  map.get("id"));
      			map2.put("text",  map.get("roleName"));
      			array.add(map2);
      		}
      	}
      	Map<String, Object> map3=new HashMap<String, Object>();
      	map3.put("children", array);
    	map3.put("id", "");
    	map3.put("text", "All");
    	arraylist.add(map3);
        	resuleMap.put("roleList", arraylist);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 用户角色新增
     * 
     * */
    @RequestMapping(value = "/saveUserRoleOne")
    public void saveUserRoleOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        	//新增前先查找是否存在
        	List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findRoleByUserId", params);
           
        	if (list.size()>0) {
        		params.put("user_role_id", list.get(0).get("user_role_id"));
        		int update=baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateRoleUserByOne", params);
        	    if(update>0) {
 	         	   resuleMap.put("code", 200);
 	         	  resuleMap.put("errorMsg","分配成功");
 	            } else {
 	         	   resuleMap.put("code", -1);
 	         	  resuleMap.put("errorMsg","分配失败");
 	            }
			}else {
			  	long insert =  baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addRoleUserByOne", params);
	        	
	            if(insert>0) {
	         	   resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","分配成功");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","分配失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 获取权限下拉
     * */
    @RequestMapping(value = "/getPermissionAll")
    public void getPermissionAll(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);

    	Map<String, Object> resuleMap=new HashMap<String, Object>();
    	JSONArray array = new JSONArray();
    	JSONArray arraylist = new JSONArray();
      List<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findPermissionListAll",params);
      	if(info.size()>0) {
      		for(Map<String, Object> map:info) {
      			Map<String, Object> map2=new HashMap<String, Object>();
      			map2.put("id",  map.get("id"));
      			map2.put("text",  map.get("permissionName"));
      			array.add(map2);
      		}
      	}
      	Map<String, Object> map3=new HashMap<String, Object>();
      	map3.put("children", array);
    	map3.put("id", "");
    	map3.put("text", "All");
    	arraylist.add(map3);
        	resuleMap.put("permissionList", arraylist);
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 根据角色Id获取权限
     * */
    @RequestMapping(value = "/getPermissionAllByroleId")
    public void getPermissionAllByroleId(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
   
    	JSONArray array = new JSONArray();
    	JSONArray arraylist = new JSONArray();
      List<Map<String, Object>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "getPermissionAllByroleId",params);
      	if(info.size()>0) {
      		for(Map<String, Object> map:info) {
      			Map<String, Object> map2=new HashMap<String, Object>();
      			map2.put("id",  map.get("id"));
      			map2.put("text",  map.get("permissionName"));
      			array.add(map2);
      		}
      	}
      	Map<String, Object> map3=new HashMap<String, Object>();
      	map3.put("children", array);
    	map3.put("id", "");
    	map3.put("text", "All");
    	arraylist.add(map3);
        ResponseUtil.outputJson(response, arraylist , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限列表
     **/
    @RequestMapping(value = "/getPermissionList")
    public void getPermissionList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);
        
        PageInfo<List<Map<String, Object>>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findPermissionListAll",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
      
        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	resuleMap.put("rows", info.getList());
        	resuleMap.put("total", info.getTotal());
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限新增
     * 
     * */
    @RequestMapping(value = "/addPermissionOne")
    public void addPermissionOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
     
        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        	//新增前先查找是否存在菜单
        	List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findPermissionByPermissionName", params);
           
        	if (list.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","权限名已存在");
			}else {
			  	long insert =  baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addPermissionOne", params);
	        	
	            if(insert>0) {
	         	   resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 权限修改
     * 
     * */
    @RequestMapping(value = "/updatePermissionOne")
    public void updatePermissionOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        		//新增前先查找是否存在菜单
            	List<Map<String, Object>> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findPermissionByPermissionName", params);
            	if (list.size()>0) {
            		 resuleMap.put("code", -3);
            		 resuleMap.put("errorMsg","权限名已存在");
            	}else {
            		int update =	baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updatePermissionOne", params);
               	 
                    if(update>0) {
                 	   resuleMap.put("code", 200);
                 	  resuleMap.put("errorMsg","修改成功");
                    } else {
                 	   resuleMap.put("code", -1);
                 	  resuleMap.put("errorMsg","修改失败");
                    }
            	}
        
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 权限删除     * 
     * */
    @RequestMapping(value = "/deletePermissionOne")
    public void deletePermissionOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        	int delete =	baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "deletePermissionOne", params);
        	 
            if(delete>0) {
         	   resuleMap.put("code", 200);

          	  resuleMap.put("errorMsg","删除成功");
            } else {
         	   resuleMap.put("code", -1);
         	  resuleMap.put("errorMsg","删除失败");
            }
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    ///////
    

    /**
     * 考核指标列表-2020.08.06 cc
     * 
     **/
    @MyLog(value = "查询并获取考核指标表数据")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/getAssessmentList")
    public void getAssessmentList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);
        
        PageInfo<List<AssessmentIndex>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findAssessmentIndex",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
      
        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	resuleMap.put("rows", info.getList());
        	resuleMap.put("total", info.getTotal());
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 考核指标新增-2020.08.06 cc
     * 
     * */
    @MyLog(value = "考核指标表数据新增")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/addAssessmentOne")
    public void addAssessmentOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);
        System.out.println(params);
        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        	//新增前先查找是否存在菜单
        	List<AssessmentIndex> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findAssessmentIndex", params);
           
        	if (list.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","数据已存在");
			}else {
			  	long insert =  baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addAssessmentOne", params);
	        	
	            if(insert>0) {
	         	   resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 考核指标修改-2020.08.06
     * 
     * */
    @MyLog(value = "考核指标表数据修改")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/updateAssessmentOne")
    public void updateAssessmentOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        		//新增前先查找是否存在菜单
            	
            		int update =	baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateAssessmentOne", params);
               	 
                    if(update>0) {
                 	   resuleMap.put("code", 200);
                 	  resuleMap.put("errorMsg","修改成功");
                    } else {
                 	   resuleMap.put("code", -1);
                 	  resuleMap.put("errorMsg","修改失败");
                    }
            	      
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 考核指标删除    --2020.08.06 cc * 
     * */
    @MyLog(value = "考核指标表数据删除")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/deleteAssessmentOne")
    public void deleteAssessmentOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        	int delete =	baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "deleteAssessmentOne", params);
        	 
            if(delete>0) {
         	   resuleMap.put("code", 200);
          	  resuleMap.put("errorMsg","删除成功");
            } else {
         	   resuleMap.put("code", -1);
         	  resuleMap.put("errorMsg","删除失败");
            }
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    
    /**
     	* 查看字段表 -2020.08.06 cc
     **/
    @MyLog(value = "查询并获取字段表数据")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/getFieldInformation")
    public void getFieldInformation(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);
        
        PageInfo<List<FieldInformation>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findFieldInformation",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
      
        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	resuleMap.put("rows", info.getList());
        	resuleMap.put("total", info.getTotal());
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 查看字段表新增 -2020.08.06 cc
     * 
     * */
    @MyLog(value = "字段表数据新增")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/addFieldInformation")
    public void addFieldInformation(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        //System.out.println(params);
        System.out.println(params);
        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        	//新增前先查找是否存在菜单
        	List<FieldInformation> list = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "quaryOneFieldInformation", params);
           
        	if (list.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","数据已存在");
			}else {
			  	long insert =  baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addFieldInformation", params);
	        	
	            if(insert>0) {
	         	   resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 查看字段表修改 -2020.08.06 cc
     * 
     * */
    @MyLog(value = "字段表数据修改")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/updateFieldInformation")
    public void updateFieldInformation(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        		//新增前先查找是否存在菜单
            	
            		int update =	baseService.updateByBatis(MapperNSEnum.BasicManagementMaper, "updateFieldInformation", params);
               	 
                    if(update>0) {
                 	   resuleMap.put("code", 200);
                 	  resuleMap.put("errorMsg","修改成功");
                    } else {
                 	   resuleMap.put("code", -1);
                 	  resuleMap.put("errorMsg","修改失败");
                    }
            	      
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    /**
     * 查看字段表删除     -2020.08.06 cc* 
     * */
    @MyLog(value = "字段表数据删除")// 这里添加了AOP的自定义注解
    @RequestMapping(value = "/deleteFieldInformation")
    public void deleteFieldInformation(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	try {
        	int delete =	baseService.deleteByBatis(MapperNSEnum.BasicManagementMaper, "removeFieldInformation", params);
        	 
            if(delete>0) {
         	   resuleMap.put("code", 200);
          	  resuleMap.put("errorMsg","删除成功");
            } else {
         	   resuleMap.put("code", -1);
         	  resuleMap.put("errorMsg","删除失败");
            }
			} catch (Exception e) {
				e.printStackTrace();
				resuleMap.put("code", -2);
				resuleMap.put("errorMsg","系统错误");
			}
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
/**
 	* 核查指标与字段表 -2020.08.06 cc
 **/
    @MyLog(value = "查询并获取核查指标与字段表的数据")// 这里添加了AOP的自定义注解    
@RequestMapping(value = "/getFormMatchList")
public void getFormMatchList(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
    //System.out.println(params);
    
    PageInfo<List<FieldInformation>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "findFormMatch",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
  
    	Map<String, Object> resuleMap=new HashMap<String, Object>();
    	resuleMap.put("rows", info.getList());
    	resuleMap.put("total", info.getTotal());
    ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
}    

    
    /**
     * 年度列表
     **/
    @RequestMapping(value = "/getYearTable")
    public void getYearTable(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {

    	//数据库连接  JDBCTemplate.getDataBaseTableJdbcTemplate() 
    	
    	PageInfo<Map<String, Object>> info =jdbcService.selectListBypage1(JDBCTemplate.getDataBaseTableJdbcTemplate(), "core_year", params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
     
        	Map<String, Object> resuleMap=new HashMap<String, Object>();
        	resuleMap.put("rows", info.getList());
        	resuleMap.put("total", info.getTotal());
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    /**
     * 年度新增
     * 
     * */
    @RequestMapping(value = "/addYearOne")
    public void addYearOne(@RequestParam Map<String, Object> params,HttpServletResponse response , HttpServletRequest request) {
        
        System.out.println(params);
        Map<String, Object> resuleMap=new HashMap<String, Object>();
        try {
        	
          List<Map<String, Object>> list =jdbcService.select(JDBCTemplate.getDataBaseTableJdbcTemplate(), "core_year", params,1,10);
        	if (list.size()>0) {
        		 resuleMap.put("code", -3);
        		 resuleMap.put("errorMsg","该年度已存在");
			}else {
				
			 int insert= 	jdbcService.add(JDBCTemplate.getDataBaseTableJdbcTemplate(),"core_year", params);
			//long insert =  baseService.insertByBatis(MapperNSEnum.BasicManagementMaper, "addYearOne", params);	        	
	            if(insert>0) {
	            	String dataBaseName="connotation_"+(String) params.get("year");
	                tc.createTableData(dataBaseName, response, request);
	            	/*jdbcService.createTableData(JDBCTemplate.getDataBaseTableJdbcTemplate(), dataBaseName, list1);*/
	            	resuleMap.put("code", 200);
	         	  resuleMap.put("errorMsg","新增成功并成功创建库表");
	            } else {
	         	   resuleMap.put("code", -1);
	         	  resuleMap.put("errorMsg","新增失败");
	            }
			}
      
		} catch (Exception e) {
			e.printStackTrace();
			resuleMap.put("code", -2);
			resuleMap.put("errorMsg","系统错误");
		}
  
        ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
	}
    
    
  //根据表编号查找对应的字段
  	@RequestMapping(value ="/selectCol")
  	public void selectCol(@RequestParam Map<String, Object> params, HttpServletResponse response, HttpServletRequest request) {
        PageInfo<List<FieldInformation>> info = baseService.selectListByBatis(MapperNSEnum.BasicManagementMaper, "getColumn",params, Integer.valueOf(params.get("page").toString()), Integer.valueOf(params.get("rows").toString()));
        
    	Map<String, Object> resuleMap=new HashMap<String, Object>();
    	resuleMap.put("rows", info.getList());
    	resuleMap.put("total", info.getTotal());
    ResponseUtil.outputJson(response, resuleMap , "yyyy-MM-dd HH:mm:ss");
  	}
}
