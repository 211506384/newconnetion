package com.quanyoo.www.newconnotation.sqlDataSource;

import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.jdbc.DataSourceBuilder;

import com.quanyoo.www.newconnotation.pojo.DbInfo;
import com.quanyoo.www.newconnotation.util.JDBCTemplate;

public class DataSourceUtil {
	static Properties pro = new Properties();
	static InputStream in = JDBCTemplate.class.getClassLoader().getResourceAsStream("db.properties");

	public static DbInfo getDbInfo(String year) {

		DbInfo dbInfo = new DbInfo();
		try {
			pro.load(in);// 加载properties配置文件
			String user = pro.getProperty(year+"_username");
			String password = pro.getProperty(year+"_password");
			String url = pro.getProperty(year+"_url");
			String driver = pro.getProperty(year+"_driver");
			dbInfo.setDriveClassName(driver);
			dbInfo.setUserName(user);
			dbInfo.setPassword(password);
			dbInfo.setUrl(url);
		} catch (Exception e) {
			// TODO: handle exception
			e.getStackTrace();
		}

		return dbInfo;
	}
	
    /**
     * 创建新的数据源，注意：此处只针对 MySQL 数据库
     */
    public static DataSource makeNewDataSource(DbInfo dbInfo){
        String url = dbInfo.getUrl();
        String driveClassName = StringUtils.isEmpty(dbInfo.getDriveClassName())? "com.mysql.cj.jdbc.Driver":dbInfo.getDriveClassName();
        return DataSourceBuilder.create().url(url)
                .driverClassName(driveClassName)
                .username(dbInfo.getUserName())
                .password(dbInfo.getPassword())
                .build();
    }



    /**
     * 根据数据库连接信息添加数据源到动态源中
     * @param key
     * @param dbInfo
     */
    public static void addDataSourceToDynamic(String key, DbInfo dbInfo){

    	DataSource dataSource=	getJdbcDataSource(key);
   	if(dataSource==null) {

        dataSource = makeNewDataSource(dbInfo);
   	}
        addDataSourceToDynamic(key,dataSource);
    }
    
    /**
     * 添加数据源到动态源中
     */
    public static void addDataSourceToDynamic(String key, DataSource dataSource){
        DynamicDataSource dynamicDataSource = SpringContextHolder.getContext().getBean(DynamicDataSource.class);
        dynamicDataSource.addDataSource(key,dataSource);
    }
    /**
     * 获取数据源
     */
    public static DataSource getJdbcDataSource(String key){

   	 DynamicDataSource dynamicDataSource = SpringContextHolder.getContext().getBean(DynamicDataSource.class);
   	   	DataSource dataSource=	dynamicDataSource.getDataSource(key);
   	   	return dataSource;
   	   	
    }
}