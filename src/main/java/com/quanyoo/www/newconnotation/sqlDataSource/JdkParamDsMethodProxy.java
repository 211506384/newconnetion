//package com.quanyoo.www.newconnotation.sqlDataSource;
//
//import java.lang.reflect.InvocationHandler;
//import java.lang.reflect.Method;
//import java.lang.reflect.Proxy;
//
//import com.quanyoo.www.newconnotation.pojo.DbInfo;
//
//public class JdkParamDsMethodProxy implements InvocationHandler {
//    // 代理对象及相应参数
//    private String dataSourceKey;
//    private DbInfo dbInfo;
//    private Object targetObject;
//    public JdkParamDsMethodProxy(Object targetObject, String dataSourceKey, DbInfo dbInfo) {
//        this.targetObject = targetObject;
//        this.dataSourceKey = dataSourceKey;
//        this.dbInfo = dbInfo;
//    }
//
//    @Override
//    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        //切换数据源
//        DataSourceUtil.addDataSourceToDynamic(dataSourceKey, dbInfo);
//        DynamicDataSourceContextHolder.setContextKey(dataSourceKey);
//        //调用方法
//        Object result = method.invoke(targetObject, args);
//        DynamicDataSourceContextHolder.removeContextKey();
//        return result;
//    }
//
//    /**
//     * 创建代理
//     */
//    public static Object createProxyInstance(Object targetObject, String dataSourceKey, DbInfo dbInfo) throws Exception {
//        return Proxy.newProxyInstance(targetObject.getClass().getClassLoader()
//                , targetObject.getClass().getInterfaces(), new JdkParamDsMethodProxy(targetObject, dataSourceKey, dbInfo));
//    }
//}