package com.quanyoo.www.newconnotation.util;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.alibaba.druid.support.json.JSONUtils;

@Component
public class SystemUtil implements ApplicationContextAware {

	private static ApplicationContext context = null;

	/**
	 * 获取不带"-"的uuid
	 *
	 * @return
	 */
	public static String getID() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * 从map获取值，若值不存在或者为null，为空字符串，返回null
	 *
	 * @param map
	 * @param key
	 * @return
	 */
	public static String getValue(Map<String, String> map, String key) {
		if(map != null) {
			return StringUtils.isBlank(map.getOrDefault(key, null)) ? null : map.getOrDefault(key, null);
		}else {
			return null;
		}
	}

	/**
	 * 用于模糊查询字符串左右加%号
	 *
	 * @param str
	 * @return
	 */
	public static String toFuzzyStr(String str) {
		return "%" + str + "%";
	}

	/*
	 * 格式化日期
	 *
	 * @param date
	 *
	 * @param formatString
	 *
	 * @return
	 */
	public static String format(Date date, String formatString) {
		SimpleDateFormat df = new SimpleDateFormat(formatString);
		return df.format(date);
	}

	// 加减n天后的日期
	public static Date calcDate(Date basicDate, int n) {
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(basicDate);
		rightNow.add(Calendar.DATE, n);
		Date result = rightNow.getTime();
		return result;
	}
//将字符串时间转为date
	public static Date shift(String date) throws ParseException {
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat format = null;
		if (date.length() == 10) {
			format = new SimpleDateFormat("yyyy-MM-dd");
		} else {
			format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
		Date data1 = format.parse(date);
		return data1;
	}
	//时间计算
    public static List<String> findDates(String stime, String etime)
            throws ParseException {
        List<String> allDate = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date dBegin = sdf.parse(stime);
        Date dEnd = sdf.parse(etime);
        allDate.add(sdf.format(dBegin));
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            allDate.add(sdf.format(calBegin.getTime()));
        }
        return allDate;
    }

    public static Integer date(String stime,String etime ) {
//    	 String stime = "2019-05-01";
//         String etime = "2019-05-05";
         List<String> list = new ArrayList<>();
         try {
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
             long s = Long.valueOf(sdf.parse(stime).getTime());
             long e = Long.valueOf(sdf.parse(etime).getTime());
             //只有结束时间大于开始时间时才进行查询
             if(s<e) {
                 list = findDates(stime, etime);
             }
         } catch (ParseException e) {
             e.printStackTrace();
         }
//         for(String time : list) {
//             System.out.println(time);
//         }
         System.out.println("间隔天数：" + list.size());
		return list.size();

	}

	// MD5加密
	public static String MD5(String key) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		try {
			byte[] btInput = key.getBytes();
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		SystemUtil.context = context;
	}

	public static ApplicationContext getContext() {
		return context;
	}

    /// <summary>
    /// 把字符串转成帕斯卡命名法的规则,帕斯卡命名法是在命名的时候将首字母大写
    /// 1. 对于常规属性  <propertyName> ， 属性名称的第一个单词小写且字母个数大于1，第二个单词首字母大写  。对应的getter/setter方法名为：get /set +  <PropertyName>()， 即属性名称的第一个单词的首字母改成大写， 前面再加上"get"或"set"前缀。
    /// 2. 对于布尔类型 <propertyName> ， 可以按常规属性的规则编写getter/setter方法外， getter方法可以使用 is +  <PropertyName>()的形式来代替。
    /// 3. 对于非常规属性<pName>， 属性名称的第一个单词小写且字母个数等于1，第二个单词首字母大写  。
    /// </summary>
    /// <param name="column">需要转换的字符串</param>
    /// <returns></returns>
	public static String camel(String column, String splitChar, Boolean toLower) {
		StringBuilder strBder = new StringBuilder();
		String[] arr = column.split(splitChar);
		if (arr != null) {
			if (arr.length == 1) {
				if (arr[0].length() > 2)
					strBder.append(arr[0].substring(0, 1).toUpperCase() + (toLower ? arr[0].substring(1).toLowerCase() : arr[0].substring(1)));
				else
					strBder.append(arr[0].toUpperCase());
			} else {
				for (int i = 0; i < arr.length; i++) {
					if (i == 0) {
						if (arr[i].length() == 1) {
							strBder.append(arr[i].toLowerCase());
						} else {
							strBder.append(arr[i].substring(0, 1).toUpperCase() + (toLower ? arr[i].substring(1).toLowerCase() : arr[i].substring(1)));
						}
					} else {
						strBder.append(arr[i].substring(0, 1).toUpperCase() + (toLower ? arr[i].substring(1).toLowerCase() : arr[i].substring(1)));
					}
				}
			}
		}
		return strBder.toString();
	}

	public static Integer getReqPageNow(HttpServletRequest request, Integer defValue) {
		String pageNowStr = request.getParameter("pageNow");
		if (StringUtils.isNotBlank(pageNowStr))
			return Integer.valueOf(pageNowStr);
		else
			return defValue;
	}

	public static Integer getReqPageIndex(HttpServletRequest request, Integer defValue) {
		String pageIndexStr = request.getParameter("pageIndex");
		if (StringUtils.isNotBlank(pageIndexStr))
			return Integer.valueOf(pageIndexStr);
		else
			return defValue;
	}

	public static Integer getReqPageSize(HttpServletRequest request, Integer defValue) {
		String pageSizeStr = request.getParameter("pageSize");
		if (StringUtils.isNotBlank(pageSizeStr))
			return Integer.valueOf(pageSizeStr);
		else
			return defValue;
	}
	/**
    *
    * Map转String
    * @param map
    * @return
    */
   public static String getMapToString(Map<String,Object> map){
	 String string =  JSONUtils.toJSONString(map); 
	 	return string;
   }
	  public static boolean ifInclude(List<String> list,String str){
	         for(int i=0;i<list.size();i++){
	                if (list.get(i).indexOf(str)!=-1) {
	                    return true;
	                }
	            }
	            return false;
	        }
}
