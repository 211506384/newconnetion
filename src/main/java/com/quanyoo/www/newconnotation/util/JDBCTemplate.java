package com.quanyoo.www.newconnotation.util;

import java.io.InputStream;
import java.util.Properties;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class JDBCTemplate {

	static Properties pro = new Properties();
	static InputStream in = JDBCTemplate.class.getClassLoader().getResourceAsStream("db.properties");

	public static JdbcTemplate getJdbcTemplate(String year) {
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		try {
			
			pro.load(in);// 加载properties配置文件
			String user = pro.getProperty(year+"_username");
			String password = pro.getProperty(year+"_password");
			String url = pro.getProperty(year+"_url");
			String driver = pro.getProperty(year+"_driver");
			dataSource.setDriverClassName(driver);
			dataSource.setUrl(url);
			dataSource.setUsername(user);
			dataSource.setPassword(password);
		} catch (Exception e) {
			// TODO: handle exception
			e.getStackTrace();
		}
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		return jdbcTemplate;
	}
	
	public static JdbcTemplate getDataBaseTableJdbcTemplate() {
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		try {
			
			pro.load(in);// 加载properties配置文件
			String user = pro.getProperty("aop_username");
			String password = pro.getProperty("aop_password");
			String url = pro.getProperty("aop_url");
			String driver = pro.getProperty("aop_driver");
			dataSource.setDriverClassName(driver);
			dataSource.setUrl(url);
			dataSource.setUsername(user);
			dataSource.setPassword(password);
		} catch (Exception e) {
			// TODO: handle exception
			e.getStackTrace();
		}
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		return jdbcTemplate;
	}
}