package com.quanyoo.www.newconnotation.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * 返回结果工具
 *
 */
public class ResponseUtil {

	/**
	 * 按json格式返回值
	 * @param response
	 * @param object
	 */
	public static void outputJson(HttpServletResponse response,Object object){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		try {
			response.getWriter().write(JSON.toJSONString(object,SerializerFeature.DisableCircularReferenceDetect));
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 按json格式返回值(含有date格式)
	 * @param response
	 * @param object
 	 * @param dateType
	 */
	public static void outputJson(HttpServletResponse response,Object object,String dateType){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
	  	try{
			PrintWriter out = response.getWriter();
			out.write(JSON.toJSONStringWithDateFormat(object, dateType));
			out.flush();
			out.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * 返回错误json格式
	 * @param response
	 * @param errorMessage 错误描述消息
	 */
	public static void outputJson(HttpServletResponse response,String errorCode,String errorMessage){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		try {
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("errcode", errorCode);
			result.put("errmsg", errorMessage);
			response.getWriter().write(JSON.toJSONString(result));
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 返回错误json格式
	 * @param response
	 * @param errorMessage 错误描述消息
	 */
	public static void outputErrorJson(HttpServletResponse response,String errorMessage){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		try {
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("errcode", -1);
			result.put("errmsg", errorMessage);
			response.getWriter().write(JSON.toJSONString(result));
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * 返回成功json格式
	 * @param response
	 * @param errorMessage 错误描述消息
	 */
	public static void outputSuccessJson(HttpServletResponse response,String errorMessage){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		try {
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("errcode", 0);
			result.put("errmsg", errorMessage);
			response.getWriter().write(JSON.toJSONString(result));
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 返回成功json格式
	 * @param response
	 * @param key 错误描述消息
	 */
	public static void outputSuccessJson(HttpServletResponse response,String key, Object data){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		try {
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("errcode", 0);
			result.put("errmsg", "");
			result.put(key, data);
			response.getWriter().write(JSON.toJSONString(result));
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
