package com.quanyoo.www.newconnotation.util;

import java.io.BufferedReader;
import java.io.Reader;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeUtil {

	public static int getTime(String time) {
		String year1[] = time.split("-");
		int year =Integer.valueOf(year1[0]);
		int month =Integer.valueOf(year1[1]);
		int day = Integer.valueOf(year1[2]);
		int  sum =0;
		int ryear =0;
		int sums =365;

		int months[] = {0,31,59,90,120,151,181,212,243,273,304,334} ;
			if (0 < month&&month <= 12) {
					sum = months[month - 1];
			}else{
			};
			  sum += day;
			if( (year % 400 == 0)  ||((year % 4 == 0) && (year % 100 != 0)))
			{
				ryear = 1	;
			}
			if (ryear == 1 &&month > 2){
				 sum += 1;
				 sums +=1;
			}

		return sums-sum;
	}


public static String ClobToString(Clob clob)  {
        Reader is;
        String reString ="";
		try {
			is = clob.getCharacterStream();
			 BufferedReader br = new BufferedReader(is);
		        String s = br.readLine();
		        StringBuffer sb = new StringBuffer();
		        while (s != null) {
		            sb.append(s);
		            s = br.readLine();
		        }
		        reString = sb.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "";
			//e.printStackTrace();
		}
        return reString;
	}
public static String getPrice(String price)  {

	String string ="0.00";
	try {
		String pattern = "(\\d+(\\.\\d+)?)";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(price);
		ArrayList<String> arrayList = new ArrayList<>();
		while (m.find()) {
			arrayList.add(m.group());
			  }
		if(arrayList.isEmpty()) {
			arrayList.add("0.00");
		}
		string = arrayList.get(0);
	} catch (Exception e) {
		// TODO: handle exception
		return "0.00";
	}
    return string;
	}
}
