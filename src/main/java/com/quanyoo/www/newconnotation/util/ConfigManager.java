package com.quanyoo.www.newconnotation.util;

import com.quanyoo.www.newconnotation.pojo.SysLog;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
/**
    * @param
    * @描述 ConfigManager类
    * @return
    * @author WuRongPeng
    * @date 2020/8/7 10:53
*/
public class ConfigManager {
    private static ConfigManager configManager;
    private static Properties properties;

    private ConfigManager() {
        String configFile = "db.properties";
        properties = new Properties();
//类.class：获取当前类的class对象，即“类的类”
//.getClassLoader():获取类加载器，从类的字符文件
        InputStream in = ConfigManager.class.getClassLoader()
                .getResourceAsStream(configFile);
        try {
            properties.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //单例模式：采用static修饰构造方法，整个项目运行中只有一个ConfigManager实例，所有方法调用的ConfigManager都是同一个
    public static ConfigManager getInstance() {
        //
        if (configManager == null) {
            configManager = new ConfigManager();

        }
        return configManager;
    }
    //传入key，通过Properties.getProperty(Stirng key)方法获取key对应的value值
    public String getValue(String key) {
        return properties.getProperty(key);
    }
/*    //建立连接Connection
    public void getConn() throws ClassNotFoundException, SQLException {
        String driver = ConfigManager.getInstance().getValue(
                "jdbc.driver_class");
        String url = ConfigManager.getInstance()
                .getValue("url");
        String userName = ConfigManager.getInstance().getValue(
                " username");
        String password = ConfigManager.getInstance().getValue(
                " password");
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, userName, password);
    }*/
    // 添加一个一条log
    public static int saveDept(SysLog sysLog) throws ClassNotFoundException, SQLException {
        //声明Connection对象
        Connection con;
/*        //驱动程序名
        String driver = "com.mysql.cj.jdbc.Driver";
        //URL指向要访问的数据库名mydata
        String url = "jdbc:mysql://192.168.1.176:3306/connotation_core?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC";
        //MySQL配置时的用户名
        String user = "quanyooroot";
        //MySQL配置时的密码
        String password = "root123456";*/
        String driver = ConfigManager.getInstance().getValue("aop_driver");
        String url = ConfigManager.getInstance().getValue("aop_url");
        String userName = ConfigManager.getInstance().getValue("aop_username");
        String password = ConfigManager.getInstance().getValue("aop_password");
        //加载驱动程序
        Class.forName(driver);
        //1.getConnection()方法，连接MySQL数据库！！
        con = DriverManager.getConnection(url,userName,password);
        // 3.创建用来执行SQL语句的Statement对象
        Statement stmt = con.createStatement();
        // 4.执行SQL语句
        String sql = "insert into user_login_record(job_number,full_name,user_name,department,login_time,login_area) values('"+sysLog.getJobNum()+"','"+sysLog.getUsername()+"','"+sysLog.getUsername()+"','"+sysLog.getDept()+"','"+sysLog.getCreateDate()+"','"+sysLog.getOperation()+"')";
        //增删改：executeUpdate(String sql)==>int，影响的行数
        int rows = stmt.executeUpdate(sql);
        // 6.释放资源
        if (stmt != null) {
            stmt.close();
        }
        if (con != null) {
            con.close();
        }
        return rows;
    }
}
