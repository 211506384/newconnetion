package com.quanyoo.www.newconnotation.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quanyoo.www.newconnotation.pojo.Menu;
import com.quanyoo.www.newconnotation.service.IBaseService;

import java.util.Comparator;

public class MenuUtil {

	/**
	 * 获取子节点
	 * 
	 * @param id      父节点id
	 * @param allMenu 所有菜单列表
	 * @return 每个根节点下，所有子菜单列表
	 */
	public static List<Menu> getChild(String id, List<Menu> allMenu) {
		// 子菜单
		List<Menu> childList = new ArrayList<Menu>();
		for (Menu nav : allMenu) {
			// 遍历所有节点，将所有菜单的父id与传过来的根节点的id比较
			// 相等说明：为该根节点的子节点。
			if (nav.getParentId().equals(id)) {
				childList.add(nav);
			}
		}
		// 递归
		for (Menu nav : childList) {
			nav.setChildren(getChild(nav.getId(), allMenu));
		}
		Collections.sort(childList, order());// 排序
		// 如果节点下没有子节点，返回一个空List（递归退出）
		if (childList.size() == 0) {
			return new ArrayList<Menu>();
		}
		return childList;
	}

	public static Comparator<Menu> order() {
		Comparator<Menu> comparator = new Comparator<Menu>() {
			@Override
			public int compare(Menu o1, Menu o2) {
				if (o1.getOrder() != o2.getOrder()) {
					return o1.getOrder() - o2.getOrder();
				}
				return 0;
			}
		};
		return comparator;
	}

	public static Map<String, Object> findTree(IBaseService baseService,Integer parentMenuId ) {
		Map<String, Object> data = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("parentMenuId", parentMenuId);
		List<Menu> allMenu = new ArrayList<Menu>();
		try {// 查询所有菜单
			if(parentMenuId==0) {
				allMenu = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "findParentMenuListAll", params);
				
			}else {
				allMenu = baseService.selectListByBatis(MapperNSEnum.LoginMaper, "findMenuListByparentId", params);
				
			}
			
			// 根节点
			List<Menu> rootMenu = new ArrayList<Menu>();
			for (Menu nav : allMenu) {
				if (nav.getParentId().equals(parentMenuId.toString())) {// 父节点是0的，为根节点。
					rootMenu.add(nav);
				}
			}
			/* 根据Menu类的order排序 */
			Collections.sort(rootMenu, order());
			// 为根菜单设置子菜单，getClild是递归调用的
			for (Menu nav : rootMenu) {
				/* 获取根节点下的所有子节点 使用getChild方法 */
				List<Menu> childList = getChild(nav.getId(), allMenu);
				nav.setChildren(childList);// 给根节点设置子节点
			}
			/**
			 * 输出构建好的菜单数据。
			 *
			 */
			data.put("success", "true");
			data.put("list", rootMenu);
			return data;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			data.put("success", "false");
			data.put("list", new ArrayList());
			return data;
		}
	}
	

	

	public static Map<String, Object> findTreeInit(String parentMenuId ) {
		Map<String, Object> data = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("parentMenuId", parentMenuId);
		List<Menu> allMenu = new ArrayList<Menu>();
		Menu menu=new Menu();
		//表名翻译管理 PastYear/tname_explain
		//表字段翻译管理PastYear/field_explain

		menu.setId("7"); 
		menu.setText("表名翻译管理");
		menu.setUrl("PastYear/tname_explain");
		menu.setParentId("init");
		menu.setOrderweight(7);;
		allMenu.add(menu);
		menu=new Menu();
		menu.setId("8"); 
		menu.setText("表字段翻译管理");
		menu.setUrl("PastYear/field_explain");
		menu.setParentId("init");
		menu.setOrderweight(8);;
		allMenu.add(menu);
		menu=new Menu();
		menu.setId("9"); 
		menu.setText("导入数据");
		menu.setUrl("excelImport/exportExcel");
		menu.setParentId("init");
		menu.setOrderweight(9);;
		allMenu.add(menu);
		menu=new Menu();
		menu.setId("init"); 
		menu.setText("数据初始化");
		menu.setUrl("#");
		menu.setParentId("root");
		menu.setOrderweight(9);;
		allMenu.add(menu);
		
		
			// 根节点
		try {
			List<Menu> rootMenu = new ArrayList<Menu>();
			for (Menu nav : allMenu) {
				if (nav.getParentId().equals(parentMenuId)) {// 父节点是0的，为根节点。
					rootMenu.add(nav);
				}
			}
			/* 根据Menu类的order排序 */
			Collections.sort(rootMenu, order());
			// 为根菜单设置子菜单，getClild是递归调用的
			for (Menu nav : rootMenu) {
				/* 获取根节点下的所有子节点 使用getChild方法 */
				List<Menu> childList = getChild(nav.getId(), allMenu);
				nav.setChildren(childList);// 给根节点设置子节点
			}
			/**
			 * 输出构建好的菜单数据。
			 *
			 */
			data.put("success", "true");
			data.put("list", rootMenu);
			return data;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			data.put("success", "false");
			data.put("list", new ArrayList());
			return data;
		}
	}
	
}
