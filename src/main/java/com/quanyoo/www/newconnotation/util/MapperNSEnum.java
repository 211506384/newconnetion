package com.quanyoo.www.newconnotation.util;

public enum MapperNSEnum {
	// LoginMaper登入
	LoginMaper("com.quanyoo.www.newconnotation.LoginDao"),
	// basicManagementMaper基础管理
	BasicManagementMaper("com.quanyoo.www.newconnotation.BasicManagementDao"),

	// basicManagementMaper基础管理
	UserManagementMaper("com.quanyoo.www.newconnotation.UserManagementDao"),

	// disciplineConstructionMaper学科建设管理
	disciplineConstructionMaper("com.quanyoo.www.newconnotation.disciplineConstructionDao"),

	DBMaper("com.quanyoo.www.newconnotation.DBDao"),

	// basicManagementMaper基础管理
	InnovationteamMaper("com.quanyoo.www.newconnotation.InnovationteamDao"),
			

	// TableMaper登入
	TableMaper("com.quanyoo.www.newconnotation.TableDao"),

	//TranslateMaper表名、字段翻译
	TranslateMaper("com.quanyoo.www.newconnotation.TranslateDao"),

	//统计管理模块
	TotalManageMaper("com.quanyoo.www.newconnotation.TotalManageDao"),
	;
	private String nsName = "";

	private MapperNSEnum(String nsName) {
		this.nsName = nsName;
	}

	public String getNSName() {
		return nsName;
	}
}
