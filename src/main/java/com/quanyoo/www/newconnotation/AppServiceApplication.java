package com.quanyoo.www.newconnotation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@EnableTransactionManagement // 开启事务管理
@EnableCaching // 开启缓存
@ComponentScan
@EnableScheduling // SpringBoot定时任务
@ServletComponentScan // 扫描session监听器
@MapperScan("com.quanyoo.www.newconnotation.dao")
public class AppServiceApplication extends SpringBootServletInitializer {// extends SpringBootServletInitializer

	public static void main(String[] args) {
		SpringApplication.run(AppServiceApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AppServiceApplication.class);
	}

}
