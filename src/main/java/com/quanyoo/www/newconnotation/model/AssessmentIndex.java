package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the assessment_index database table.
 * 
 */
@Entity
@Table(name="assessment_index")
@NamedQuery(name="AssessmentIndex.findAll", query="SELECT a FROM AssessmentIndex a")
public class AssessmentIndex implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="assessment_classification")
	private String assessmentClassification;

	@Column(name="f_explain")
	private String fExplain;

	private String firstchar;

	@Column(name="five_level")
	private String fiveLevel;

	@Column(name="fourth_level")
	private String fourthLevel;

	private String grade;

	@Column(name="id_assessment_index")
	private String idAssessmentIndex;

	private String name;

	@Column(name="responsible_unit")
	private String responsibleUnit;

	private String score;

	@Column(name="second_lever")
	private String secondLever;

	@Column(name="serial_number")
	private int serialNumber;

	@Column(name="third_level")
	private String thirdLevel;

	@Column(name="unit_measurement")
	private String unitMeasurement;
	
	@Column(name="main_table_name")
	private String mainTableName;
	@Column(name="adopt_table_name")
	private String adoptTableName;
	
	public String getfExplain() {
		return fExplain;
	}

	public void setfExplain(String fExplain) {
		this.fExplain = fExplain;
	}

	public String getAdoptTableName() {
		return adoptTableName;
	}

	public void setAdoptTableName(String adoptTableName) {
		this.adoptTableName = adoptTableName;
	}

	public String getMainTableName() {
		return mainTableName;
	}

	public void setMainTableName(String mainTableName) {
		this.mainTableName = mainTableName;
	}

	public AssessmentIndex() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAssessmentClassification() {
		return this.assessmentClassification;
	}

	public void setAssessmentClassification(String assessmentClassification) {
		this.assessmentClassification = assessmentClassification;
	}

	public String getFExplain() {
		return this.fExplain;
	}

	public void setFExplain(String fExplain) {
		this.fExplain = fExplain;
	}

	public String getFirstchar() {
		return this.firstchar;
	}

	public void setFirstchar(String firstchar) {
		this.firstchar = firstchar;
	}

	public String getFiveLevel() {
		return this.fiveLevel;
	}

	public void setFiveLevel(String fiveLevel) {
		this.fiveLevel = fiveLevel;
	}

	public String getFourthLevel() {
		return this.fourthLevel;
	}

	public void setFourthLevel(String fourthLevel) {
		this.fourthLevel = fourthLevel;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getIdAssessmentIndex() {
		return this.idAssessmentIndex;
	}

	public void setIdAssessmentIndex(String idAssessmentIndex) {
		this.idAssessmentIndex = idAssessmentIndex;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResponsibleUnit() {
		return this.responsibleUnit;
	}

	public void setResponsibleUnit(String responsibleUnit) {
		this.responsibleUnit = responsibleUnit;
	}

	public String getScore() {
		return this.score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getSecondLever() {
		return this.secondLever;
	}

	public void setSecondLever(String secondLever) {
		this.secondLever = secondLever;
	}

	public int getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getThirdLevel() {
		return this.thirdLevel;
	}

	public void setThirdLevel(String thirdLevel) {
		this.thirdLevel = thirdLevel;
	}

	public String getUnitMeasurement() {
		return this.unitMeasurement;
	}

	public void setUnitMeasurement(String unitMeasurement) {
		this.unitMeasurement = unitMeasurement;
	}

}