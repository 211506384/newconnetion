package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the core_test database table.
 * 
 */
@Entity
@Table(name="core_test")
@NamedQuery(name="CoreTest.findAll", query="SELECT c FROM CoreTest c")
public class CoreTest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="department_score")
	private String departmentScore;

	@Column(name="full_name")
	private String fullName;

	@Column(name="job_number")
	private int jobNumber;

	private String jurisdiction;

	@Column(name="password_score")
	private String passwordScore;

	public CoreTest() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDepartmentScore() {
		return this.departmentScore;
	}

	public void setDepartmentScore(String departmentScore) {
		this.departmentScore = departmentScore;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getJurisdiction() {
		return this.jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getPasswordScore() {
		return this.passwordScore;
	}

	public void setPasswordScore(String passwordScore) {
		this.passwordScore = passwordScore;
	}

}