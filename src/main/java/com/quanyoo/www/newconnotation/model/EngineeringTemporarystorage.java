package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the engineering_temporarystorage database table.
 * 
 */
@Entity
@Table(name="engineering_temporarystorage")
@NamedQuery(name="EngineeringTemporarystorage.findAll", query="SELECT e FROM EngineeringTemporarystorage e")
public class EngineeringTemporarystorage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="address_supporting_materials")
	private String addressSupportingMaterials;

	@Column(name="affiliated_unit")
	private String affiliatedUnit;

	@Column(name="approval_scoreof_first")
	private String approvalScoreofFirst;

	@Column(name="approval_scoreof_second")
	private String approvalScoreofSecond;

	@Column(name="audit_status")
	private String auditStatus;

	@Column(name="award_approval_number")
	private String awardApprovalNumber;

	@Column(name="award_level")
	private String awardLevel;

	@Column(name="award_winner_level")
	private String awardWinnerLevel;

	@Column(name="award_winner_name")
	private String awardWinnerName;

	@Column(name="award_year")
	private String awardYear;

	@Column(name="awarding_unit")
	private String awardingUnit;

	private String college;

	@Column(name="comments_author")
	private String commentsAuthor;

	@Column(name="completion_unit")
	private String completionUnit;

	@Column(name="data_index")
	private String dataIndex;

	@Column(name="first_approval_level")
	private String firstApprovalLevel;

	@Column(name="first_completion")
	private String firstCompletion;

	@Column(name="first_level_discipline")
	private String firstLevelDiscipline;

	@Column(name="form_of_achievements")
	private String formOfAchievements;

	@Column(name="full_name")
	private String fullName;

	@Column(name="job_number")
	private String jobNumber;

	@Column(name="name_originator")
	private String nameOriginator;

	@Column(name="nameof_submitted")
	private String nameofSubmitted;

	@Column(name="nameof_uploader")
	private String nameofUploader;

	@Column(name="number_final_second")
	private String numberFinalSecond;

	@Column(name="number_initiating")
	private String numberInitiating;

	@Column(name="number_last_reviewer")
	private String numberLastReviewer;

	@Column(name="number_of_winners")
	private String numberOfWinners;

	@Column(name="other_finishers")
	private String otherFinishers;

	@Column(name="personnel_final_second")
	private String personnelFinalSecond;

	@Column(name="personnel_last")
	private String personnelLast;

	@Column(name="project_source")
	private String projectSource;

	private String remarks;

	@Column(name="remarks_first")
	private String remarksFirst;

	@Column(name="remarks_second")
	private String remarksSecond;

	@Column(name="return_remarks")
	private String returnRemarks;

	@Column(name="return_time")
	private String returnTime;

	@Column(name="returned_receivername")
	private String returnedReceivername;

	@Column(name="returned_receivernum")
	private String returnedReceivernum;

	@Column(name="reward_category")
	private String rewardCategory;

	@Column(name="second_approval_level")
	private String secondApprovalLevel;

	@Column(name="subject_category")
	private String subjectCategory;

	@Column(name="submission_time")
	private String submissionTime;

	@Column(name="submitted_jobno")
	private String submittedJobno;

	@Column(name="submitted_number")
	private int submittedNumber;

	@Column(name="the_first_type_of_adult")
	private String theFirstTypeOfAdult;

	@Column(name="time_final_second")
	private String timeFinalSecond;

	@Column(name="time_last_trial")
	private String timeLastTrial;

	@Column(name="title_of_achievements")
	private String titleOfAchievements;

	@Column(name="total_number_participating_units")
	private String totalNumberParticipatingUnits;

	@Column(name="unit_ranking")
	private String unitRanking;

	@Column(name="upload_time")
	private String uploadTime;

	@Column(name="uploader_notes")
	private String uploaderNotes;

	@Column(name="uploader_num")
	private String uploaderNum;

	private String winner;

	public EngineeringTemporarystorage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressSupportingMaterials() {
		return this.addressSupportingMaterials;
	}

	public void setAddressSupportingMaterials(String addressSupportingMaterials) {
		this.addressSupportingMaterials = addressSupportingMaterials;
	}

	public String getAffiliatedUnit() {
		return this.affiliatedUnit;
	}

	public void setAffiliatedUnit(String affiliatedUnit) {
		this.affiliatedUnit = affiliatedUnit;
	}

	public String getApprovalScoreofFirst() {
		return this.approvalScoreofFirst;
	}

	public void setApprovalScoreofFirst(String approvalScoreofFirst) {
		this.approvalScoreofFirst = approvalScoreofFirst;
	}

	public String getApprovalScoreofSecond() {
		return this.approvalScoreofSecond;
	}

	public void setApprovalScoreofSecond(String approvalScoreofSecond) {
		this.approvalScoreofSecond = approvalScoreofSecond;
	}

	public String getAuditStatus() {
		return this.auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getAwardApprovalNumber() {
		return this.awardApprovalNumber;
	}

	public void setAwardApprovalNumber(String awardApprovalNumber) {
		this.awardApprovalNumber = awardApprovalNumber;
	}

	public String getAwardLevel() {
		return this.awardLevel;
	}

	public void setAwardLevel(String awardLevel) {
		this.awardLevel = awardLevel;
	}

	public String getAwardWinnerLevel() {
		return this.awardWinnerLevel;
	}

	public void setAwardWinnerLevel(String awardWinnerLevel) {
		this.awardWinnerLevel = awardWinnerLevel;
	}

	public String getAwardWinnerName() {
		return this.awardWinnerName;
	}

	public void setAwardWinnerName(String awardWinnerName) {
		this.awardWinnerName = awardWinnerName;
	}

	public String getAwardYear() {
		return this.awardYear;
	}

	public void setAwardYear(String awardYear) {
		this.awardYear = awardYear;
	}

	public String getAwardingUnit() {
		return this.awardingUnit;
	}

	public void setAwardingUnit(String awardingUnit) {
		this.awardingUnit = awardingUnit;
	}

	public String getCollege() {
		return this.college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCommentsAuthor() {
		return this.commentsAuthor;
	}

	public void setCommentsAuthor(String commentsAuthor) {
		this.commentsAuthor = commentsAuthor;
	}

	public String getCompletionUnit() {
		return this.completionUnit;
	}

	public void setCompletionUnit(String completionUnit) {
		this.completionUnit = completionUnit;
	}

	public String getDataIndex() {
		return this.dataIndex;
	}

	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}

	public String getFirstApprovalLevel() {
		return this.firstApprovalLevel;
	}

	public void setFirstApprovalLevel(String firstApprovalLevel) {
		this.firstApprovalLevel = firstApprovalLevel;
	}

	public String getFirstCompletion() {
		return this.firstCompletion;
	}

	public void setFirstCompletion(String firstCompletion) {
		this.firstCompletion = firstCompletion;
	}

	public String getFirstLevelDiscipline() {
		return this.firstLevelDiscipline;
	}

	public void setFirstLevelDiscipline(String firstLevelDiscipline) {
		this.firstLevelDiscipline = firstLevelDiscipline;
	}

	public String getFormOfAchievements() {
		return this.formOfAchievements;
	}

	public void setFormOfAchievements(String formOfAchievements) {
		this.formOfAchievements = formOfAchievements;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getNameOriginator() {
		return this.nameOriginator;
	}

	public void setNameOriginator(String nameOriginator) {
		this.nameOriginator = nameOriginator;
	}

	public String getNameofSubmitted() {
		return this.nameofSubmitted;
	}

	public void setNameofSubmitted(String nameofSubmitted) {
		this.nameofSubmitted = nameofSubmitted;
	}

	public String getNameofUploader() {
		return this.nameofUploader;
	}

	public void setNameofUploader(String nameofUploader) {
		this.nameofUploader = nameofUploader;
	}

	public String getNumberFinalSecond() {
		return this.numberFinalSecond;
	}

	public void setNumberFinalSecond(String numberFinalSecond) {
		this.numberFinalSecond = numberFinalSecond;
	}

	public String getNumberInitiating() {
		return this.numberInitiating;
	}

	public void setNumberInitiating(String numberInitiating) {
		this.numberInitiating = numberInitiating;
	}

	public String getNumberLastReviewer() {
		return this.numberLastReviewer;
	}

	public void setNumberLastReviewer(String numberLastReviewer) {
		this.numberLastReviewer = numberLastReviewer;
	}

	public String getNumberOfWinners() {
		return this.numberOfWinners;
	}

	public void setNumberOfWinners(String numberOfWinners) {
		this.numberOfWinners = numberOfWinners;
	}

	public String getOtherFinishers() {
		return this.otherFinishers;
	}

	public void setOtherFinishers(String otherFinishers) {
		this.otherFinishers = otherFinishers;
	}

	public String getPersonnelFinalSecond() {
		return this.personnelFinalSecond;
	}

	public void setPersonnelFinalSecond(String personnelFinalSecond) {
		this.personnelFinalSecond = personnelFinalSecond;
	}

	public String getPersonnelLast() {
		return this.personnelLast;
	}

	public void setPersonnelLast(String personnelLast) {
		this.personnelLast = personnelLast;
	}

	public String getProjectSource() {
		return this.projectSource;
	}

	public void setProjectSource(String projectSource) {
		this.projectSource = projectSource;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarksFirst() {
		return this.remarksFirst;
	}

	public void setRemarksFirst(String remarksFirst) {
		this.remarksFirst = remarksFirst;
	}

	public String getRemarksSecond() {
		return this.remarksSecond;
	}

	public void setRemarksSecond(String remarksSecond) {
		this.remarksSecond = remarksSecond;
	}

	public String getReturnRemarks() {
		return this.returnRemarks;
	}

	public void setReturnRemarks(String returnRemarks) {
		this.returnRemarks = returnRemarks;
	}

	public String getReturnTime() {
		return this.returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getReturnedReceivername() {
		return this.returnedReceivername;
	}

	public void setReturnedReceivername(String returnedReceivername) {
		this.returnedReceivername = returnedReceivername;
	}

	public String getReturnedReceivernum() {
		return this.returnedReceivernum;
	}

	public void setReturnedReceivernum(String returnedReceivernum) {
		this.returnedReceivernum = returnedReceivernum;
	}

	public String getRewardCategory() {
		return this.rewardCategory;
	}

	public void setRewardCategory(String rewardCategory) {
		this.rewardCategory = rewardCategory;
	}

	public String getSecondApprovalLevel() {
		return this.secondApprovalLevel;
	}

	public void setSecondApprovalLevel(String secondApprovalLevel) {
		this.secondApprovalLevel = secondApprovalLevel;
	}

	public String getSubjectCategory() {
		return this.subjectCategory;
	}

	public void setSubjectCategory(String subjectCategory) {
		this.subjectCategory = subjectCategory;
	}

	public String getSubmissionTime() {
		return this.submissionTime;
	}

	public void setSubmissionTime(String submissionTime) {
		this.submissionTime = submissionTime;
	}

	public String getSubmittedJobno() {
		return this.submittedJobno;
	}

	public void setSubmittedJobno(String submittedJobno) {
		this.submittedJobno = submittedJobno;
	}

	public int getSubmittedNumber() {
		return this.submittedNumber;
	}

	public void setSubmittedNumber(int submittedNumber) {
		this.submittedNumber = submittedNumber;
	}

	public String getTheFirstTypeOfAdult() {
		return this.theFirstTypeOfAdult;
	}

	public void setTheFirstTypeOfAdult(String theFirstTypeOfAdult) {
		this.theFirstTypeOfAdult = theFirstTypeOfAdult;
	}

	public String getTimeFinalSecond() {
		return this.timeFinalSecond;
	}

	public void setTimeFinalSecond(String timeFinalSecond) {
		this.timeFinalSecond = timeFinalSecond;
	}

	public String getTimeLastTrial() {
		return this.timeLastTrial;
	}

	public void setTimeLastTrial(String timeLastTrial) {
		this.timeLastTrial = timeLastTrial;
	}

	public String getTitleOfAchievements() {
		return this.titleOfAchievements;
	}

	public void setTitleOfAchievements(String titleOfAchievements) {
		this.titleOfAchievements = titleOfAchievements;
	}

	public String getTotalNumberParticipatingUnits() {
		return this.totalNumberParticipatingUnits;
	}

	public void setTotalNumberParticipatingUnits(String totalNumberParticipatingUnits) {
		this.totalNumberParticipatingUnits = totalNumberParticipatingUnits;
	}

	public String getUnitRanking() {
		return this.unitRanking;
	}

	public void setUnitRanking(String unitRanking) {
		this.unitRanking = unitRanking;
	}

	public String getUploadTime() {
		return this.uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getUploaderNotes() {
		return this.uploaderNotes;
	}

	public void setUploaderNotes(String uploaderNotes) {
		this.uploaderNotes = uploaderNotes;
	}

	public String getUploaderNum() {
		return this.uploaderNum;
	}

	public void setUploaderNum(String uploaderNum) {
		this.uploaderNum = uploaderNum;
	}

	public String getWinner() {
		return this.winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

}