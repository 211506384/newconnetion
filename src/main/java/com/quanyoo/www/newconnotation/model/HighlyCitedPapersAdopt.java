package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the highly_cited_papers_adopt database table.
 * 
 */
@Entity
@Table(name="highly_cited_papers_adopt")
@NamedQuery(name="HighlyCitedPapersAdopt.findAll", query="SELECT h FROM HighlyCitedPapersAdopt h")
public class HighlyCitedPapersAdopt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="abstract_highly")
	private String abstractHighly;

	@Column(name="address_supporting_materials")
	private String addressSupportingMaterials;

	@Column(name="affiliated_unit")
	private String affiliatedUnit;

	@Column(name="all_authors")
	private String allAuthors;

	@Column(name="approval_scoreof_first")
	private String approvalScoreofFirst;

	@Column(name="approval_scoreof_second")
	private String approvalScoreofSecond;

	@Column(name="audit_status")
	private String auditStatus;

	private String category;

	@Column(name="collection_papers")
	private String collectionPapers;

	private String college;

	@Column(name="comments_author")
	private String commentsAuthor;

	@Column(name="corresponding_author")
	private String correspondingAuthor;

	@Column(name="data_index")
	private String dataIndex;

	@Column(name="date_publication")
	private String datePublication;

	private String doi;

	@Column(name="english_title")
	private String englishTitle;

	@Column(name="first_approval_level")
	private String firstApprovalLevel;

	@Column(name="first_author")
	private String firstAuthor;

	@Column(name="first_author_type")
	private String firstAuthorType;

	@Column(name="full_name")
	private String fullName;

	@Column(name="issue_page")
	private String issuePage;

	@Column(name="job_number")
	private String jobNumber;

	private String keyword;

	private String languages;

	@Column(name="meeting_address")
	private String meetingAddress;

	@Column(name="name_meeting")
	private String nameMeeting;

	@Column(name="name_originator")
	private String nameOriginator;

	@Column(name="nameof_submitted")
	private String nameofSubmitted;

	@Column(name="nameof_uploader")
	private String nameofUploader;

	@Column(name="number_final_second")
	private String numberFinalSecond;

	@Column(name="number_initiating")
	private String numberInitiating;

	@Column(name="number_last_reviewer")
	private String numberLastReviewer;

	@Column(name="number_words")
	private String numberWords;

	@Column(name="organizer_conference")
	private String organizerConference;

	@Column(name="other_authors")
	private String otherAuthors;

	private String page;

	@Column(name="personnel_final_second")
	private String personnelFinalSecond;

	@Column(name="personnel_last")
	private String personnelLast;

	@Column(name="project_source")
	private String projectSource;

	@Column(name="publication_papers")
	private String publicationPapers;

	@Column(name="record_number")
	private String recordNumber;

	private String remarks;

	@Column(name="remarks_first")
	private String remarksFirst;

	@Column(name="remarks_second")
	private String remarksSecond;

	@Column(name="reprint_paper")
	private String reprintPaper;

	@Column(name="return_remarks")
	private String returnRemarks;

	@Column(name="return_time")
	private String returnTime;

	@Column(name="returned_receivername")
	private String returnedReceivername;

	@Column(name="returned_receivernum")
	private String returnedReceivernum;

	@Column(name="reward_level")
	private String rewardLevel;

	@Column(name="school_signature")
	private String schoolSignature;

	@Column(name="scope_publication")
	private String scopePublication;

	@Column(name="second_approval_level")
	private String secondApprovalLevel;

	@Column(name="subject_category")
	private String subjectCategory;

	@Column(name="subject_classification")
	private String subjectClassification;

	@Column(name="submission_time")
	private String submissionTime;

	@Column(name="submitted_jobno")
	private String submittedJobno;

	@Column(name="submitted_number")
	private int submittedNumber;

	@Column(name="thesis_title")
	private String thesisTitle;

	@Column(name="time_final_second")
	private String timeFinalSecond;

	@Column(name="time_last_trial")
	private String timeLastTrial;

	@Column(name="types_papers")
	private String typesPapers;

	@Column(name="upload_time")
	private String uploadTime;

	@Column(name="uploader_notes")
	private String uploaderNotes;

	@Column(name="uploader_num")
	private String uploaderNum;

	public HighlyCitedPapersAdopt() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAbstractHighly() {
		return this.abstractHighly;
	}

	public void setAbstractHighly(String abstractHighly) {
		this.abstractHighly = abstractHighly;
	}

	public String getAddressSupportingMaterials() {
		return this.addressSupportingMaterials;
	}

	public void setAddressSupportingMaterials(String addressSupportingMaterials) {
		this.addressSupportingMaterials = addressSupportingMaterials;
	}

	public String getAffiliatedUnit() {
		return this.affiliatedUnit;
	}

	public void setAffiliatedUnit(String affiliatedUnit) {
		this.affiliatedUnit = affiliatedUnit;
	}

	public String getAllAuthors() {
		return this.allAuthors;
	}

	public void setAllAuthors(String allAuthors) {
		this.allAuthors = allAuthors;
	}

	public String getApprovalScoreofFirst() {
		return this.approvalScoreofFirst;
	}

	public void setApprovalScoreofFirst(String approvalScoreofFirst) {
		this.approvalScoreofFirst = approvalScoreofFirst;
	}

	public String getApprovalScoreofSecond() {
		return this.approvalScoreofSecond;
	}

	public void setApprovalScoreofSecond(String approvalScoreofSecond) {
		this.approvalScoreofSecond = approvalScoreofSecond;
	}

	public String getAuditStatus() {
		return this.auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCollectionPapers() {
		return this.collectionPapers;
	}

	public void setCollectionPapers(String collectionPapers) {
		this.collectionPapers = collectionPapers;
	}

	public String getCollege() {
		return this.college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCommentsAuthor() {
		return this.commentsAuthor;
	}

	public void setCommentsAuthor(String commentsAuthor) {
		this.commentsAuthor = commentsAuthor;
	}

	public String getCorrespondingAuthor() {
		return this.correspondingAuthor;
	}

	public void setCorrespondingAuthor(String correspondingAuthor) {
		this.correspondingAuthor = correspondingAuthor;
	}

	public String getDataIndex() {
		return this.dataIndex;
	}

	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}

	public String getDatePublication() {
		return this.datePublication;
	}

	public void setDatePublication(String datePublication) {
		this.datePublication = datePublication;
	}

	public String getDoi() {
		return this.doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public String getEnglishTitle() {
		return this.englishTitle;
	}

	public void setEnglishTitle(String englishTitle) {
		this.englishTitle = englishTitle;
	}

	public String getFirstApprovalLevel() {
		return this.firstApprovalLevel;
	}

	public void setFirstApprovalLevel(String firstApprovalLevel) {
		this.firstApprovalLevel = firstApprovalLevel;
	}

	public String getFirstAuthor() {
		return this.firstAuthor;
	}

	public void setFirstAuthor(String firstAuthor) {
		this.firstAuthor = firstAuthor;
	}

	public String getFirstAuthorType() {
		return this.firstAuthorType;
	}

	public void setFirstAuthorType(String firstAuthorType) {
		this.firstAuthorType = firstAuthorType;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getIssuePage() {
		return this.issuePage;
	}

	public void setIssuePage(String issuePage) {
		this.issuePage = issuePage;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getKeyword() {
		return this.keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getLanguages() {
		return this.languages;
	}

	public void setLanguages(String languages) {
		this.languages = languages;
	}

	public String getMeetingAddress() {
		return this.meetingAddress;
	}

	public void setMeetingAddress(String meetingAddress) {
		this.meetingAddress = meetingAddress;
	}

	public String getNameMeeting() {
		return this.nameMeeting;
	}

	public void setNameMeeting(String nameMeeting) {
		this.nameMeeting = nameMeeting;
	}

	public String getNameOriginator() {
		return this.nameOriginator;
	}

	public void setNameOriginator(String nameOriginator) {
		this.nameOriginator = nameOriginator;
	}

	public String getNameofSubmitted() {
		return this.nameofSubmitted;
	}

	public void setNameofSubmitted(String nameofSubmitted) {
		this.nameofSubmitted = nameofSubmitted;
	}

	public String getNameofUploader() {
		return this.nameofUploader;
	}

	public void setNameofUploader(String nameofUploader) {
		this.nameofUploader = nameofUploader;
	}

	public String getNumberFinalSecond() {
		return this.numberFinalSecond;
	}

	public void setNumberFinalSecond(String numberFinalSecond) {
		this.numberFinalSecond = numberFinalSecond;
	}

	public String getNumberInitiating() {
		return this.numberInitiating;
	}

	public void setNumberInitiating(String numberInitiating) {
		this.numberInitiating = numberInitiating;
	}

	public String getNumberLastReviewer() {
		return this.numberLastReviewer;
	}

	public void setNumberLastReviewer(String numberLastReviewer) {
		this.numberLastReviewer = numberLastReviewer;
	}

	public String getNumberWords() {
		return this.numberWords;
	}

	public void setNumberWords(String numberWords) {
		this.numberWords = numberWords;
	}

	public String getOrganizerConference() {
		return this.organizerConference;
	}

	public void setOrganizerConference(String organizerConference) {
		this.organizerConference = organizerConference;
	}

	public String getOtherAuthors() {
		return this.otherAuthors;
	}

	public void setOtherAuthors(String otherAuthors) {
		this.otherAuthors = otherAuthors;
	}

	public String getPage() {
		return this.page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getPersonnelFinalSecond() {
		return this.personnelFinalSecond;
	}

	public void setPersonnelFinalSecond(String personnelFinalSecond) {
		this.personnelFinalSecond = personnelFinalSecond;
	}

	public String getPersonnelLast() {
		return this.personnelLast;
	}

	public void setPersonnelLast(String personnelLast) {
		this.personnelLast = personnelLast;
	}

	public String getProjectSource() {
		return this.projectSource;
	}

	public void setProjectSource(String projectSource) {
		this.projectSource = projectSource;
	}

	public String getPublicationPapers() {
		return this.publicationPapers;
	}

	public void setPublicationPapers(String publicationPapers) {
		this.publicationPapers = publicationPapers;
	}

	public String getRecordNumber() {
		return this.recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarksFirst() {
		return this.remarksFirst;
	}

	public void setRemarksFirst(String remarksFirst) {
		this.remarksFirst = remarksFirst;
	}

	public String getRemarksSecond() {
		return this.remarksSecond;
	}

	public void setRemarksSecond(String remarksSecond) {
		this.remarksSecond = remarksSecond;
	}

	public String getReprintPaper() {
		return this.reprintPaper;
	}

	public void setReprintPaper(String reprintPaper) {
		this.reprintPaper = reprintPaper;
	}

	public String getReturnRemarks() {
		return this.returnRemarks;
	}

	public void setReturnRemarks(String returnRemarks) {
		this.returnRemarks = returnRemarks;
	}

	public String getReturnTime() {
		return this.returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getReturnedReceivername() {
		return this.returnedReceivername;
	}

	public void setReturnedReceivername(String returnedReceivername) {
		this.returnedReceivername = returnedReceivername;
	}

	public String getReturnedReceivernum() {
		return this.returnedReceivernum;
	}

	public void setReturnedReceivernum(String returnedReceivernum) {
		this.returnedReceivernum = returnedReceivernum;
	}

	public String getRewardLevel() {
		return this.rewardLevel;
	}

	public void setRewardLevel(String rewardLevel) {
		this.rewardLevel = rewardLevel;
	}

	public String getSchoolSignature() {
		return this.schoolSignature;
	}

	public void setSchoolSignature(String schoolSignature) {
		this.schoolSignature = schoolSignature;
	}

	public String getScopePublication() {
		return this.scopePublication;
	}

	public void setScopePublication(String scopePublication) {
		this.scopePublication = scopePublication;
	}

	public String getSecondApprovalLevel() {
		return this.secondApprovalLevel;
	}

	public void setSecondApprovalLevel(String secondApprovalLevel) {
		this.secondApprovalLevel = secondApprovalLevel;
	}

	public String getSubjectCategory() {
		return this.subjectCategory;
	}

	public void setSubjectCategory(String subjectCategory) {
		this.subjectCategory = subjectCategory;
	}

	public String getSubjectClassification() {
		return this.subjectClassification;
	}

	public void setSubjectClassification(String subjectClassification) {
		this.subjectClassification = subjectClassification;
	}

	public String getSubmissionTime() {
		return this.submissionTime;
	}

	public void setSubmissionTime(String submissionTime) {
		this.submissionTime = submissionTime;
	}

	public String getSubmittedJobno() {
		return this.submittedJobno;
	}

	public void setSubmittedJobno(String submittedJobno) {
		this.submittedJobno = submittedJobno;
	}

	public int getSubmittedNumber() {
		return this.submittedNumber;
	}

	public void setSubmittedNumber(int submittedNumber) {
		this.submittedNumber = submittedNumber;
	}

	public String getThesisTitle() {
		return this.thesisTitle;
	}

	public void setThesisTitle(String thesisTitle) {
		this.thesisTitle = thesisTitle;
	}

	public String getTimeFinalSecond() {
		return this.timeFinalSecond;
	}

	public void setTimeFinalSecond(String timeFinalSecond) {
		this.timeFinalSecond = timeFinalSecond;
	}

	public String getTimeLastTrial() {
		return this.timeLastTrial;
	}

	public void setTimeLastTrial(String timeLastTrial) {
		this.timeLastTrial = timeLastTrial;
	}

	public String getTypesPapers() {
		return this.typesPapers;
	}

	public void setTypesPapers(String typesPapers) {
		this.typesPapers = typesPapers;
	}

	public String getUploadTime() {
		return this.uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getUploaderNotes() {
		return this.uploaderNotes;
	}

	public void setUploaderNotes(String uploaderNotes) {
		this.uploaderNotes = uploaderNotes;
	}

	public String getUploaderNum() {
		return this.uploaderNum;
	}

	public void setUploaderNum(String uploaderNum) {
		this.uploaderNum = uploaderNum;
	}

}