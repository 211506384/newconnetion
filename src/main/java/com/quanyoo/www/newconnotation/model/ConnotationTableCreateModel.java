package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the connotation_table_create_model database table.
 * 
 */
@Entity
@Table(name="connotation_table_create_model")
@NamedQuery(name="ConnotationTableCreateModel.findAll", query="SELECT c FROM ConnotationTableCreateModel c")
public class ConnotationTableCreateModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="create_table_chinese_name")
	private String createTableChineseName;

	@Column(name="create_table_description")
	private String createTableDescription;

	@Column(name="create_table_english_name")
	private String createTableEnglishName;

	@Column(name="create_table_num")
	private int createTableNum;

	@Column(name="create_table_type")
	private String createTableType;

	public ConnotationTableCreateModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateTableChineseName() {
		return this.createTableChineseName;
	}

	public void setCreateTableChineseName(String createTableChineseName) {
		this.createTableChineseName = createTableChineseName;
	}

	public String getCreateTableDescription() {
		return this.createTableDescription;
	}

	public void setCreateTableDescription(String createTableDescription) {
		this.createTableDescription = createTableDescription;
	}

	public String getCreateTableEnglishName() {
		return this.createTableEnglishName;
	}

	public void setCreateTableEnglishName(String createTableEnglishName) {
		this.createTableEnglishName = createTableEnglishName;
	}

	public int getCreateTableNum() {
		return this.createTableNum;
	}

	public void setCreateTableNum(int createTableNum) {
		this.createTableNum = createTableNum;
	}

	public String getCreateTableType() {
		return this.createTableType;
	}

	public void setCreateTableType(String createTableType) {
		this.createTableType = createTableType;
	}

}