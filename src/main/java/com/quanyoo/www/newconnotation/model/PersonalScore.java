package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the personal_score database table.
 * 
 */
@Entity
@Table(name="personal_score")
@NamedQuery(name="PersonalScore.findAll", query="SELECT p FROM PersonalScore p")
public class PersonalScore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String department;

	@Column(name="full_name")
	private String fullName;

	@Column(name="job_number")
	private BigDecimal jobNumber;

	@Column(name="score_in_2018")
	private double scoreIn2018;

	@Column(name="score_in_2019")
	private double scoreIn2019;

	public PersonalScore() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public BigDecimal getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(BigDecimal jobNumber) {
		this.jobNumber = jobNumber;
	}

	public double getScoreIn2018() {
		return this.scoreIn2018;
	}

	public void setScoreIn2018(double scoreIn2018) {
		this.scoreIn2018 = scoreIn2018;
	}

	public double getScoreIn2019() {
		return this.scoreIn2019;
	}

	public void setScoreIn2019(double scoreIn2019) {
		this.scoreIn2019 = scoreIn2019;
	}

}