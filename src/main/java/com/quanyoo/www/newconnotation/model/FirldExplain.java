package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the firld_explain database table.
 * 
 */
@Entity
@Table(name="firld_explain")
@NamedQuery(name="FirldExplain.findAll", query="SELECT f FROM FirldExplain f")
public class FirldExplain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="firld_explain_1")
	private String firldExplain1;

	@Column(name="firld_explain_10")
	private String firldExplain10;

	@Column(name="firld_explain_11")
	private String firldExplain11;

	@Column(name="firld_explain_12")
	private String firldExplain12;

	@Column(name="firld_explain_13")
	private String firldExplain13;

	@Column(name="firld_explain_14")
	private String firldExplain14;

	@Column(name="firld_explain_15")
	private String firldExplain15;

	@Column(name="firld_explain_16")
	private String firldExplain16;

	@Column(name="firld_explain_17")
	private String firldExplain17;

	@Column(name="firld_explain_18")
	private String firldExplain18;

	@Column(name="firld_explain_19")
	private String firldExplain19;

	@Column(name="firld_explain_2")
	private String firldExplain2;

	@Column(name="firld_explain_20")
	private String firldExplain20;

	@Column(name="firld_explain_21")
	private String firldExplain21;

	@Column(name="firld_explain_22")
	private String firldExplain22;

	@Column(name="firld_explain_23")
	private String firldExplain23;

	@Column(name="firld_explain_24")
	private String firldExplain24;

	@Column(name="firld_explain_25")
	private String firldExplain25;

	@Column(name="firld_explain_26")
	private String firldExplain26;

	@Column(name="firld_explain_27")
	private String firldExplain27;

	@Column(name="firld_explain_28")
	private String firldExplain28;

	@Column(name="firld_explain_29")
	private String firldExplain29;

	@Column(name="firld_explain_3")
	private String firldExplain3;

	@Column(name="firld_explain_30")
	private String firldExplain30;

	@Column(name="firld_explain_31")
	private String firldExplain31;

	@Column(name="firld_explain_32")
	private String firldExplain32;

	@Column(name="firld_explain_33")
	private String firldExplain33;

	@Column(name="firld_explain_34")
	private String firldExplain34;

	@Column(name="firld_explain_35")
	private String firldExplain35;

	@Column(name="firld_explain_36")
	private String firldExplain36;

	@Column(name="firld_explain_37")
	private String firldExplain37;

	@Column(name="firld_explain_38")
	private String firldExplain38;

	@Column(name="firld_explain_39")
	private String firldExplain39;

	@Column(name="firld_explain_4")
	private String firldExplain4;

	@Column(name="firld_explain_40")
	private String firldExplain40;

	@Column(name="firld_explain_5")
	private String firldExplain5;

	@Column(name="firld_explain_6")
	private String firldExplain6;

	@Column(name="firld_explain_7")
	private String firldExplain7;

	@Column(name="firld_explain_8")
	private String firldExplain8;

	@Column(name="firld_explain_9")
	private String firldExplain9;

	@Column(name="table_name")
	private String tableName;

	public FirldExplain() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirldExplain1() {
		return this.firldExplain1;
	}

	public void setFirldExplain1(String firldExplain1) {
		this.firldExplain1 = firldExplain1;
	}

	public String getFirldExplain10() {
		return this.firldExplain10;
	}

	public void setFirldExplain10(String firldExplain10) {
		this.firldExplain10 = firldExplain10;
	}

	public String getFirldExplain11() {
		return this.firldExplain11;
	}

	public void setFirldExplain11(String firldExplain11) {
		this.firldExplain11 = firldExplain11;
	}

	public String getFirldExplain12() {
		return this.firldExplain12;
	}

	public void setFirldExplain12(String firldExplain12) {
		this.firldExplain12 = firldExplain12;
	}

	public String getFirldExplain13() {
		return this.firldExplain13;
	}

	public void setFirldExplain13(String firldExplain13) {
		this.firldExplain13 = firldExplain13;
	}

	public String getFirldExplain14() {
		return this.firldExplain14;
	}

	public void setFirldExplain14(String firldExplain14) {
		this.firldExplain14 = firldExplain14;
	}

	public String getFirldExplain15() {
		return this.firldExplain15;
	}

	public void setFirldExplain15(String firldExplain15) {
		this.firldExplain15 = firldExplain15;
	}

	public String getFirldExplain16() {
		return this.firldExplain16;
	}

	public void setFirldExplain16(String firldExplain16) {
		this.firldExplain16 = firldExplain16;
	}

	public String getFirldExplain17() {
		return this.firldExplain17;
	}

	public void setFirldExplain17(String firldExplain17) {
		this.firldExplain17 = firldExplain17;
	}

	public String getFirldExplain18() {
		return this.firldExplain18;
	}

	public void setFirldExplain18(String firldExplain18) {
		this.firldExplain18 = firldExplain18;
	}

	public String getFirldExplain19() {
		return this.firldExplain19;
	}

	public void setFirldExplain19(String firldExplain19) {
		this.firldExplain19 = firldExplain19;
	}

	public String getFirldExplain2() {
		return this.firldExplain2;
	}

	public void setFirldExplain2(String firldExplain2) {
		this.firldExplain2 = firldExplain2;
	}

	public String getFirldExplain20() {
		return this.firldExplain20;
	}

	public void setFirldExplain20(String firldExplain20) {
		this.firldExplain20 = firldExplain20;
	}

	public String getFirldExplain21() {
		return this.firldExplain21;
	}

	public void setFirldExplain21(String firldExplain21) {
		this.firldExplain21 = firldExplain21;
	}

	public String getFirldExplain22() {
		return this.firldExplain22;
	}

	public void setFirldExplain22(String firldExplain22) {
		this.firldExplain22 = firldExplain22;
	}

	public String getFirldExplain23() {
		return this.firldExplain23;
	}

	public void setFirldExplain23(String firldExplain23) {
		this.firldExplain23 = firldExplain23;
	}

	public String getFirldExplain24() {
		return this.firldExplain24;
	}

	public void setFirldExplain24(String firldExplain24) {
		this.firldExplain24 = firldExplain24;
	}

	public String getFirldExplain25() {
		return this.firldExplain25;
	}

	public void setFirldExplain25(String firldExplain25) {
		this.firldExplain25 = firldExplain25;
	}

	public String getFirldExplain26() {
		return this.firldExplain26;
	}

	public void setFirldExplain26(String firldExplain26) {
		this.firldExplain26 = firldExplain26;
	}

	public String getFirldExplain27() {
		return this.firldExplain27;
	}

	public void setFirldExplain27(String firldExplain27) {
		this.firldExplain27 = firldExplain27;
	}

	public String getFirldExplain28() {
		return this.firldExplain28;
	}

	public void setFirldExplain28(String firldExplain28) {
		this.firldExplain28 = firldExplain28;
	}

	public String getFirldExplain29() {
		return this.firldExplain29;
	}

	public void setFirldExplain29(String firldExplain29) {
		this.firldExplain29 = firldExplain29;
	}

	public String getFirldExplain3() {
		return this.firldExplain3;
	}

	public void setFirldExplain3(String firldExplain3) {
		this.firldExplain3 = firldExplain3;
	}

	public String getFirldExplain30() {
		return this.firldExplain30;
	}

	public void setFirldExplain30(String firldExplain30) {
		this.firldExplain30 = firldExplain30;
	}

	public String getFirldExplain31() {
		return this.firldExplain31;
	}

	public void setFirldExplain31(String firldExplain31) {
		this.firldExplain31 = firldExplain31;
	}

	public String getFirldExplain32() {
		return this.firldExplain32;
	}

	public void setFirldExplain32(String firldExplain32) {
		this.firldExplain32 = firldExplain32;
	}

	public String getFirldExplain33() {
		return this.firldExplain33;
	}

	public void setFirldExplain33(String firldExplain33) {
		this.firldExplain33 = firldExplain33;
	}

	public String getFirldExplain34() {
		return this.firldExplain34;
	}

	public void setFirldExplain34(String firldExplain34) {
		this.firldExplain34 = firldExplain34;
	}

	public String getFirldExplain35() {
		return this.firldExplain35;
	}

	public void setFirldExplain35(String firldExplain35) {
		this.firldExplain35 = firldExplain35;
	}

	public String getFirldExplain36() {
		return this.firldExplain36;
	}

	public void setFirldExplain36(String firldExplain36) {
		this.firldExplain36 = firldExplain36;
	}

	public String getFirldExplain37() {
		return this.firldExplain37;
	}

	public void setFirldExplain37(String firldExplain37) {
		this.firldExplain37 = firldExplain37;
	}

	public String getFirldExplain38() {
		return this.firldExplain38;
	}

	public void setFirldExplain38(String firldExplain38) {
		this.firldExplain38 = firldExplain38;
	}

	public String getFirldExplain39() {
		return this.firldExplain39;
	}

	public void setFirldExplain39(String firldExplain39) {
		this.firldExplain39 = firldExplain39;
	}

	public String getFirldExplain4() {
		return this.firldExplain4;
	}

	public void setFirldExplain4(String firldExplain4) {
		this.firldExplain4 = firldExplain4;
	}

	public String getFirldExplain40() {
		return this.firldExplain40;
	}

	public void setFirldExplain40(String firldExplain40) {
		this.firldExplain40 = firldExplain40;
	}

	public String getFirldExplain5() {
		return this.firldExplain5;
	}

	public void setFirldExplain5(String firldExplain5) {
		this.firldExplain5 = firldExplain5;
	}

	public String getFirldExplain6() {
		return this.firldExplain6;
	}

	public void setFirldExplain6(String firldExplain6) {
		this.firldExplain6 = firldExplain6;
	}

	public String getFirldExplain7() {
		return this.firldExplain7;
	}

	public void setFirldExplain7(String firldExplain7) {
		this.firldExplain7 = firldExplain7;
	}

	public String getFirldExplain8() {
		return this.firldExplain8;
	}

	public void setFirldExplain8(String firldExplain8) {
		this.firldExplain8 = firldExplain8;
	}

	public String getFirldExplain9() {
		return this.firldExplain9;
	}

	public void setFirldExplain9(String firldExplain9) {
		this.firldExplain9 = firldExplain9;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}