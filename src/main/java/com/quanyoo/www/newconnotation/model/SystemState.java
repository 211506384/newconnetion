package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the system_state database table.
 * 
 */
@Entity
@Table(name="system_state")
@NamedQuery(name="SystemState.findAll", query="SELECT s FROM SystemState s")
public class SystemState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="system_name")
	private String systemName;

	@Column(name="system_state")
	private String systemState;

	private String systemexplain;

	private String systemname;

	private String systemstate;

	public SystemState() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSystemName() {
		return this.systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getSystemState() {
		return this.systemState;
	}

	public void setSystemState(String systemState) {
		this.systemState = systemState;
	}

	public String getSystemexplain() {
		return this.systemexplain;
	}

	public void setSystemexplain(String systemexplain) {
		this.systemexplain = systemexplain;
	}

	public String getSystemname() {
		return this.systemname;
	}

	public void setSystemname(String systemname) {
		this.systemname = systemname;
	}

	public String getSystemstate() {
		return this.systemstate;
	}

	public void setSystemstate(String systemstate) {
		this.systemstate = systemstate;
	}

}