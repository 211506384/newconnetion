package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the teaching_materials_notadopted database table.
 * 
 */
@Entity
@Table(name="teaching_materials_notadopted")
@NamedQuery(name="TeachingMaterialsNotadopted.findAll", query="SELECT t FROM TeachingMaterialsNotadopted t")
public class TeachingMaterialsNotadopted implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="address_supporting_materials")
	private String addressSupportingMaterials;

	@Column(name="approval_department")
	private String approvalDepartment;

	@Column(name="approval_number")
	private String approvalNumber;

	@Column(name="approval_scoreof_first")
	private String approvalScoreofFirst;

	@Column(name="approval_scoreof_second")
	private String approvalScoreofSecond;

	private String college;

	@Column(name="comments_author")
	private String commentsAuthor;

	@Column(name="data_index")
	private String dataIndex;

	@Column(name="first_approval_level")
	private String firstApprovalLevel;

	@Column(name="first_person_job_number")
	private String firstPersonJobNumber;

	@Column(name="full_name")
	private String fullName;

	@Column(name="job_number")
	private String jobNumber;

	@Column(name="name_editor")
	private String nameEditor;

	@Column(name="name_originator")
	private String nameOriginator;

	@Column(name="nameof_submitted")
	private String nameofSubmitted;

	@Column(name="nameof_uploader")
	private String nameofUploader;

	@Column(name="names_other_editors")
	private String namesOtherEditors;

	@Column(name="number_editors")
	private String numberEditors;

	@Column(name="number_final_second")
	private String numberFinalSecond;

	@Column(name="number_initiating")
	private String numberInitiating;

	@Column(name="number_last_reviewer")
	private String numberLastReviewer;

	@Column(name="personnel_final_second")
	private String personnelFinalSecond;

	@Column(name="personnel_last")
	private String personnelLast;

	private String press;

	@Column(name="project_approval_time")
	private String projectApprovalTime;

	@Column(name="remarks_first")
	private String remarksFirst;

	@Column(name="remarks_second")
	private String remarksSecond;

	@Column(name="return_remarks")
	private String returnRemarks;

	@Column(name="return_time")
	private String returnTime;

	@Column(name="returned_receivername")
	private String returnedReceivername;

	@Column(name="returned_receivernum")
	private String returnedReceivernum;

	@Column(name="second_approval_level")
	private String secondApprovalLevel;

	@Column(name="submission_time")
	private String submissionTime;

	@Column(name="submitted_jobno")
	private String submittedJobno;

	@Column(name="submitted_number")
	private int submittedNumber;

	@Column(name="teaching_award_level")
	private String teachingAwardLevel;

	@Column(name="textbook_name")
	private String textbookName;

	@Column(name="time_final_second")
	private String timeFinalSecond;

	@Column(name="time_last_trial")
	private String timeLastTrial;

	@Column(name="types_teaching_materials")
	private String typesTeachingMaterials;

	@Column(name="upload_time")
	private String uploadTime;

	@Column(name="uploader_notes")
	private String uploaderNotes;

	@Column(name="uploader_num")
	private String uploaderNum;

	public TeachingMaterialsNotadopted() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressSupportingMaterials() {
		return this.addressSupportingMaterials;
	}

	public void setAddressSupportingMaterials(String addressSupportingMaterials) {
		this.addressSupportingMaterials = addressSupportingMaterials;
	}

	public String getApprovalDepartment() {
		return this.approvalDepartment;
	}

	public void setApprovalDepartment(String approvalDepartment) {
		this.approvalDepartment = approvalDepartment;
	}

	public String getApprovalNumber() {
		return this.approvalNumber;
	}

	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public String getApprovalScoreofFirst() {
		return this.approvalScoreofFirst;
	}

	public void setApprovalScoreofFirst(String approvalScoreofFirst) {
		this.approvalScoreofFirst = approvalScoreofFirst;
	}

	public String getApprovalScoreofSecond() {
		return this.approvalScoreofSecond;
	}

	public void setApprovalScoreofSecond(String approvalScoreofSecond) {
		this.approvalScoreofSecond = approvalScoreofSecond;
	}

	public String getCollege() {
		return this.college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCommentsAuthor() {
		return this.commentsAuthor;
	}

	public void setCommentsAuthor(String commentsAuthor) {
		this.commentsAuthor = commentsAuthor;
	}

	public String getDataIndex() {
		return this.dataIndex;
	}

	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}

	public String getFirstApprovalLevel() {
		return this.firstApprovalLevel;
	}

	public void setFirstApprovalLevel(String firstApprovalLevel) {
		this.firstApprovalLevel = firstApprovalLevel;
	}

	public String getFirstPersonJobNumber() {
		return this.firstPersonJobNumber;
	}

	public void setFirstPersonJobNumber(String firstPersonJobNumber) {
		this.firstPersonJobNumber = firstPersonJobNumber;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getNameEditor() {
		return this.nameEditor;
	}

	public void setNameEditor(String nameEditor) {
		this.nameEditor = nameEditor;
	}

	public String getNameOriginator() {
		return this.nameOriginator;
	}

	public void setNameOriginator(String nameOriginator) {
		this.nameOriginator = nameOriginator;
	}

	public String getNameofSubmitted() {
		return this.nameofSubmitted;
	}

	public void setNameofSubmitted(String nameofSubmitted) {
		this.nameofSubmitted = nameofSubmitted;
	}

	public String getNameofUploader() {
		return this.nameofUploader;
	}

	public void setNameofUploader(String nameofUploader) {
		this.nameofUploader = nameofUploader;
	}

	public String getNamesOtherEditors() {
		return this.namesOtherEditors;
	}

	public void setNamesOtherEditors(String namesOtherEditors) {
		this.namesOtherEditors = namesOtherEditors;
	}

	public String getNumberEditors() {
		return this.numberEditors;
	}

	public void setNumberEditors(String numberEditors) {
		this.numberEditors = numberEditors;
	}

	public String getNumberFinalSecond() {
		return this.numberFinalSecond;
	}

	public void setNumberFinalSecond(String numberFinalSecond) {
		this.numberFinalSecond = numberFinalSecond;
	}

	public String getNumberInitiating() {
		return this.numberInitiating;
	}

	public void setNumberInitiating(String numberInitiating) {
		this.numberInitiating = numberInitiating;
	}

	public String getNumberLastReviewer() {
		return this.numberLastReviewer;
	}

	public void setNumberLastReviewer(String numberLastReviewer) {
		this.numberLastReviewer = numberLastReviewer;
	}

	public String getPersonnelFinalSecond() {
		return this.personnelFinalSecond;
	}

	public void setPersonnelFinalSecond(String personnelFinalSecond) {
		this.personnelFinalSecond = personnelFinalSecond;
	}

	public String getPersonnelLast() {
		return this.personnelLast;
	}

	public void setPersonnelLast(String personnelLast) {
		this.personnelLast = personnelLast;
	}

	public String getPress() {
		return this.press;
	}

	public void setPress(String press) {
		this.press = press;
	}

	public String getProjectApprovalTime() {
		return this.projectApprovalTime;
	}

	public void setProjectApprovalTime(String projectApprovalTime) {
		this.projectApprovalTime = projectApprovalTime;
	}

	public String getRemarksFirst() {
		return this.remarksFirst;
	}

	public void setRemarksFirst(String remarksFirst) {
		this.remarksFirst = remarksFirst;
	}

	public String getRemarksSecond() {
		return this.remarksSecond;
	}

	public void setRemarksSecond(String remarksSecond) {
		this.remarksSecond = remarksSecond;
	}

	public String getReturnRemarks() {
		return this.returnRemarks;
	}

	public void setReturnRemarks(String returnRemarks) {
		this.returnRemarks = returnRemarks;
	}

	public String getReturnTime() {
		return this.returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getReturnedReceivername() {
		return this.returnedReceivername;
	}

	public void setReturnedReceivername(String returnedReceivername) {
		this.returnedReceivername = returnedReceivername;
	}

	public String getReturnedReceivernum() {
		return this.returnedReceivernum;
	}

	public void setReturnedReceivernum(String returnedReceivernum) {
		this.returnedReceivernum = returnedReceivernum;
	}

	public String getSecondApprovalLevel() {
		return this.secondApprovalLevel;
	}

	public void setSecondApprovalLevel(String secondApprovalLevel) {
		this.secondApprovalLevel = secondApprovalLevel;
	}

	public String getSubmissionTime() {
		return this.submissionTime;
	}

	public void setSubmissionTime(String submissionTime) {
		this.submissionTime = submissionTime;
	}

	public String getSubmittedJobno() {
		return this.submittedJobno;
	}

	public void setSubmittedJobno(String submittedJobno) {
		this.submittedJobno = submittedJobno;
	}

	public int getSubmittedNumber() {
		return this.submittedNumber;
	}

	public void setSubmittedNumber(int submittedNumber) {
		this.submittedNumber = submittedNumber;
	}

	public String getTeachingAwardLevel() {
		return this.teachingAwardLevel;
	}

	public void setTeachingAwardLevel(String teachingAwardLevel) {
		this.teachingAwardLevel = teachingAwardLevel;
	}

	public String getTextbookName() {
		return this.textbookName;
	}

	public void setTextbookName(String textbookName) {
		this.textbookName = textbookName;
	}

	public String getTimeFinalSecond() {
		return this.timeFinalSecond;
	}

	public void setTimeFinalSecond(String timeFinalSecond) {
		this.timeFinalSecond = timeFinalSecond;
	}

	public String getTimeLastTrial() {
		return this.timeLastTrial;
	}

	public void setTimeLastTrial(String timeLastTrial) {
		this.timeLastTrial = timeLastTrial;
	}

	public String getTypesTeachingMaterials() {
		return this.typesTeachingMaterials;
	}

	public void setTypesTeachingMaterials(String typesTeachingMaterials) {
		this.typesTeachingMaterials = typesTeachingMaterials;
	}

	public String getUploadTime() {
		return this.uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getUploaderNotes() {
		return this.uploaderNotes;
	}

	public void setUploaderNotes(String uploaderNotes) {
		this.uploaderNotes = uploaderNotes;
	}

	public String getUploaderNum() {
		return this.uploaderNum;
	}

	public void setUploaderNum(String uploaderNum) {
		this.uploaderNum = uploaderNum;
	}

}