package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the connotation_table_column_create database table.
 * 
 */
@Entity
@Table(name="connotation_table_column_create")
@NamedQuery(name="ConnotationTableColumnCreate.findAll", query="SELECT c FROM ConnotationTableColumnCreate c")
public class ConnotationTableColumnCreate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="create_table_chinese_column")
	private String createTableChineseColumn;

	@Column(name="create_table_column")
	private String createTableColumn;

	@Column(name="create_table_column_type")
	private String createTableColumnType;

	@Column(name="create_table_id")
	private int createTableId;

	@Column(name="create_table_name")
	private String createTableName;

	@Column(name="field_weight")
	private int fieldWeight;

	@Column(name="is_need")
	private int isNeed;

	@Column(name="is_statistics")
	private String isStatistics;

	public ConnotationTableColumnCreate() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateTableChineseColumn() {
		return this.createTableChineseColumn;
	}

	public void setCreateTableChineseColumn(String createTableChineseColumn) {
		this.createTableChineseColumn = createTableChineseColumn;
	}

	public String getCreateTableColumn() {
		return this.createTableColumn;
	}

	public void setCreateTableColumn(String createTableColumn) {
		this.createTableColumn = createTableColumn;
	}

	public String getCreateTableColumnType() {
		return this.createTableColumnType;
	}

	public void setCreateTableColumnType(String createTableColumnType) {
		this.createTableColumnType = createTableColumnType;
	}

	public int getCreateTableId() {
		return this.createTableId;
	}

	public void setCreateTableId(int createTableId) {
		this.createTableId = createTableId;
	}

	public String getCreateTableName() {
		return this.createTableName;
	}

	public void setCreateTableName(String createTableName) {
		this.createTableName = createTableName;
	}

	public int getFieldWeight() {
		return this.fieldWeight;
	}

	public void setFieldWeight(int fieldWeight) {
		this.fieldWeight = fieldWeight;
	}

	public int getIsNeed() {
		return this.isNeed;
	}

	public void setIsNeed(int isNeed) {
		this.isNeed = isNeed;
	}

	public String getIsStatistics() {
		return this.isStatistics;
	}

	public void setIsStatistics(String isStatistics) {
		this.isStatistics = isStatistics;
	}

}