package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the student_competition_teachers_adopt database table.
 * 
 */
@Entity
@Table(name="student_competition_teachers_adopt")
@NamedQuery(name="StudentCompetitionTeachersAdopt.findAll", query="SELECT s FROM StudentCompetitionTeachersAdopt s")
public class StudentCompetitionTeachersAdopt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="address_supporting_materials")
	private String addressSupportingMaterials;

	@Column(name="approval_scoreof_first")
	private String approvalScoreofFirst;

	@Column(name="approval_scoreof_second")
	private String approvalScoreofSecond;

	@Column(name="award_level")
	private String awardLevel;

	@Column(name="award_time")
	private String awardTime;

	private String college;

	@Column(name="comments_author")
	private String commentsAuthor;

	@Column(name="data_index")
	private String dataIndex;

	@Column(name="first_approval_level")
	private String firstApprovalLevel;

	@Column(name="full_name")
	private String fullName;

	@Column(name="job_number")
	private String jobNumber;

	@Column(name="levelsports_school")
	private String levelsportsSchool;

	@Column(name="name_event")
	private String nameEvent;

	@Column(name="name_originator")
	private String nameOriginator;

	@Column(name="name_project_or_work")
	private String nameProjectOrWork;

	@Column(name="nameof_submitted")
	private String nameofSubmitted;

	@Column(name="nameof_uploader")
	private String nameofUploader;

	@Column(name="national_ministerial_level")
	private String nationalMinisterialLevel;

	@Column(name="number_final_second")
	private String numberFinalSecond;

	@Column(name="number_initiating")
	private String numberInitiating;

	@Column(name="number_last_reviewer")
	private String numberLastReviewer;

	private String organizer;

	@Column(name="personnel_final_second")
	private String personnelFinalSecond;

	@Column(name="personnel_last")
	private String personnelLast;

	@Column(name="remarks_first")
	private String remarksFirst;

	@Column(name="remarks_second")
	private String remarksSecond;

	@Column(name="return_remarks")
	private String returnRemarks;

	@Column(name="return_time")
	private String returnTime;

	@Column(name="returned_receivername")
	private String returnedReceivername;

	@Column(name="returned_receivernum")
	private String returnedReceivernum;

	@Column(name="second_approval_level")
	private String secondApprovalLevel;

	@Column(name="submission_time")
	private String submissionTime;

	@Column(name="submitted_jobno")
	private String submittedJobno;

	@Column(name="submitted_number")
	private int submittedNumber;

	@Column(name="time_final_second")
	private String timeFinalSecond;

	@Column(name="time_last_trial")
	private String timeLastTrial;

	@Column(name="total_number_instructors")
	private String totalNumberInstructors;

	@Column(name="total_number_students")
	private String totalNumberStudents;

	@Column(name="upload_time")
	private String uploadTime;

	@Column(name="uploader_notes")
	private String uploaderNotes;

	@Column(name="uploader_num")
	private String uploaderNum;

	public StudentCompetitionTeachersAdopt() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressSupportingMaterials() {
		return this.addressSupportingMaterials;
	}

	public void setAddressSupportingMaterials(String addressSupportingMaterials) {
		this.addressSupportingMaterials = addressSupportingMaterials;
	}

	public String getApprovalScoreofFirst() {
		return this.approvalScoreofFirst;
	}

	public void setApprovalScoreofFirst(String approvalScoreofFirst) {
		this.approvalScoreofFirst = approvalScoreofFirst;
	}

	public String getApprovalScoreofSecond() {
		return this.approvalScoreofSecond;
	}

	public void setApprovalScoreofSecond(String approvalScoreofSecond) {
		this.approvalScoreofSecond = approvalScoreofSecond;
	}

	public String getAwardLevel() {
		return this.awardLevel;
	}

	public void setAwardLevel(String awardLevel) {
		this.awardLevel = awardLevel;
	}

	public String getAwardTime() {
		return this.awardTime;
	}

	public void setAwardTime(String awardTime) {
		this.awardTime = awardTime;
	}

	public String getCollege() {
		return this.college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCommentsAuthor() {
		return this.commentsAuthor;
	}

	public void setCommentsAuthor(String commentsAuthor) {
		this.commentsAuthor = commentsAuthor;
	}

	public String getDataIndex() {
		return this.dataIndex;
	}

	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}

	public String getFirstApprovalLevel() {
		return this.firstApprovalLevel;
	}

	public void setFirstApprovalLevel(String firstApprovalLevel) {
		this.firstApprovalLevel = firstApprovalLevel;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getLevelsportsSchool() {
		return this.levelsportsSchool;
	}

	public void setLevelsportsSchool(String levelsportsSchool) {
		this.levelsportsSchool = levelsportsSchool;
	}

	public String getNameEvent() {
		return this.nameEvent;
	}

	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}

	public String getNameOriginator() {
		return this.nameOriginator;
	}

	public void setNameOriginator(String nameOriginator) {
		this.nameOriginator = nameOriginator;
	}

	public String getNameProjectOrWork() {
		return this.nameProjectOrWork;
	}

	public void setNameProjectOrWork(String nameProjectOrWork) {
		this.nameProjectOrWork = nameProjectOrWork;
	}

	public String getNameofSubmitted() {
		return this.nameofSubmitted;
	}

	public void setNameofSubmitted(String nameofSubmitted) {
		this.nameofSubmitted = nameofSubmitted;
	}

	public String getNameofUploader() {
		return this.nameofUploader;
	}

	public void setNameofUploader(String nameofUploader) {
		this.nameofUploader = nameofUploader;
	}

	public String getNationalMinisterialLevel() {
		return this.nationalMinisterialLevel;
	}

	public void setNationalMinisterialLevel(String nationalMinisterialLevel) {
		this.nationalMinisterialLevel = nationalMinisterialLevel;
	}

	public String getNumberFinalSecond() {
		return this.numberFinalSecond;
	}

	public void setNumberFinalSecond(String numberFinalSecond) {
		this.numberFinalSecond = numberFinalSecond;
	}

	public String getNumberInitiating() {
		return this.numberInitiating;
	}

	public void setNumberInitiating(String numberInitiating) {
		this.numberInitiating = numberInitiating;
	}

	public String getNumberLastReviewer() {
		return this.numberLastReviewer;
	}

	public void setNumberLastReviewer(String numberLastReviewer) {
		this.numberLastReviewer = numberLastReviewer;
	}

	public String getOrganizer() {
		return this.organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	public String getPersonnelFinalSecond() {
		return this.personnelFinalSecond;
	}

	public void setPersonnelFinalSecond(String personnelFinalSecond) {
		this.personnelFinalSecond = personnelFinalSecond;
	}

	public String getPersonnelLast() {
		return this.personnelLast;
	}

	public void setPersonnelLast(String personnelLast) {
		this.personnelLast = personnelLast;
	}

	public String getRemarksFirst() {
		return this.remarksFirst;
	}

	public void setRemarksFirst(String remarksFirst) {
		this.remarksFirst = remarksFirst;
	}

	public String getRemarksSecond() {
		return this.remarksSecond;
	}

	public void setRemarksSecond(String remarksSecond) {
		this.remarksSecond = remarksSecond;
	}

	public String getReturnRemarks() {
		return this.returnRemarks;
	}

	public void setReturnRemarks(String returnRemarks) {
		this.returnRemarks = returnRemarks;
	}

	public String getReturnTime() {
		return this.returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getReturnedReceivername() {
		return this.returnedReceivername;
	}

	public void setReturnedReceivername(String returnedReceivername) {
		this.returnedReceivername = returnedReceivername;
	}

	public String getReturnedReceivernum() {
		return this.returnedReceivernum;
	}

	public void setReturnedReceivernum(String returnedReceivernum) {
		this.returnedReceivernum = returnedReceivernum;
	}

	public String getSecondApprovalLevel() {
		return this.secondApprovalLevel;
	}

	public void setSecondApprovalLevel(String secondApprovalLevel) {
		this.secondApprovalLevel = secondApprovalLevel;
	}

	public String getSubmissionTime() {
		return this.submissionTime;
	}

	public void setSubmissionTime(String submissionTime) {
		this.submissionTime = submissionTime;
	}

	public String getSubmittedJobno() {
		return this.submittedJobno;
	}

	public void setSubmittedJobno(String submittedJobno) {
		this.submittedJobno = submittedJobno;
	}

	public int getSubmittedNumber() {
		return this.submittedNumber;
	}

	public void setSubmittedNumber(int submittedNumber) {
		this.submittedNumber = submittedNumber;
	}

	public String getTimeFinalSecond() {
		return this.timeFinalSecond;
	}

	public void setTimeFinalSecond(String timeFinalSecond) {
		this.timeFinalSecond = timeFinalSecond;
	}

	public String getTimeLastTrial() {
		return this.timeLastTrial;
	}

	public void setTimeLastTrial(String timeLastTrial) {
		this.timeLastTrial = timeLastTrial;
	}

	public String getTotalNumberInstructors() {
		return this.totalNumberInstructors;
	}

	public void setTotalNumberInstructors(String totalNumberInstructors) {
		this.totalNumberInstructors = totalNumberInstructors;
	}

	public String getTotalNumberStudents() {
		return this.totalNumberStudents;
	}

	public void setTotalNumberStudents(String totalNumberStudents) {
		this.totalNumberStudents = totalNumberStudents;
	}

	public String getUploadTime() {
		return this.uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getUploaderNotes() {
		return this.uploaderNotes;
	}

	public void setUploaderNotes(String uploaderNotes) {
		this.uploaderNotes = uploaderNotes;
	}

	public String getUploaderNum() {
		return this.uploaderNum;
	}

	public void setUploaderNum(String uploaderNum) {
		this.uploaderNum = uploaderNum;
	}

}