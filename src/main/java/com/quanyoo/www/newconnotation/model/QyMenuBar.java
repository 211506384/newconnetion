package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the qy_menu_bar database table.
 * 
 */
@Entity
@Table(name="qy_menu_bar")
@NamedQuery(name="QyMenuBar.findAll", query="SELECT q FROM QyMenuBar q")
public class QyMenuBar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String description;

	@Column(name="is_delete")
	private String isDelete;

	@Column(name="menu_father_id")
	private int menuFatherId;

	@Column(name="menu_name")
	private String menuName;

	@Column(name="menu_order")
	private String menuOrder;

	@Column(name="menu_url")
	private String menuUrl;

	public QyMenuBar() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public int getMenuFatherId() {
		return this.menuFatherId;
	}

	public void setMenuFatherId(int menuFatherId) {
		this.menuFatherId = menuFatherId;
	}

	public String getMenuName() {
		return this.menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuOrder() {
		return this.menuOrder;
	}

	public void setMenuOrder(String menuOrder) {
		this.menuOrder = menuOrder;
	}

	public String getMenuUrl() {
		return this.menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

}