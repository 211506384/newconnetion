package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the table_exist database table.
 * 
 */
@Entity
@Table(name="table_exist")
@NamedQuery(name="TableExist.findAll", query="SELECT t FROM TableExist t")
public class TableExist implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="does_it_exist")
	private String doesItExist;

	@Column(name="table_name")
	private String tableName;

	public TableExist() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDoesItExist() {
		return this.doesItExist;
	}

	public void setDoesItExist(String doesItExist) {
		this.doesItExist = doesItExist;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}