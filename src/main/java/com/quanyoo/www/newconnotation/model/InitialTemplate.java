package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the initial_template database table.
 * 
 */
@Entity
@Table(name="initial_template")
@NamedQuery(name="InitialTemplate.findAll", query="SELECT i FROM InitialTemplate i")
public class InitialTemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	public InitialTemplate() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

}