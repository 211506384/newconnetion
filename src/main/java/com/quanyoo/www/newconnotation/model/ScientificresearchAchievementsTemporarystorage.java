package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the scientificresearch_achievements_temporarystorage database table.
 * 
 */
@Entity
@Table(name="scientificresearch_achievements_temporarystorage")
@NamedQuery(name="ScientificresearchAchievementsTemporarystorage.findAll", query="SELECT s FROM ScientificresearchAchievementsTemporarystorage s")
public class ScientificresearchAchievementsTemporarystorage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="address_supporting_materials")
	private String addressSupportingMaterials;

	@Column(name="affiliated_unit")
	private String affiliatedUnit;

	private String applicant;

	@Column(name="application_date")
	private String applicationDate;

	@Column(name="application_number")
	private String applicationNumber;

	@Column(name="approval_scoreof_first")
	private String approvalScoreofFirst;

	@Column(name="approval_scoreof_second")
	private String approvalScoreofSecond;

	@Column(name="audit_status")
	private String auditStatus;

	@Column(name="authorization_number")
	private String authorizationNumber;

	private String college;

	@Column(name="comments_author")
	private String commentsAuthor;

	@Column(name="data_index")
	private String dataIndex;

	@Column(name="date_authorization")
	private String dateAuthorization;

	@Column(name="enter_country")
	private String enterCountry;

	@Column(name="first_approval_level")
	private String firstApprovalLevel;

	@Column(name="first_inventor")
	private String firstInventor;

	@Column(name="full_name")
	private String fullName;

	@Column(name="is_it_invalid")
	private String isItInvalid;

	@Column(name="is_it_job_patent")
	private String isItJobPatent;

	@Column(name="is_it_pct_patent")
	private String isItPctPatent;

	@Column(name="job_number")
	private String jobNumber;

	@Column(name="name_originator")
	private String nameOriginator;

	@Column(name="nameof_submitted")
	private String nameofSubmitted;

	@Column(name="nameof_uploader")
	private String nameofUploader;

	@Column(name="number_final_second")
	private String numberFinalSecond;

	@Column(name="number_initiating")
	private String numberInitiating;

	@Column(name="number_last_reviewer")
	private String numberLastReviewer;

	@Column(name="other_inventors")
	private String otherInventors;

	@Column(name="patent_inventor")
	private String patentInventor;

	@Column(name="patent_name")
	private String patentName;

	@Column(name="patent_status")
	private String patentStatus;

	@Column(name="patent_transfertime")
	private String patentTransfertime;

	@Column(name="patent_type")
	private String patentType;

	private String patentee;

	@Column(name="pct_application_date")
	private String pctApplicationDate;

	@Column(name="pct_application_no")
	private String pctApplicationNo;

	@Column(name="pct_name")
	private String pctName;

	@Column(name="pct_priority_date")
	private String pctPriorityDate;

	@Column(name="personnel_final_second")
	private String personnelFinalSecond;

	@Column(name="personnel_last")
	private String personnelLast;

	@Column(name="public_number")
	private String publicNumber;

	private String remarks;

	@Column(name="remarks_first")
	private String remarksFirst;

	@Column(name="remarks_second")
	private String remarksSecond;

	@Column(name="return_remarks")
	private String returnRemarks;

	@Column(name="return_time")
	private String returnTime;

	@Column(name="returned_receivername")
	private String returnedReceivername;

	@Column(name="returned_receivernum")
	private String returnedReceivernum;

	@Column(name="school_signature")
	private String schoolSignature;

	@Column(name="scope_of_patent_patent_status")
	private String scopeOfPatentPatentStatus;

	@Column(name="second_approval_level")
	private String secondApprovalLevel;

	@Column(name="submission_time")
	private String submissionTime;

	@Column(name="submitted_jobno")
	private String submittedJobno;

	@Column(name="submitted_number")
	private int submittedNumber;

	@Column(name="termination_date")
	private String terminationDate;

	@Column(name="time_final_second")
	private String timeFinalSecond;

	@Column(name="time_last_trial")
	private String timeLastTrial;

	@Column(name="time_patent_transfer")
	private String timePatentTransfer;

	@Column(name="type_of_first_inventor")
	private String typeOfFirstInventor;

	@Column(name="types_cooperation")
	private String typesCooperation;

	@Column(name="upload_time")
	private String uploadTime;

	@Column(name="uploader_notes")
	private String uploaderNotes;

	@Column(name="uploader_num")
	private String uploaderNum;

	public ScientificresearchAchievementsTemporarystorage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressSupportingMaterials() {
		return this.addressSupportingMaterials;
	}

	public void setAddressSupportingMaterials(String addressSupportingMaterials) {
		this.addressSupportingMaterials = addressSupportingMaterials;
	}

	public String getAffiliatedUnit() {
		return this.affiliatedUnit;
	}

	public void setAffiliatedUnit(String affiliatedUnit) {
		this.affiliatedUnit = affiliatedUnit;
	}

	public String getApplicant() {
		return this.applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getApplicationDate() {
		return this.applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationNumber() {
		return this.applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getApprovalScoreofFirst() {
		return this.approvalScoreofFirst;
	}

	public void setApprovalScoreofFirst(String approvalScoreofFirst) {
		this.approvalScoreofFirst = approvalScoreofFirst;
	}

	public String getApprovalScoreofSecond() {
		return this.approvalScoreofSecond;
	}

	public void setApprovalScoreofSecond(String approvalScoreofSecond) {
		this.approvalScoreofSecond = approvalScoreofSecond;
	}

	public String getAuditStatus() {
		return this.auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getAuthorizationNumber() {
		return this.authorizationNumber;
	}

	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	public String getCollege() {
		return this.college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCommentsAuthor() {
		return this.commentsAuthor;
	}

	public void setCommentsAuthor(String commentsAuthor) {
		this.commentsAuthor = commentsAuthor;
	}

	public String getDataIndex() {
		return this.dataIndex;
	}

	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}

	public String getDateAuthorization() {
		return this.dateAuthorization;
	}

	public void setDateAuthorization(String dateAuthorization) {
		this.dateAuthorization = dateAuthorization;
	}

	public String getEnterCountry() {
		return this.enterCountry;
	}

	public void setEnterCountry(String enterCountry) {
		this.enterCountry = enterCountry;
	}

	public String getFirstApprovalLevel() {
		return this.firstApprovalLevel;
	}

	public void setFirstApprovalLevel(String firstApprovalLevel) {
		this.firstApprovalLevel = firstApprovalLevel;
	}

	public String getFirstInventor() {
		return this.firstInventor;
	}

	public void setFirstInventor(String firstInventor) {
		this.firstInventor = firstInventor;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getIsItInvalid() {
		return this.isItInvalid;
	}

	public void setIsItInvalid(String isItInvalid) {
		this.isItInvalid = isItInvalid;
	}

	public String getIsItJobPatent() {
		return this.isItJobPatent;
	}

	public void setIsItJobPatent(String isItJobPatent) {
		this.isItJobPatent = isItJobPatent;
	}

	public String getIsItPctPatent() {
		return this.isItPctPatent;
	}

	public void setIsItPctPatent(String isItPctPatent) {
		this.isItPctPatent = isItPctPatent;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getNameOriginator() {
		return this.nameOriginator;
	}

	public void setNameOriginator(String nameOriginator) {
		this.nameOriginator = nameOriginator;
	}

	public String getNameofSubmitted() {
		return this.nameofSubmitted;
	}

	public void setNameofSubmitted(String nameofSubmitted) {
		this.nameofSubmitted = nameofSubmitted;
	}

	public String getNameofUploader() {
		return this.nameofUploader;
	}

	public void setNameofUploader(String nameofUploader) {
		this.nameofUploader = nameofUploader;
	}

	public String getNumberFinalSecond() {
		return this.numberFinalSecond;
	}

	public void setNumberFinalSecond(String numberFinalSecond) {
		this.numberFinalSecond = numberFinalSecond;
	}

	public String getNumberInitiating() {
		return this.numberInitiating;
	}

	public void setNumberInitiating(String numberInitiating) {
		this.numberInitiating = numberInitiating;
	}

	public String getNumberLastReviewer() {
		return this.numberLastReviewer;
	}

	public void setNumberLastReviewer(String numberLastReviewer) {
		this.numberLastReviewer = numberLastReviewer;
	}

	public String getOtherInventors() {
		return this.otherInventors;
	}

	public void setOtherInventors(String otherInventors) {
		this.otherInventors = otherInventors;
	}

	public String getPatentInventor() {
		return this.patentInventor;
	}

	public void setPatentInventor(String patentInventor) {
		this.patentInventor = patentInventor;
	}

	public String getPatentName() {
		return this.patentName;
	}

	public void setPatentName(String patentName) {
		this.patentName = patentName;
	}

	public String getPatentStatus() {
		return this.patentStatus;
	}

	public void setPatentStatus(String patentStatus) {
		this.patentStatus = patentStatus;
	}

	public String getPatentTransfertime() {
		return this.patentTransfertime;
	}

	public void setPatentTransfertime(String patentTransfertime) {
		this.patentTransfertime = patentTransfertime;
	}

	public String getPatentType() {
		return this.patentType;
	}

	public void setPatentType(String patentType) {
		this.patentType = patentType;
	}

	public String getPatentee() {
		return this.patentee;
	}

	public void setPatentee(String patentee) {
		this.patentee = patentee;
	}

	public String getPctApplicationDate() {
		return this.pctApplicationDate;
	}

	public void setPctApplicationDate(String pctApplicationDate) {
		this.pctApplicationDate = pctApplicationDate;
	}

	public String getPctApplicationNo() {
		return this.pctApplicationNo;
	}

	public void setPctApplicationNo(String pctApplicationNo) {
		this.pctApplicationNo = pctApplicationNo;
	}

	public String getPctName() {
		return this.pctName;
	}

	public void setPctName(String pctName) {
		this.pctName = pctName;
	}

	public String getPctPriorityDate() {
		return this.pctPriorityDate;
	}

	public void setPctPriorityDate(String pctPriorityDate) {
		this.pctPriorityDate = pctPriorityDate;
	}

	public String getPersonnelFinalSecond() {
		return this.personnelFinalSecond;
	}

	public void setPersonnelFinalSecond(String personnelFinalSecond) {
		this.personnelFinalSecond = personnelFinalSecond;
	}

	public String getPersonnelLast() {
		return this.personnelLast;
	}

	public void setPersonnelLast(String personnelLast) {
		this.personnelLast = personnelLast;
	}

	public String getPublicNumber() {
		return this.publicNumber;
	}

	public void setPublicNumber(String publicNumber) {
		this.publicNumber = publicNumber;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarksFirst() {
		return this.remarksFirst;
	}

	public void setRemarksFirst(String remarksFirst) {
		this.remarksFirst = remarksFirst;
	}

	public String getRemarksSecond() {
		return this.remarksSecond;
	}

	public void setRemarksSecond(String remarksSecond) {
		this.remarksSecond = remarksSecond;
	}

	public String getReturnRemarks() {
		return this.returnRemarks;
	}

	public void setReturnRemarks(String returnRemarks) {
		this.returnRemarks = returnRemarks;
	}

	public String getReturnTime() {
		return this.returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getReturnedReceivername() {
		return this.returnedReceivername;
	}

	public void setReturnedReceivername(String returnedReceivername) {
		this.returnedReceivername = returnedReceivername;
	}

	public String getReturnedReceivernum() {
		return this.returnedReceivernum;
	}

	public void setReturnedReceivernum(String returnedReceivernum) {
		this.returnedReceivernum = returnedReceivernum;
	}

	public String getSchoolSignature() {
		return this.schoolSignature;
	}

	public void setSchoolSignature(String schoolSignature) {
		this.schoolSignature = schoolSignature;
	}

	public String getScopeOfPatentPatentStatus() {
		return this.scopeOfPatentPatentStatus;
	}

	public void setScopeOfPatentPatentStatus(String scopeOfPatentPatentStatus) {
		this.scopeOfPatentPatentStatus = scopeOfPatentPatentStatus;
	}

	public String getSecondApprovalLevel() {
		return this.secondApprovalLevel;
	}

	public void setSecondApprovalLevel(String secondApprovalLevel) {
		this.secondApprovalLevel = secondApprovalLevel;
	}

	public String getSubmissionTime() {
		return this.submissionTime;
	}

	public void setSubmissionTime(String submissionTime) {
		this.submissionTime = submissionTime;
	}

	public String getSubmittedJobno() {
		return this.submittedJobno;
	}

	public void setSubmittedJobno(String submittedJobno) {
		this.submittedJobno = submittedJobno;
	}

	public int getSubmittedNumber() {
		return this.submittedNumber;
	}

	public void setSubmittedNumber(int submittedNumber) {
		this.submittedNumber = submittedNumber;
	}

	public String getTerminationDate() {
		return this.terminationDate;
	}

	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getTimeFinalSecond() {
		return this.timeFinalSecond;
	}

	public void setTimeFinalSecond(String timeFinalSecond) {
		this.timeFinalSecond = timeFinalSecond;
	}

	public String getTimeLastTrial() {
		return this.timeLastTrial;
	}

	public void setTimeLastTrial(String timeLastTrial) {
		this.timeLastTrial = timeLastTrial;
	}

	public String getTimePatentTransfer() {
		return this.timePatentTransfer;
	}

	public void setTimePatentTransfer(String timePatentTransfer) {
		this.timePatentTransfer = timePatentTransfer;
	}

	public String getTypeOfFirstInventor() {
		return this.typeOfFirstInventor;
	}

	public void setTypeOfFirstInventor(String typeOfFirstInventor) {
		this.typeOfFirstInventor = typeOfFirstInventor;
	}

	public String getTypesCooperation() {
		return this.typesCooperation;
	}

	public void setTypesCooperation(String typesCooperation) {
		this.typesCooperation = typesCooperation;
	}

	public String getUploadTime() {
		return this.uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getUploaderNotes() {
		return this.uploaderNotes;
	}

	public void setUploaderNotes(String uploaderNotes) {
		this.uploaderNotes = uploaderNotes;
	}

	public String getUploaderNum() {
		return this.uploaderNum;
	}

	public void setUploaderNum(String uploaderNum) {
		this.uploaderNum = uploaderNum;
	}

}