package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the department_information database table.
 * 
 */
@Entity
@Table(name="department_information")
@NamedQuery(name="DepartmentInformation.findAll", query="SELECT d FROM DepartmentInformation d")
public class DepartmentInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;


	@Column(name="department_category")
	private String departmentCategory;

	@Column(name="department_serial_number")
	private int departmentSerialNumber;

	@Column(name="is_it_assessed")
	private String isItAssessed;

	private String name;

	@Column(name="people_in_2016")
	private int peopleIn2016;

	@Column(name="people_in_2017")
	private int peopleIn2017;

	@Column(name="people_in_2018")
	private int peopleIn2018;

	@Column(name="people_in_2019")
	private int peopleIn2019;

	@Column(name="people_in_2020")
	private int peopleIn2020;

	@Column(name="people_in_2021")
	private int peopleIn2021;

	public DepartmentInformation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getDepartmentCategory() {
		return this.departmentCategory;
	}

	public void setDepartmentCategory(String departmentCategory) {
		this.departmentCategory = departmentCategory;
	}

	public int getDepartmentSerialNumber() {
		return this.departmentSerialNumber;
	}

	public void setDepartmentSerialNumber(int departmentSerialNumber) {
		this.departmentSerialNumber = departmentSerialNumber;
	}

	public String getIsItAssessed() {
		return this.isItAssessed;
	}

	public void setIsItAssessed(String isItAssessed) {
		this.isItAssessed = isItAssessed;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPeopleIn2016() {
		return this.peopleIn2016;
	}

	public void setPeopleIn2016(int peopleIn2016) {
		this.peopleIn2016 = peopleIn2016;
	}

	public int getPeopleIn2017() {
		return this.peopleIn2017;
	}

	public void setPeopleIn2017(int peopleIn2017) {
		this.peopleIn2017 = peopleIn2017;
	}

	public int getPeopleIn2018() {
		return this.peopleIn2018;
	}

	public void setPeopleIn2018(int peopleIn2018) {
		this.peopleIn2018 = peopleIn2018;
	}

	public int getPeopleIn2019() {
		return this.peopleIn2019;
	}

	public void setPeopleIn2019(int peopleIn2019) {
		this.peopleIn2019 = peopleIn2019;
	}

	public int getPeopleIn2020() {
		return this.peopleIn2020;
	}

	public void setPeopleIn2020(int peopleIn2020) {
		this.peopleIn2020 = peopleIn2020;
	}

	public int getPeopleIn2021() {
		return this.peopleIn2021;
	}

	public void setPeopleIn2021(int peopleIn2021) {
		this.peopleIn2021 = peopleIn2021;
	}

}