package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the calendar_year database table.
 * 
 */
@Entity
@Table(name="calendar_year")
@NamedQuery(name="CalendarYear.findAll", query="SELECT c FROM CalendarYear c")
public class CalendarYear implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="department_name")
	private String departmentName;

	@Column(name="progress_score")
	private double progressScore;

	@Column(name="total_project_score")
	private double totalProjectScore;

	@Column(name="total_score")
	private double totalScore;

	private String year;

	public CalendarYear() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return this.departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public double getProgressScore() {
		return this.progressScore;
	}

	public void setProgressScore(double progressScore) {
		this.progressScore = progressScore;
	}

	public double getTotalProjectScore() {
		return this.totalProjectScore;
	}

	public void setTotalProjectScore(double totalProjectScore) {
		this.totalProjectScore = totalProjectScore;
	}

	public double getTotalScore() {
		return this.totalScore;
	}

	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

}