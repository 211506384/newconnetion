package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the scientificresearch_projects_file database table.
 * 
 */
@Entity
@Table(name="scientificresearch_projects_file")
@NamedQuery(name="ScientificresearchProjectsFile.findAll", query="SELECT s FROM ScientificresearchProjectsFile s")
public class ScientificresearchProjectsFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="account_number")
	private String accountNumber;

	@Column(name="actual_calculation")
	private String actualCalculation;

	@Column(name="address_supporting_materials")
	private String addressSupportingMaterials;

	@Column(name="approval_department")
	private String approvalDepartment;

	@Column(name="approval_number")
	private String approvalNumber;

	@Column(name="approval_scoreof_first")
	private String approvalScoreofFirst;

	@Column(name="approval_scoreof_second")
	private String approvalScoreofSecond;

	@Column(name="arts_science")
	private String artsScience;

	@Column(name="audit_status")
	private String auditStatus;

	@Column(name="closing_date")
	private String closingDate;

	private String college;

	@Column(name="comments_author")
	private String commentsAuthor;

	@Column(name="cooperation_unit")
	private String cooperationUnit;

	@Column(name="data_index")
	private String dataIndex;

	@Column(name="entry_name")
	private String entryName;

	private String expenditure;

	@Column(name="first_approval_level")
	private String firstApprovalLevel;

	@Column(name="form_achievements")
	private String formAchievements;

	@Column(name="full_name")
	private String fullName;

	@Column(name="funds_received")
	private String fundsReceived;

	@Column(name="job_number")
	private String jobNumber;

	@Column(name="main_contract_name")
	private String mainContractName;

	@Column(name="name_originator")
	private String nameOriginator;

	@Column(name="nameof_submitted")
	private String nameofSubmitted;

	@Column(name="nameof_uploader")
	private String nameofUploader;

	@Column(name="number_final_second")
	private String numberFinalSecond;

	@Column(name="number_initiating")
	private String numberInitiating;

	@Column(name="number_last_reviewer")
	private String numberLastReviewer;

	private String participants;

	@Column(name="personnel_final_second")
	private String personnelFinalSecond;

	@Column(name="personnel_last")
	private String personnelLast;

	@Column(name="planned_completion_date")
	private String plannedCompletionDate;

	@Column(name="project_approval")
	private String projectApproval;

	@Column(name="project_approval_date")
	private String projectApprovalDate;

	@Column(name="project_classification")
	private String projectClassification;

	@Column(name="project_code")
	private String projectCode;

	@Column(name="project_level")
	private String projectLevel;

	@Column(name="project_members")
	private String projectMembers;

	@Column(name="project_number")
	private String projectNumber;

	@Column(name="project_status")
	private String projectStatus;

	@Column(name="project_subclass")
	private String projectSubclass;

	@Column(name="project_type")
	private String projectType;

	@Column(name="ranking_undertaking_units")
	private String rankingUndertakingUnits;

	private String remarks;

	@Column(name="remarks_first")
	private String remarksFirst;

	@Column(name="remarks_second")
	private String remarksSecond;

	@Column(name="return_remarks")
	private String returnRemarks;

	@Column(name="return_time")
	private String returnTime;

	@Column(name="returned_receivername")
	private String returnedReceivername;

	@Column(name="returned_receivernum")
	private String returnedReceivernum;

	@Column(name="second_approval_level")
	private String secondApprovalLevel;

	@Column(name="self_financing")
	private String selfFinancing;

	@Column(name="start_time")
	private String startTime;

	@Column(name="subject_category")
	private String subjectCategory;

	@Column(name="subject_classification")
	private String subjectClassification;

	@Column(name="submission_time")
	private String submissionTime;

	@Column(name="submitted_jobno")
	private String submittedJobno;

	@Column(name="submitted_number")
	private int submittedNumber;

	@Column(name="time_final_second")
	private String timeFinalSecond;

	@Column(name="time_last_trial")
	private String timeLastTrial;

	@Column(name="upload_time")
	private String uploadTime;

	@Column(name="uploader_notes")
	private String uploaderNotes;

	@Column(name="uploader_num")
	private String uploaderNum;

	public ScientificresearchProjectsFile() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getActualCalculation() {
		return this.actualCalculation;
	}

	public void setActualCalculation(String actualCalculation) {
		this.actualCalculation = actualCalculation;
	}

	public String getAddressSupportingMaterials() {
		return this.addressSupportingMaterials;
	}

	public void setAddressSupportingMaterials(String addressSupportingMaterials) {
		this.addressSupportingMaterials = addressSupportingMaterials;
	}

	public String getApprovalDepartment() {
		return this.approvalDepartment;
	}

	public void setApprovalDepartment(String approvalDepartment) {
		this.approvalDepartment = approvalDepartment;
	}

	public String getApprovalNumber() {
		return this.approvalNumber;
	}

	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public String getApprovalScoreofFirst() {
		return this.approvalScoreofFirst;
	}

	public void setApprovalScoreofFirst(String approvalScoreofFirst) {
		this.approvalScoreofFirst = approvalScoreofFirst;
	}

	public String getApprovalScoreofSecond() {
		return this.approvalScoreofSecond;
	}

	public void setApprovalScoreofSecond(String approvalScoreofSecond) {
		this.approvalScoreofSecond = approvalScoreofSecond;
	}

	public String getArtsScience() {
		return this.artsScience;
	}

	public void setArtsScience(String artsScience) {
		this.artsScience = artsScience;
	}

	public String getAuditStatus() {
		return this.auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getClosingDate() {
		return this.closingDate;
	}

	public void setClosingDate(String closingDate) {
		this.closingDate = closingDate;
	}

	public String getCollege() {
		return this.college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCommentsAuthor() {
		return this.commentsAuthor;
	}

	public void setCommentsAuthor(String commentsAuthor) {
		this.commentsAuthor = commentsAuthor;
	}

	public String getCooperationUnit() {
		return this.cooperationUnit;
	}

	public void setCooperationUnit(String cooperationUnit) {
		this.cooperationUnit = cooperationUnit;
	}

	public String getDataIndex() {
		return this.dataIndex;
	}

	public void setDataIndex(String dataIndex) {
		this.dataIndex = dataIndex;
	}

	public String getEntryName() {
		return this.entryName;
	}

	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}

	public String getExpenditure() {
		return this.expenditure;
	}

	public void setExpenditure(String expenditure) {
		this.expenditure = expenditure;
	}

	public String getFirstApprovalLevel() {
		return this.firstApprovalLevel;
	}

	public void setFirstApprovalLevel(String firstApprovalLevel) {
		this.firstApprovalLevel = firstApprovalLevel;
	}

	public String getFormAchievements() {
		return this.formAchievements;
	}

	public void setFormAchievements(String formAchievements) {
		this.formAchievements = formAchievements;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFundsReceived() {
		return this.fundsReceived;
	}

	public void setFundsReceived(String fundsReceived) {
		this.fundsReceived = fundsReceived;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getMainContractName() {
		return this.mainContractName;
	}

	public void setMainContractName(String mainContractName) {
		this.mainContractName = mainContractName;
	}

	public String getNameOriginator() {
		return this.nameOriginator;
	}

	public void setNameOriginator(String nameOriginator) {
		this.nameOriginator = nameOriginator;
	}

	public String getNameofSubmitted() {
		return this.nameofSubmitted;
	}

	public void setNameofSubmitted(String nameofSubmitted) {
		this.nameofSubmitted = nameofSubmitted;
	}

	public String getNameofUploader() {
		return this.nameofUploader;
	}

	public void setNameofUploader(String nameofUploader) {
		this.nameofUploader = nameofUploader;
	}

	public String getNumberFinalSecond() {
		return this.numberFinalSecond;
	}

	public void setNumberFinalSecond(String numberFinalSecond) {
		this.numberFinalSecond = numberFinalSecond;
	}

	public String getNumberInitiating() {
		return this.numberInitiating;
	}

	public void setNumberInitiating(String numberInitiating) {
		this.numberInitiating = numberInitiating;
	}

	public String getNumberLastReviewer() {
		return this.numberLastReviewer;
	}

	public void setNumberLastReviewer(String numberLastReviewer) {
		this.numberLastReviewer = numberLastReviewer;
	}

	public String getParticipants() {
		return this.participants;
	}

	public void setParticipants(String participants) {
		this.participants = participants;
	}

	public String getPersonnelFinalSecond() {
		return this.personnelFinalSecond;
	}

	public void setPersonnelFinalSecond(String personnelFinalSecond) {
		this.personnelFinalSecond = personnelFinalSecond;
	}

	public String getPersonnelLast() {
		return this.personnelLast;
	}

	public void setPersonnelLast(String personnelLast) {
		this.personnelLast = personnelLast;
	}

	public String getPlannedCompletionDate() {
		return this.plannedCompletionDate;
	}

	public void setPlannedCompletionDate(String plannedCompletionDate) {
		this.plannedCompletionDate = plannedCompletionDate;
	}

	public String getProjectApproval() {
		return this.projectApproval;
	}

	public void setProjectApproval(String projectApproval) {
		this.projectApproval = projectApproval;
	}

	public String getProjectApprovalDate() {
		return this.projectApprovalDate;
	}

	public void setProjectApprovalDate(String projectApprovalDate) {
		this.projectApprovalDate = projectApprovalDate;
	}

	public String getProjectClassification() {
		return this.projectClassification;
	}

	public void setProjectClassification(String projectClassification) {
		this.projectClassification = projectClassification;
	}

	public String getProjectCode() {
		return this.projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectLevel() {
		return this.projectLevel;
	}

	public void setProjectLevel(String projectLevel) {
		this.projectLevel = projectLevel;
	}

	public String getProjectMembers() {
		return this.projectMembers;
	}

	public void setProjectMembers(String projectMembers) {
		this.projectMembers = projectMembers;
	}

	public String getProjectNumber() {
		return this.projectNumber;
	}

	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	public String getProjectStatus() {
		return this.projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public String getProjectSubclass() {
		return this.projectSubclass;
	}

	public void setProjectSubclass(String projectSubclass) {
		this.projectSubclass = projectSubclass;
	}

	public String getProjectType() {
		return this.projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getRankingUndertakingUnits() {
		return this.rankingUndertakingUnits;
	}

	public void setRankingUndertakingUnits(String rankingUndertakingUnits) {
		this.rankingUndertakingUnits = rankingUndertakingUnits;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarksFirst() {
		return this.remarksFirst;
	}

	public void setRemarksFirst(String remarksFirst) {
		this.remarksFirst = remarksFirst;
	}

	public String getRemarksSecond() {
		return this.remarksSecond;
	}

	public void setRemarksSecond(String remarksSecond) {
		this.remarksSecond = remarksSecond;
	}

	public String getReturnRemarks() {
		return this.returnRemarks;
	}

	public void setReturnRemarks(String returnRemarks) {
		this.returnRemarks = returnRemarks;
	}

	public String getReturnTime() {
		return this.returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getReturnedReceivername() {
		return this.returnedReceivername;
	}

	public void setReturnedReceivername(String returnedReceivername) {
		this.returnedReceivername = returnedReceivername;
	}

	public String getReturnedReceivernum() {
		return this.returnedReceivernum;
	}

	public void setReturnedReceivernum(String returnedReceivernum) {
		this.returnedReceivernum = returnedReceivernum;
	}

	public String getSecondApprovalLevel() {
		return this.secondApprovalLevel;
	}

	public void setSecondApprovalLevel(String secondApprovalLevel) {
		this.secondApprovalLevel = secondApprovalLevel;
	}

	public String getSelfFinancing() {
		return this.selfFinancing;
	}

	public void setSelfFinancing(String selfFinancing) {
		this.selfFinancing = selfFinancing;
	}

	public String getStartTime() {
		return this.startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getSubjectCategory() {
		return this.subjectCategory;
	}

	public void setSubjectCategory(String subjectCategory) {
		this.subjectCategory = subjectCategory;
	}

	public String getSubjectClassification() {
		return this.subjectClassification;
	}

	public void setSubjectClassification(String subjectClassification) {
		this.subjectClassification = subjectClassification;
	}

	public String getSubmissionTime() {
		return this.submissionTime;
	}

	public void setSubmissionTime(String submissionTime) {
		this.submissionTime = submissionTime;
	}

	public String getSubmittedJobno() {
		return this.submittedJobno;
	}

	public void setSubmittedJobno(String submittedJobno) {
		this.submittedJobno = submittedJobno;
	}

	public int getSubmittedNumber() {
		return this.submittedNumber;
	}

	public void setSubmittedNumber(int submittedNumber) {
		this.submittedNumber = submittedNumber;
	}

	public String getTimeFinalSecond() {
		return this.timeFinalSecond;
	}

	public void setTimeFinalSecond(String timeFinalSecond) {
		this.timeFinalSecond = timeFinalSecond;
	}

	public String getTimeLastTrial() {
		return this.timeLastTrial;
	}

	public void setTimeLastTrial(String timeLastTrial) {
		this.timeLastTrial = timeLastTrial;
	}

	public String getUploadTime() {
		return this.uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getUploaderNotes() {
		return this.uploaderNotes;
	}

	public void setUploaderNotes(String uploaderNotes) {
		this.uploaderNotes = uploaderNotes;
	}

	public String getUploaderNum() {
		return this.uploaderNum;
	}

	public void setUploaderNum(String uploaderNum) {
		this.uploaderNum = uploaderNum;
	}

}