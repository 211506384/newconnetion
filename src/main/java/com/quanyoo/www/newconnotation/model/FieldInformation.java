package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the field_information database table.
 * 
 */
@Entity
@Table(name="field_information")
@NamedQuery(name="FieldInformation.findAll", query="SELECT f FROM FieldInformation f")
public class FieldInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="assessment_classification")
	private String assessmentClassification;

	@Column(name="field_1")
	private String field1;

	@Column(name="field_10")
	private String field10;

	@Column(name="field_11")
	private String field11;

	@Column(name="field_12")
	private String field12;

	@Column(name="field_13")
	private String field13;

	@Column(name="field_14")
	private String field14;

	@Column(name="field_15")
	private String field15;

	@Column(name="field_16")
	private String field16;

	@Column(name="field_17")
	private String field17;

	@Column(name="field_18")
	private String field18;

	@Column(name="field_19")
	private String field19;

	@Column(name="field_2")
	private String field2;

	@Column(name="field_20")
	private String field20;

	@Column(name="field_21")
	private String field21;

	@Column(name="field_22")
	private String field22;

	@Column(name="field_23")
	private String field23;

	@Column(name="field_24")
	private String field24;

	@Column(name="field_25")
	private String field25;

	@Column(name="field_26")
	private String field26;

	@Column(name="field_27")
	private String field27;

	@Column(name="field_28")
	private String field28;

	@Column(name="field_29")
	private String field29;

	@Column(name="field_3")
	private String field3;

	@Column(name="field_30")
	private String field30;

	@Column(name="field_31")
	private String field31;

	@Column(name="field_32")
	private String field32;

	@Column(name="field_33")
	private String field33;

	@Column(name="field_34")
	private String field34;

	@Column(name="field_35")
	private String field35;

	@Column(name="field_36")
	private String field36;

	@Column(name="field_37")
	private String field37;

	@Column(name="field_38")
	private String field38;

	@Column(name="field_39")
	private String field39;

	@Column(name="field_4")
	private String field4;

	@Column(name="field_40")
	private String field40;

	@Column(name="field_5")
	private String field5;

	@Column(name="field_6")
	private String field6;

	@Column(name="field_7")
	private String field7;

	@Column(name="field_8")
	private String field8;

	@Column(name="field_9")
	private String field9;

	@Column(name="five_level")
	private String fiveLevel;

	@Column(name="form_no")
	private int formNo;

	@Column(name="fourth_level")
	private String fourthLevel;

	@Column(name="second_level")
	private String secondLevel;

	@Column(name="table_name")
	private String tableName;

	@Column(name="third_level")
	private String thirdLevel;

	public FieldInformation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAssessmentClassification() {
		return this.assessmentClassification;
	}

	public void setAssessmentClassification(String assessmentClassification) {
		this.assessmentClassification = assessmentClassification;
	}

	public String getField1() {
		return this.field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField10() {
		return this.field10;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public String getField11() {
		return this.field11;
	}

	public void setField11(String field11) {
		this.field11 = field11;
	}

	public String getField12() {
		return this.field12;
	}

	public void setField12(String field12) {
		this.field12 = field12;
	}

	public String getField13() {
		return this.field13;
	}

	public void setField13(String field13) {
		this.field13 = field13;
	}

	public String getField14() {
		return this.field14;
	}

	public void setField14(String field14) {
		this.field14 = field14;
	}

	public String getField15() {
		return this.field15;
	}

	public void setField15(String field15) {
		this.field15 = field15;
	}

	public String getField16() {
		return this.field16;
	}

	public void setField16(String field16) {
		this.field16 = field16;
	}

	public String getField17() {
		return this.field17;
	}

	public void setField17(String field17) {
		this.field17 = field17;
	}

	public String getField18() {
		return this.field18;
	}

	public void setField18(String field18) {
		this.field18 = field18;
	}

	public String getField19() {
		return this.field19;
	}

	public void setField19(String field19) {
		this.field19 = field19;
	}

	public String getField2() {
		return this.field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField20() {
		return this.field20;
	}

	public void setField20(String field20) {
		this.field20 = field20;
	}

	public String getField21() {
		return this.field21;
	}

	public void setField21(String field21) {
		this.field21 = field21;
	}

	public String getField22() {
		return this.field22;
	}

	public void setField22(String field22) {
		this.field22 = field22;
	}

	public String getField23() {
		return this.field23;
	}

	public void setField23(String field23) {
		this.field23 = field23;
	}

	public String getField24() {
		return this.field24;
	}

	public void setField24(String field24) {
		this.field24 = field24;
	}

	public String getField25() {
		return this.field25;
	}

	public void setField25(String field25) {
		this.field25 = field25;
	}

	public String getField26() {
		return this.field26;
	}

	public void setField26(String field26) {
		this.field26 = field26;
	}

	public String getField27() {
		return this.field27;
	}

	public void setField27(String field27) {
		this.field27 = field27;
	}

	public String getField28() {
		return this.field28;
	}

	public void setField28(String field28) {
		this.field28 = field28;
	}

	public String getField29() {
		return this.field29;
	}

	public void setField29(String field29) {
		this.field29 = field29;
	}

	public String getField3() {
		return this.field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField30() {
		return this.field30;
	}

	public void setField30(String field30) {
		this.field30 = field30;
	}

	public String getField31() {
		return this.field31;
	}

	public void setField31(String field31) {
		this.field31 = field31;
	}

	public String getField32() {
		return this.field32;
	}

	public void setField32(String field32) {
		this.field32 = field32;
	}

	public String getField33() {
		return this.field33;
	}

	public void setField33(String field33) {
		this.field33 = field33;
	}

	public String getField34() {
		return this.field34;
	}

	public void setField34(String field34) {
		this.field34 = field34;
	}

	public String getField35() {
		return this.field35;
	}

	public void setField35(String field35) {
		this.field35 = field35;
	}

	public String getField36() {
		return this.field36;
	}

	public void setField36(String field36) {
		this.field36 = field36;
	}

	public String getField37() {
		return this.field37;
	}

	public void setField37(String field37) {
		this.field37 = field37;
	}

	public String getField38() {
		return this.field38;
	}

	public void setField38(String field38) {
		this.field38 = field38;
	}

	public String getField39() {
		return this.field39;
	}

	public void setField39(String field39) {
		this.field39 = field39;
	}

	public String getField4() {
		return this.field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField40() {
		return this.field40;
	}

	public void setField40(String field40) {
		this.field40 = field40;
	}

	public String getField5() {
		return this.field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public String getField6() {
		return this.field6;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public String getField7() {
		return this.field7;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public String getField8() {
		return this.field8;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public String getField9() {
		return this.field9;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public String getFiveLevel() {
		return this.fiveLevel;
	}

	public void setFiveLevel(String fiveLevel) {
		this.fiveLevel = fiveLevel;
	}

	public int getFormNo() {
		return this.formNo;
	}

	public void setFormNo(int formNo) {
		this.formNo = formNo;
	}

	public String getFourthLevel() {
		return this.fourthLevel;
	}

	public void setFourthLevel(String fourthLevel) {
		this.fourthLevel = fourthLevel;
	}

	public String getSecondLevel() {
		return this.secondLevel;
	}

	public void setSecondLevel(String secondLevel) {
		this.secondLevel = secondLevel;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getThirdLevel() {
		return this.thirdLevel;
	}

	public void setThirdLevel(String thirdLevel) {
		this.thirdLevel = thirdLevel;
	}

}