package com.quanyoo.www.newconnotation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_information database table.
 * 
 */
@Entity
@Table(name="user_information")
@NamedQuery(name="UserInformation.findAll", query="SELECT u FROM UserInformation u")
public class UserInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="collective_identity_authority")
	private String collectiveIdentityAuthority;

	private String department;

	@Column(name="full_name")
	private String fullName;

	@Column(name="job_number")
	private String jobNumber;

	private String jurisdiction;

	private String pwd;

	@Column(name="user_name")
	private String userName;

	public UserInformation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCollectiveIdentityAuthority() {
		return this.collectiveIdentityAuthority;
	}

	public void setCollectiveIdentityAuthority(String collectiveIdentityAuthority) {
		this.collectiveIdentityAuthority = collectiveIdentityAuthority;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getJobNumber() {
		return this.jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getJurisdiction() {
		return this.jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}