package com.quanyoo.www.newconnotation.pojo;


public class User {
    private String id;
    private String userName;
    private String jobNum;
    private String password;
	private String department;
	private String fullName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getJobNum() {
		return jobNum;
	}
	public void setJobNum(String jobNum) {
		this.jobNum = jobNum;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public User(String id, String userName, String jobNum, String password) {
		super();
		this.id = id;
		this.userName = userName;
		this.jobNum = jobNum;
		this.password = password;
	}
	public User() {
		super();
	}

}
