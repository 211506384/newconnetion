package com.quanyoo.www.newconnotation.pojo;

import java.util.List;

public class Table{
    private int id;

    private String create_table_num;
	private String create_table_english_name;
	private String create_table_chinese_name;
	private String create_table_description;
	private String create_table_type;
	private List<TableColumn>  tableColumn;

	public String getCreate_table_type() {
		return create_table_type;
	}

	public void setCreate_table_type(String create_table_type) {
		this.create_table_type = create_table_type;
	}


	
	public Table(int id, String create_table_num, String create_table_english_name, String create_table_chinese_name,
			String create_table_description, String create_table_type, List<TableColumn> tableColumn) {
		super();
		this.id = id;
		this.create_table_num = create_table_num;
		this.create_table_english_name = create_table_english_name;
		this.create_table_chinese_name = create_table_chinese_name;
		this.create_table_description = create_table_description;
		this.create_table_type = create_table_type;
		this.tableColumn = tableColumn;
	}

	public Table() {
		super();
	}
	
	public List<TableColumn> getTableColumn() {
		return tableColumn;
	}
	public void setTableColumn(List<TableColumn> tableColumn) {
		this.tableColumn = tableColumn;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCreate_table_num() {
		return create_table_num;
	}
	public void setCreate_table_num(String create_table_num) {
		this.create_table_num = create_table_num;
	}
	public String getCreate_table_english_name() {
		return create_table_english_name;
	}
	public void setCreate_table_english_name(String create_table_english_name) {
		this.create_table_english_name = create_table_english_name;
	}
	public String getCreate_table_chinese_name() {
		return create_table_chinese_name;
	}
	public void setCreate_table_chinese_name(String create_table_chinese_name) {
		this.create_table_chinese_name = create_table_chinese_name;
	}
	public String getCreate_table_description() {
		return create_table_description;
	}
	public void setCreate_table_description(String create_table_description) {
		this.create_table_description = create_table_description;
	}

}
