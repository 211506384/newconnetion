package com.quanyoo.www.newconnotation.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ：WURP
 * @description：TODO
 * @date ：2020/8/5 17:38
 */
public class SysLog implements Serializable {
    private Long id; //用户id

    private String jobNum; //用户工号

    private String dept; //用户部门

    private String username; //用户名

    private String fullName;//姓名

    private String operation; //操作

    private String method; //方法名

    private String params; //参数

    private String ip; //ip地址

    private String createDate; //操作时间
    //创建getter和setter方法

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getJobNum() {
        return jobNum;
    }

    public void setJobNum(String jobNum) {
        this.jobNum = jobNum;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "SysLog{" +
                "id=" + id +
                ", jobNum=" + jobNum +
                ", dept='" + dept + '\'' +
                ", username='" + username + '\'' +
                ", fullName='" + fullName + '\'' +
                ", operation='" + operation + '\'' +
                ", method='" + method + '\'' +
                ", params='" + params + '\'' +
                ", ip='" + ip + '\'' +
                ", createDate='" + createDate + '\'' +
                '}';
    }
}
