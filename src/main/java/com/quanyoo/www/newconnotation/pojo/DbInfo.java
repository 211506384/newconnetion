package com.quanyoo.www.newconnotation.pojo;

public class DbInfo {
	String driveClassName;
	String password;
	String userName;
	String url;
	public String getDriveClassName() {
		return driveClassName;
	}
	public void setDriveClassName(String driveClassName) {
		this.driveClassName = driveClassName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public DbInfo(String driveClassName, String password, String userName, String url) {
		super();
		this.driveClassName = driveClassName;
		this.password = password;
		this.userName = userName;
		this.url = url;
	}
	public DbInfo() {
		super();
	}
	@Override
	public String toString() {
		return "DbInfo [driveClassName=" + driveClassName + ", password=" + password + ", userName=" + userName
				+ ", url=" + url + "]";
	}

	

}
