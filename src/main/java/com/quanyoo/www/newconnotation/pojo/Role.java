package com.quanyoo.www.newconnotation.pojo;

import java.util.Set;

public class Role {
    private String roleid;
    private String roleName;
    /**
     * 角色对应权限集合
     */

    private Set<Permissions> permissions;

	public Role(String id, String roleName, Set<Permissions> permissions) {
		super();
		this.roleid = id;
		this.roleName = roleName;
		this.permissions = permissions;
	}
	public String getId() {
		return roleid;
	}
	public void setId(String id) {
		this.roleid = id;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Set<Permissions> getPermissions() {
		return permissions;
	}
	public void setPermissions(Set<Permissions> permissions) {
		this.permissions = permissions;
	}

}
