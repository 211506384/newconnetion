package com.quanyoo.www.newconnotation.pojo;

public class Permissions {
	private String permissionsid;
    private String permissionsName;
	public Permissions(String id, String permissionsName) {
		super();
		this.permissionsid = id;
		this.permissionsName = permissionsName;
	}
	public String getId() {
		return permissionsid;
	}

	public void setId(String id) {
		this.permissionsid = id;
	}
	public String getPermissionsName() {
		return permissionsName;
	}
	public void setPermissionsName(String permissionsName) {
		this.permissionsName = permissionsName;
	}

}
