package com.quanyoo.www.newconnotation.pojo;

import java.util.List;

public class Menu {
	// 菜单id
	private String id;
	// 菜单名称
	private String text;
	// 父菜单id
	private String parentId;
	// 菜单url
	private String url;
	// 菜单state
	private String state;
	// 菜单顺序
	private int orderweight;
	// 子菜单
	private List<Menu> children;
	private List<Menu> parents;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getOrderweight() {
		return orderweight;
	}

	public void setOrderweight(int orderweight) {
		this.orderweight = orderweight;
	}

	public List<Menu> getParents() {
		return parents;
	}

	public void setParents(List<Menu> parents) {
		this.parents = parents;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getOrder() {
		return orderweight;
	}

	public void setOrder(int order) {
		this.orderweight = order;
	}

	public List<Menu> getChildren() {
		return children;
	}

	public void setChildren(List<Menu> children) {
		this.children = children;
	}


}