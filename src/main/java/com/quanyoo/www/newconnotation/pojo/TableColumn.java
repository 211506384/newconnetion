package com.quanyoo.www.newconnotation.pojo;


/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
public class TableColumn implements  Comparable<TableColumn>{
    private int id;
    private int  create_table_id;
    private int  is_need;
    private String create_table_name;
	private String create_table_column;
	private String create_table_column_type;
	private String create_table_chinese_column;
	
	private String is_statistics;
	private int field_weight;
	

	public TableColumn(int id, int create_table_id, int is_need, String create_table_name, String create_table_column,
			String create_table_column_type, String create_table_chinese_column, String is_statistics,
			int field_weight) {
		super();
		this.id = id;
		this.create_table_id = create_table_id;
		this.is_need = is_need;
		this.create_table_name = create_table_name;
		this.create_table_column = create_table_column;
		this.create_table_column_type = create_table_column_type;
		this.create_table_chinese_column = create_table_chinese_column;
		this.is_statistics = is_statistics;
		this.field_weight = field_weight;
	}
	public String getIs_statistics() {
		return is_statistics;
	}
	public void setIs_statistics(String is_statistics) {
		this.is_statistics = is_statistics;
	}
	public int getField_weight() {
		return field_weight;
	}
	public void setField_weight(int field_weight) {
		this.field_weight = field_weight;
	}
	public TableColumn() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCreate_table_id() {
		return create_table_id;
	}
	public void setCreate_table_id(int create_table_id) {
		this.create_table_id = create_table_id;
	}
	public int getIs_need() {
		return is_need;
	}
	public void setIs_need(int is_need) {
		this.is_need = is_need;
	}
	public String getCreate_table_name() {
		return create_table_name;
	}
	public void setCreate_table_name(String create_table_name) {
		this.create_table_name = create_table_name;
	}
	public String getCreate_table_column() {
		return create_table_column;
	}
	public void setCreate_table_column(String create_table_column) {
		this.create_table_column = create_table_column;
	}
	public String getCreate_table_column_type() {
		return create_table_column_type;
	}
	public void setCreate_table_column_type(String create_table_column_type) {
		this.create_table_column_type = create_table_column_type;
	}
	public String getCreate_table_chinese_column() {
		return create_table_chinese_column;
	}
	public void setCreate_table_chinese_column(String create_table_chinese_column) {
		this.create_table_chinese_column = create_table_chinese_column;
	}
	
	@Override
	public String toString() {
		return "TableColumn [id=" + id + ", create_table_id=" + create_table_id + ", is_need=" + is_need
				+ ", create_table_name=" + create_table_name + ", create_table_column=" + create_table_column
				+ ", create_table_column_type=" + create_table_column_type + ", create_table_chinese_column="
				+ create_table_chinese_column + ", is_statistics=" + is_statistics + ", field_weight=" + field_weight
				+ "]";
	}
	@Override
	public int compareTo(TableColumn o) {
		// TODO Auto-generated method stub
		return o.getField_weight()-this.field_weight;
	}


}
