package com.quanyoo.www.newconnotation.authConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;
public class DefaultView implements WebMvcConfigurer {
	@Autowired
	ImageConfig imageConfig;

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("login.html");
	
	}
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
		.allowedOrigins("*").allowCredentials(true)
		.allowedMethods("GET","POST","PUT","DELETE","OPTIONS")
		.maxAge(3600);
	}
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		/** 图片传路径**/
		registry.addResourceHandler("/static/**").addResourceLocations("file:///" + imageConfig.getUploadPath());
	}
}
