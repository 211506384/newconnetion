package com.quanyoo.www.newconnotation.authConfig;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.quanyoo.www.newconnotation.pojo.User;
import com.quanyoo.www.newconnotation.service.LoginService;



public class CustomRealm extends AuthorizingRealm {

    @Autowired
    private LoginService loginService;


    /**
     * 大坑！，必须重写此方法，不然Shiro会报错
     */
//    @Override
//    public boolean supports(AuthenticationToken token) {
//        return token instanceof JWTToken;
//    }
/*   有当需要检测用户权限的时候才会调用此方法*/
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //String username = JWTUtil.getUsername(principalCollection.toString());
    	 // 获取到登录用户的主体信息
        User user = (User) SecurityUtils.getSubject().getPrincipal();

    	// 创建简单授权信息对象
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        String userId = user.getId();
        //根据userId查找角色；权限
        List<Map<String, Object>>  roleslisst =  loginService.getUserRolesByUserId(userId);

       // 存放用户角色信息集合
        Set<String> roleSet = new HashSet<>();
        // 下面两行后续从数据库获取用户拥有的角色名称
        List<Map<String, Object>> permissionlist = new ArrayList<Map<String,Object>>();
        if(roleslisst.size()>0) {
        	for(Map<String, Object> map :roleslisst) {

        		roleSet.add(map.get("role_name").toString());
        	}

        	permissionlist = loginService.getUserPermissonsByUserId(userId);
        }


        // 存放用户的获得授权的资源信息
        Set<String> permissionSet = new HashSet<>();
        // 下面三行后续从数据库获取用户拥有的授权资源名称
        if(permissionlist.size()>0) {
        	for(Map<String, Object> map :permissionlist) {

        		permissionSet.add(map.get("permission_name").toString());
        	}
        }
        authorizationInfo.setRoles(roleSet);
        authorizationInfo.setStringPermissions(permissionSet);
        return authorizationInfo;
    }
    /**
     * 默认使用此方法进行用户名正确与否验证，错误抛出异常即可。
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
      
        //加这一步的目的是在Post请求的时候会先进认证，然后在到请求
        if (authenticationToken.getPrincipal() == null) {
            return null;
        }
        // 从token中获取到用户输入的电话号码
        String username = authenticationToken.getPrincipal().toString();
        // 从token中获取到用户输入的密码
        String password = new String((char[]) authenticationToken.getCredentials());
    	//String token = (String) authenticationToken.getCredentials();

        // 解密获得username，用于和数据库进行对比
       // String username = JWTUtil.getUsername(token);
        if (username == null) {
            throw new AuthenticationException("token 无效");
        }

        User user = loginService.findUserByphone(username);
        if (user == null) {
            throw new AuthenticationException("用户不存在！");
        }
        
        if (!(user.getPassword().equals(password))) {
            throw new AuthenticationException("用户名与密码不匹配！");
        }

        //加这一步的目的是在Post请求的时候会先进认证，然后在到请求
        if (authenticationToken.getPrincipal() == null) {
            return null;
        }
        //SimpleAuthenticationInfo
        return new SimpleAuthenticationInfo(user, user.getPassword().toString(), getName());
    }
}
