package com.quanyoo.www.newconnotation.authConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Description: 自定义资源映射
 * <p>
 *   设置虚拟路径，访问绝对路径下资源
 * </p>
 */
/*
@ComponentScan
@Configuration
public class WebConfigurer extends WebMvcConfigurerAdapter {
    @Autowired
    ImageConfig imageConfig;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("file:///"+imageConfig.getUploadPath());
    }
}
*/
