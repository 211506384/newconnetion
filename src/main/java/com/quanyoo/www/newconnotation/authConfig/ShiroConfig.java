package com.quanyoo.www.newconnotation.authConfig;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

	// 不加这个注解不生效，添加注解支持
	@Bean
	@ConditionalOnMissingBean
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator defaultAAP = new DefaultAdvisorAutoProxyCreator();
		defaultAAP.setProxyTargetClass(true);
		return defaultAAP;
	}

	// 将自己的验证方式加入容器
	@Bean
	public CustomRealm myShiroRealm() {
		CustomRealm customRealm = new CustomRealm();
		return customRealm;
	}

	// 权限管理，配置主要是Realm的管理认证
	@Bean
	public SecurityManager securityManager() {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		securityManager.setRealm(myShiroRealm());
		return securityManager;
	}

	// Filter工厂，设置对应的过滤条件和跳转条件
	@Bean
	public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setSecurityManager(securityManager);

		// Map<String, Filter> filtersMap = new LinkedHashMap<String, Filter>();
		// filtersMap.put("JWTFilter", new JWTFilter());
		// shiroFilterFactoryBean.setFilters(filtersMap);
		shiroFilterFactoryBean.setUnauthorizedUrl("/401");
		// map.put("/*", "myAccessControlFilter");
		Map<String, String> filterRuleMap = new HashMap<>();
		// 登出
		// map.put("/logout", "logout");
		// 对所有用户认证
		// anon 不认证
		// authc 需认证
		// 放行静态资源
		filterRuleMap.put("/static/**", "anon");
		filterRuleMap.put("/**", "anon");
		filterRuleMap.put("/401", "anon");
		filterRuleMap.put("/dologin", "anon");
		filterRuleMap.put("/dologinPhone", "anon");
		filterRuleMap.put("/PastYear/year_index", "authc");
		// filterRuleMap.put("/index", "authc");
		// 登录成功后要跳转的链接
		shiroFilterFactoryBean.setLoginUrl("/login");
		shiroFilterFactoryBean.setFilterChainDefinitionMap(filterRuleMap);
		return shiroFilterFactoryBean;
	}

	// 加入注解的使用，不加入这个注解不生效
	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
		return authorizationAttributeSourceAdvisor;
	}

	/**
	 * 配置ShiroDialect，用于Shiro和thymeleaf标签配合使用
	 * 
	 * @return
	 */
	@Bean
	public ShiroDialect shiroDialect() {
		return new ShiroDialect();
	}

}
