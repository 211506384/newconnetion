package com.quanyoo.www.newconnotation.exportExcel;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

public class toStringUtil {
	/*
	 * 把List集合转化成指定格式的String类型
	 */
	public static String ListToString(List<String> list){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i));	
			sb.append(",");	
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}	
	/*
	 * 判断字符串是否为数字
	 */
	public static boolean isNumber(String str){
		for (int i = 0; i < str.length(); i++) {
			if(!Character.isDigit(str.charAt(i))){
				return false;
			}
		}
		return true;
	}

}
