package com.quanyoo.www.newconnotation.exportExcel;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.quanyoo.www.newconnotation.pojo.Table;
import com.quanyoo.www.newconnotation.pojo.TableColumn;
import com.quanyoo.www.newconnotation.service.AopYearJdbcService;
import com.quanyoo.www.newconnotation.service.AopYearService;
 
@Service
@Transactional
public class ExcelImportService {
	@Autowired
	private AopYearJdbcService aopYearJdbcService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private AopYearService aopYearService;
	
 
	/**
	 * 
	 * @param tableName		指定Excel文件导入到哪个表
	 * @param file			Excel文件
	 * @param start			指定Excel文件导入数据的起始值，全部导入则为1
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean ExcelImport(String year,String tableName, MultipartFile file, int start) throws Exception {
		
		boolean notNull=false;    //判断Excel文件是否有内容
		String filename=file.getOriginalFilename();
		
		if (!filename.matches("^.+\\.(?i)(xls)$") && !filename.matches("^.+\\.(?i)(xlsx)$")) {
                    throw new Exception("上传文件格式不正确");
                }
		//判断Excel文件的版本
		boolean isExcel2003=true;
	        if (filename.matches("^.+\\.(?i)(xlsx)$")) {
                    isExcel2003 = false;
                }
	    
	        InputStream is=file.getInputStream();
	        Workbook wb=null;	    
	        if (isExcel2003) {
                wb = new HSSFWorkbook(is);
                } else {
                wb = new XSSFWorkbook(is);
                }
	    
	        //获取要导入的表的字段值，因为我的属性文件里表名前都加了cqwf2
  		//getProperties=SystemConfig.getProperty("cqwf2."+tableName);
	        	Map<String, Object> params=new HashMap<String, Object>();
	        	params.put("tableName", tableName);
	        	List<Table> listMap= aopYearJdbcService.getTableAndColumns(year,jdbcTemplate, tableName);
	        	List<TableColumn> columns = listMap.get(0).getTableColumn();
	        	//String[] info=getProperties.split(",");
  		
  		//获取Excel文件的第一页sheet，判断是否有信息
	      
	        	Sheet sheet=wb.getSheetAt(0);
	        if(sheet!=null){
	    	    notNull=true;
	        }
	    
	        //创建两个List集合，一个保存属性文件中的字段值，一个保存Excel文件中每一行的单元格的值
	        List<String> keysList=new ArrayList<String>();
	        List<String> valuesList=new ArrayList<String>();
	   
	    //遍历Excel文件
	    int totalRows = sheet.getPhysicalNumberOfRows();	//获取行数，第一行是标题
	    Row row=null;
	    int totalCells=0;	
        int coloumNum=sheet.getRow(0).getPhysicalNumberOfCells();
    	// 表中的字段
	    for (int k = 0; k < coloumNum;k++) {
	    	Cell cell_2 = sheet.getRow(0).getCell(k);
			cell_2.setCellType(CellType.STRING);
			String cell_3 = cell_2.getStringCellValue();
			for(TableColumn column:columns) {
				if(cell_3.equals(column.getCreate_table_chinese_column())) {
					keysList.add(column.getCreate_table_column());
					break;
				}
			}
	    }
	    for (int i = start; i < totalRows; i++) {
	    	valuesList.clear();
	    	//遍历单元格
	    	row=sheet.getRow(i);
	    	totalCells= row.getPhysicalNumberOfCells();		//获取每一行的单元格数

	    	//循环设置每个单元格的数据类型为String类型
	    	for (int j = 0; j < totalCells; j++) {
	    		if(row.getCell(j)!=null){
			          row.getCell(j).setCellType(CellType.STRING);
			    }
//	    		 row.getCell(j).getStringCellValue();
			}
			
	    	//获取每个单元格的数据，保存到集合中
	    	for (int t = 0; t < totalCells; t++) {	    		
	    		//判断单元格类型是否为空
	    		if(row.getCell(t, row.RETURN_BLANK_AS_NULL)==null){
	    			Cell cell=row.createCell(t); 	    			
	    			cell.setCellValue("NULL");
	    		}
	    		String cellInfo=row.getCell(t).getStringCellValue();
	    		//判断单元格内容是否为空
	    		if(StringUtils.isBlank(cellInfo)){
	    			cellInfo="NULL";
	    		}	    		
	    		//再判断是数字类型还是字符类型还是空类型
	    		if(toStringUtil.isNumber(cellInfo) || cellInfo.equals("NULL")){
	    			valuesList.add(cellInfo);
	 	    	}else{
	    			valuesList.add("\""+cellInfo+"\"");
	 	    	}			
			}
			int nullvalue = keysList.size()-valuesList.size();
			for (int j = 0; j < nullvalue; j++) {
				valuesList.add(null);
			}
	    	StringBuilder sb = new StringBuilder(toStringUtil.ListToString(valuesList));
	    	if(sb.charAt(sb.length() - 1)==','){
	    		sb.deleteCharAt(sb.length() - 1);
	    	}
	    	//每循环一行，调用数据访问层的方法,导入一行数据到数据库	    	
	    	aopYearJdbcService.addInfo(year,jdbcTemplate,tableName, toStringUtil.ListToString(keysList), sb.toString());    	
		}
		return notNull;
	}
	/**
	 * 
	 * @param tableName		指定Excel文件导入到哪个表
	 * @param file			Excel文件
	 * @param start			指定Excel文件导入数据的起始值，全部导入则为1
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor=Exception.class)
	public boolean ExcelImportUpdate(String year,String tableName, MultipartFile file, int start) throws Exception {
		
		boolean notNull=false;    //判断Excel文件是否有内容
		String filename=file.getOriginalFilename();
		
		if (!filename.matches("^.+\\.(?i)(xls)$") && !filename.matches("^.+\\.(?i)(xlsx)$")) {
                    throw new Exception("上传文件格式不正确");
                }
		//判断Excel文件的版本
		boolean isExcel2003=true;
	        if (filename.matches("^.+\\.(?i)(xlsx)$")) {
                    isExcel2003 = false;
                }
	    
	        InputStream is=file.getInputStream();
	        Workbook wb=null;	    
	        if (isExcel2003) {
                wb = new HSSFWorkbook(is);
                } else {
                wb = new XSSFWorkbook(is);
                }
	    
	        //获取要导入的表的字段值，因为我的属性文件里表名前都加了cqwf2
  		//getProperties=SystemConfig.getProperty("cqwf2."+tableName);
	        	Map<String, Object> params=new HashMap<String, Object>();
	        	params.put("tableName", tableName);
	        	List<Table> listMap= aopYearJdbcService.getTableAndColumns(year,jdbcTemplate, tableName);
	        	List<TableColumn> columns = listMap.get(0).getTableColumn();
	        	//String[] info=getProperties.split(",");
  		
  		//获取Excel文件的第一页sheet，判断是否有信息
	      
	        	Sheet sheet=wb.getSheetAt(0);
	        if(sheet!=null){
	    	    notNull=true;
	        }
	    
	        //创建两个List集合，一个保存属性文件中的字段值，一个保存Excel文件中每一行的单元格的值
	        List<String> keysList=new ArrayList<String>();
	        List<String> valuesList=new ArrayList<String>();
	   
	    //遍历Excel文件
	    int totalRows = sheet.getPhysicalNumberOfRows();	//获取行数，第一行是标题
	    Row row=null;
	    int totalCells=0;	
        int coloumNum=sheet.getRow(0).getPhysicalNumberOfCells();
    	// 表中的字段
	    for (int k = 0; k < coloumNum;k++) {
	    	Cell cell_2 = sheet.getRow(0).getCell(k);
			cell_2.setCellType(CellType.STRING);
			String cell_3 = cell_2.getStringCellValue();
			for(TableColumn column:columns) {
				if(cell_3.equals(column.getCreate_table_chinese_column())) {
					keysList.add(column.getCreate_table_column());
					break;
				}
			}
	    }
	    for (int i = start; i < totalRows; i++) {
	    	valuesList.clear();
	    	String idvString="";
	    	//遍历单元格
	    	row=sheet.getRow(i);
	    	totalCells= row.getPhysicalNumberOfCells();		//获取每一行的单元格数

	    	//循环设置每个单元格的数据类型为String类型
	    	for (int j = 0; j < totalCells; j++) {
	    		if(row.getCell(j)!=null){
			          row.getCell(j).setCellType(CellType.STRING);
			    }
//	    		 row.getCell(j).getStringCellValue();
			}
	    	//获取每个单元格的数据，保存到集合中
	    	for (int t = 0; t < totalCells; t++) {	    		
	    		//判断单元格类型是否为空
	    		if(row.getCell(t, row.RETURN_BLANK_AS_NULL)==null){
	    			Cell cell=row.createCell(t); 	    			
	    			cell.setCellValue("NULL");
	    		}
	    		String cellInfo=row.getCell(t).getStringCellValue();
	    		//判断单元格内容是否为空
	    		if(StringUtils.isBlank(cellInfo)){
	    			cellInfo="NULL";
	    		}	    		
	    		//再判断是数字类型还是字符类型还是空类型
	    		if(toStringUtil.isNumber(cellInfo) || cellInfo.equals("NULL")){
	    			if(keysList.get(t).equals("id")) {
	    				idvString=keysList.get(t)+"="+cellInfo;
	    			}else {
	    				valuesList.add(keysList.get(t)+"="+cellInfo);
	    			}
	    			
	 	    	}else{
	    			valuesList.add(keysList.get(t)+"="+"\""+cellInfo+"\"");
	 	    	}			
			}
		/*	int nullvalue = keysList.size()-valuesList.size();
			for (int j = 0; j < nullvalue; j++) {
				
				valuesList.add(null);
			}*/
	    	StringBuilder sb = new StringBuilder(toStringUtil.ListToString(valuesList));
	    	if(sb.charAt(sb.length() - 1)==','){
	    		sb.deleteCharAt(sb.length() - 1);
	    	}
	    	if(!"".equals(idvString)) {

		    	//每循环一行，调用数据访问层的方法,导入一行数据到数据库	    	
		    	aopYearJdbcService.updateInfo(year,jdbcTemplate,tableName, sb.toString(),idvString);    
	    	}	
		}
		return notNull;
	}
}