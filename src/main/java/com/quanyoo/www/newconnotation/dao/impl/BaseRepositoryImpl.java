package com.quanyoo.www.newconnotation.dao.impl;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import com.quanyoo.www.newconnotation.dao.BaseRepository;




@Transactional
@NoRepositoryBean
public class BaseRepositoryImpl <T, ID extends Serializable> extends SimpleJpaRepository<T,ID>
implements BaseRepository<T,ID> {
	private final EntityManager entityManager;

	public BaseRepositoryImpl(Class<T> domainClass, EntityManager em) {
		super(domainClass, em);
        this.entityManager = em;
	}

}
