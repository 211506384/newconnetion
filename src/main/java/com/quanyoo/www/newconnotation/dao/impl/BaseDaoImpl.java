package com.quanyoo.www.newconnotation.dao.impl;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlCommandType;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.dao.IBaseDao;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;
import com.quanyoo.www.newconnotation.util.SystemUtil;


@Repository("baseDao")
@Scope("prototype")
public class BaseDaoImpl implements IBaseDao {
	private static final Logger logger = LoggerFactory.getLogger(BaseDaoImpl.class);
	@Autowired
    SqlSessionTemplate sqlSessionTemplate;

	@Override
	public <T, E> E select(MapperNSEnum namespace, String id, T params) {
		if (params == null) {
            return sqlSessionTemplate.selectOne(namespace.getNSName() + "." + id);
        } else {
            return sqlSessionTemplate.selectOne(namespace.getNSName() + "." + id, params);
        }
	}

	@Override
	public <T, E> List<E> selectList(MapperNSEnum namespace, String id, T params) {
		if (params == null) {
            return sqlSessionTemplate.selectList(namespace.getNSName() + "." + id);
        } else {
            return sqlSessionTemplate.selectList(namespace.getNSName() + "." + id, params);
        }
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T, E> PageInfo<E> selectList(MapperNSEnum namespace, String id, T params, int pageNow, int pageSize) {
		if(pageNow > 0 && pageSize > 0){
			PageHelper.startPage(pageNow, pageSize);
		}
		List<E> list = sqlSessionTemplate.selectList(namespace.getNSName() + "." + id, params);
        //用PageInfo对结果进行包装
		PageInfo<E> page = new PageInfo<E>(list);
        return page;
	}

	@Override
	public <T> int update(MapperNSEnum namespace, String id, T params) {
		if (params == null) {
            return sqlSessionTemplate.update(namespace.getNSName() + "." + id);
        } else {
            return sqlSessionTemplate.update(namespace.getNSName() + "." + id, params);
        }
	}

	@Override
	public <T> List<Long> updateList(MapperNSEnum namespace, String id, List<T> list) {
		try {
            if (list == null || list.isEmpty()) {
                return null;
            }
            MappedStatement ms = sqlSessionTemplate.getConfiguration().getMappedStatement(namespace.getNSName() + "." + id);
            SqlCommandType sqlCommandType = ms.getSqlCommandType();
            BoundSql boundSql = ms.getSqlSource().getBoundSql(list.get(0));
            String sql = boundSql.getSql();
            List<ParameterMapping> parMapperList = boundSql.getParameterMappings();
            Connection connection = sqlSessionTemplate.getConnection();
            PreparedStatement statement = null;
            if (sqlCommandType == SqlCommandType.INSERT) {
                statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                statement = connection.prepareStatement(sql);
            }
            for (T item : list) {
                if (null == item) {
                    continue;
                }
                if (item instanceof Map) {
                    Map<String, Object> map = (Map<String, Object>) item;
                    for (int index = 0; index < parMapperList.size(); index++) {
                        ParameterMapping pm = parMapperList.get(index);
                        Object value = map.get(pm.getProperty());
                        statement.setObject(index + 1, value);
                    }
                } else if (item instanceof Long || item instanceof String || item instanceof Integer) {
                    statement.setObject(1, item);

                } else {
                    for (int index = 0; index < parMapperList.size(); index++) {
                        ParameterMapping pm = parMapperList.get(index);
                        String methodName = "get" + SystemUtil.camel(pm.getProperty(), "_",false);
                        Method method = item.getClass().getMethod(methodName);
                        Object value = method.invoke(item);
                        statement.setObject(index + 1, value);
                    }
                }
                statement.addBatch();
            }
            List<Long> resultList = new ArrayList<Long>();
            int[] resultArray = statement.executeBatch();
            if (sqlCommandType != SqlCommandType.INSERT) {
                for (int intval : resultArray) {
                    resultList.add(Long.valueOf(intval + ""));
                }
            } else {
                ResultSet resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    resultList.add(resultSet.getLong(0));
                }
            }
            return resultList;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
	}

	@Override
	public <T> long insert(MapperNSEnum namespace, String id, T params) {
		return update(namespace, id, params);
	}

	@Override
	public <T> List<Long> insertList(MapperNSEnum namespace, String id, List<T> list) {
		return updateList(namespace, id, list);
	}

	@Override
	public <T> int delete(MapperNSEnum namespace, String id, T params) {
		return update(namespace, id, params);
	}

	@Override
	public <T> List<Long> deleteList(MapperNSEnum namespace, String id, List<T> list) {
		return updateList(namespace, id, list);
	}

	@Override
	public <T> void batchAll(MapperNSEnum namespace, String sqlId, List<T> params, Integer bathcount) {
		List<T> data = new ArrayList<>();
        for (int i = 0; i < params.size(); i++) {
            data.add(params.get(i));
            if (data.size() == bathcount || i == params.size() - 1) {
                this.batchUtil(namespace, sqlId, data);
                data.clear();
            }
        }
	}
	@SuppressWarnings("unchecked")
    private <T> void batchUtil(MapperNSEnum namespace, String sqlId, List<T> list) {
        try {
            if (list == null || list.isEmpty()) {
                return;
            }
            MappedStatement ms = sqlSessionTemplate.getConfiguration().getMappedStatement(namespace.getNSName() + "." + sqlId);
            SqlCommandType sqlCommandType = ms.getSqlCommandType();
            BoundSql boundSql = ms.getSqlSource().getBoundSql(list.get(0));
            String sql = boundSql.getSql();
            List<ParameterMapping> list2 = boundSql.getParameterMappings();
            Connection connection = sqlSessionTemplate.getSqlSessionFactory().openSession().getConnection();
            PreparedStatement statement = null;
            if (sqlCommandType == SqlCommandType.INSERT) {
                statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                statement = connection.prepareStatement(sql);
            }
            sql = sql.replaceAll("\\n", "");
            sql = sql.replaceAll("\\t", "");
            sql = sql.replaceAll("[[ ]]{2,}", " ");
            logger.info("==>  Preparing：" + sql);
            for (T item : list) {
                if (null == item) {
                    continue;
                }
                StringBuffer values = new StringBuffer();
                if (item instanceof Map) {
                    Map<String, Object> map = (Map<String, Object>) item;
                    for (int index = 0; index < list2.size(); index++) {
                        ParameterMapping pm = list2.get(index);
                        Object value = map.get(pm.getProperty());
                        values.append(value).append("(").append(value.getClass()).append("),");
                        statement.setObject(index + 1, value);
                    }
                } else if (item instanceof Long || item instanceof String || item instanceof Integer) {
                    statement.setObject(1, item);
                    values.append(item).append("(").append(StringUtils.substringAfterLast(item.getClass().toString(), ".")).append("),");
                } else {
                    List<String> params = new ArrayList<>();
                    for (int index = 0; index < list2.size(); index++) {
                        ParameterMapping pm = list2.get(index);
                        String methodName ="get" +  SystemUtil.camel(pm.getProperty(), "_",false);
                        Method method = item.getClass().getMethod(methodName);
                        Object value = method.invoke(item);
                        params.add(value.toString());
                        statement.setObject(index + 1, value);
                        values.append(value).append("(").append(StringUtils.substringAfterLast(value.getClass().toString(), ".")).append("),");
                    }
                }
                statement.addBatch();
                values.delete(values.length() - 1, values.length());
                logger.info("==> Parameters：" + values);
            }
            List<Long> resultList = new ArrayList<>();
            int[] resultArray = statement.executeBatch();
            if (sqlCommandType != SqlCommandType.INSERT) {
                for (int intval : resultArray) {
                    resultList.add(Long.valueOf(intval + ""));
                }
            } else {
                ResultSet resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    try {
                        resultList.add(resultSet.getLong(1));
                    } catch (Exception e) {
                    	logger.error("错误：" + e.toString());
                    }
                }
            }
            return;
        } catch (Exception e) {
        	logger.error("错误：" + e.toString());
            throw new RuntimeException(e.toString());
        }
    }


    @SuppressWarnings("unchecked")
    protected <T> void printSql(String id, T params) {
        try {
            MappedStatement ms = sqlSessionTemplate.getConfiguration().getMappedStatement(id);
            BoundSql boundSql = ms.getSqlSource().getBoundSql(params);
            String sql = boundSql.getSql();
            sql = sql.replaceAll("\\n", "");
            sql = sql.replaceAll("\\t", "");
            sql = sql.replaceAll("[[ ]]{2,}", " ");
            List<ParameterMapping> list2 = boundSql.getParameterMappings();
            if (params == null) {

            } else if (params instanceof Map) {
                Map<String, Object> map = (Map<String, Object>) params;
                for (int index = 0; index < list2.size(); index++) {
                    ParameterMapping pm = list2.get(index);
                    Object value = map.get(pm.getProperty());
                    sql = sql.replaceFirst("[?]", value + "");
                }
            } else if (params instanceof Long || params instanceof String || params instanceof Integer) {
                sql = sql.replaceFirst("[?]", params + "");
            } else {
                for (int index = 0; index < list2.size(); index++) {
                    ParameterMapping pm = list2.get(index);
                    String methodName = "get_" + SystemUtil.camel(pm.getProperty(), "_",false);
                    Method method = params.getClass().getMethod(methodName);
                    Object value = method.invoke(params);
                    sql = sql.replaceFirst("[?]", value + "");
                }
            }
            logger.info(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
