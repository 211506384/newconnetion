package com.quanyoo.www.newconnotation.dao;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.quanyoo.www.newconnotation.util.MapperNSEnum;




public interface IBaseDao {
	<T, E> E select(MapperNSEnum namespace, String id, T params);

    <T, E> List<E> selectList(MapperNSEnum namespace, String id, T params);

    <T, E> PageInfo<E> selectList(MapperNSEnum namespace, String id, T params, int pageNow, int pageSize);

    <T> int update(MapperNSEnum namespace, String id, T params);

    <T> List<Long> updateList(MapperNSEnum namespace, String id, List<T> list);

    <T> long insert(MapperNSEnum namespace, String id, T params);

    <T> List<Long> insertList(MapperNSEnum namespace, String id, List<T> list);

    <T> int delete(MapperNSEnum namespace, String id, T params);

    <T> List<Long> deleteList(MapperNSEnum namespace, String id, List<T> list);

    <T> void batchAll(MapperNSEnum namespace, String id, List<T> params, Integer bathcount);
}
